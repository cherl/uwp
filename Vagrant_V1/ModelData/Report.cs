﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vagrant_V1.ModelData
{
    class Report
    {
        public DateTime start { get; set; }
        public DateTime finish { get; set; }
        public string employeeId { get; set; }
        public int timeStep { get; set; }
    }
}
