﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace Vagrant_V1.ModelData
{
    class DescriptionData : INotifyDataErrorInfo, INotifyPropertyChanged
    {
        private Dictionary<string, List<string>> _errors = new Dictionary<string, List<string>>();
        private string description;
        private string color;


        public event PropertyChangedEventHandler PropertyChanged;
        public event EventHandler<DataErrorsChangedEventArgs> ErrorsChanged;

        private void NotifyPropertyChanged([CallerMemberName] String propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        bool INotifyDataErrorInfo.HasErrors
        {
            get
            {
                return _errors.Keys.Count > 0;
            }
        }

        IEnumerable INotifyDataErrorInfo.GetErrors(string propertyName)
        {
            if (propertyName == null)
            {
                propertyName = string.Empty;
            }

            if (_errors.Keys.Contains(propertyName))
            {
                return _errors[propertyName];
            }
            else
            {
                return null;
            }
        }

        public string Description
        {
            get { return description; }
            set
            {
                if (description != value)
                {
                    description = value;
                    bool isdescriptionValid = !_errors.Keys.Contains("Description");
                    if (description == string.Empty && isdescriptionValid)
                    {
                        List<string> errors = new List<string>();
                        errors.Add("Description name cannot be empty");
                        _errors.Add("Description", errors);
                        this.ErrorsChanged?.Invoke(this, new DataErrorsChangedEventArgs("Description"));
                    }
                    else if (description != string.Empty && !isdescriptionValid)
                    {
                        _errors.Remove("Description");
                        this.ErrorsChanged?.Invoke(this, new DataErrorsChangedEventArgs("Description"));
                    }
                }
                NotifyPropertyChanged();
            }
        }

        public string Color
        {
            get { return color; }
            set
            {
                if (color != value)
                {
                    color = value;
                    bool isdescriptionValid = !_errors.Keys.Contains("Color");
                    if (color == string.Empty && isdescriptionValid)
                    {
                        List<string> errors = new List<string>();
                        errors.Add("Color name cannot be empty");
                        _errors.Add("Color", errors);
                        this.ErrorsChanged?.Invoke(this, new DataErrorsChangedEventArgs("Color"));
                    }
                    else if (color != string.Empty && !isdescriptionValid)
                    {
                        _errors.Remove("Description");
                        this.ErrorsChanged?.Invoke(this, new DataErrorsChangedEventArgs("Color"));
                    }
                }
                NotifyPropertyChanged();
            }
        }


    }

}
