﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vagrant_V1.ModelData
{
    public class InfoData
    {
        public string From { get; set; }

        public string Subject { get; set; }

        public string Body { get; set; }

    }
}
