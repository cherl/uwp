﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vagrant_V1.ModelData
{
    public class PositionDataBaseList
    {
        public PositionDataBase[] Property1 { get; set; }
    }

    public class PositionDataBase
    {
        public string id { get; set; }
        public string gridElementId { get; set; }
    }
}
