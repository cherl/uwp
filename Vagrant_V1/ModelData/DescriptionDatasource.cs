﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Vagrant_V1.Server;
using Windows.System.Threading;
using Windows.UI.Core;
using Windows.UI.Xaml.Data;


namespace Vagrant_V1.ModelData
{
    [Bindable]
    class DescriptionDatasource
    {
        private static ObservableCollection<DescriptionData> _items;
        private string _cachedSortedColumn = string.Empty;
        public static string _cachedSortedColumnstatus = string.Empty;

        public async Task<IEnumerable<DescriptionData>> GetDataAsync()
        {
            //_items = new ObservableCollection<DescriptionData>(
            //Employer_View
            //    .Zon
            //    .Select(v => new DescriptionData()
            //    {
            //        Description = v.description
            //    }));
            _items = new ObservableCollection<DescriptionData>();
            for (int i = 0; i < Employer_View.Zon.Count; i++)
            {
                bool flag = false;
                for (int j = 0; j < _items.Count; j++)
                {
                    if (Employer_View.Zon[i].description == _items[j].Description && Employer_View.Zon[i].color.ToUpper() == _items[j].Color.ToUpper())
                    {
                        flag = true;
                    }
                }
                if (flag == false)
                {
                    _items.Add(new DescriptionData());
                    _items[_items.Count - 1].Description = Employer_View.Zon[i].description;
                    _items[_items.Count - 1].Color = Employer_View.Zon[i].color;
                }
            }
            return _items;
        }

        public static async Task<IEnumerable<DescriptionData>> UpdateDataAsync()
        {
            _items.Clear();
            for (int i = 0; i < Employer_View.Zon.Count; i++)
            {
                bool flag = false;
                for (int j = 0; j < _items.Count; j++)
                {
                    if (Employer_View.Zon[i].description == _items[j].Description && Employer_View.Zon[i].color.ToUpper() == _items[j].Color.ToUpper())
                    {
                        flag = true;
                    }
                }
                if (flag == false)
                {
                    _items.Add(new DescriptionData());
                    _items[_items.Count - 1].Description = Employer_View.Zon[i].description;
                    _items[_items.Count - 1].Color = Employer_View.Zon[i].color;
                }
            }
            return _items;
        }

        public string CachedSortedColumn
        {
            get
            {
                return _cachedSortedColumn;
            }

            set
            {
                _cachedSortedColumn = value;
                _cachedSortedColumnstatus = value;
            }
        }

        public ObservableCollection<DescriptionData> SortData(string sortBy, bool ascending)
        {
            _cachedSortedColumn = sortBy;
            _cachedSortedColumnstatus = ascending.ToString();
                switch (sortBy)
                {
                    case "Description":
                        return ascending
                            ? new ObservableCollection<DescriptionData>(_items.OrderBy(t => t.Description))
                            : new ObservableCollection<DescriptionData>(_items.OrderByDescending(t => t.Description));
                }

            return _items;
        }

        public enum FilterOptions
        {
            All = -1,
        }

        public ObservableCollection<DescriptionData> FilterData(FilterOptions filterBy)
        {
            switch (filterBy)
            {
                case FilterOptions.All:
                    return new ObservableCollection<DescriptionData>(_items);
            }
            return _items;
        }
    }
}
