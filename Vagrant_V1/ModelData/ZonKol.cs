﻿using SharpDX;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vagrant_V1.ModelData
{

    public class ZonKolList
    {
        public ZonKol[] Property1 { get; set; }
    }

    public class ZonKol
    {
        public string name { get; set; }
        public int kol { get; set; }
        public Vector3 centre { get; set; }

    }
}
