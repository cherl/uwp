﻿using GalaSoft.MvvmLight;
using HelixToolkit.UWP;
using SharpDX;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Vagrant_V1.Data;
using Vagrant_V1.ModelData;
using Vagrant_V1.Pageees;
using Vagrant_V1.Server;
using Windows.System.Threading;
using Windows.UI.Core;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;

namespace Vagrant_V1.Modeling3d._2D
{

    public class OITModelText : ObservableObject, INotifyPropertyChanged
    {
        public Geometry3D ModelText { private set; get; }

        public OITModelText(Geometry3D model)
        {
            ModelText = model;
        }
    }

    public class Index_Name
    {
        public string nameZone { private set; get; }
        public int indexZon { set; get; }
        public List<string> nameUnityObject { set; get; }

        public Index_Name(string name, int index, List<string> UnityObject)
        {
            nameZone = name;
            indexZon = index;
            nameUnityObject = UnityObject;
        }
    }

    public class OITModel : ObservableObject, INotifyPropertyChanged
    {

        public Geometry3D Model { set; get; }

        public Material Material { set; get; }

        public MeshGeometryModel3D Mesh { private set; get; }

        public string NameModel { set; get; }

        public int startIndex { set; get; }

        public int endIndex { set; get; }

        public List<int> IndexMass { set; get; } = new List<int>();

        public int Indexposition { set; get; }

        public bool IsTransparent { private set; get; }

        private bool showWireframe = false;

        public event PropertyChangedEventHandler PropertyChanged;

        private void NotifyPropertyChanged([CallerMemberName] String propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        public string NAMEMODEL
        {
            get { return NameModel; }
            set
            {
                if (NameModel != value)
                {
                    NameModel = value;
                }
                NotifyPropertyChanged();
            }
        }

        public bool ShowWireframe
        {
            set
            {
                Set(ref showWireframe, value);
            }
            get
            {
                return showWireframe;
            }
        }

        public OITModel(Geometry3D model, Material material, bool isTransparent, MeshGeometryModel3D mesh, string Name, int start, int end, int position )
        {
            Model = model;
            Material = material;
            IsTransparent = isTransparent;
            Mesh = mesh;
            NameModel = Name;
            if(start >0 && end > 0)
            {
                for (int i = start; i < end +1; i++)
                {
                    IndexMass.Add(i);
                }
            }
            Indexposition = position;
            startIndex = start;
            endIndex = end;
        }
    }

    public class OITDemoViewStatistic : ObservableObject
    {
        public static ObservableCollection<OITModel> Statistic { get; private set; } = new ObservableCollection<OITModel>();

        public static List<OITModel> StatisticList { get; private set; } = new List<OITModel>();

        public Matrix Transform { private set; get; } = Matrix.Translation(0, 0, 15);
        public static Matrix Transform1 { set; get; } = Matrix.Translation(0, 0, 15);
        public LineGeometry3D GridModel { private set; get; }
        public Matrix GridTransform { private set; get; } = Matrix.Translation(0, 0, 15);
        public static Matrix GridTransform1 { set; get; } = Matrix.Translation(0, 0, 15);
        public OITWeightMode[] OITWeights { get; } = new OITWeightMode[] { OITWeightMode.Linear0, OITWeightMode.Linear1, OITWeightMode.Linear2, OITWeightMode.NonLinear };
        public static Vector3 centor = new Vector3();
        int startindex = 0;

        private bool showWireframe = false;

        public bool ShowWireframe
        {
            set
            {
                if (Set(ref showWireframe, value))
                {
                    foreach (var item in Statistic)
                    {
                        item.ShowWireframe = value;
                    }
                }
            }
            get
            {
                return showWireframe;
            }
        }

        private SynchronizationContext syncContext = SynchronizationContext.Current;

        static private string GetExeDirectory()
        {
            string codeBase = Assembly.GetExecutingAssembly().CodeBase;
            UriBuilder uri = new UriBuilder(codeBase);
            string path = Uri.UnescapeDataString(uri.Path);
            path = System.IO.Path.GetDirectoryName(path);
            return path;
        }

        public OITDemoViewStatistic()
        {
            if (Statistic.Count == 0)
            {
                var dir = Windows.ApplicationModel.Package.Current.InstalledLocation.Path + @"\3DModelstest";
                var level_1 = Path.Combine(dir, "Wall_1.stl");
                var level_2 = Path.Combine(dir, "Wall_2.stl");
                var level_3 = Path.Combine(dir, "Wall_3.stl");
                var level_4 = Path.Combine(dir, "Wall_4.stl");
                var packageFolder = Path.Combine(dir, "Taishet_Clear.stl");
                BuildGrid();
                Task.Run(async () => {await LoadStlcoordinate(packageFolder); await LoadStl(level_1); await LoadStl(level_2); await LoadStl(level_3); await LoadStl(level_4); });
            }
        }

        private void BuildGrid()
        {
            var builder = new LineBuilder();
            int zOff = -45;
            for (int i = 0; i < 10; ++i)
            {
                for (int j = 0; j < 10; ++j)
                {
                    builder.AddLine(new SharpDX.Vector3(-i * 5, 0, j * 5), new SharpDX.Vector3(i * 5, 0, j * 5));
                    builder.AddLine(new SharpDX.Vector3(-i * 5, 0, -j * 5), new SharpDX.Vector3(i * 5, 0, -j * 5));
                    builder.AddLine(new SharpDX.Vector3(i * 5, 0, -j * 5), new SharpDX.Vector3(i * 5, 0, j * 5));
                    builder.AddLine(new SharpDX.Vector3(-i * 5, 0, -j * 5), new SharpDX.Vector3(-i * 5, 0, j * 5));
                    builder.AddLine(new SharpDX.Vector3(-i * 5, j * 5, zOff), new SharpDX.Vector3(i * 5, j * 5, zOff));
                    builder.AddLine(new SharpDX.Vector3(i * 5, 0, zOff), new SharpDX.Vector3(i * 5, j * 5, zOff));
                    builder.AddLine(new SharpDX.Vector3(-i * 5, 0, zOff), new SharpDX.Vector3(-i * 5, j * 5, zOff));
                }
            }
            GridModel = builder.ToLineGeometry3D();
        }

        public async Task LoadStlcoordinate(string path)
        {
            var reader = new StLReader();
            var objCol = reader.Read(path);
            centor = objCol[0].Geometry.Bound.Center;
        }

        public async Task LoadStl(string path)
        {
            var reader = new StLReader();
            var objCol = reader.Read(path);
            objCol[0].Name = Path.GetFileName(path);
            AttachModelList(objCol);
        }

        public void AttachModelList(List<Object3D> objs)
        {

            Random rnd = new Random(DateTime.Now.Millisecond);
            foreach (var ob in objs)
            {
                //MeshGeometryModel3D Meshmodel = new MeshGeometryModel3D();
                //Meshmodel.Geometry = ob.Geometry;
                //Meshmodel.IsTransparent = true;
                ob.Geometry.UpdateOctree();
                Task.Delay(100).Wait();
                syncContext.Post(
                    async (o) =>
                    {
                        if (ob.Material is HelixToolkit.UWP.Model.PhongMaterialCore p)
                        {
                            //var diffuse = p.DiffuseColor;
                            Color diffuse = Color.LightGray;
                            //diffuse. = 0.5f;//(float)(Math.Min(0.8, Math.Max(0.2, rnd.NextDouble())));
                            p.DiffuseColor = new Color(97, 97, 97, 160);
                            p.EmissiveColor = new Color(97, 97, 97, 90);
                            //p.ReflectiveColor = diffuse;
                            Statistic.Add(new OITModel(ob.Geometry, p, true, null, ob.Name, 0, 0, Statistic.Count));
                            GroupModel3D Sphere1 = new GroupModel3D();
                            Sphere1.Name = ob.Name.Substring(ob.Name.Length - 5, 1);
                            DynamicReflectionMap3D Sphere1dynamic = new DynamicReflectionMap3D();
                            Sphere1dynamic.IsDynamicScene = true;
                            startindex += 1;
                            switch (int.Parse(Sphere1.Name))
                            {
                                case 1:
                                    startindex = await Create_Zon_1(centor, Sphere1dynamic, Sphere1, startindex);
                                    break;
                                case 2:
                                    startindex = await Create_Zon_2(centor, Sphere1dynamic, Sphere1, startindex);
                                    break;
                                case 3:
                                    startindex = await Create_Zon_3(centor, Sphere1dynamic, Sphere1, startindex);
                                    break;
                                case 4:
                                    startindex = await Create_Zon_4(centor, Sphere1dynamic, Sphere1, startindex);
                                    break;
                            }
                        }
                    }, null);
            }

        }


        private async Task<int> Create_Zon_1(Vector3 centre1, DynamicReflectionMap3D Sphere1dynamic, GroupModel3D Sphere1, int startindex)
        {
            for (int i = 0; i < Employer_View.Zon.Count; i++)
            {
                if (Employer_View.Zon[i].title.Substring(0, 3) == "UR1")
                {
                    List<UnityObject> ObjectZOn = Employer_View.unityObjects.FindAll(u => u.siteZone != null && u.siteZone.title == Employer_View.Zon[i].title);
                    MeshGeometryModel3D Spheremodel = new MeshGeometryModel3D();
                    Spheremodel.CullMode = SharpDX.Direct3D11.CullMode.Back;
                    var builder = new MeshBuilder(true, true, true);
                    builder = new MeshBuilder();
                    for (int j = 0; j < ObjectZOn.Count; j++)
                    {
                        var builder1 = new MeshBuilder(true, true, true);
                        builder1 = new MeshBuilder();
                        for (int k = 0; k < ObjectZOn[j].objectCoordinates.Length; k++)
                        {
                            Vector3 centre = centre1;
                            centre.X = centre.X + ((ObjectZOn[j].objectCoordinates[k].c1.x + ObjectZOn[j].objectCoordinates[k].c2.x) / 2);
                            centre.Y = centre.Y - ((ObjectZOn[j].objectCoordinates[k].c1.y + ObjectZOn[j].objectCoordinates[k].c2.y) / 2);
                            centre.Z = centre.Z + ((ObjectZOn[j].objectCoordinates[k].c1.z + ObjectZOn[j].objectCoordinates[k].c2.z) / 2);
                            builder.AddBox(centre, Math.Abs(ObjectZOn[j].objectCoordinates[k].c1.x - ObjectZOn[j].objectCoordinates[k].c2.x)
                                , Math.Abs(ObjectZOn[j].objectCoordinates[k].c1.y - ObjectZOn[j].objectCoordinates[k].c2.y), 1);
                            builder1.AddBox(centre, Math.Abs(ObjectZOn[j].objectCoordinates[k].c1.x - ObjectZOn[j].objectCoordinates[k].c2.x)
                                , Math.Abs(ObjectZOn[j].objectCoordinates[k].c1.y - ObjectZOn[j].objectCoordinates[k].c2.y), 1);
                        }
                    }
                    startindex += (ObjectZOn.Count);
                    Geometry3D SphereGeometry;
                    SphereGeometry = builder.ToMeshGeometry3D();
                    //BillboardTextModel3D text1 = new BillboardTextModel3D();
                    //BillboardSingleText3D text = new BillboardSingleText3D();
                    //text.TextInfo = new TextInfo("23", new Vector3(10.8280244f, 10.665166f, 10.356252f));
                    //text.FontColor = Color.Blue.ToColor4();
                    //text.FontSize = 20;
                    ////text.Bound = SphereGeometry.Bound;
                    //text.BackgroundColor = Color.DarkGray;
                    //text1.Geometry = text;
                    SphereGeometry.UpdateOctree();
                    Spheremodel.Geometry = SphereGeometry;
                    Spheremodel.Name = Employer_View.Zon[i].title;
                    Spheremodel.IsThrowingShadow = true;
                    var a = (byte)220f;
                    var r = (byte)System.Convert.ToUInt32(Employer_View.Zon[i].color.Substring(1, 2), 16);
                    var g = (byte)System.Convert.ToUInt32(Employer_View.Zon[i].color.Substring(3, 2), 16);
                    var b = (byte)System.Convert.ToUInt32(Employer_View.Zon[i].color.Substring(5, 2), 16);
                    Color colorZon = new Color(r, g, b, a);
                    PhongMaterial Material1;
                    Material1 = new PhongMaterial()
                    {
                        DiffuseColor = colorZon,
                        EmissiveColor = colorZon,
                    };
                    Spheremodel.Material = Material1;
                    Sphere1dynamic.Children.Add(Spheremodel);
                    Sphere1.Children.Add(Sphere1dynamic);
                    Statistic.Add(new OITModel(SphereGeometry, Material1, true, Spheremodel, Employer_View.Zon[i].title, 0, 0, Statistic.Count));
                    //ModelGeometryText.Add(new OITModelText(text));
                }
            }
            return startindex;
        }

        private async Task<int> Create_Zon_4(Vector3 centre1, DynamicReflectionMap3D Sphere1dynamic, GroupModel3D Sphere1, int startindex)
        {
            for (int i = 0; i < Employer_View.Zon.Count; i++)
            {
                if (Employer_View.Zon[i].title.Substring(0, 3) == "UR4")
                {
                    List<UnityObject> ObjectZOn = Employer_View.unityObjects.FindAll(u => u.siteZone != null && u.siteZone.title == Employer_View.Zon[i].title);
                    MeshGeometryModel3D Spheremodel = new MeshGeometryModel3D();
                    Spheremodel.CullMode = SharpDX.Direct3D11.CullMode.Back;
                    var builder = new MeshBuilder(true, true, true);
                    builder = new MeshBuilder();
                    for (int j = 0; j < ObjectZOn.Count; j++)
                    {
                        var builder1 = new MeshBuilder(true, true, true);
                        builder1 = new MeshBuilder();
                        for (int k = 0; k < ObjectZOn[j].objectCoordinates.Length; k++)
                        {
                            Vector3 centre = centre1;
                            centre.X = centre.X + ((ObjectZOn[j].objectCoordinates[k].c1.x + ObjectZOn[j].objectCoordinates[k].c2.x) / 2);
                            centre.Y = centre.Y - ((ObjectZOn[j].objectCoordinates[k].c1.y + ObjectZOn[j].objectCoordinates[k].c2.y) / 2);
                            centre.Z = centre.Z + ((ObjectZOn[j].objectCoordinates[k].c1.z + ObjectZOn[j].objectCoordinates[k].c2.z) / 2);
                            builder.AddBox(centre, Math.Abs(ObjectZOn[j].objectCoordinates[k].c1.x - ObjectZOn[j].objectCoordinates[k].c2.x)
                                , Math.Abs(ObjectZOn[j].objectCoordinates[k].c1.y - ObjectZOn[j].objectCoordinates[k].c2.y), 1);
                            builder1.AddBox(centre, Math.Abs(ObjectZOn[j].objectCoordinates[k].c1.x - ObjectZOn[j].objectCoordinates[k].c2.x)
                                , Math.Abs(ObjectZOn[j].objectCoordinates[k].c1.y - ObjectZOn[j].objectCoordinates[k].c2.y), 1);
                        }
                    }
                    startindex += (ObjectZOn.Count);
                    Geometry3D SphereGeometry;
                    SphereGeometry = builder.ToMeshGeometry3D();
                    SphereGeometry.UpdateOctree();
                    Spheremodel.Geometry = SphereGeometry;
                    Spheremodel.Name = Employer_View.Zon[i].title;
                    Spheremodel.IsThrowingShadow = true;
                    var a = (byte)220f;
                    var r = (byte)System.Convert.ToUInt32(Employer_View.Zon[i].color.Substring(1, 2), 16);
                    var g = (byte)System.Convert.ToUInt32(Employer_View.Zon[i].color.Substring(3, 2), 16);
                    var b = (byte)System.Convert.ToUInt32(Employer_View.Zon[i].color.Substring(5, 2), 16);
                    Color colorZon = new Color(r, g, b, a);
                    PhongMaterial Material1;
                    Material1 = new PhongMaterial()
                    {
                        DiffuseColor = colorZon,
                        EmissiveColor = colorZon,
                    };
                    Spheremodel.Material = Material1;
                    Sphere1dynamic.Children.Add(Spheremodel);
                    Sphere1.Children.Add(Sphere1dynamic);
                    Statistic.Add(new OITModel(SphereGeometry, Material1, true, Spheremodel, Employer_View.Zon[i].title, 0, 0, Statistic.Count));
                }
            }
            return startindex;
        }

        private async Task<int> Create_Zon_3(Vector3 centre1, DynamicReflectionMap3D Sphere1dynamic, GroupModel3D Sphere1, int startindex)
        {
            for (int i = 0; i < Employer_View.Zon.Count; i++)
            {
                if (Employer_View.Zon[i].title.Substring(0, 3) == "UR3")
                {
                    List<UnityObject> ObjectZOn = Employer_View.unityObjects.FindAll(u => u.siteZone != null && u.siteZone.title == Employer_View.Zon[i].title);
                    MeshGeometryModel3D Spheremodel = new MeshGeometryModel3D();
                    Spheremodel.CullMode = SharpDX.Direct3D11.CullMode.Back;
                    var builder = new MeshBuilder(true, true, true);
                    builder = new MeshBuilder();
                    for (int j = 0; j < ObjectZOn.Count; j++)
                    {
                        var builder1 = new MeshBuilder(true, true, true);
                        builder1 = new MeshBuilder();
                        for (int k = 0; k < ObjectZOn[j].objectCoordinates.Length; k++)
                        {
                            Vector3 centre = centre1;
                            centre.X = centre.X + ((ObjectZOn[j].objectCoordinates[k].c1.x + ObjectZOn[j].objectCoordinates[k].c2.x) / 2);
                            centre.Y = centre.Y - ((ObjectZOn[j].objectCoordinates[k].c1.y + ObjectZOn[j].objectCoordinates[k].c2.y) / 2);
                            centre.Z = centre.Z + ((ObjectZOn[j].objectCoordinates[k].c1.z + ObjectZOn[j].objectCoordinates[k].c2.z) / 2);
                            builder.AddBox(centre, Math.Abs(ObjectZOn[j].objectCoordinates[k].c1.x - ObjectZOn[j].objectCoordinates[k].c2.x)
                                , Math.Abs(ObjectZOn[j].objectCoordinates[k].c1.y - ObjectZOn[j].objectCoordinates[k].c2.y), 1);
                            builder1.AddBox(centre, Math.Abs(ObjectZOn[j].objectCoordinates[k].c1.x - ObjectZOn[j].objectCoordinates[k].c2.x)
                                 , Math.Abs(ObjectZOn[j].objectCoordinates[k].c1.y - ObjectZOn[j].objectCoordinates[k].c2.y), 1);

                        }
                    }
                    startindex += (ObjectZOn.Count);
                    Geometry3D SphereGeometry;
                    SphereGeometry = builder.ToMeshGeometry3D();
                    SphereGeometry.UpdateOctree();
                    Spheremodel.Geometry = SphereGeometry;
                    Spheremodel.Name = Employer_View.Zon[i].title;
                    Spheremodel.IsThrowingShadow = true;
                    var a = (byte)220f;
                    var r = (byte)System.Convert.ToUInt32(Employer_View.Zon[i].color.Substring(1, 2), 16);
                    var g = (byte)System.Convert.ToUInt32(Employer_View.Zon[i].color.Substring(3, 2), 16);
                    var b = (byte)System.Convert.ToUInt32(Employer_View.Zon[i].color.Substring(5, 2), 16);
                    Color colorZon = new Color(r, g, b, a);
                    PhongMaterial Material1;
                    Material1 = new PhongMaterial()
                    {
                        DiffuseColor = colorZon,
                        EmissiveColor = colorZon,
                    };
                    Spheremodel.Material = Material1;
                    Sphere1dynamic.Children.Add(Spheremodel);
                    Sphere1.Children.Add(Sphere1dynamic);
                    Statistic.Add(new OITModel(SphereGeometry, Material1, true, Spheremodel, Employer_View.Zon[i].title, 0, 0, Statistic.Count));
                    List<string> Objectunity = Employer_View.Zon[i].unityObjects.ToList();
                }
            }
            return startindex;
        }

        private async Task<int> Create_Zon_2(Vector3 centre1, DynamicReflectionMap3D Sphere1dynamic, GroupModel3D Sphere1, int startindex)
        {
            for (int i = 0; i < Employer_View.Zon.Count; i++)
            {
                if (Employer_View.Zon[i].title.Substring(0, 3) == "UR2")
                {
                    List<UnityObject> ObjectZOn = Employer_View.unityObjects.FindAll(u => u.siteZone != null && u.siteZone.title == Employer_View.Zon[i].title);
                    MeshGeometryModel3D Spheremodel = new MeshGeometryModel3D();
                    Spheremodel.CullMode = SharpDX.Direct3D11.CullMode.Back;
                    var builder = new MeshBuilder(true, true, true);
                    builder = new MeshBuilder();
                    for (int j = 0; j < ObjectZOn.Count; j++)
                    {
                        var builder1 = new MeshBuilder(true, true, true);
                        builder1 = new MeshBuilder();
                        for (int k = 0; k < ObjectZOn[j].objectCoordinates.Length; k++)
                        {
                            Vector3 centre = centre1;
                            centre.X = centre.X + ((ObjectZOn[j].objectCoordinates[k].c1.x + ObjectZOn[j].objectCoordinates[k].c2.x) / 2);
                            centre.Y = centre.Y - ((ObjectZOn[j].objectCoordinates[k].c1.y + ObjectZOn[j].objectCoordinates[k].c2.y) / 2);
                            centre.Z = centre.Z + ((ObjectZOn[j].objectCoordinates[k].c1.z + ObjectZOn[j].objectCoordinates[k].c2.z) / 2);
                            builder.AddBox(centre, Math.Abs(ObjectZOn[j].objectCoordinates[k].c1.x - ObjectZOn[j].objectCoordinates[k].c2.x)
                                , Math.Abs(ObjectZOn[j].objectCoordinates[k].c1.y - ObjectZOn[j].objectCoordinates[k].c2.y), 1);
                            builder1.AddBox(centre, Math.Abs(ObjectZOn[j].objectCoordinates[k].c1.x - ObjectZOn[j].objectCoordinates[k].c2.x)
                                , Math.Abs(ObjectZOn[j].objectCoordinates[k].c1.y - ObjectZOn[j].objectCoordinates[k].c2.y), 1);
                        }
                    }
                    startindex += (ObjectZOn.Count);
                    Geometry3D SphereGeometry;
                    SphereGeometry = builder.ToMeshGeometry3D();
                    SphereGeometry.UpdateOctree();
                    Spheremodel.Geometry = SphereGeometry;
                    Spheremodel.Name = Employer_View.Zon[i].title;
                    Spheremodel.IsThrowingShadow = true;
                    var a = (byte)220f;
                    var r = (byte)System.Convert.ToUInt32(Employer_View.Zon[i].color.Substring(1, 2), 16);
                    var g = (byte)System.Convert.ToUInt32(Employer_View.Zon[i].color.Substring(3, 2), 16);
                    var b = (byte)System.Convert.ToUInt32(Employer_View.Zon[i].color.Substring(5, 2), 16);
                    Color colorZon = new Color(r, g, b, a);
                    PhongMaterial Material1;
                    Material1 = new PhongMaterial()
                    {
                        DiffuseColor = colorZon,
                        EmissiveColor = colorZon,
                    };
                    Spheremodel.Material = Material1;
                    Sphere1dynamic.Children.Add(Spheremodel);
                    Sphere1.Children.Add(Sphere1dynamic);
                    Statistic.Add(new OITModel(SphereGeometry, Material1, true, Spheremodel, Employer_View.Zon[i].title, 0, 0, Statistic.Count));
                    List<string> Objectunity = Employer_View.Zon[i].unityObjects.ToList();
                }
            }
            return startindex;
        }
    }

    public class OITDemoViewModel : ObservableObject
    {
        public static ObservableCollection<OITModel> ModelGeometry { get; private set; } = new ObservableCollection<OITModel>();

        public static ObservableCollection<OITModel> ModelGeometryZons { get; private set; } = new ObservableCollection<OITModel>();
        public static ObservableCollection<OITModelText> ModelGeometryText { get; private set; } = new ObservableCollection<OITModelText>();
        public static List<Index_Name> Collection { get; private set; } = new List<Index_Name>();
        public static bool init = false;
        public Matrix Transform { private set; get; } = Matrix.Translation(0, 0, 15);
        public static Matrix Transform1 { set; get; } = Matrix.Translation(0, 0, 15);
        public LineGeometry3D GridModel { private set; get; }
        public Matrix GridTransform { private set; get; } = Matrix.Translation(0, 0, 15);
        public static Matrix GridTransform1 { set; get; } = Matrix.Translation(0, 0, 15);
        public OITWeightMode[] OITWeights { get; } = new OITWeightMode[] { OITWeightMode.Linear0, OITWeightMode.Linear1, OITWeightMode.Linear2, OITWeightMode.NonLinear };
        public static Vector3 centor = new Vector3();
        int startindex = 0;

        private bool showWireframe = false;

        public bool ShowWireframe
        {
            set
            {
                if (Set(ref showWireframe, value))
                {
                    foreach (var item in ModelGeometry)
                    {
                        item.ShowWireframe = value;
                    }
                }
            }
            get
            {
                return showWireframe;
            }
        }

        private SynchronizationContext syncContext = SynchronizationContext.Current;

        static private string GetExeDirectory()
        {
            string codeBase = Assembly.GetExecutingAssembly().CodeBase;
            UriBuilder uri = new UriBuilder(codeBase);
            string path = Uri.UnescapeDataString(uri.Path);
            path = System.IO.Path.GetDirectoryName(path);
            return path;
        }

        public OITDemoViewModel()
        {
            if(ModelGeometry.Count == 0)
            {
                var dir = Windows.ApplicationModel.Package.Current.InstalledLocation.Path + @"\3DModelstest";
                var level_1 = Path.Combine(dir, "Wall_1.stl");
                var level_2 = Path.Combine(dir, "Wall_2.stl");
                var level_3 = Path.Combine(dir, "Wall_3.stl");
                var level_4 = Path.Combine(dir, "Wall_4.stl");
                var packageFolder = Path.Combine(dir, "Taishet_Clear.stl");
                BuildGrid();
                Task.Run(() => { LoadStlcoordinate(packageFolder); LoadStl(level_1); LoadStl(level_2); LoadStl(level_3); LoadStl(level_4); });
            }
        }


        private void BuildGrid()
        {
            var builder = new LineBuilder();
            int zOff = -45;
            for (int i = 0; i < 10; ++i)
            {
                for (int j = 0; j < 10; ++j)
                {
                    builder.AddLine(new SharpDX.Vector3(-i * 5, 0, j * 5), new SharpDX.Vector3(i * 5, 0, j * 5));
                    builder.AddLine(new SharpDX.Vector3(-i * 5, 0, -j * 5), new SharpDX.Vector3(i * 5, 0, -j * 5));
                    builder.AddLine(new SharpDX.Vector3(i * 5, 0, -j * 5), new SharpDX.Vector3(i * 5, 0, j * 5));
                    builder.AddLine(new SharpDX.Vector3(-i * 5, 0, -j * 5), new SharpDX.Vector3(-i * 5, 0, j * 5));
                    builder.AddLine(new SharpDX.Vector3(-i * 5, j * 5, zOff), new SharpDX.Vector3(i * 5, j * 5, zOff));
                    builder.AddLine(new SharpDX.Vector3(i * 5, 0, zOff), new SharpDX.Vector3(i * 5, j * 5, zOff));
                    builder.AddLine(new SharpDX.Vector3(-i * 5, 0, zOff), new SharpDX.Vector3(-i * 5, j * 5, zOff));
                }
            }
            GridModel = builder.ToLineGeometry3D();
        }

        public void Load3ds(string path)
        {
            var reader = new StudioReader();
            var objCol = reader.Read(path);
            AttachModelList(objCol);
        }

        public void LoadObj(string path)
        {
            var reader = new ObjReader();
            var objCol = reader.Read(path);
            AttachModelList(objCol);
        }

        public void LoadStlcoordinate(string path)
        {
            var reader = new StLReader();
            var objCol = reader.Read(path);
            centor = objCol[0].Geometry.Bound.Center;
        }

        public void LoadStl(string path)
        {
            var reader = new StLReader();
            var objCol = reader.Read(path);
            objCol[0].Name = Path.GetFileName(path);
            AttachModelList(objCol);
        }


        public void AttachModelList(List<Object3D> objs)
        {

            Random rnd = new Random(DateTime.Now.Millisecond);
            foreach (var ob in objs)
            {
                //MeshGeometryModel3D Meshmodel = new MeshGeometryModel3D();
                //Meshmodel.Geometry = ob.Geometry;
                //Meshmodel.IsTransparent = true;
                ob.Geometry.UpdateOctree();
                Task.Delay(100).Wait();
                syncContext.Post(
                    (o) =>
                    {
                        if (ob.Material is HelixToolkit.UWP.Model.PhongMaterialCore p)
                        {
                            //var diffuse = p.DiffuseColor;
                            Color diffuse = Color.LightGray;
                            //diffuse. = 0.5f;//(float)(Math.Min(0.8, Math.Max(0.2, rnd.NextDouble())));
                            p.DiffuseColor = new Color(97, 97, 97, 160);
                            p.EmissiveColor = new Color(97, 97, 97, 90);
                            //p.ReflectiveColor = diffuse;
                            ModelGeometry.Add(new OITModel(ob.Geometry, p, true, null, ob.Name,0,0, ModelGeometry.Count));
                            ModelGeometryZons.Add(new OITModel(ob.Geometry, p, true, null, ob.Name,0,0, ModelGeometryZons.Count));
                            GroupModel3D Sphere1 = new GroupModel3D();
                            Sphere1.Name = ob.Name.Substring(ob.Name.Length - 5, 1);
                            DynamicReflectionMap3D Sphere1dynamic = new DynamicReflectionMap3D();
                            Sphere1dynamic.IsDynamicScene = true;
                            startindex += 1;
                            switch (int.Parse(Sphere1.Name))
                            {
                                case 1:
                                    startindex = Create_Zon_1(centor, Sphere1dynamic, Sphere1, startindex);
                                    break;
                                case 2:
                                    startindex = Create_Zon_2(centor, Sphere1dynamic, Sphere1, startindex);
                                    break;
                                case 3:
                                    startindex = Create_Zon_3(centor, Sphere1dynamic, Sphere1, startindex);
                                    break;
                                case 4:
                                    startindex = Create_Zon_4(centor, Sphere1dynamic, Sphere1, startindex);
                                    break;
                            }
                        }
                    }, null);
            }
        }

        public static async Task<string> ModifyZon(int index, ZonDataBase Zon)
        {
            MeshGeometryModel3D Spheremodel = new MeshGeometryModel3D();
            Spheremodel.CullMode = SharpDX.Direct3D11.CullMode.Back;
            List<UnityObject> ObjectZOn = Employer_View.unityObjects.FindAll(u => u.siteZone != null && u.siteZone.title == Zon.title);
            Vector3 centre = new Vector3();
            var builder1 = new MeshBuilder(true, true, true);
            builder1 = new MeshBuilder();
            for (int j = 0; j < ObjectZOn.Count; j++)
            {
                for (int k = 0; k < ObjectZOn[j].objectCoordinates.Length; k++)
                {
                    centre = centor;
                    centre.X = centre.X + ((ObjectZOn[j].objectCoordinates[k].c1.x + ObjectZOn[j].objectCoordinates[k].c2.x) / 2);
                    centre.Y = centre.Y - ((ObjectZOn[j].objectCoordinates[k].c1.y + ObjectZOn[j].objectCoordinates[k].c2.y) / 2);
                    centre.Z = centre.Z + ((ObjectZOn[j].objectCoordinates[k].c1.z + ObjectZOn[j].objectCoordinates[k].c2.z) / 2);
                    builder1.AddBox(centre, Math.Abs(ObjectZOn[j].objectCoordinates[k].c1.x - ObjectZOn[j].objectCoordinates[k].c2.x)
                        , Math.Abs(ObjectZOn[j].objectCoordinates[k].c1.y - ObjectZOn[j].objectCoordinates[k].c2.y), 1);
                }
            }
            Geometry3D SphereGeometry;
            SphereGeometry = builder1.ToMeshGeometry3D();
            SphereGeometry.UpdateOctree();
            //ModelGeometry[index].Model = SphereGeometry;
            //ModelGeometry[index].NameModel = Zon.title;
            ModelGeometry.RemoveAt(index);
            var a = (byte)220f;
            var r = (byte)System.Convert.ToUInt32(Zon.color.Substring(1, 2), 16);
            var g = (byte)System.Convert.ToUInt32(Zon.color.Substring(3, 2), 16);
            var b = (byte)System.Convert.ToUInt32(Zon.color.Substring(5, 2), 16);
            Color colorZon = new Color(r, g, b, a);
            PhongMaterial Material1;
            Material1 = new PhongMaterial()
            {
                DiffuseColor = colorZon,
                EmissiveColor = colorZon,
            };
            Spheremodel.Geometry = SphereGeometry;
            Spheremodel.Name = Zon.title;
            Spheremodel.IsThrowingShadow = true;
            Spheremodel.Material = Material1;
            ModelGeometry.Insert(index, new OITModel(SphereGeometry, Material1, true, Spheremodel, Zon.title, 0, 0, ModelGeometry.Count));
            //ModelGeometry[index].Material = Material1;
            return "OK";
        }

        public static async Task<string> CreateZone(string name, List<GeometryModel3D> unityObject, string colorobject)
        {
            MeshGeometryModel3D Spheremodel = new MeshGeometryModel3D();
            Spheremodel.CullMode = SharpDX.Direct3D11.CullMode.Back;
            var builder = new MeshBuilder(true, true, true);
            builder = new MeshBuilder();
            var a = (byte)220f;
            var r = (byte)System.Convert.ToUInt32(colorobject.Substring(1, 2), 16);
            var g = (byte)System.Convert.ToUInt32(colorobject.Substring(3, 2), 16);
            var b = (byte)System.Convert.ToUInt32(colorobject.Substring(5, 2), 16);
            Color colorZon = new Color(r, g, b, a);
            PhongMaterial Material1;
            Material1 = new PhongMaterial()
            {
                DiffuseColor = colorZon,
                EmissiveColor = colorZon,
            };
            Vector3 centre = new Vector3();
            List<int> indexin = new List<int>();
            for (int k = 0; k < unityObject.Count; k++)
            {

                centre = unityObject[k].Bounds.Center;
                indexin.Add(((OITModel)unityObject[k].DataContext).Indexposition);
                builder.AddBox(centre, Math.Abs(unityObject[k].Bounds.Minimum.X - unityObject[k].Bounds.Maximum.X)
                  , Math.Abs(unityObject[k].Bounds.Minimum.Y - unityObject[k].Bounds.Maximum.Y), 1);
            }
            for (int k = 0; k < unityObject.Count; k++)
            {
                ((OITModel)unityObject[k].DataContext).IndexMass.Clear();
                ((OITModel)unityObject[k].DataContext).IndexMass = indexin;
            }
            for (int k = 0; k < unityObject.Count; k++)
            {
                ((PhongMaterial)((OITModel)unityObject[k].DataContext).Material).DiffuseColor = Material1.DiffuseColor;
                ((PhongMaterial)((OITModel)unityObject[k].DataContext).Material).EmissiveColor = Material1.EmissiveColor;
            }
            Geometry3D SphereGeometry;
            SphereGeometry = builder.ToMeshGeometry3D();
            SphereGeometry.UpdateOctree();
            Spheremodel.Geometry = SphereGeometry;
            Spheremodel.Name = name;
            Spheremodel.IsThrowingShadow = true;
            Spheremodel.Material = Material1;
            ModelGeometry.Add(new OITModel(SphereGeometry, Material1, true, Spheremodel, name, 0, 0, ModelGeometry.Count));
            List<string> Objectunity = Employer_View.Zon.Find(u => u.title == name).unityObjects.ToList();
            Collection.Add(new Index_Name(name, ModelGeometry.Count - 1, Objectunity));
            return "OK";
        }

        public static async Task<string> UpdateZon(string name, List<GeometryModel3D> unityObject, string colorobject,int index)
        {
            MeshGeometryModel3D Spheremodel = new MeshGeometryModel3D();
            Spheremodel.CullMode = SharpDX.Direct3D11.CullMode.Back;
            List<UnityObject> ObjectZOn = Employer_View.unityObjects.FindAll(u => u.siteZone != null && u.siteZone.title == name);
            Vector3 centre = new Vector3();
            var builder1 = new MeshBuilder(true, true, true);
            builder1 = new MeshBuilder();
            List<int> indexin = new List<int>();
            var a = (byte)220f;
            var r = (byte)System.Convert.ToUInt32(colorobject.Substring(1, 2), 16);
            var g = (byte)System.Convert.ToUInt32(colorobject.Substring(3, 2), 16);
            var b = (byte)System.Convert.ToUInt32(colorobject.Substring(5, 2), 16);
            Color colorZon = new Color(r, g, b, a);
            PhongMaterial Material1;
            Material1 = new PhongMaterial()
            {
                DiffuseColor = colorZon,
                EmissiveColor = colorZon,
            };
            for (int j = 0; j < ObjectZOn.Count; j++)
            {
                indexin.Add(((OITModel)unityObject[j].DataContext).Indexposition);
                for (int k = 0; k < ObjectZOn[j].objectCoordinates.Length; k++)
                {
                    centre = centor;
                    centre.X = centre.X + ((ObjectZOn[j].objectCoordinates[k].c1.x + ObjectZOn[j].objectCoordinates[k].c2.x) / 2);
                    centre.Y = centre.Y - ((ObjectZOn[j].objectCoordinates[k].c1.y + ObjectZOn[j].objectCoordinates[k].c2.y) / 2);
                    centre.Z = centre.Z + ((ObjectZOn[j].objectCoordinates[k].c1.z + ObjectZOn[j].objectCoordinates[k].c2.z) / 2);
                    builder1.AddBox(centre, Math.Abs(ObjectZOn[j].objectCoordinates[k].c1.x - ObjectZOn[j].objectCoordinates[k].c2.x)
                        , Math.Abs(ObjectZOn[j].objectCoordinates[k].c1.y - ObjectZOn[j].objectCoordinates[k].c2.y), 1);
                }
            }
            for (int k = 0; k < unityObject.Count; k++)
            {
                ((OITModel)unityObject[k].DataContext).IndexMass.Clear();
                ((OITModel)unityObject[k].DataContext).IndexMass = indexin;
            }
            for (int k = 0; k < unityObject.Count; k++)
            {
                ((PhongMaterial)((OITModel)unityObject[k].DataContext).Material).DiffuseColor = Material1.DiffuseColor;
                ((PhongMaterial)((OITModel)unityObject[k].DataContext).Material).EmissiveColor = Material1.EmissiveColor;
            }
            Geometry3D SphereGeometry;
            SphereGeometry = builder1.ToMeshGeometry3D();
            SphereGeometry.UpdateOctree();
            ModelGeometry.RemoveAt(index);
            Spheremodel.Geometry = SphereGeometry;
            Spheremodel.Name = name;
            Spheremodel.IsThrowingShadow = true;
            Spheremodel.Material = Material1;
            ModelGeometry.Insert(index, new OITModel(SphereGeometry, Material1, true, Spheremodel, name, 0, 0, ModelGeometry.Count));
            List<string> Objectunity = Employer_View.Zon.Find(u => u.title == name).unityObjects.ToList();
            Collection.Find(u => u.nameZone == name).nameUnityObject = Objectunity;
            return "OK";
        }

        private void Zoning(object build, string name, ZonDataBase zon, int startindex, int lastindex)
        {
            Geometry3D SphereGeometry;
            SphereGeometry = ((MeshBuilder)build).ToMeshGeometry3D();
            SphereGeometry.UpdateOctree();
            MeshGeometryModel3D Spheremodel = new MeshGeometryModel3D();
            Spheremodel.Geometry = SphereGeometry;
            Spheremodel.Name = name;
            Spheremodel.IsThrowingShadow = true;
            if (zon != null)
            {
                var a = (byte)220f;
                var r = (byte)System.Convert.ToUInt32(zon.color.Substring(1, 2), 16);
                var g = (byte)System.Convert.ToUInt32(zon.color.Substring(3, 2), 16);
                var b = (byte)System.Convert.ToUInt32(zon.color.Substring(5, 2), 16);
                Color colorZon = new Color(r, g, b, a);
                PhongMaterial Material1;
                Material1 = new PhongMaterial()
                {
                    DiffuseColor = colorZon,
                    EmissiveColor = colorZon,
                };
                Spheremodel.Material = Material1;

                ModelGeometryZons.Add(new OITModel(SphereGeometry, Material1, true, Spheremodel, name, startindex, lastindex, ModelGeometryZons.Count));
            }
            else
            {
                Color colorZon = Color.White;
                PhongMaterial Material1;
                Material1 = new PhongMaterial()
                {
                    DiffuseColor = colorZon,
                    EmissiveColor = colorZon,
                };
                Spheremodel.Material = Material1;
                ModelGeometryZons.Add(new OITModel(SphereGeometry, Material1, true, Spheremodel, name, startindex, lastindex, ModelGeometryZons.Count));
            }
        }

        private int Create_Zon_4(Vector3 centre1, DynamicReflectionMap3D Sphere1dynamic, GroupModel3D Sphere1, int startindex)
        {
            for (int i = 0; i < Employer_View.Zon.Count; i++)
            {
                if (Employer_View.Zon[i].title.Substring(0, 3) == "UR4")
                {
                    List<UnityObject> ObjectZOn = Employer_View.unityObjects.FindAll(u => u.siteZone != null && u.siteZone.title == Employer_View.Zon[i].title);
                    MeshGeometryModel3D Spheremodel = new MeshGeometryModel3D();
                    Spheremodel.CullMode = SharpDX.Direct3D11.CullMode.Back;
                    var builder = new MeshBuilder(true, true, true);
                    builder = new MeshBuilder();
                    for (int j = 0; j < ObjectZOn.Count; j++)
                    {
                        var builder1 = new MeshBuilder(true, true, true);
                        builder1 = new MeshBuilder();
                        for (int k = 0; k < ObjectZOn[j].objectCoordinates.Length; k++)
                        {
                            Vector3 centre = centre1;
                            centre.X = centre.X + ((ObjectZOn[j].objectCoordinates[k].c1.x + ObjectZOn[j].objectCoordinates[k].c2.x) / 2);
                            centre.Y = centre.Y - ((ObjectZOn[j].objectCoordinates[k].c1.y + ObjectZOn[j].objectCoordinates[k].c2.y) / 2);
                            centre.Z = centre.Z + ((ObjectZOn[j].objectCoordinates[k].c1.z + ObjectZOn[j].objectCoordinates[k].c2.z) / 2);
                            builder.AddBox(centre, Math.Abs(ObjectZOn[j].objectCoordinates[k].c1.x - ObjectZOn[j].objectCoordinates[k].c2.x)
                                , Math.Abs(ObjectZOn[j].objectCoordinates[k].c1.y - ObjectZOn[j].objectCoordinates[k].c2.y), 1);
                            builder1.AddBox(centre, Math.Abs(ObjectZOn[j].objectCoordinates[k].c1.x - ObjectZOn[j].objectCoordinates[k].c2.x)
                                , Math.Abs(ObjectZOn[j].objectCoordinates[k].c1.y - ObjectZOn[j].objectCoordinates[k].c2.y), 1);
                        }
                        Zoning(builder1, ObjectZOn[j].unityObjectId, ObjectZOn[j].siteZone, startindex, startindex + ObjectZOn.Count - 1);
                    }
                    startindex += (ObjectZOn.Count);
                    Geometry3D SphereGeometry;
                    SphereGeometry = builder.ToMeshGeometry3D();
                    SphereGeometry.UpdateOctree();
                    Spheremodel.Geometry = SphereGeometry;
                    Spheremodel.Name = Employer_View.Zon[i].title;
                    Spheremodel.IsThrowingShadow = true;
                    var a = (byte)220f;
                    var r = (byte)System.Convert.ToUInt32(Employer_View.Zon[i].color.Substring(1, 2), 16);
                    var g = (byte)System.Convert.ToUInt32(Employer_View.Zon[i].color.Substring(3, 2), 16);
                    var b = (byte)System.Convert.ToUInt32(Employer_View.Zon[i].color.Substring(5, 2), 16);
                    Color colorZon = new Color(r, g, b, a);
                    PhongMaterial Material1;
                    Material1 = new PhongMaterial()
                    {
                        DiffuseColor = colorZon,
                        EmissiveColor = colorZon,
                    };
                    Spheremodel.Material = Material1;
                    Sphere1dynamic.Children.Add(Spheremodel);
                    Sphere1.Children.Add(Sphere1dynamic);
                    ModelGeometry.Add(new OITModel(SphereGeometry, Material1, true, Spheremodel, Employer_View.Zon[i].title,0,0, ModelGeometry.Count));
                    List<string> Objectunity = Employer_View.Zon[i].unityObjects.ToList();
                    Collection.Add(new Index_Name(Employer_View.Zon[i].title, ModelGeometry.Count - 1, Objectunity));
                }
            }
            startindex = create_Clear_Zon(centor, Sphere1dynamic, Sphere1, startindex);
            init = true;
            return startindex;
        }

        private int Create_Zon_3(Vector3 centre1, DynamicReflectionMap3D Sphere1dynamic, GroupModel3D Sphere1, int startindex)
        {
            for (int i = 0; i < Employer_View.Zon.Count; i++)
            {
                if (Employer_View.Zon[i].title.Substring(0, 3) == "UR3")
                {
                    List<UnityObject> ObjectZOn = Employer_View.unityObjects.FindAll(u => u.siteZone != null && u.siteZone.title == Employer_View.Zon[i].title);
                    MeshGeometryModel3D Spheremodel = new MeshGeometryModel3D();
                    Spheremodel.CullMode = SharpDX.Direct3D11.CullMode.Back;
                    var builder = new MeshBuilder(true, true, true);
                    builder = new MeshBuilder();
                    for (int j = 0; j < ObjectZOn.Count; j++)
                    {
                        var builder1 = new MeshBuilder(true, true, true);
                        builder1 = new MeshBuilder();
                        for (int k = 0; k < ObjectZOn[j].objectCoordinates.Length; k++)
                        {
                            Vector3 centre = centre1;
                            centre.X = centre.X + ((ObjectZOn[j].objectCoordinates[k].c1.x + ObjectZOn[j].objectCoordinates[k].c2.x) / 2);
                            centre.Y = centre.Y - ((ObjectZOn[j].objectCoordinates[k].c1.y + ObjectZOn[j].objectCoordinates[k].c2.y) / 2);
                            centre.Z = centre.Z + ((ObjectZOn[j].objectCoordinates[k].c1.z + ObjectZOn[j].objectCoordinates[k].c2.z) / 2);
                            builder.AddBox(centre, Math.Abs(ObjectZOn[j].objectCoordinates[k].c1.x - ObjectZOn[j].objectCoordinates[k].c2.x)
                                , Math.Abs(ObjectZOn[j].objectCoordinates[k].c1.y - ObjectZOn[j].objectCoordinates[k].c2.y), 1);
                            builder1.AddBox(centre, Math.Abs(ObjectZOn[j].objectCoordinates[k].c1.x - ObjectZOn[j].objectCoordinates[k].c2.x)
                                 , Math.Abs(ObjectZOn[j].objectCoordinates[k].c1.y - ObjectZOn[j].objectCoordinates[k].c2.y), 1);

                        }
                        Zoning(builder1, ObjectZOn[j].unityObjectId, ObjectZOn[j].siteZone, startindex, startindex + ObjectZOn.Count - 1);
                    }
                    startindex += (ObjectZOn.Count);
                    Geometry3D SphereGeometry;
                    SphereGeometry = builder.ToMeshGeometry3D();
                    SphereGeometry.UpdateOctree();
                    Spheremodel.Geometry = SphereGeometry;
                    Spheremodel.Name = Employer_View.Zon[i].title;
                    Spheremodel.IsThrowingShadow = true;
                    var a = (byte)220f;
                    var r = (byte)System.Convert.ToUInt32(Employer_View.Zon[i].color.Substring(1, 2), 16);
                    var g = (byte)System.Convert.ToUInt32(Employer_View.Zon[i].color.Substring(3, 2), 16);
                    var b = (byte)System.Convert.ToUInt32(Employer_View.Zon[i].color.Substring(5, 2), 16);
                    Color colorZon = new Color(r, g, b, a);
                    PhongMaterial Material1;
                    Material1 = new PhongMaterial()
                    {
                        DiffuseColor = colorZon,
                        EmissiveColor = colorZon,
                    };
                    Spheremodel.Material = Material1;
                    Sphere1dynamic.Children.Add(Spheremodel);
                    Sphere1.Children.Add(Sphere1dynamic);
                    ModelGeometry.Add(new OITModel(SphereGeometry, Material1, true, Spheremodel, Employer_View.Zon[i].title,0,0, ModelGeometry.Count));
                    List<string> Objectunity = Employer_View.Zon[i].unityObjects.ToList();
                    Collection.Add(new Index_Name(Employer_View.Zon[i].title, ModelGeometry.Count - 1, Objectunity));
                }
            }
            return startindex;
        }

        private int Create_Zon_2(Vector3 centre1, DynamicReflectionMap3D Sphere1dynamic, GroupModel3D Sphere1, int startindex)
        {
            for (int i = 0; i < Employer_View.Zon.Count; i++)
            {
                if (Employer_View.Zon[i].title.Substring(0, 3) == "UR2")
                {
                    List<UnityObject> ObjectZOn = Employer_View.unityObjects.FindAll(u => u.siteZone != null && u.siteZone.title == Employer_View.Zon[i].title);
                    MeshGeometryModel3D Spheremodel = new MeshGeometryModel3D();
                    Spheremodel.CullMode = SharpDX.Direct3D11.CullMode.Back;
                    var builder = new MeshBuilder(true, true, true);
                    builder = new MeshBuilder();
                    for (int j = 0; j < ObjectZOn.Count; j++)
                    {
                        var builder1 = new MeshBuilder(true, true, true);
                        builder1 = new MeshBuilder();
                        for (int k = 0; k < ObjectZOn[j].objectCoordinates.Length; k++)
                        {
                            Vector3 centre = centre1;
                            centre.X = centre.X + ((ObjectZOn[j].objectCoordinates[k].c1.x + ObjectZOn[j].objectCoordinates[k].c2.x) / 2);
                            centre.Y = centre.Y - ((ObjectZOn[j].objectCoordinates[k].c1.y + ObjectZOn[j].objectCoordinates[k].c2.y) / 2);
                            centre.Z = centre.Z + ((ObjectZOn[j].objectCoordinates[k].c1.z + ObjectZOn[j].objectCoordinates[k].c2.z) / 2);
                            builder.AddBox(centre, Math.Abs(ObjectZOn[j].objectCoordinates[k].c1.x - ObjectZOn[j].objectCoordinates[k].c2.x)
                                , Math.Abs(ObjectZOn[j].objectCoordinates[k].c1.y - ObjectZOn[j].objectCoordinates[k].c2.y), 1);
                            builder1.AddBox(centre, Math.Abs(ObjectZOn[j].objectCoordinates[k].c1.x - ObjectZOn[j].objectCoordinates[k].c2.x)
                                , Math.Abs(ObjectZOn[j].objectCoordinates[k].c1.y - ObjectZOn[j].objectCoordinates[k].c2.y), 1);
                        }
                        Zoning(builder1, ObjectZOn[j].unityObjectId, ObjectZOn[j].siteZone, startindex, startindex + ObjectZOn.Count - 1);
                    }
                    startindex += (ObjectZOn.Count);
                    Geometry3D SphereGeometry;
                    SphereGeometry = builder.ToMeshGeometry3D();
                    SphereGeometry.UpdateOctree();
                    Spheremodel.Geometry = SphereGeometry;
                    Spheremodel.Name = Employer_View.Zon[i].title;
                    Spheremodel.IsThrowingShadow = true;
                    var a = (byte)220f;
                    var r = (byte)System.Convert.ToUInt32(Employer_View.Zon[i].color.Substring(1, 2), 16);
                    var g = (byte)System.Convert.ToUInt32(Employer_View.Zon[i].color.Substring(3, 2), 16);
                    var b = (byte)System.Convert.ToUInt32(Employer_View.Zon[i].color.Substring(5, 2), 16);
                    Color colorZon = new Color(r, g, b, a);
                    PhongMaterial Material1;
                    Material1 = new PhongMaterial()
                    {
                        DiffuseColor = colorZon,
                        EmissiveColor = colorZon,
                    };
                    Spheremodel.Material = Material1;
                    Sphere1dynamic.Children.Add(Spheremodel);
                    Sphere1.Children.Add(Sphere1dynamic);
                    ModelGeometry.Add(new OITModel(SphereGeometry, Material1, true, Spheremodel, Employer_View.Zon[i].title,0,0, ModelGeometry.Count));
                    List<string> Objectunity = Employer_View.Zon[i].unityObjects.ToList();
                    Collection.Add(new Index_Name(Employer_View.Zon[i].title, ModelGeometry.Count - 1, Objectunity));
                }
            }
            return startindex;
        }

        private int create_Clear_Zon(Vector3 centre1, DynamicReflectionMap3D Sphere1dynamic, GroupModel3D Sphere1, int startindex)
        {
            List<UnityObject> ObjectZOn = Employer_View.unityObjects.FindAll(u => u.siteZone == null);
            MeshGeometryModel3D Spheremodel = new MeshGeometryModel3D();
            Spheremodel.CullMode = SharpDX.Direct3D11.CullMode.Back;
            for (int j = 0; j < ObjectZOn.Count; j++)
            {
                var builder = new MeshBuilder(true, true, true);
                builder = new MeshBuilder();
                for (int k = 0; k < ObjectZOn[j].objectCoordinates.Length; k++)
                {
                    Vector3 centre = centre1;
                    centre.X = centre.X + ((ObjectZOn[j].objectCoordinates[k].c1.x + ObjectZOn[j].objectCoordinates[k].c2.x) / 2);
                    centre.Y = centre.Y - ((ObjectZOn[j].objectCoordinates[k].c1.y + ObjectZOn[j].objectCoordinates[k].c2.y) / 2);
                    centre.Z = centre.Z + ((ObjectZOn[j].objectCoordinates[k].c1.z + ObjectZOn[j].objectCoordinates[k].c2.z) / 2);
                    builder.AddBox(centre, Math.Abs(ObjectZOn[j].objectCoordinates[k].c1.x - ObjectZOn[j].objectCoordinates[k].c2.x)
                        , Math.Abs(ObjectZOn[j].objectCoordinates[k].c1.y - ObjectZOn[j].objectCoordinates[k].c2.y), 1);
                }
                Zoning(builder, ObjectZOn[j].unityObjectId, ObjectZOn[j].siteZone, -1, -1);
            }
            startindex += ObjectZOn.Count;
            return startindex;
        }

        private int Create_Zon_1(Vector3 centre1, DynamicReflectionMap3D Sphere1dynamic, GroupModel3D Sphere1, int startindex)
        {
            for (int i = 0; i < Employer_View.Zon.Count; i++)
            {
                if (Employer_View.Zon[i].title.Substring(0, 3) == "UR1") // if (Employer_View.unityObjects.Find(u => u.id == Employer_View.Zon[i].unityObjects[0]).objectCoordinates[0].c1.y < 0)
                {
                    List<UnityObject> ObjectZOn = Employer_View.unityObjects.FindAll(u => u.siteZone != null && u.siteZone.title == Employer_View.Zon[i].title);
                    MeshGeometryModel3D Spheremodel = new MeshGeometryModel3D();
                    Spheremodel.CullMode = SharpDX.Direct3D11.CullMode.Back;
                    var builder = new MeshBuilder(true, true, true);
                    builder = new MeshBuilder();
                    for (int j = 0; j < ObjectZOn.Count; j++)
                    {
                        var builder1 = new MeshBuilder(true, true, true);
                        builder1 = new MeshBuilder();
                        for (int k = 0; k < ObjectZOn[j].objectCoordinates.Length; k++)
                        {
                            Vector3 centre = centre1;
                            centre.X = centre.X + ((ObjectZOn[j].objectCoordinates[k].c1.x + ObjectZOn[j].objectCoordinates[k].c2.x) / 2);
                            centre.Y = centre.Y - ((ObjectZOn[j].objectCoordinates[k].c1.y + ObjectZOn[j].objectCoordinates[k].c2.y) / 2);
                            centre.Z = centre.Z + ((ObjectZOn[j].objectCoordinates[k].c1.z + ObjectZOn[j].objectCoordinates[k].c2.z) / 2);
                            builder.AddBox(centre, Math.Abs(ObjectZOn[j].objectCoordinates[k].c1.x - ObjectZOn[j].objectCoordinates[k].c2.x)
                                , Math.Abs(ObjectZOn[j].objectCoordinates[k].c1.y - ObjectZOn[j].objectCoordinates[k].c2.y), 1);
                            builder1.AddBox(centre, Math.Abs(ObjectZOn[j].objectCoordinates[k].c1.x - ObjectZOn[j].objectCoordinates[k].c2.x)
                                , Math.Abs(ObjectZOn[j].objectCoordinates[k].c1.y - ObjectZOn[j].objectCoordinates[k].c2.y), 1);
                        }
                        Zoning(builder1, ObjectZOn[j].unityObjectId, ObjectZOn[j].siteZone, startindex, startindex + ObjectZOn.Count -1);
                    }
                    startindex += (ObjectZOn.Count);
                    Geometry3D SphereGeometry;
                    SphereGeometry = builder.ToMeshGeometry3D();
                    //BillboardTextModel3D text1 = new BillboardTextModel3D();
                    //BillboardSingleText3D text = new BillboardSingleText3D();
                    //text.TextInfo = new TextInfo("23", new Vector3(10.8280244f, 10.665166f, 10.356252f));
                    //text.FontColor = Color.Blue.ToColor4();
                    //text.FontSize = 20;
                    ////text.Bound = SphereGeometry.Bound;
                    //text.BackgroundColor = Color.DarkGray;
                    //text1.Geometry = text;
                    SphereGeometry.UpdateOctree();
                    Spheremodel.Geometry = SphereGeometry;
                    Spheremodel.Name = Employer_View.Zon[i].title;
                    Spheremodel.IsThrowingShadow = true;
                    var a = (byte)220f;
                    var r = (byte)System.Convert.ToUInt32(Employer_View.Zon[i].color.Substring(1, 2), 16);
                    var g = (byte)System.Convert.ToUInt32(Employer_View.Zon[i].color.Substring(3, 2), 16);
                    var b = (byte)System.Convert.ToUInt32(Employer_View.Zon[i].color.Substring(5, 2), 16);
                    Color colorZon = new Color(r, g, b, a);
                    PhongMaterial Material1;
                    Material1 = new PhongMaterial()
                    {
                        DiffuseColor = colorZon,
                        EmissiveColor = colorZon,
                    };
                    Spheremodel.Material = Material1;
                    Sphere1dynamic.Children.Add(Spheremodel);
                    Sphere1.Children.Add(Sphere1dynamic);
                    ModelGeometry.Add(new OITModel(SphereGeometry, Material1, true, Spheremodel, Employer_View.Zon[i].title,0,0, ModelGeometry.Count));
                    List<string> Objectunity = Employer_View.Zon[i].unityObjects.ToList();
                    Collection.Add(new Index_Name(Employer_View.Zon[i].title, ModelGeometry.Count - 1, Objectunity));
                    //ModelGeometryText.Add(new OITModelText(text));
                }
            }
            return startindex;
        }
    }
}
