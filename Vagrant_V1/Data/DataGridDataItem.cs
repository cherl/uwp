﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using Vagrant_V1.ModelData;
using Vagrant_V1.Pageees;

namespace Vagrant_V1.Data
{
        public class DataGridDataItem : INotifyDataErrorInfo, IComparable, INotifyPropertyChanged
    {
            private Dictionary<string, List<string>> _errors = new Dictionary<string, List<string>>();
            private string fio;
            private string id_employer;
            private string position;
            private string unit;
            private uint team;
            private string tagg;
            private string zon_name;
            private string name;
            private string zon;
            private uint number;
            private uint time;

        public event PropertyChangedEventHandler PropertyChanged;
        public event EventHandler<DataErrorsChangedEventArgs> ErrorsChanged;

        private void NotifyPropertyChanged([CallerMemberName] String propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        public string Tagg
        {
                get { return tagg; }
                set 
                {
                    string value1="";
                    if (value == "")
                    {
                        value1 = "Нет метки";
                    }
                    else
                    {
                       value1 = value;
                    }
                if (tagg != value1)
                    {
                       if (framePage.flagRename == true)
                       {
                        if(tagg == "Нет метки")
                        {
                            AdministrationPage.Update_DataAsync(value, "");
                        }
                        else
                        {
                            AdministrationPage.Update_DataAsync(value, tagg);
                        }
                       }
                       tagg = value1;
                    }
                NotifyPropertyChanged();
                }
        }


            public string FIO
            {
                get { return fio; }
                set
                {
                    if (fio != value)
                    {
                        if(value == "")
                           fio = "Нет данных";
                        else
                           fio = value;
                        bool isFioValid = !_errors.Keys.Contains("FIO");
                        if (fio == string.Empty && isFioValid)
                        {
                            List<string> errors = new List<string>();
                            errors.Add("FIO name cannot be empty");
                            _errors.Add("FIO", errors);
                            this.ErrorsChanged?.Invoke(this, new DataErrorsChangedEventArgs("FIO"));
                        }
                        else if (fio != string.Empty && !isFioValid)
                        {
                            _errors.Remove("FIO");
                            this.ErrorsChanged?.Invoke(this, new DataErrorsChangedEventArgs("FIO"));
                        }
                    }
                NotifyPropertyChanged();
                }
            }

            public string Id_Employer
            {
                get { return id_employer; }
                set
                {
                    if (id_employer != value)
                    {
                    if (value == "")
                        id_employer = "Нет данных";
                    else
                        id_employer = value;
                        bool isid_employerValid = !_errors.Keys.Contains("Tag");
                        if (id_employer == string.Empty && isid_employerValid)
                        {
                            List<string> errors = new List<string>();
                            errors.Add("Tag name cannot be empty");
                            _errors.Add("Tag", errors);
                            this.ErrorsChanged?.Invoke(this, new DataErrorsChangedEventArgs("Tag"));
                        }
                        else if (id_employer != string.Empty && !isid_employerValid)
                        {
                            _errors.Remove("Tag");
                            this.ErrorsChanged?.Invoke(this, new DataErrorsChangedEventArgs("Tag"));
                        }
                    }
                NotifyPropertyChanged();
            }
            }

            public string Position
            {
                get { return position; }
                set
                {
                if(value != null)
                {
                    if (position != value)
                    {
                        if (value == "")
                            position = "Нет данных";
                        else
                            position = value;
                        bool ispositionValid = !_errors.Keys.Contains("Position");
                        if (position == string.Empty && ispositionValid)
                        {
                            List<string> errors = new List<string>();
                            errors.Add("Position name cannot be empty");
                            _errors.Add("Position", errors);
                            this.ErrorsChanged?.Invoke(this, new DataErrorsChangedEventArgs("Position"));
                        }
                        else if (position != string.Empty && !ispositionValid)
                        {
                            _errors.Remove("Position");
                            this.ErrorsChanged?.Invoke(this, new DataErrorsChangedEventArgs("Position"));
                        }
                    }
                }
                else
                {
                        position = "Нет данных";
                }
                NotifyPropertyChanged();
            }
            }

            public string Unit
            {
                get { return unit; }
                set
                {
                    if (unit != value)
                    {
                    if (value == "")
                        unit = "Нет данных";
                    else
                        unit = value;
                        bool isunitValid = !_errors.Keys.Contains("Unit");
                        if (unit == string.Empty && isunitValid)
                        {
                            List<string> errors = new List<string>();
                            errors.Add("Unit name cannot be empty");
                            _errors.Add("Unit", errors);
                            this.ErrorsChanged?.Invoke(this, new DataErrorsChangedEventArgs("Unit"));
                        }
                        else if (unit != string.Empty && !isunitValid)
                        {
                            _errors.Remove("Unit");
                            this.ErrorsChanged?.Invoke(this, new DataErrorsChangedEventArgs("Unit"));
                        }
                    }
                NotifyPropertyChanged();
            }
            }

        public string Zon_name
        {
            get { return zon_name; }
            set
            {
                if (value != null)
                {
                    if (zon_name != value)
                    {
                            zon_name = value;
                    }
                }
                else
                {
                    zon_name = "";
                }
                NotifyPropertyChanged();
            }
        }

        public uint Team
            {
                get { return team; }
                set
                {
                    if (team != value)
                    {
                        team = value;
                    }
                NotifyPropertyChanged();
            }
            }


            public string Name
            {
            get { return name; }
            set
            {
                if (name != value)
                {
                    if (value == "")
                        name = "Нет данных";
                    else
                        name = value;
                }
                NotifyPropertyChanged();
            }
            }

            public uint Time
            {
            get { return time; }
            set
            {
                if (time != value)
                {
                    time = value;
                }
                NotifyPropertyChanged();
            }
            }

            public uint Number
            {
            get { return number; }
            set
            {
                if (number != value)
                {
                    number = value;
                }
                NotifyPropertyChanged();
            }
            }

            public string Zon
            {
            get { return zon; }
            set
            {
                if (zon != value)
                {
                    zon = value;
                }
                NotifyPropertyChanged();
            }
            }

        bool INotifyDataErrorInfo.HasErrors
            {
                get
                {
                    return _errors.Keys.Count > 0;
                }
            }

            IEnumerable INotifyDataErrorInfo.GetErrors(string propertyName)
            {
                if (propertyName == null)
                {
                    propertyName = string.Empty;
                }

                if (_errors.Keys.Contains(propertyName))
                {
                    return _errors[propertyName];
                }
                else
                {
                    return null;
                }
            }

            int IComparable.CompareTo(object obj)
            {
                int lnCompare = team.CompareTo((obj as DataGridDataItem).team);

                if (lnCompare == 0)
                {
                    return fio.CompareTo((obj as DataGridDataItem).fio);
                }
                else
                {
                    return lnCompare;
                }
            }
        }

}
