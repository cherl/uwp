﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Vagrant_V1.Server;
using Windows.System.Threading;
using Windows.UI.Core;
using Windows.UI.Xaml.Data;

namespace Vagrant_V1.Data
{
    [Bindable]
    class DataGridDataSourceadmin
    {
        private static ObservableCollection<DataGridDataItem> _items;
        private static List<string> id_employer;
        public static List<string> Tag_all;
        private static CollectionViewSource groupedItems;
        private string _cachedSortedColumn = string.Empty;
        public static string _cachedSortedColumnstatus = string.Empty;

        public async Task<IEnumerable<DataGridDataItem>> GetDataAsync()
        {
            _items = new ObservableCollection<DataGridDataItem>(DataGridDataSource._itemsALL);
            return _items;
        }
        public async Task<IEnumerable<string>> GetFIO()
        {
            if (_items == null || !_items.Any())
            {
                await GetDataAsync();
            }

            id_employer = _items?.OrderBy(x => x.Id_Employer).Select(x => x.Id_Employer).Distinct().ToList();

            return id_employer;
        }

        public async Task<IEnumerable<string>> GetTag()
        {
            if (Employer_View.Tag == null || !Employer_View.Tag.Any())
            {
                await GetDataAsync();
            }

            Tag_all = await Employer_View.IninClearTagAsync();

            return Tag_all;
        }

        public static async Task<IEnumerable<string>> Update()
        {
            Tag_all = await Employer_View.IninClearTagAsync();
            DataGridDataSource.name1.Clear();
            DataGridDataSource.name2.Clear();
            foreach (DataGridDataItem v in DataGridDataSource._itemsALL)
            {
                if (v.Tagg != "")
                {
                    DataGridDataSource.name1.Add(v.FIO);
                    DataGridDataSource.name1.Add(v.Id_Employer);
                    DataGridDataSource.name1.Add(v.Tagg);
                    DataGridDataSource.name2.Add(v.Tagg);
                }
                DataGridDataSource.name2.Add(v.FIO);
                DataGridDataSource.name2.Add(v.Id_Employer);
            }
            return Tag_all;
        }

        // Sorting implementation using LINQ
        public string CachedSortedColumn
        {
            get
            {
                return _cachedSortedColumn;
            }

            set
            {
                _cachedSortedColumn = value;
                _cachedSortedColumnstatus = value;
            }
        }

        public ObservableCollection<DataGridDataItem> SortData(string sortBy, bool ascending, string name)
        {
            _cachedSortedColumn = sortBy;
            _cachedSortedColumnstatus = ascending.ToString();
            if (name == "")
                switch (sortBy)
                {
                    case "FIO":
                        return ascending
                            ? new ObservableCollection<DataGridDataItem>(_items.OrderBy(t => t.FIO))
                            : new ObservableCollection<DataGridDataItem>(_items.OrderByDescending(t => t.FIO));
                    case "Id_Employer":
                        return ascending
                            ? new ObservableCollection<DataGridDataItem>(_items.OrderBy(t => t.Id_Employer))
                            : new ObservableCollection<DataGridDataItem>(_items.OrderByDescending(t => t.Id_Employer));

                    case "Position":
                        return ascending
                        ? new ObservableCollection<DataGridDataItem>(_items.OrderBy(t => t.Position))
                        : new ObservableCollection<DataGridDataItem>(_items.OrderByDescending(t => t.Position));

                    case "Unit":
                        return ascending
                        ? new ObservableCollection<DataGridDataItem>(_items.OrderBy(t => t.Unit))
                        : new ObservableCollection<DataGridDataItem>(_items.OrderByDescending(t => t.Unit));

                    case "Team":
                        return ascending
                        ? new ObservableCollection<DataGridDataItem>(_items.OrderBy(t => t.Team))
                        : new ObservableCollection<DataGridDataItem>(_items.OrderByDescending(t => t.Team));
                    case "Tagg":
                        return ascending
                        ? new ObservableCollection<DataGridDataItem>(_items.OrderBy(t => t.Tagg))
                        : new ObservableCollection<DataGridDataItem>(_items.OrderByDescending(t => t.Tagg));
                    case "Zon_name":
                        return ascending
                        ? new ObservableCollection<DataGridDataItem>(_items.OrderBy(t => t.Zon_name))
                        : new ObservableCollection<DataGridDataItem>(_items.OrderByDescending(t => t.Zon_name));
                }
            else if (name != "")
            {
                switch (sortBy)
                {
                    case "FIO":
                        return ascending
                            ? new ObservableCollection<DataGridDataItem>(_items.Where(t => t.Zon_name == name).OrderBy(t => t.FIO))
                            : new ObservableCollection<DataGridDataItem>(_items.Where(t => t.Zon_name == name).OrderByDescending(t => t.FIO));
                    case "Id_Employer":
                        return ascending
                            ? new ObservableCollection<DataGridDataItem>(_items.Where(t => t.Zon_name == name).OrderBy(t => t.Id_Employer))
                            : new ObservableCollection<DataGridDataItem>(_items.Where(t => t.Zon_name == name).OrderByDescending(t => t.Id_Employer));

                    case "Position":
                        return ascending
                        ? new ObservableCollection<DataGridDataItem>(_items.Where(t => t.Zon_name == name).OrderBy(t => t.Position))
                        : new ObservableCollection<DataGridDataItem>(_items.Where(t => t.Zon_name == name).OrderByDescending(t => t.Position));

                    case "Unit":
                        return ascending
                        ? new ObservableCollection<DataGridDataItem>(_items.Where(t => t.Zon_name == name).OrderBy(t => t.Unit))
                        : new ObservableCollection<DataGridDataItem>(_items.Where(t => t.Zon_name == name).OrderByDescending(t => t.Unit));

                    case "Team":
                        return ascending
                        ? new ObservableCollection<DataGridDataItem>(_items.Where(t => t.Zon_name == name).OrderBy(t => t.Team))
                        : new ObservableCollection<DataGridDataItem>(_items.Where(t => t.Zon_name == name).OrderByDescending(t => t.Team));
                    case "Tagg":
                        return ascending
                        ? new ObservableCollection<DataGridDataItem>(_items.Where(t => t.Zon_name == name).OrderBy(t => t.Tagg))
                        : new ObservableCollection<DataGridDataItem>(_items.Where(t => t.Zon_name == name).OrderByDescending(t => t.Tagg));
                    case "Zon_name":
                        return ascending
                        ? new ObservableCollection<DataGridDataItem>(_items.Where(t => t.Zon_name == name).OrderBy(t => t.Zon_name))
                        : new ObservableCollection<DataGridDataItem>(_items.Where(t => t.Zon_name == name).OrderByDescending(t => t.Zon_name));
                }
            }


            return _items;
        }

        // Grouping implementation using LINQ
        public CollectionViewSource GroupData()
        {
            ObservableCollection<GroupInfoCollection<DataGridDataItem>> groups = new ObservableCollection<GroupInfoCollection<DataGridDataItem>>();

            //_items.


            var query = from item in _items
                        orderby item
                        group item by item.Team into g
                        select new { GroupName = g.Key, Items = g };

            foreach (var g in query)
            {
                GroupInfoCollection<DataGridDataItem> info = new GroupInfoCollection<DataGridDataItem>
                {
                    Key = g.GroupName
                };
                g.Items.ToList().ForEach(item => info.Add(item));
                groups.Add(info);
            }


            //foreach (var g in query)
            //{
            //    GroupInfoCollection<DataGridDataItem> info = new GroupInfoCollection<DataGridDataItem>();
            //    info.Key = g.GroupName;
            //    foreach (var item in g.Items)
            //    {
            //        info.Add(item);
            //    }

            //    groups.Add(info);
            //}

            groupedItems = new CollectionViewSource();
            groupedItems.IsSourceGrouped = true;
            groupedItems.Source = groups;

            return groupedItems;
        }

        public class GroupInfoCollection<T> : ObservableCollection<T>
        {
            public object Key { get; set; }

            public new IEnumerator<T> GetEnumerator()
            {
                return (IEnumerator<T>)base.GetEnumerator();
            }
        }

        // Filtering implementation using LINQ
        public enum FilterOptions
        {
            All = -1,
            Rank_Low = 0,
            Rank_High = 1,
            Height_Low = 2,
            Height_High = 3,
            Zon = 4
        }
    }
}
