﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Vagrant_V1.ModelData;
using Vagrant_V1.Server;
using Windows.System.Threading;
using Windows.UI.Core;
using Windows.UI.Xaml.Data;

namespace Vagrant_V1.Data
{
    [Bindable]
    public class DataGridDataSource
    {
        private static ObservableCollection<DataGridDataItem> _items;
        public static List<DataGridDataItem> _itemsALL;
        public static List<DataGridDataItem> _itemsList = new List<DataGridDataItem>();
        private static List<string> id_employer;
        private static List<string> Tag_all;
        private static CollectionViewSource groupedItems;
        private string _cachedSortedColumn = string.Empty;
        public static List<string> name1 = new List<string>();
        public static List<string> name2 = new List<string>();
        public static string _cachedSortedColumnstatus = string.Empty;

        // Loading data
        public async Task<IEnumerable<DataGridDataItem>> GetDataAsync()
        {
            await Employer_View.GeneratePositionAsync();
            //_itemsALL = new List<DataGridDataItem>();
            //foreach(EmployerDataBase v in Employer_View.EmployerDataBase)
            //{
            //    string title = Employer_View.Zon.Find(z => z.id == Employer_View.Position.Find(u => u.id == v.id).gridElementId).title;
            //    string description = Employer_View.Zon.Find(z => z.id == Employer_View.Position.Find(u => u.id == v.id).gridElementId).description;
            //    _itemsALL.Add(v.title,v.id,v.position, uint.Parse(v.team), v.unit, v.title, v.tag == null ? "" : v.tag.title, title,0,1000, description);
            //}
            _itemsALL = new List<DataGridDataItem>(
            Employer_View
                .EmployerDataBase
                .Select(v => new DataGridDataItem()
                {
                    FIO = v.title,
                    Id_Employer = v.id,
                    Position = (v.position == null || v.position == "") ? "" : v.position,
                    Team = (v.team == null || v.team == "") ? 0 : uint.Parse(v.team),
                    Unit = (v.unit == null || v.unit == "") ? "" : v.unit,
                    Name = v.title,
                    Tagg = (v.tag == null) ? "" : v.tag.title,
                    Zon_name = (Employer_View.Position.Find(u => u.id == v.id) == null) 
                    ? "" 
                    : Employer_View.Zon.Find(z => z.id == Employer_View.Position.Find(u => u.id == v.id).gridElementId) == null 
                    ? ""
                    : Employer_View.Zon.Find(z => z.id == Employer_View.Position.Find(u => u.id == v.id).gridElementId).title,
                    Time = 0,
                    Number = 0,
                    Zon = (Employer_View.Position.Find(u => u.id == v.id) == null)
                    ? ""
                    : Employer_View.Zon.Find(z => z.id == Employer_View.Position.Find(u => u.id == v.id).gridElementId) == null 
                    ? ""
                    : Employer_View.Zon.Find(z => z.id == Employer_View.Position.Find(u => u.id == v.id).gridElementId).description
                }));
                _items = new ObservableCollection<DataGridDataItem>(_itemsALL.FindAll(u => u.Tagg != "Нет метки" && u.Zon_name != ""));
                _itemsList = _items.ToList();
            name1.Clear();
            name2.Clear();
            foreach (DataGridDataItem v in _items)
            {
                    name1.Add(v.FIO);
                    name1.Add(v.Id_Employer);
                    name1.Add(v.Tagg);
                    name2.Add(v.Tagg);
                    name2.Add(v.FIO);
                    name2.Add(v.Id_Employer);
            }
            return _items;
        }

        public static async Task<IEnumerable<DataGridDataItem>> Updatedata(float timer)
        {
            if(MainPage.modifytag == false)
            {
                if (timer == 5)
                {
                    for (int i = 0; i < _itemsALL.Count; i++)
                    {
                        if (Employer_View.Position.Find(y => y.id == _itemsALL[i].Id_Employer) != null && Employer_View.Zon.Find(u => u.id == Employer_View.Position.Find(y => y.id == _itemsALL[i].Id_Employer).gridElementId) != null)
                        {
                            if (_itemsALL[i].Zon_name != Employer_View.Zon.Find(u => u.id == Employer_View.Position.Find(y => y.id == _itemsALL[i].Id_Employer).gridElementId).title)
                            {
                                if (Employer_View.Position.Find(y => y.id == _itemsALL[i].Id_Employer).gridElementId != null)
                                {
                                    if (_itemsALL[i].Id_Employer == MainPage.name_people)
                                    {
                                        MainPage.name_zon = Employer_View.Zon.Find(u => u.id == Employer_View.Position.Find(y => y.id == _itemsALL[i].Id_Employer).gridElementId).title;
                                    }
                                    _itemsALL[i].Time += 1;
                                    _itemsALL[i].Zon_name = Employer_View.Zon.Find(u => u.id == Employer_View.Position.Find(y => y.id == _itemsALL[i].Id_Employer).gridElementId).title;
                                    _itemsALL[i].Zon = Employer_View.Zon.Find(u => u.id == Employer_View.Position.Find(y => y.id == _itemsALL[i].Id_Employer).gridElementId).description;
                                    _itemsALL[i].Time = 0;
                                }
                                else
                                {
                                    if (_itemsALL[i].Id_Employer == MainPage.name_people)
                                    {
                                        MainPage.name_zon = "";
                                        _itemsALL[i].Time = 0;
                                        _itemsALL[i].Zon = "";
                                        _itemsALL[i].Zon_name = "";
                                    }
                                }
                            }
                            else
                            {
                                _itemsALL[i].Time += 1;
                            }
                        }
                        else
                        {
                            _itemsALL[i].Zon_name = "";
                            _itemsALL[i].Zon = "";
                            _itemsALL[i].Time = 0;
                        }
                    }
                    List<DataGridDataItem> List = new List<DataGridDataItem>();
                    List = _itemsALL.FindAll(u => u.Zon == "");
                    for (int i=0; i< List.Count; i++)
                    {
                        if(_itemsList.Find(u => u.Id_Employer == List[i].Id_Employer) != null)
                        {
                            _items.Remove(_itemsList.Find(u => u.Id_Employer == List[i].Id_Employer));
                        }
                    }
                    List.Clear();
                    List = _itemsALL.FindAll(u => u.Zon != "" && u.Tagg != "Нет метки");
                    for (int i = 0; i < List.Count; i++)
                    {
                        if (_itemsList.Find(u => u.Id_Employer == List[i].Id_Employer) == null)
                        {
                            _items.Add(_itemsList.Find(u => u.Id_Employer == List[i].Id_Employer));
                        }
                    }
                }
               else
               {
                  foreach (DataGridDataItem v in _items)
                  {
                      v.Time += 1;
                  }
               }
            }
            else
            {
                for (int i = 0; i < _itemsALL.Count; i++)
                {
                    if (timer == 5)
                    {
                        if (Employer_View.Zon.Find(u => u.id == Employer_View.Position.Find(y => y.id == _itemsALL[i].Id_Employer).gridElementId) != null)
                        {
                            if (_itemsALL[i].Zon_name != Employer_View.Zon.Find(u => u.id == Employer_View.Position.Find(y => y.id == _itemsALL[i].Id_Employer).gridElementId).title)
                            {
                                if (Employer_View.Position.Find(y => y.id == _itemsALL[i].Id_Employer).gridElementId != null)
                                {
                                    if (_itemsALL[i].Id_Employer == MainPage.name_people)
                                    {
                                        MainPage.name_zon = Employer_View.Zon.Find(u => u.id == Employer_View.Position.Find(y => y.id == _itemsALL[i].Id_Employer).gridElementId).title;
                                    }
                                    _itemsALL[i].Time += 1;
                                    _itemsALL[i].Zon_name = Employer_View.Zon.Find(u => u.id == Employer_View.Position.Find(y => y.id == _itemsALL[i].Id_Employer).gridElementId).title;
                                    _itemsALL[i].Zon = Employer_View.Zon.Find(u => u.id == Employer_View.Position.Find(y => y.id == _itemsALL[i].Id_Employer).gridElementId).description;
                                    _itemsALL[i].Time = 0;
                                }
                                else
                                {
                                    if (_itemsALL[i].Id_Employer == MainPage.name_people)
                                    {
                                        MainPage.name_zon = "";
                                        _itemsALL[i].Time = 0;
                                        _itemsALL[i].Zon = "-";
                                        _itemsALL[i].Zon_name = "Не на объекте";
                                    }
                                }
                            }
                            else
                            {
                                _itemsALL[i].Time += 1;
                            }
                        }
                        else
                        {
                            _itemsALL[i].Zon_name = "Обновляется";
                            _itemsALL[i].Zon = "Обновляется";
                            _itemsALL[i].Time = 0;
                        }
                    }
                    else
                    {
                        _itemsALL[i].Time += 1;
                    }
                }

            }
            return _items;
        }

        public static async Task<IEnumerable<DataGridDataItem>> UpdateTag()
        {
            _items.Clear();
            _items = new ObservableCollection<DataGridDataItem>(_itemsALL.FindAll(u => u.Tagg != "Нет метки"));
            return _items;
        }


        // Load mountains into separate collection for use in combobox column
        public async Task<IEnumerable<string>> GetFIO()
        {
            if (_items == null || !_items.Any())
            {
                await GetDataAsync();
            }

            id_employer = _items?.OrderBy(x => x.Id_Employer).Select(x => x.Id_Employer).Distinct().ToList();

            return id_employer;
        }

        public async Task<IEnumerable<string>> GetTag()
        {
            if (Employer_View.Tag == null || !Employer_View.Tag.Any())
            {
                await GetDataAsync();
            }

            Tag_all = await Employer_View.IninClearTagAsync();

            return Tag_all;
        }

        // Sorting implementation using LINQ
        public string CachedSortedColumn
        {
            get
            {
                return _cachedSortedColumn;
            }

            set
            {
                _cachedSortedColumn = value;
                _cachedSortedColumnstatus = value;
            }
        }

        public ObservableCollection<DataGridDataItem> SortData(string sortBy, bool ascending, string name)
        {
            _cachedSortedColumn = sortBy;
            _cachedSortedColumnstatus = ascending.ToString();
            if (name == "")
                switch (sortBy)
                {
                    case "FIO":
                        return ascending
                            ? new ObservableCollection<DataGridDataItem>(_items.OrderBy(t => t.FIO))
                            : new ObservableCollection<DataGridDataItem>(_items.OrderByDescending(t => t.FIO));
                    case "Id_Employer":
                        return ascending
                            ? new ObservableCollection<DataGridDataItem>(_items.OrderBy(t => t.Id_Employer))
                            : new ObservableCollection<DataGridDataItem>(_items.OrderByDescending(t => t.Id_Employer));

                    case "Position":
                            return ascending
                            ? new ObservableCollection<DataGridDataItem>(_items.OrderBy(t => t.Position))
                            : new ObservableCollection<DataGridDataItem>(_items.OrderByDescending(t => t.Position));

                    case "Unit":
                        return ascending
                        ? new ObservableCollection<DataGridDataItem>(_items.OrderBy(t => t.Unit))
                        : new ObservableCollection<DataGridDataItem>(_items.OrderByDescending(t => t.Unit));

                    case "Team":
                        return ascending
                        ? new ObservableCollection<DataGridDataItem>(_items.OrderBy(t => t.Team))
                        : new ObservableCollection<DataGridDataItem>(_items.OrderByDescending(t => t.Team));
                    case "Tagg":
                        return ascending
                        ? new ObservableCollection<DataGridDataItem>(_items.OrderBy(t => t.Tagg))
                        : new ObservableCollection<DataGridDataItem>(_items.OrderByDescending(t => t.Tagg));
                    case "Zon_name":
                        return ascending
                        ? new ObservableCollection<DataGridDataItem>(_items.OrderBy(t => t.Zon_name))
                        : new ObservableCollection<DataGridDataItem>(_items.OrderByDescending(t => t.Zon_name));
                }
            else if(name !="")
            {
                switch (sortBy)
                {
                    case "FIO":
                        return ascending
                            ? new ObservableCollection<DataGridDataItem>(_items.Where(t => t.Zon_name == name).OrderBy(t => t.FIO))
                            : new ObservableCollection<DataGridDataItem>(_items.Where(t => t.Zon_name == name).OrderByDescending(t => t.FIO));
                    case "Id_Employer":
                        return ascending
                            ? new ObservableCollection<DataGridDataItem>(_items.Where(t => t.Zon_name == name).OrderBy(t => t.Id_Employer))
                            : new ObservableCollection<DataGridDataItem>(_items.Where(t => t.Zon_name == name).OrderByDescending(t => t.Id_Employer));

                    case "Position":
                        return ascending
                        ? new ObservableCollection<DataGridDataItem>(_items.Where(t => t.Zon_name == name).OrderBy(t => t.Position))
                        : new ObservableCollection<DataGridDataItem>(_items.Where(t => t.Zon_name == name).OrderByDescending(t => t.Position));

                    case "Unit":
                        return ascending
                        ? new ObservableCollection<DataGridDataItem>(_items.Where(t => t.Zon_name == name).OrderBy(t => t.Unit))
                        : new ObservableCollection<DataGridDataItem>(_items.Where(t => t.Zon_name == name).OrderByDescending(t => t.Unit));

                    case "Team":
                        return ascending
                        ? new ObservableCollection<DataGridDataItem>(_items.Where(t => t.Zon_name == name).OrderBy(t => t.Team))
                        : new ObservableCollection<DataGridDataItem>(_items.Where(t => t.Zon_name == name).OrderByDescending(t => t.Team));
                    case "Tagg":
                        return ascending
                        ? new ObservableCollection<DataGridDataItem>(_items.Where(t => t.Zon_name == name).OrderBy(t => t.Tagg))
                        : new ObservableCollection<DataGridDataItem>(_items.Where(t => t.Zon_name == name).OrderByDescending(t => t.Tagg));
                    case "Zon_name":
                        return ascending
                        ? new ObservableCollection<DataGridDataItem>(_items.Where(t => t.Zon_name == name).OrderBy(t => t.Zon_name))
                        : new ObservableCollection<DataGridDataItem>(_items.Where(t => t.Zon_name == name).OrderByDescending(t => t.Zon_name));
                }
            }


            return _items;
        }

        // Grouping implementation using LINQ
        public CollectionViewSource GroupData()
        {
            ObservableCollection<GroupInfoCollection<DataGridDataItem>> groups = new ObservableCollection<GroupInfoCollection<DataGridDataItem>>();

            //_items.


            var query = from item in _items
                        orderby item
                        group item by item.Team into g
                        select new { GroupName = g.Key, Items = g };

            foreach (var g in query)
            {
                GroupInfoCollection<DataGridDataItem> info = new GroupInfoCollection<DataGridDataItem>
                {
                    Key = g.GroupName
                };
                g.Items.ToList().ForEach(item => info.Add(item));
                groups.Add(info);
            }


            //foreach (var g in query)
            //{
            //    GroupInfoCollection<DataGridDataItem> info = new GroupInfoCollection<DataGridDataItem>();
            //    info.Key = g.GroupName;
            //    foreach (var item in g.Items)
            //    {
            //        info.Add(item);
            //    }

            //    groups.Add(info);
            //}

            groupedItems = new CollectionViewSource();
            groupedItems.IsSourceGrouped = true;
            groupedItems.Source = groups;

            return groupedItems;
        }

        public class GroupInfoCollection<T> : ObservableCollection<T>
        {
            public object Key { get; set; }

            public new IEnumerator<T> GetEnumerator()
            {
                return (IEnumerator<T>)base.GetEnumerator();
            }
        }

        // Filtering implementation using LINQ
        public enum FilterOptions
        {
            All = -1,
            Rank_Low = 0,
            Rank_High = 1,
            Height_Low = 2,
            Height_High = 3,
            Zon = 4
        }


        public ObservableCollection<DataGridDataItem> ZondataSorting(string name)
        {
            if(_cachedSortedColumn == "FIO")
              if(name != "")
                if(_cachedSortedColumnstatus == "True")
                    return new ObservableCollection<DataGridDataItem>(from item in _items
                                                                      where item.Zon_name == name 
                                                                      orderby item.FIO ascending
                                                                      select item);
                else
                    return new ObservableCollection<DataGridDataItem>(from item in _items
                                                                      where item.Zon_name == name
                                                                      orderby item.FIO descending
                                                                      select item);
             else
                {
                    if (_cachedSortedColumnstatus == "True")
                        return new ObservableCollection<DataGridDataItem>(from item in _items
                                                                          orderby item.FIO ascending
                                                                          select item);
                    else
                        return new ObservableCollection<DataGridDataItem>(from item in _items
                                                                          orderby item.FIO descending
                                                                          select item);
                }
            else if(_cachedSortedColumn == "Id_Employer")
                if (name != "")
                    if (_cachedSortedColumnstatus == "True")
                    return new ObservableCollection<DataGridDataItem>(from item in _items
                                                                      where item.Zon_name == name
                                                                      orderby item.Id_Employer ascending
                                                                      select item);
                    else
                    return new ObservableCollection<DataGridDataItem>(from item in _items
                                                                      where item.Zon_name == name
                                                                      orderby item.Id_Employer descending
                                                                      select item);
                else
                {
                    if (_cachedSortedColumnstatus == "True")
                        return new ObservableCollection<DataGridDataItem>(from item in _items
                                                                          orderby item.Id_Employer ascending
                                                                          select item);
                    else
                        return new ObservableCollection<DataGridDataItem>(from item in _items
                                                                          orderby item.Id_Employer descending
                                                                          select item);
                }
            else if (_cachedSortedColumn == "Tagg")
                if (name != "")
                    if (_cachedSortedColumnstatus == "True")
                    return new ObservableCollection<DataGridDataItem>(from item in _items
                                                                      where item.Zon_name == name
                                                                      orderby item.Tagg ascending
                                                                      select item);
                else
                    return new ObservableCollection<DataGridDataItem>(from item in _items
                                                                      where item.Zon_name == name
                                                                      orderby item.Tagg descending
                                                                      select item);
                else
                {
                    if (_cachedSortedColumnstatus == "True")
                        return new ObservableCollection<DataGridDataItem>(from item in _items
                                                                          orderby item.Tagg ascending
                                                                          select item);
                    else
                        return new ObservableCollection<DataGridDataItem>(from item in _items
                                                                          orderby item.Tagg descending
                                                                          select item);
                }
            else if (_cachedSortedColumn == "Position")
                if (name != "")
                    if (_cachedSortedColumnstatus == "True")
                    return new ObservableCollection<DataGridDataItem>(from item in _items
                                                                      where item.Zon_name == name
                                                                      orderby item.Position ascending
                                                                      select item);
                else
                    return new ObservableCollection<DataGridDataItem>(from item in _items
                                                                      where item.Zon_name == name
                                                                      orderby item.Position descending
                                                                      select item);
                else
                {
                    if (_cachedSortedColumnstatus == "True")
                        return new ObservableCollection<DataGridDataItem>(from item in _items
                                                                          orderby item.Position ascending
                                                                          select item);
                    else
                        return new ObservableCollection<DataGridDataItem>(from item in _items
                                                                          orderby item.Position descending
                                                                          select item);
                }
            else if (_cachedSortedColumn == "Unit")
                if (name != "")
                    if (_cachedSortedColumnstatus == "True")
                    return new ObservableCollection<DataGridDataItem>(from item in _items
                                                                      where item.Zon_name == name
                                                                      orderby item.Unit ascending
                                                                      select item);
                else
                    return new ObservableCollection<DataGridDataItem>(from item in _items
                                                                      where item.Zon_name == name
                                                                      orderby item.Unit descending
                                                                      select item);
                else
                {
                    if (_cachedSortedColumnstatus == "True")
                        return new ObservableCollection<DataGridDataItem>(from item in _items
                                                                          orderby item.Unit ascending
                                                                          select item);
                    else
                        return new ObservableCollection<DataGridDataItem>(from item in _items
                                                                          orderby item.Unit descending
                                                                          select item);
                }
            else if (_cachedSortedColumn == "Team")
                if (name != "")
                    if (_cachedSortedColumnstatus == "True")
                    return new ObservableCollection<DataGridDataItem>(from item in _items
                                                                      where item.Zon_name == name
                                                                      orderby item.Team ascending
                                                                      select item);
                else
                    return new ObservableCollection<DataGridDataItem>(from item in _items
                                                                      where item.Zon_name == name
                                                                      orderby item.Team descending
                                                                      select item);
                else
                {
                    if (_cachedSortedColumnstatus == "True")
                        return new ObservableCollection<DataGridDataItem>(from item in _items
                                                                          orderby item.Team ascending
                                                                          select item);
                    else
                        return new ObservableCollection<DataGridDataItem>(from item in _items
                                                                          orderby item.Team descending
                                                                          select item);
                }
            else if (_cachedSortedColumn == "Zon_name")
                    if (name != "")
                        if (_cachedSortedColumnstatus == "True")
                    return new ObservableCollection<DataGridDataItem>(from item in _items
                                                                      where item.Zon_name == name
                                                                      orderby item.Zon_name ascending
                                                                      select item);
                else
                    return new ObservableCollection<DataGridDataItem>(from item in _items
                                                                      where item.Zon_name == name
                                                                      orderby item.Zon_name descending
                                                                      select item);
                else
                {
                    if (_cachedSortedColumnstatus == "True")
                        return new ObservableCollection<DataGridDataItem>(from item in _items
                                                                          orderby item.Zon_name ascending
                                                                          select item);
                    else
                        return new ObservableCollection<DataGridDataItem>(from item in _items
                                                                          orderby item.Zon_name descending
                                                                          select item);
                }
            return _items;

        }

        public ObservableCollection<DataGridDataItem> FilterData(FilterOptions filterBy, string name)
        {
            if(_cachedSortedColumn != "")
            {
                switch (filterBy)
                {
                    case FilterOptions.All:
                        return ZondataSorting(name);

                    case FilterOptions.Zon:
                        return ZondataSorting(name);
                }
            }
            else
            {
                switch (filterBy)
                {
                    case FilterOptions.All:
                        return new ObservableCollection<DataGridDataItem>(_items);

                    case FilterOptions.Zon:
                        return new ObservableCollection<DataGridDataItem>(from item in _items
                                                                          where item.Zon_name == name
                                                                          select item);
                }
            }
            return _items;
        }

    }
}
