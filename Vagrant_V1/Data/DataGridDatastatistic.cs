﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Vagrant_V1.Server;
using Windows.System.Threading;
using Windows.UI.Core;
using Windows.UI.Xaml.Data;

namespace Vagrant_V1.Data
{
    [Bindable]
    class DataGridDatastatistic
    {
        private static ObservableCollection<DataGridDataItem> _items;
        private string _cachedSortedColumn = string.Empty;
        private static CollectionViewSource groupedItems;
        public static List<string> filterUnit = new List<string>();
        public static List<string> filterPosition = new List<string>();
        public static List<string> filterteam = new List<string>();
        public static string _cachedSortedColumnstatus = string.Empty;

        public async Task<IEnumerable<DataGridDataItem>> GetDataAsync()
        {
            _items = new ObservableCollection<DataGridDataItem>(DataGridDataSource._itemsALL);
            filterUnit.Clear();
            filterPosition.Clear();
            filterteam.Clear();
            foreach (DataGridDataItem v in _items)
            {
                bool flagUnit = false;
                bool flagteam = false;
                bool flagposition = false;
                foreach (string h in filterUnit)
                {
                    if(v.Unit == h)
                    {
                        flagUnit = true;
                    }
                }
                foreach (string h in filterPosition)
                {
                    if (v.Position == h)
                    {
                        flagposition = true;
                    }
                }
                foreach (string h in filterteam)
                {
                    if (v.Team.ToString() == h)
                    {
                        flagteam = true;
                    }
                }
                if (flagUnit == false)
                {
                    filterUnit.Add(v.Unit.ToString());
                }
                if(flagteam == false)
                {
                    filterteam.Add(v.Team.ToString());
                }
                if (flagposition == false)
                {
                    filterPosition.Add(v.Position.ToString());
                }
            }
            return _items;
        }

        public string CachedSortedColumn
        {
            get
            {
                return _cachedSortedColumn;
            }

            set
            {
                _cachedSortedColumn = value;
                _cachedSortedColumnstatus = value;
            }
        }

        public ObservableCollection<DataGridDataItem> SortData(string sortBy, bool ascending, string name)
        {
            _cachedSortedColumn = sortBy;
            _cachedSortedColumnstatus = ascending.ToString();
            if (name == "")
                switch (sortBy)
                {
                    case "FIO":
                        return ascending
                            ? new ObservableCollection<DataGridDataItem>(_items.OrderBy(t => t.FIO))
                            : new ObservableCollection<DataGridDataItem>(_items.OrderByDescending(t => t.FIO));
                    case "Id_Employer":
                        return ascending
                            ? new ObservableCollection<DataGridDataItem>(_items.OrderBy(t => t.Id_Employer))
                            : new ObservableCollection<DataGridDataItem>(_items.OrderByDescending(t => t.Id_Employer));

                    case "Position":
                        return ascending
                        ? new ObservableCollection<DataGridDataItem>(_items.OrderBy(t => t.Position))
                        : new ObservableCollection<DataGridDataItem>(_items.OrderByDescending(t => t.Position));

                    case "Unit":
                        return ascending
                        ? new ObservableCollection<DataGridDataItem>(_items.OrderBy(t => t.Unit))
                        : new ObservableCollection<DataGridDataItem>(_items.OrderByDescending(t => t.Unit));

                    case "Team":
                        return ascending
                        ? new ObservableCollection<DataGridDataItem>(_items.OrderBy(t => t.Team))
                        : new ObservableCollection<DataGridDataItem>(_items.OrderByDescending(t => t.Team));
                    case "Tagg":
                        return ascending
                        ? new ObservableCollection<DataGridDataItem>(_items.OrderBy(t => t.Tagg))
                        : new ObservableCollection<DataGridDataItem>(_items.OrderByDescending(t => t.Tagg));
                    case "Zon_name":
                        return ascending
                        ? new ObservableCollection<DataGridDataItem>(_items.OrderBy(t => t.Zon_name))
                        : new ObservableCollection<DataGridDataItem>(_items.OrderByDescending(t => t.Zon_name));
                }
            else if (name != "")
            {
                switch (sortBy)
                {
                    case "FIO":
                        return ascending
                            ? new ObservableCollection<DataGridDataItem>(_items.Where(t => t.Zon_name == name).OrderBy(t => t.FIO))
                            : new ObservableCollection<DataGridDataItem>(_items.Where(t => t.Zon_name == name).OrderByDescending(t => t.FIO));
                    case "Id_Employer":
                        return ascending
                            ? new ObservableCollection<DataGridDataItem>(_items.Where(t => t.Zon_name == name).OrderBy(t => t.Id_Employer))
                            : new ObservableCollection<DataGridDataItem>(_items.Where(t => t.Zon_name == name).OrderByDescending(t => t.Id_Employer));

                    case "Position":
                        return ascending
                        ? new ObservableCollection<DataGridDataItem>(_items.Where(t => t.Zon_name == name).OrderBy(t => t.Position))
                        : new ObservableCollection<DataGridDataItem>(_items.Where(t => t.Zon_name == name).OrderByDescending(t => t.Position));

                    case "Unit":
                        return ascending
                        ? new ObservableCollection<DataGridDataItem>(_items.Where(t => t.Zon_name == name).OrderBy(t => t.Unit))
                        : new ObservableCollection<DataGridDataItem>(_items.Where(t => t.Zon_name == name).OrderByDescending(t => t.Unit));

                    case "Team":
                        return ascending
                        ? new ObservableCollection<DataGridDataItem>(_items.Where(t => t.Zon_name == name).OrderBy(t => t.Team))
                        : new ObservableCollection<DataGridDataItem>(_items.Where(t => t.Zon_name == name).OrderByDescending(t => t.Team));
                    case "Tagg":
                        return ascending
                        ? new ObservableCollection<DataGridDataItem>(_items.Where(t => t.Zon_name == name).OrderBy(t => t.Tagg))
                        : new ObservableCollection<DataGridDataItem>(_items.Where(t => t.Zon_name == name).OrderByDescending(t => t.Tagg));
                    case "Zon_name":
                        return ascending
                        ? new ObservableCollection<DataGridDataItem>(_items.Where(t => t.Zon_name == name).OrderBy(t => t.Zon_name))
                        : new ObservableCollection<DataGridDataItem>(_items.Where(t => t.Zon_name == name).OrderByDescending(t => t.Zon_name));
                }
            }


            return _items;
        }

        // Grouping implementation using LINQ
        public CollectionViewSource GroupData()
        {
            ObservableCollection<GroupInfoCollection<DataGridDataItem>> groups = new ObservableCollection<GroupInfoCollection<DataGridDataItem>>();

            //_items.


            var query = from item in _items
                        orderby item
                        group item by item.Team into g
                        select new { GroupName = g.Key, Items = g };

            foreach (var g in query)
            {
                GroupInfoCollection<DataGridDataItem> info = new GroupInfoCollection<DataGridDataItem>
                {
                    Key = g.GroupName
                };
                g.Items.ToList().ForEach(item => info.Add(item));
                groups.Add(info);
            }
            //foreach (var g in query)
            //{
            //    GroupInfoCollection<DataGridDataItem> info = new GroupInfoCollection<DataGridDataItem>();
            //    info.Key = g.GroupName;
            //    foreach (var item in g.Items)
            //    {
            //        info.Add(item);
            //    }

            //    groups.Add(info);
            //}

            groupedItems = new CollectionViewSource();
            groupedItems.IsSourceGrouped = true;
            groupedItems.Source = groups;

            return groupedItems;
        }

        public class GroupInfoCollection<T> : ObservableCollection<T>
        {
            public object Key { get; set; }

            public new IEnumerator<T> GetEnumerator()
            {
                return (IEnumerator<T>)base.GetEnumerator();
            }
        }

        // Filtering implementation using LINQ
        public enum FilterOptions
        {
            All = -1,
            Rank_Low = 0,
            Rank_High = 1,
            Height_Low = 2,
            Height_High = 3,
            Zon = 4
        }

        public static ObservableCollection<DataGridDataItem> FilterData(string namecolumn, string nameparametr)
        {
            if(namecolumn == "Должность")
            {
                return new ObservableCollection<DataGridDataItem>(from item in _items
                                                                  where item.Position == (nameparametr)
                                                                  select item);
            }
            else if(namecolumn == "Отдел")
            {
                return new ObservableCollection<DataGridDataItem>(from item in _items
                                                                  where item.Unit == (nameparametr)
                                                                  select item);
            } else if(namecolumn == "Бригада")
            {
                return new ObservableCollection<DataGridDataItem>(from item in _items
                                                                  where item.Team == int.Parse(nameparametr)
                                                                  select item);
            }
            else if(namecolumn =="Очистить")
            {
                return new ObservableCollection<DataGridDataItem>(from item in _items
                                                                  select item);
            }

            return _items;
        }
    }
}