﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Handlers;
using System.Text;
using System.Threading.Tasks;
using Vagrant_V1.ModelData;
using Vagrant_V1.Pageees;
using Windows.Storage;
using Windows.UI.Xaml.Controls;

namespace Vagrant_V1.Server
{
    public class Employer_View
    {
        public static List<EmployerDataBase> EmployerDataBase = new List<EmployerDataBase>();
        public static List<PositionDataBase> Position = new List<PositionDataBase>();
        public static List<ZonDataBase> Zon = new List<ZonDataBase>();
        public static List<TagData> Tag = new List<TagData>();
        public static List<UnityObject> unityObjects = new List<UnityObject>();
        public static string URl_start = "";
        public static string Simulate = "";

        //public Employer_View()
        //{
        //    var service = new ApiCrudService();
        //    tasks.Add(service.LoadAsync<ZonDataBase>("Zone"));

        //    var tasks = new List<Task>
        //    {
        //        new Task(_ => {Zon = service.LoadAsync<ZonDataBase>("Zone"); }),
        //        GenerateEmployersAsync(),
        //        GeneratePositionAsync(),
        //        GenerateZonAsync(),
        //        GenerateTagAsync()
        //    };
        //    Task.WaitAll(tasks.ToArray());
        //}

        public Employer_View()
        {
        }

        public async Task Employer_Viewr()
        {
            await GenerateEmployersAsync();
            await GenerateZonAsync();
            await GenerateUnityObjectAsync();
            await GenerateTagAsync();
        }


        // Получения сотрудников Get запрос
        public static async Task GenerateEmployersAsync()
        {
            Windows.Web.Http.HttpClient httpClient = new Windows.Web.Http.HttpClient();
            Uri requestUri = new Uri(URl_start + "/api/project/000T/Employee");
            Windows.Web.Http.HttpResponseMessage httpResponse = new Windows.Web.Http.HttpResponseMessage();
            string httpResponseBody = @"";
            try
            {
                httpResponse = await httpClient.GetAsync(requestUri);
                httpResponse.EnsureSuccessStatusCode();
                httpResponseBody = await httpResponse.Content.ReadAsStringAsync();
                EmployerDataBase = JsonConvert.DeserializeObject<List<EmployerDataBase>>(httpResponseBody);
            }
            catch (Exception ex)
            {
                httpResponseBody = "Error: " + ex.HResult.ToString("X") + " Message: " + ex.Message;
            }
        }

        // Получения позицию сотрудника Get запрос
        public static async Task GeneratePositionAsync()
        {
            Windows.Web.Http.HttpClient httpClient = new Windows.Web.Http.HttpClient();
            Uri requestUri = new Uri(URl_start+ "/api/project/000T/EmployeeZonePosition?simulate=" + Simulate);
            Windows.Web.Http.HttpResponseMessage httpResponse = new Windows.Web.Http.HttpResponseMessage();
            string httpResponseBody = @"";
            try
            {
                httpResponse = await httpClient.GetAsync(requestUri);
                httpResponse.EnsureSuccessStatusCode();
                httpResponseBody = await httpResponse.Content.ReadAsStringAsync();
                Position = JsonConvert.DeserializeObject<List<PositionDataBase>>(httpResponseBody);
            }
            catch (Exception ex)
            {
                httpResponseBody = "Error: " + ex.HResult.ToString("X") + " Message: " + ex.Message;
            }
        }
        // Получения позицию сотрудника Get запрос статистика
        public static async Task<List<PositionDataBase>> LoadPositionAsync(DateTime TIme)
        {
            Windows.Web.Http.HttpClient httpClient = new Windows.Web.Http.HttpClient();
            Uri requestUri = new Uri(URl_start + "/api/project/000T/EmployeeZonePosition?time=" + TIme.Year + "-" + TIme.Month + "-" + TIme.Day +"T" + (TIme.TimeOfDay - TimeZoneInfo.Local.GetUtcOffset(DateTime.UtcNow))  + "&simulate=" + Simulate);
            Windows.Web.Http.HttpResponseMessage httpResponse = new Windows.Web.Http.HttpResponseMessage();
            string httpResponseBody = @"";
            try
            {
                httpResponse = await httpClient.GetAsync(requestUri);
                httpResponse.EnsureSuccessStatusCode();
                httpResponseBody = await httpResponse.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<List<PositionDataBase>>(httpResponseBody);
            }
            catch (Exception ex)
            {
                httpResponseBody = "Error: " + ex.HResult.ToString("X") + " Message: " + ex.Message;
                return null;
            }
        }

        public static async Task<string> ModifyUnityObject(UnityObject unity, string siteZoneID)
        {
            HttpClient httpClient = new HttpClient();
            Uri requestUri = new Uri(URl_start + "/api/UnityObject/" + unity.id);
            HttpResponseMessage httpResponse = new HttpResponseMessage();
            UnityObjectupdate unityre = new UnityObjectupdate()
            {
                project = unity.project,
                unityObjectId = unity.unityObjectId,
                objectCoordinates = unity.objectCoordinates,
                siteZone = siteZoneID,
                id = unity.id
            };
            HttpContent content = new StringContent(JsonConvert.SerializeObject(unityre), Encoding.UTF8, "application/json-patch+json");
            try
            {
                httpResponse = await httpClient.PutAsync(requestUri, content);
                if (httpResponse.IsSuccessStatusCode)
                {
                    await httpResponse.Content.ReadAsStringAsync();
                    return httpResponse.StatusCode.ToString();
                }
                else
                {
                    await httpResponse.Content.ReadAsStringAsync();
                    return httpResponse.StatusCode.ToString();
                }
            }
            catch (Exception ex)
            {
                return "Error: " + ex.HResult.ToString("X") + " Message: " + ex.Message;
            }
        }


        public static async Task<string> ClearTag(TagData ClearTag, string name)
        {
            HttpClient httpClient = new HttpClient();
            Uri requestUri = new Uri(URl_start + "/api/Tag/" + ClearTag.id);
            HttpResponseMessage httpResponse = new HttpResponseMessage();
            TagData unityre = new TagData()
            {
                title = ClearTag.title,
                project = ClearTag.project,
                employee = name,
                id = ClearTag.id
            };
            HttpContent content = new StringContent(JsonConvert.SerializeObject(unityre), Encoding.UTF8, "application/json-patch+json");
            try
            {
                httpResponse = await httpClient.PutAsync(requestUri, content);
                if (httpResponse.IsSuccessStatusCode)
                {
                    await httpResponse.Content.ReadAsStringAsync();
                    return httpResponse.StatusCode.ToString();
                }
                else
                {
                    await httpResponse.Content.ReadAsStringAsync();
                    return httpResponse.StatusCode.ToString();
                }
            }
            catch (Exception ex)
            {
                return "Error: " + ex.HResult.ToString("X") + " Message: " + ex.Message;
            }
        }

        public static async Task<string> DeleateZon(string id)
        {
            HttpClient httpClient = new HttpClient();
            Uri requestUri = new Uri(URl_start + "/api/project/000T/Zone/" + id);
            HttpResponseMessage httpResponse = new HttpResponseMessage();
            try
            {
                httpResponse = await httpClient.DeleteAsync(requestUri);
                if (httpResponse.IsSuccessStatusCode)
                {
                    await httpResponse.Content.ReadAsStringAsync();
                    return httpResponse.StatusCode.ToString();
                }
                else
                {
                    await httpResponse.Content.ReadAsStringAsync();
                    return httpResponse.StatusCode.ToString();
                }
            }
            catch (Exception ex)
            {
                return "Error: " + ex.HResult.ToString("X") + " Message: " + ex.Message;
            }
        }

        public static async Task<string> CreateZon(string color, string title, string description, string id, string[] unityobject)
        {
            HttpClient httpClient = new HttpClient();
            Uri requestUri = new Uri(URl_start + "/api/Zone");
            HttpResponseMessage httpResponse = new HttpResponseMessage();
            List<string>clearunity = new List<string>();
            ZonDataBaseCreate newzon = new ZonDataBaseCreate()
            {
                project = "000T",
                color = "#"+ color,
                title = title,
                description = description,
                unityObjects = clearunity.ToArray(),
                id = id
            };
            HttpContent content = new StringContent(JsonConvert.SerializeObject(newzon), Encoding.UTF8, "application/json-patch+json");
            try
            {
                httpResponse = await httpClient.PostAsync(requestUri, content);
                if(httpResponse.IsSuccessStatusCode)
                {
                    await httpResponse.Content.ReadAsStringAsync();
                    return httpResponse.StatusCode.ToString();
                }
                else
                {
                    await httpResponse.Content.ReadAsStringAsync();
                    return httpResponse.StatusCode.ToString();
                }
            }
            catch (Exception ex)
            {
                return "Error: " + ex.HResult.ToString("X") + " Message: " + ex.Message;
            }
        }

        public static async Task<string> PutZon(string color, string title, string description, string id, string[] unityobject)
        {
            HttpClient httpClient = new HttpClient();
            Uri requestUri = new Uri(URl_start + "/api/Zone/" + id);
            HttpResponseMessage httpResponse = new HttpResponseMessage();
            ZonDataBaseCreate newzon = new ZonDataBaseCreate()
            {
                project = "000T",
                color = color,
                title = title,
                description = description,
                unityObjects = unityobject,
                id = id
            };
            HttpContent content = new StringContent(JsonConvert.SerializeObject(newzon), Encoding.UTF8, "application/json-patch+json");
            try
            {
                httpResponse = await httpClient.PutAsync(requestUri, content);
                if (httpResponse.IsSuccessStatusCode)
                {
                    await httpResponse.Content.ReadAsStringAsync();
                    return httpResponse.StatusCode.ToString();
                }
                else
                {
                    await httpResponse.Content.ReadAsStringAsync();
                    return httpResponse.StatusCode.ToString();
                }
            }
            catch (Exception ex)
            {
                return "Error: " + ex.HResult.ToString("X") + " Message: " + ex.Message;
            }
        }

        public static async Task<string> PostReport(DateTime start, DateTime end, string idemployer, int timeStep, string name, StorageFolder folder)
        {
            HttpClient httpClient = new HttpClient();
            Uri requestUri = new Uri(URl_start + "/api/project/000T/EmployeeReport");
            HttpResponseMessage httpResponse = new HttpResponseMessage();
            Report newzon = new Report()
            {
                start = start - TimeZoneInfo.Local.GetUtcOffset(DateTime.UtcNow),
                finish = end -TimeZoneInfo.Local.GetUtcOffset(DateTime.UtcNow),
                employeeId = idemployer,
                timeStep = timeStep,
            };
            HttpContent content = new StringContent(JsonConvert.SerializeObject(newzon), Encoding.UTF8, "application/json-patch+json");
            try
            {
                httpResponse = await httpClient.PostAsync(requestUri, content);
                if (httpResponse.IsSuccessStatusCode)
                {
                    byte [] result = await httpResponse.Content.ReadAsByteArrayAsync();
                    string name1 = name + "_" + start.Date.Year.ToString() + " " + start.Date.Month.ToString() + " " + start.Date.Day.ToString() + "_"
                       + start.TimeOfDay.Hours.ToString() + " " + start.Date.Minute.ToString() + "_" + end.Date.Year.ToString() + " " + end.Date.Month.ToString() + " " + end.Date.Day.ToString() + "_"
                       + end.TimeOfDay.Hours.ToString() + " " + end.Date.Minute.ToString();
                    StorageFile file = await folder.CreateFileAsync(name1 + ".xlsx", CreationCollisionOption.ReplaceExisting);
                    await FileIO.WriteBytesAsync(file, result);
                    return httpResponse.StatusCode.ToString();
                }
                else
                {
                    await httpResponse.Content.ReadAsStringAsync();
                    return httpResponse.StatusCode.ToString();
                }
            }
            catch (Exception ex)
            {
                return "Error: " + ex.HResult.ToString("X") + " Message: " + ex.Message;
            }
        }

        public static async Task<string> LoadZonNameAsync(string id, string[] unityobjext) // для загрузки новой зоны
        {
            Windows.Web.Http.HttpClient httpClient = new Windows.Web.Http.HttpClient();
            Uri requestUri = new Uri(URl_start + "/api/project/000T/Zone/" + id);
            //Uri requestUri = new Uri(URl_start + "/api/project/"+ id + "/Zone/000T");
            Windows.Web.Http.HttpResponseMessage httpResponse = new Windows.Web.Http.HttpResponseMessage();
            string httpResponseBody = @"";
            try
            {
                httpResponse = await httpClient.GetAsync(requestUri);
                httpResponse.EnsureSuccessStatusCode();
                if (httpResponse.IsSuccessStatusCode)
                {
                    httpResponseBody = await httpResponse.Content.ReadAsStringAsync();
                    ZonDataBase newZon = JsonConvert.DeserializeObject<ZonDataBase>(httpResponseBody);
                    newZon.unityObjects = unityobjext;
                    Zon.Add(newZon);
                    return httpResponse.StatusCode.ToString();
                }
                else
                {
                    await httpResponse.Content.ReadAsStringAsync();
                    return httpResponse.StatusCode.ToString();
                }
            }
            catch (Exception ex)
            {
                return "Error: " + ex.HResult.ToString("X") + " Message: " + ex.Message;
            }
        }

        public static async Task<string> LoadZonNameUpdateAsync(string id)// Для загрузки изменений зоны
        {
            Windows.Web.Http.HttpClient httpClient = new Windows.Web.Http.HttpClient();
            Uri requestUri = new Uri(URl_start + "/api/project/000T/Zone/" + id);
            //Uri requestUri = new Uri(URl_start + "/api/project/" + id + "/Zone/000T");
            Windows.Web.Http.HttpResponseMessage httpResponse = new Windows.Web.Http.HttpResponseMessage();
            string httpResponseBody = @"";
            try
            {
                httpResponse = await httpClient.GetAsync(requestUri);
                httpResponse.EnsureSuccessStatusCode();
                if (httpResponse.IsSuccessStatusCode)
                {
                    httpResponseBody = await httpResponse.Content.ReadAsStringAsync();
                    ZonDataBase newZon = JsonConvert.DeserializeObject<ZonDataBase>(httpResponseBody);
                    Zon[Zon.FindIndex(u => u.id == newZon.id)] = newZon;
                    return httpResponse.StatusCode.ToString();
                }
                else
                {
                    await httpResponse.Content.ReadAsStringAsync();
                    return httpResponse.StatusCode.ToString();
                }
            }
            catch (Exception ex)
            {
                return "Error: " + ex.HResult.ToString("X") + " Message: " + ex.Message;
            }
        }

        //// Получения зоны нахождения сотрудника Get запрос
        //public async Task<T> LoadAsync<T>(string method)
        //{
        //    Windows.Web.Http.HttpClient httpClient = new Windows.Web.Http.HttpClient();
        //    Uri requestUri = new Uri($"https://vagrantapi.azurewebsites.net/api/project/000T/{method}");
        //    Windows.Web.Http.HttpResponseMessage httpResponse = new Windows.Web.Http.HttpResponseMessage();
        //    string httpResponseBody = @"";
        //    try
        //    {
        //        httpResponse = await httpClient.GetAsync(requestUri);
        //        httpResponse.EnsureSuccessStatusCode();
        //        httpResponseBody = await httpResponse.Content.ReadAsStringAsync();
        //        return JsonConvert.DeserializeObject<List<T>>(httpResponseBody);
        //    }
        //    catch (Exception ex)
        //    {
        //        httpResponseBody = "Error: " + ex.HResult.ToString("X") + " Message: " + ex.Message;
        //        throw;
        //    }
        //}

        // Получения зоны нахождения сотрудника Get запрос
        public static async Task GenerateZonAsync()
        {
            Windows.Web.Http.HttpClient httpClient = new Windows.Web.Http.HttpClient();
            Uri requestUri = new Uri(URl_start + "/api/project/000T/Zone");
            Windows.Web.Http.HttpResponseMessage httpResponse = new Windows.Web.Http.HttpResponseMessage();
            string httpResponseBody = @"";
            try
            {
                httpResponse = await httpClient.GetAsync(requestUri);
                httpResponse.EnsureSuccessStatusCode();
                httpResponseBody = await httpResponse.Content.ReadAsStringAsync();
                Zon = JsonConvert.DeserializeObject<List<ZonDataBase>>(httpResponseBody);
            }
            catch (Exception ex)
            {
                httpResponseBody = "Error: " + ex.HResult.ToString("X") + " Message: " + ex.Message;
            }
        }



        public static async Task GenerateUnityObjectAsync()
        {
            Windows.Web.Http.HttpClient httpClient = new Windows.Web.Http.HttpClient();
            Uri requestUri = new Uri(URl_start + "/api/project/000T/UnityObject?freeOnly=false");
            Windows.Web.Http.HttpResponseMessage httpResponse = new Windows.Web.Http.HttpResponseMessage();
            string httpResponseBody = @"";
            try
            {
                httpResponse = await httpClient.GetAsync(requestUri);
                httpResponse.EnsureSuccessStatusCode();
                httpResponseBody = await httpResponse.Content.ReadAsStringAsync();
                unityObjects = JsonConvert.DeserializeObject<List<UnityObject>>(httpResponseBody);
            }
            catch (Exception ex)
            {
                httpResponseBody = "Error: " + ex.HResult.ToString("X") + " Message: " + ex.Message;
            }
        }

        // Получения Тэга  сотрудника Get запрос
        public static async Task GenerateTagAsync()
        {
            Windows.Web.Http.HttpClient httpClient = new Windows.Web.Http.HttpClient();
            Uri requestUri = new Uri(URl_start + "/api/project/000T/Tag?freeOnly=false");
            Windows.Web.Http.HttpResponseMessage httpResponse = new Windows.Web.Http.HttpResponseMessage();
            string httpResponseBody = @"";
            try
            {
                httpResponse = await httpClient.GetAsync(requestUri);
                httpResponse.EnsureSuccessStatusCode();
                httpResponseBody = await httpResponse.Content.ReadAsStringAsync();
                Tag =  JsonConvert.DeserializeObject<List<TagData>>(httpResponseBody);
            }
            catch (Exception ex)
            {
                httpResponseBody = "Error: " + ex.HResult.ToString("X") + " Message: " + ex.Message;
            }
        }

        public static async Task<List<string>> IninClearTagAsync()
        {
            Windows.Web.Http.HttpClient httpClient = new Windows.Web.Http.HttpClient();
            Uri requestUri = new Uri(URl_start + "/api/project/000T/Tag?freeOnly=true");
            Windows.Web.Http.HttpResponseMessage httpResponse = new Windows.Web.Http.HttpResponseMessage();
            string httpResponseBody = @"";
            try
            {
                httpResponse = await httpClient.GetAsync(requestUri);
                httpResponse.EnsureSuccessStatusCode();
                httpResponseBody = await httpResponse.Content.ReadAsStringAsync();
                List<TagData> clear = JsonConvert.DeserializeObject<List<TagData>>(httpResponseBody);
                List<string> clearList = new List<string>();
                foreach(TagData v in clear)
                {
                    clearList.Add(v.title);
                }
                clearList.Add("");
                return clearList;
            }
            catch (Exception ex)
            {
                httpResponseBody = "Error: " + ex.HResult.ToString("X") + " Message: " + ex.Message;
                return null;
            }
        }

    }
}
