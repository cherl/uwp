﻿using HelixToolkit.UWP;
using Microsoft.Toolkit.Uwp.UI.Controls;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading;
using System.Threading.Tasks;
using Vagrant_V1.Data;
using Vagrant_V1.Modeling3d._2D;
using Vagrant_V1.Pageees;
using Vagrant_V1.Server;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.System.Threading;
using Windows.UI.Core;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using SharpDX;
using Windows.Devices.Input;
using System.Collections.ObjectModel;

// Документацию по шаблону элемента "Пустая страница" см. по адресу https://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x419

namespace Vagrant_V1
{
    /// <summary>
    /// Пустая страница, которую можно использовать саму по себе или для перехода внутри фрейма.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        private GeometryModel3D selectedElement = null;
        private DataGridDataSource viewModel1 = new DataGridDataSource();
        public string name = "";
        public float timer = 0;
        private static float timepreset = 1;
        public int level = 1;
        private Button Statelast = null;
        public static string name_zon = "";
        public static string name_people ="";
        private bool flag_click_zon = false;
        private static bool status_mouse_zon = false;
        public static bool modifytag = false;
        public static DataGrid grid;
        private float correct = 1.94f;
        private static Viewport3DX view;
        private static bool flagAnswer = false;
        private static DataGridCellEditEndedEventArgs lastdata;
        public static bool flag3D = false; 
        public static ContentDialog AnswerUpdate = new ContentDialog()
        {
            Title = "Принять изменения?",
            PrimaryButtonText = "Да",
            CloseButtonText = "Нет"

        };

        public MainPage()
        {
            this.InitializeComponent();
            NavigationCacheMode = NavigationCacheMode.Required;
            grid = dataGrid;
            view = viewport;
            OnXamlRendered();
        }




        private void AutoSuggestBox_TextChanged(AutoSuggestBox sender, AutoSuggestBoxTextChangedEventArgs args)
        {
            if (args.Reason == AutoSuggestionBoxTextChangeReason.UserInput)
            {
                List<string> name = DataGridDataSource.name1.FindAll(u => u.StartsWith(Find.Text));
                sender.ItemsSource = name;
            }
        }


        private void AutoSuggestBox_SuggestionChosen(AutoSuggestBox sender, AutoSuggestBoxSuggestionChosenEventArgs args)
        {
            //Set sender.Text. You can use args.SelectedItem to build your text string.
        }


        private void AutoSuggestBox_QuerySubmitted(AutoSuggestBox sender, AutoSuggestBoxQuerySubmittedEventArgs args)
        {
            foreach(DataGridDataItem v in ((ObservableCollection<DataGridDataItem>)dataGrid.ItemsSource))
            {
                if (v.FIO == Find.Text || v.Id_Employer == Find.Text || v.Tagg == Find.Text)
                {
                    List<object> added = new List<object>();
                    List<object> remove = new List<object>();
                    added.Add(v);
                    if(dataGrid.SelectedItem != null)
                    {
                        remove.Add(dataGrid.SelectedItem);
                    }
                    SelectionChangedEventArgs Event = new SelectionChangedEventArgs(remove, added);
                    dataGrid.SelectedItem = v;
                    dataGrid_SelectionChanged(dataGrid, Event);
                    dataGrid.ScrollIntoView(dataGrid.SelectedItem, dataGrid.Columns[0]);
                }
            }
        }


        // Работа с базой данных!!!!!!!!!!!!!!!!!!!!!!!!
        public async void OnXamlRendered()
        {
            if (dataGrid != null)
            {
                dataGrid.Sorting -= DataGrid_Sorting;
                dataGrid.LoadingRowGroup -= DataGrid_LoadingRowGroup;
            }

            if (dataGrid != null)
            {
                dataGrid.Sorting += DataGrid_Sorting;
                dataGrid.LoadingRowGroup += DataGrid_LoadingRowGroup;
                if(dataGrid.ItemsSource == null)
                {
                    dataGrid.ItemsSource = await viewModel1.GetDataAsync();
                    Kolpeople.Text = ((ObservableCollection<DataGridDataItem>)dataGrid.ItemsSource).Count.ToString();
                    Timer();
                }
                else
                {
                    dataGrid.ItemsSource = await viewModel1.GetDataAsync();
                }
                var comboBoxColumn = dataGrid.Columns.FirstOrDefault(x => x.Tag.Equals("Id_Employer")) as DataGridComboBoxColumn;
                if (comboBoxColumn != null)
                {
                    comboBoxColumn.ItemsSource = await viewModel1.GetFIO();
                }

                var comboBoxColumnTag = dataGrid.Columns.FirstOrDefault(x => x.Tag.Equals("Tagg")) as DataGridComboBoxColumn;
                if (comboBoxColumnTag != null)
                {
                    comboBoxColumnTag.ItemsSource = await viewModel1.GetTag();
                }
            }

            //if (groupButton != null)
            //{
            //    groupButton.Click -= GroupButton_Click;
            //}

            //if (groupButton != null)
            //{
            //    groupButton.Click += GroupButton_Click;
            //}

            //if (rankLowItem != null)
            //{
            //    rankLowItem.Click -= RankLowItem_Click;
            //}

            ////rankLowItem = control.FindName("rankLow") as MenuFlyoutItem;
            //if (rankLowItem != null)
            //{
            //    rankLowItem.Click += RankLowItem_Click;
            //}

            //if (rankHighItem != null)
            //{
            //    rankHighItem.Click -= RankHigh_Click;
            //}

            ////rankHighItem = control.FindName("rankHigh") as MenuFlyoutItem;
            //if (rankHighItem != null)
            //{
            //    rankHighItem.Click += RankHigh_Click;
            //}

            //if (heightLowItem != null)
            //{
            //    heightLowItem.Click -= HeightLow_Click;
            //}

            ////heightLowItem = control.FindName("heightLow") as MenuFlyoutItem;
            //if (heightLowItem != null)
            //{
            //    heightLowItem.Click += HeightLow_Click;
            //}

            //if (heightHighItem != null)
            //{
            //    heightHighItem.Click -= HeightHigh_Click;
            //}

            ////heightHighItem = control.FindName("heightHigh") as MenuFlyoutItem;
            //if (heightHighItem != null)
            //{
            //    heightHighItem.Click += HeightHigh_Click;
            //}

            ////var clearFilter = control.FindName("clearFilter") as MenuFlyoutItem;
            //if (clearFilter != null)
            //{
            //    clearFilter.Click += this.ClearFilter_Click;
            //}
        }

        private void DataGrid_LoadingRowGroup(object sender, DataGridRowGroupHeaderEventArgs e)
        {
            ICollectionViewGroup group = e.RowGroupHeader.CollectionViewGroup;
            DataGridDataItem item = group.GroupItems[0] as DataGridDataItem;
            e.RowGroupHeader.PropertyValue = item.Team.ToString();
        }

        private void GroupButton_Click(object sender, RoutedEventArgs e)
        {
            if (dataGrid != null)
            {
                dataGrid.ItemsSource = viewModel1.GroupData().View;
            }
        }

        private void DataGrid_Sorting(object sender, DataGridColumnEventArgs e)
        {
            // Clear previous sorted column if we start sorting a different column
            string previousSortedColumn = viewModel1.CachedSortedColumn;
            if (previousSortedColumn != string.Empty)
            {
                foreach (DataGridColumn dataGridColumn in dataGrid.Columns)
                {
                    if (dataGridColumn.Tag != null && dataGridColumn.Tag.ToString() == previousSortedColumn &&
                        (e.Column.Tag == null || previousSortedColumn != e.Column.Tag.ToString()))
                    {
                        dataGridColumn.SortDirection = null;
                    }
                }
            }

            // Toggle clicked column's sorting method
            if (e.Column.Tag != null)
            {
                if (e.Column.SortDirection == null)
                {
                    dataGrid.ItemsSource = viewModel1.SortData(e.Column.Tag.ToString(), true, name);
                    e.Column.SortDirection = DataGridSortDirection.Ascending;
                }
                else if (e.Column.SortDirection == DataGridSortDirection.Ascending)
                {
                    dataGrid.ItemsSource = viewModel1.SortData(e.Column.Tag.ToString(), false, name);
                    e.Column.SortDirection = DataGridSortDirection.Descending;
                }
                else
                {
                    dataGrid.ItemsSource = viewModel1.FilterData(DataGridDataSource.FilterOptions.All, name);
                    e.Column.SortDirection = null;
                    viewModel1.CachedSortedColumn = "";
                    DataGridDataSource._cachedSortedColumnstatus = "";

                }
            }
        }

        private void RankLowItem_Click(object sender, RoutedEventArgs e)
        {
            //if (dataGrid != null)
            //{
            //    dataGrid.ItemsSource = viewModel1.FilterData(DataGridDataSource.FilterOptions.Rank_Low);
            //}
        }

        private void RankHigh_Click(object sender, RoutedEventArgs e)
        {
            //if (dataGrid != null)
            //{
            //    dataGrid.ItemsSource = viewModel1.FilterData(DataGridDataSource.FilterOptions.Rank_High);
            //}
        }

        private void HeightLow_Click(object sender, RoutedEventArgs e)
        {
            //if (dataGrid != null)
            //{
            //    dataGrid.ItemsSource = viewModel1.FilterData(DataGridDataSource.FilterOptions.Height_Low);
            //}
        }

        private void HeightHigh_Click(object sender, RoutedEventArgs e)
        {
            //if (dataGrid != null)
            //{
            //    dataGrid.ItemsSource = viewModel1.FilterData(DataGridDataSource.FilterOptions.Height_High);
            //}
        }

        private void ClearFilter_Click(object sender, RoutedEventArgs e)
        {
            //if (dataGrid != null)
            //{
            //    dataGrid.ItemsSource = viewModel1.FilterData(DataGridDataSource.FilterOptions.All);
            //}
        }

        private void Button_3D_Click(object sender, RoutedEventArgs e)
        {
            flag3D = true;
            framePage.numberpage = 5;
            Frame.Navigate(typeof(ThreeD));
        }


        // обработчики событий для работы с таблицей
        private void dataGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if(modifytag == false)
            {
                if (flag_click_zon == false)
                {
                    selectedElement = null;
                    if (e.AddedItems.Count != 0)
                    {
                        name_zon = ((DataGridDataItem)e.AddedItems[e.AddedItems.Count - 1]).Zon_name;
                        name_people = ((DataGridDataItem)e.AddedItems[e.AddedItems.Count - 1]).Id_Employer;
                        DataGridDataItem people = ((DataGridDataItem)e.AddedItems[e.AddedItems.Count - 1]);
                        if (name != "")
                        {
                            DataGrid y = new DataGrid();
                            y.SelectedItem = ((DataGrid)sender).SelectedItem;
                            name = "";
                            dataGrid.ItemsSource = viewModel1.FilterData(DataGridDataSource.FilterOptions.All, name);
                            ((DataGrid)sender).SelectedItem = y.SelectedItem;
                        }
                        if (e.RemovedItems.Count != 0)
                        {
                            string name_zon_last = ((DataGridDataItem)e.RemovedItems[e.RemovedItems.Count - 1]).Zon_name;
                            Remove_Selection(name_zon_last);
                        }
                        View_Zon(name_zon, true);
                    }
                    else
                    {
                        name_zon = ((DataGridDataItem)e.RemovedItems[e.RemovedItems.Count - 1]).Zon_name;
                        Remove_Selection(name_zon);
                        View_Zon(name_zon, false);
                        if (name != "")
                        {
                            name = "";
                            dataGrid.ItemsSource = viewModel1.FilterData(DataGridDataSource.FilterOptions.All, name);
                        }
                        name_zon = "";
                        name_people = "";
                    }
                }
                else
                {
                    flag_click_zon = false;
                }
            }
        }


        // Отображение зоны в которой находится человек
        private void View_Zon(string name_zone, bool flagstatus)
        {
            if(name_zone != "Обновляется")
            {
                switch (int.Parse(name_zone.Substring(2, 1)))
                {
                    case 1:
                        Clik_Zon_Level(Level1, null);
                        break;
                    case 2:
                        Clik_Zon_Level(Level2, null);
                        break;
                    case 3:
                        Clik_Zon_Level(Level3, null);
                        break;
                    case 4:
                        Clik_Zon_Level(Level4, null);
                        break;
                }
                if (flagstatus == true)
                {
                    Bloc_Button(true);
                    Hide_Zone(name_zone);
                }
                else
                {
                    Bloc_Button(false);
                }
            }
        }

        private void Remove_Selection(string name)
        {
            foreach (Element3D v in ItemsData.Children)
            {
                if (name == ((OITModel)((Element3D)v).DataContext).NameModel)
                {
                    ((MeshGeometryModel3D)v).PostEffects = null;
                    ((Element3D)v).Visibility = Visibility.Collapsed;
                }
            }
        }

        private void Hide_Zone(string name_zone)
        {
            foreach (Element3D v in ItemsData.Children)
            {
                string name1 = ((OITModel)((Element3D)v).DataContext).NameModel;
                if (((OITModel)((Element3D)v).DataContext).Mesh != null)
                {
                    if(int.Parse(name1.Substring(2,1)) == level)
                        if(name_zone != ((OITModel)((Element3D)v).DataContext).NameModel)
                        {
                            ((Element3D)v).Visibility = Visibility.Collapsed;
                        }
                        else
                        {
                                ((MeshGeometryModel3D)v).PostEffects = "border[color:#00FFDE]";
                        }
                }
            }
        }

        private void Info_Click(object sender, RoutedEventArgs e)
        {
            Frame.Navigate(typeof(EnterPage), "NOENABLE");
        }


        private void Ipdate_Kol()
        {
            Kolpeople.Text = ((ObservableCollection<DataGridDataItem>)dataGrid.ItemsSource).Count.ToString();
        }
        //Таймер
        public void Timer()
        {
            ThreadPoolTimer DelayTimer = ThreadPoolTimer.CreateTimer(
            async (source) =>
            {
                timer += 1;
                if (timer == 5)//Происходит раз в 5 секунд, считываем данные позиции и применяем обновления
                {
                    await this.Dispatcher.RunAsync(CoreDispatcherPriority.Normal, async () => // Перенаправляем поток
                    {
                        string lastzon = name_zon;
                        await Employer_View.GeneratePositionAsync();
                        await DataGridDataSource.Updatedata(timer);
                        Ipdate_Kol();
                        if (name_zon != "" && name_zon != lastzon)
                        {
                            Remove_Selection(lastzon);
                            View_Zon(name_zon, true);
                        } 
                        if(name != "")
                        {
                            dataGrid.ItemsSource = viewModel1.FilterData(DataGridDataSource.FilterOptions.Zon, name);
                        }
                        timer = 0;
                        if(framePage.numberpage == 5)
                        {
                            ThreeD.Update_text();
                        }
                        Timer();
                    });
                }
                else// Считаем время нахождения в зоне
                {
                    await this.Dispatcher.RunAsync(CoreDispatcherPriority.Normal, async () =>
                    {
                        await DataGridDataSource.Updatedata(timer);
                        Timer();
                    });
                }

            }, TimeSpan.FromSeconds(1));
        }


        //Работа с 3D
        private void Viewport3DX_OnMouse3DDown(object sender, HelixToolkit.UWP.MouseDown3DEventArgs e)
        {
            if(modifytag == false)
            {
                if (e.HitTestResult != null && e.HitTestResult.ModelHit is GeometryModel3D element)
                {
                    if (((OITModel)element.DataContext).Mesh != null)
                    {
                        if (selectedElement == null && name_zon != "")
                        {
                            name_zon = "";
                            name_people = "";
                            selectedElement = element;
                            foreach (Element3D v1 in ((ItemsModel3D)element.Parent).Children)//Скрываем лишние элементы
                            {
                                if (v1.GUID == element.GUID)
                                {
                                    name = ((OITModel)v1.DataContext).NameModel;
                                    flag_click_zon = true;
                                    dataGrid.ItemsSource = viewModel1.FilterData(DataGridDataSource.FilterOptions.Zon, name);// Находим имя зоны и отправляем на работу в таблицу, для отображения людей в зоне
                                }
                            }
                        }
                        else
                        {
                            if (selectedElement == element)
                            {
                                Bloc_Button(false);
                                selectedElement.PostEffects = null;
                                selectedElement = null;
                                name = "";
                                dataGrid.ItemsSource = viewModel1.FilterData(DataGridDataSource.FilterOptions.All, name);
                                foreach (Element3D v1 in ((ItemsModel3D)element.Parent).Children)//Отображает скрытые элементы
                                {
                                    string nameinfo = ((OITModel)v1.DataContext).NameModel;
                                    if (((OITModel)v1.DataContext).Mesh != null)
                                        if (v1.GUID != element.GUID && ((OITModel)v1.DataContext).NameModel.Substring(0, 3) == "UR" + level.ToString())
                                        {
                                            v1.Visibility = Visibility.Visible;
                                        }
                                }
                                return;
                            }
                            if (selectedElement != null)
                            {
                                selectedElement.PostEffects = null;
                            }
                            // Работаем с выделянным объектом, все остальные скрываем
                            selectedElement = element;
                            Bloc_Button(true);
                            foreach (Element3D v1 in ((ItemsModel3D)element.Parent).Children)//Скрываем лишние элементы
                            {
                                string nameinfo = ((OITModel)v1.DataContext).NameModel;
                                if (((OITModel)v1.DataContext).Mesh != null)
                                    if (v1.GUID != element.GUID)
                                    {
                                        v1.Visibility = Visibility.Collapsed;
                                        if (((OITModel)v1.DataContext).NameModel.Substring(((OITModel)v1.DataContext).NameModel.Length - 5, 1) == level.ToString())
                                        {
                                            v1.Visibility = Visibility.Visible;
                                        }
                                    }
                                    else if (v1.GUID == element.GUID)
                                    {
                                        name = ((OITModel)v1.DataContext).NameModel;
                                        dataGrid.ItemsSource = viewModel1.FilterData(DataGridDataSource.FilterOptions.Zon, name);// Находим имя зоны и отправляем на работу в таблицу, для отображения людей в зоне
                                        selectedElement.PostEffects = string.IsNullOrEmpty(selectedElement.PostEffects) ? "border[color:#00FFDE]" : null;// Подсвечиваем выбранную зону
                                    }
                            }
                        }
                    }
                }
            }
        }// Работы с 3D объектом, отвечает за взаимодействие

        private void Bloc_Button(bool flagblock)//Отвечает за блокировку кнопок зон, при выборе конкретной зоны
        {
            if (flagblock == true)
            {
                if (int.Parse(((TextBlock)((Viewbox)((Button)Level1).Content).Child).Text) != level)
                {
                    Level1.IsEnabled = false;
                }
                else
                {
                    Level1.IsEnabled = true;
                }
                if (int.Parse(((TextBlock)((Viewbox)((Button)Level2).Content).Child).Text) != level)
                {
                    Level2.IsEnabled = false;
                }
                else
                {
                    Level2.IsEnabled = true;
                }
                if (int.Parse(((TextBlock)((Viewbox)((Button)Level3).Content).Child).Text) != level)
                {
                    Level3.IsEnabled = false;
                }
                else
                {
                    Level3.IsEnabled = true;
                }
                if (int.Parse(((TextBlock)((Viewbox)((Button)Level4).Content).Child).Text) != level)
                {
                    Level4.IsEnabled = false;
                }
                else
                {
                    Level4.IsEnabled = true;
                }
            }
            else
            {
                Level1.IsEnabled = true;
                Level2.IsEnabled = true;
                Level3.IsEnabled = true;
                Level4.IsEnabled = true;
            }
        }


        public void Update_Zon(object button) // Управляем уровнями отображения моделей
        {   
            if(OITDemoViewModel.init == false)
            {
                Wait();
            }
            else
            {
                foreach (Element3D v in ItemsData.Children)
                {
                    string name1 = ((OITModel)((Element3D)v).DataContext).NameModel;
                    if (name1.Substring(0, 3) != "UR" + level.ToString() && name1.Substring(0, 1) != "W"
                        || name1.Substring(name1.Length - 5, 1) != level.ToString() && name1.Substring(0, 1) != "U")
                    {
                        ((Element3D)v).Visibility = Visibility.Collapsed;
                    }
                    else
                    {
                        ((Element3D)v).Visibility = Visibility.Visible;
                    }
                }
            }
        }


        private void LoadData(object sender, RoutedEventArgs e) //Срабатывает по завершинии инициализации 3D моделей
        {
            Wait();//пауза необходима для внесения данных
        }

        public void Wait()
        {
            ThreadPoolTimer DelayTimer = ThreadPoolTimer.CreateTimer(
            async (source) =>
            {
                await this.Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () => // Перенаправляем поток
                {
                    Clik_Zon_Level(Level1, null);
                });

            }, TimeSpan.FromMilliseconds(40));
        }

        private void Create_text()
        {
            foreach (Element3D v in ItemsData.Children)
            {
                if(((OITModel)((Element3D)v).DataContext).Mesh != null)
                {
                    //FlyoutBase number_on_zon = new Flyout();
                    //TextBlock infokol = new TextBlock();
                    //infokol.Text = "123";
                    //View2D.Children.Add(infokol);
                    //((Flyout)number_on_zon).Content = infokol;
                    //((Flyout)((Element3D)v).ContextFlyout).ShowAt(((Element3D)v));
                }
            }
        }

        // обработчики нажатия кнопок интерфейса
        private void Clik_Zon_Level(object sender, RoutedEventArgs e)
        {
            if (Statelast != null)
            {
                Statelast.Style = this.Resources["Button_Zoning_normal"] as Style;
            }
            level = int.Parse(((TextBlock)((Viewbox)((Button)sender).Content).Child).Text);
            Statelast = (Button)sender;
            Statelast.Style = this.Resources["Button_Zoning_presed"] as Style;
            Update_Zon(sender);
        }




        private void Press(object sender, PointerRoutedEventArgs e) // проверяет нажатие клавиши колесика мыши
        {
            int i = 1;
            i += 1;
        }

        public static void KeyDownOnpage(KeyRoutedEventArgs e) //Обработка нажатий клавишь в окне
        {
            if (status_mouse_zon == true)
            {
                if (e.Key == Windows.System.VirtualKey.W)
                {
                    view.AddZoomForce(-0.1);
                }
                if (e.Key == Windows.System.VirtualKey.S)
                {
                    view.AddZoomForce(0.1);
                }
                if (e.Key == Windows.System.VirtualKey.Left)
                {
                    timepreset += 1;
                    if (MainPageViewModel.Camera.Position.X < 90)
                    {
                        Vector3 Positionleft = new Vector3(0.01f, 0, 0) * timepreset;
                        MainPageViewModel.Camera.Position += Positionleft;
                    }
                    else
                    {
                        timepreset = 1;
                    }
                }
                if (e.Key == Windows.System.VirtualKey.Right)
                {
                    timepreset += 1;
                    if (MainPageViewModel.Camera.Position.X > -90)
                    {
                        Vector3 Positionleft = new Vector3(-0.01f, 0, 0) * timepreset;
                        MainPageViewModel.Camera.Position += Positionleft;
                    }
                    else
                    {
                        timepreset = 1;
                    }
                }
                if (e.Key == Windows.System.VirtualKey.Up)
                {
                    timepreset += 1;
                    if (MainPageViewModel.Camera.Position.Y > -32)
                    {
                        Vector3 Positionleft = new Vector3(0, -0.01f, 0) * timepreset;
                        MainPageViewModel.Camera.Position += Positionleft;
                    }
                    else
                    {
                        timepreset = 1;
                    }
                }
                if (e.Key == Windows.System.VirtualKey.Down)
                {
                    timepreset += 1;
                    if (MainPageViewModel.Camera.Position.Y < 25)
                    {
                        Vector3 Positionleft = new Vector3(0, 0.01f, 0) * timepreset;
                        MainPageViewModel.Camera.Position += Positionleft;
                    }
                    else
                    {
                        timepreset = 1;
                    }
                }
            }
        } 



        private void ActiveKey(object sender, MouseMove3DEventArgs e)// Фокус на 3D управлении
        {
            status_mouse_zon = true;
        }

        private void LoseFocus(object sender, PointerRoutedEventArgs e) // Потеря фокуса 3D управления, фокус на таблице
        {
            status_mouse_zon = false;
        }


        private async Task UpdateAsync()
        {
            dataGrid.ItemsSource = await DataGridDataSource.UpdateTag();
        }



    }
}
