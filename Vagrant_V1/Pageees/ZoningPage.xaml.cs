﻿using HelixToolkit.UWP;
using Microsoft.Toolkit.Uwp.UI.Controls;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Vagrant_V1.Data;
using Vagrant_V1.ModelData;
using Vagrant_V1.Modeling3d._2D;
using Vagrant_V1.Server;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.System.Threading;
using Windows.UI;
using Windows.UI.Core;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// Документацию по шаблону элемента "Пустая страница" см. по адресу https://go.microsoft.com/fwlink/?LinkId=234238

namespace Vagrant_V1.Pageees
{
    /// <summary>
    /// Пустая страница, которую можно использовать саму по себе или для перехода внутри фрейма.
    /// </summary>
    public sealed partial class ZoningPage : Page
    {
        private DescriptionDatasource viewModel1 = new DescriptionDatasource();
        public int level = 1;
        private Button Statelast = null;
        private int state = 0;
        private List<GeometryModel3D> selectedElement = new List<GeometryModel3D>();
        private List<GeometryModel3D> selectedElementstartposition = new List<GeometryModel3D>();
        private bool statusdatagrid = false;
        public ZoningPage()
        {
            this.InitializeComponent();
            NavigationCacheMode = NavigationCacheMode.Required;
            OnXamlRendered();
        }

        public async void OnXamlRendered()
        {
            if (dataGrid != null)
            {
                dataGrid.Sorting -= DataGrid_Sorting;
            }

            if (dataGrid != null)
            {
                dataGrid.Sorting += DataGrid_Sorting;
                if (dataGrid.ItemsSource == null)
                {
                    dataGrid.ItemsSource = await viewModel1.GetDataAsync();
                }
                else
                {
                    dataGrid.ItemsSource = await viewModel1.GetDataAsync();
                }
            }
        }
        private void DataGrid_Sorting(object sender, DataGridColumnEventArgs e)
        {
            // Clear previous sorted column if we start sorting a different column
            string previousSortedColumn = viewModel1.CachedSortedColumn;
            if (previousSortedColumn != string.Empty)
            {
                foreach (DataGridColumn dataGridColumn in dataGrid.Columns)
                {
                    if (dataGridColumn.Tag != null && dataGridColumn.Tag.ToString() == previousSortedColumn &&
                        (e.Column.Tag == null || previousSortedColumn != e.Column.Tag.ToString()))
                    {
                        dataGridColumn.SortDirection = null;
                    }
                }
            }

            // Toggle clicked column's sorting method
            if (e.Column.Tag != null)
            {
                if (e.Column.SortDirection == null)
                {
                    dataGrid.ItemsSource = viewModel1.SortData(e.Column.Tag.ToString(), true);
                    e.Column.SortDirection = DataGridSortDirection.Ascending;
                }
                else if (e.Column.SortDirection == DataGridSortDirection.Ascending)
                {
                    dataGrid.ItemsSource = viewModel1.SortData(e.Column.Tag.ToString(), false);
                    e.Column.SortDirection = DataGridSortDirection.Descending;
                }
                else
                {
                    dataGrid.ItemsSource = viewModel1.FilterData(DescriptionDatasource.FilterOptions.All);
                    e.Column.SortDirection = null;
                    viewModel1.CachedSortedColumn = "";
                    DataGridDataSource._cachedSortedColumnstatus = "";

                }
            }
        }

        private void dataGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if(statusdatagrid)
            {
                if (e.AddedItems.Count != 0)
                {
                    Name.Text = ((DescriptionData)e.AddedItems[e.AddedItems.Count - 1]).Description;
                    var a = (byte)220f;
                    var r = (byte)System.Convert.ToUInt32(((DescriptionData)e.AddedItems[e.AddedItems.Count - 1]).Color.Substring(1, 2), 16);
                    var g = (byte)System.Convert.ToUInt32(((DescriptionData)e.AddedItems[e.AddedItems.Count - 1]).Color.Substring(3, 2), 16);
                    var b = (byte)System.Convert.ToUInt32(((DescriptionData)e.AddedItems[e.AddedItems.Count - 1]).Color.Substring(5, 2), 16);
                    ColorPickerZon.Color = Color.FromArgb(a, r, g, b);

                }
                if (e.AddedItems.Count == 0)
                {
                    if (e.RemovedItems.Count != 0)
                    {
                        Name.Text = "";
                        ColorPickerZon.Color = Color.FromArgb(255, 255, 255, 255);
                    }
                }
            }
            else
            {
                if(dataGrid.SelectedItem != null)
                {
                    dataGrid.SelectedItem = null;
                }
            }
        }

        public void Update_Zon(object button) // Управляем уровнями отображения моделей
        {
            foreach (Element3D v in ItemsDataZon.Children)
            {
                string name1 = ((OITModel)((Element3D)v).DataContext).NameModel;
                if (name1.Substring(0, 3) != "UR" + level.ToString() && name1.Substring(0, 1) != "W"
                    || name1.Substring(name1.Length - 5, 1) != level.ToString() && name1.Substring(0, 1) != "U")
                {
                    ((Element3D)v).Visibility = Visibility.Collapsed;
                }
                else
                {
                    ((Element3D)v).Visibility = Visibility.Visible;
                }
            }
        }

        private void LoadData(object sender, RoutedEventArgs e) //Срабатывает по завершинии инициализации 3D моделей
        {
            Wait();//пауза необходима для внесения данных
        }

        public void Wait()
        {
            ThreadPoolTimer DelayTimer = ThreadPoolTimer.CreateTimer(
            async (source) =>
            {
                await this.Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () => // Перенаправляем поток
                {
                    Clik_Zon_Level(Level1, null);
                });

            }, TimeSpan.FromMilliseconds(10));
        }

        private void Clik_Zon_Level(object sender, RoutedEventArgs e)
        {
            if(Statelast != null)
            {
                Statelast.Style = this.Resources["Button_Zoning_normal"] as Style;
            }
            level = int.Parse(((TextBlock)((Viewbox)((Button)sender).Content).Child).Text);
            Statelast = (Button)sender;
            Statelast.Style = this.Resources["Button_Zoning_presed"] as Style;
            Update_Zon(sender);
        }

        private static void ModifyList(GeometryModel3D model)
        {
            model.PostEffects = string.IsNullOrEmpty(model.PostEffects) ? "border[color:#00FFDE]" : null;
        }

        private void Bloc_Button(bool flagblock)//Отвечает за блокировку кнопок зон, при выборе конкретной зоны
        {
            if (flagblock == true)
            {
                if (int.Parse(((TextBlock)((Viewbox)((Button)Level1).Content).Child).Text) != level)
                {
                    Level1.IsEnabled = false;
                }
                else
                {
                    Level1.IsEnabled = true;
                }
                if (int.Parse(((TextBlock)((Viewbox)((Button)Level2).Content).Child).Text) != level)
                {
                    Level2.IsEnabled = false;
                }
                else
                {
                    Level2.IsEnabled = true;
                }
                if (int.Parse(((TextBlock)((Viewbox)((Button)Level3).Content).Child).Text) != level)
                {
                    Level3.IsEnabled = false;
                }
                else
                {
                    Level3.IsEnabled = true;
                }
                if (int.Parse(((TextBlock)((Viewbox)((Button)Level4).Content).Child).Text) != level)
                {
                    Level4.IsEnabled = false;
                }
                else
                {
                    Level4.IsEnabled = true;
                }
            }
            else
            {
                Level1.IsEnabled = true;
                Level2.IsEnabled = true;
                Level3.IsEnabled = true;
                Level4.IsEnabled = true;
            }
        }

        private void Viewport3DX_OnMouse3DDown(object sender, HelixToolkit.UWP.MouseDown3DEventArgs e)
        {
            var statekey = CoreWindow.GetForCurrentThread().GetKeyState(Windows.System.VirtualKey.LeftButton);
            string nameZon = "";
            if (e.HitTestResult != null && e.HitTestResult.ModelHit is GeometryModel3D element)
            {
                if (((OITModel)element.DataContext).Mesh != null)
                {
                    switch (state)
                    {
                        case 1:
                            statekey = CoreWindow.GetForCurrentThread().GetKeyState(Windows.System.VirtualKey.LeftMenu);
                            if (statekey != CoreVirtualKeyStates.None && statekey != CoreVirtualKeyStates.Locked)
                            {
                                if (selectedElement.Count != 0)
                                {
                                    selectedElement.ForEach(ModifyList);
                                    selectedElement.Clear();
                                    Bloc_Button(false);
                                    return;
                                }
                            }
                            else
                            {
                                if (selectedElement.Count == 0)
                                {
                                    selectedElement.Add(element);
                                    selectedElement.ForEach(ModifyList);
                                    Bloc_Button(true);
                                    return;
                                }
                                statekey = CoreWindow.GetForCurrentThread().GetKeyState(Windows.System.VirtualKey.LeftControl);
                                if (statekey != CoreVirtualKeyStates.None && statekey != CoreVirtualKeyStates.Locked)
                                {
                                    if (selectedElement.Find(u => u == element) != null)
                                    {
                                        selectedElement.Find(u => u == element).PostEffects = null;
                                        selectedElement.Remove(selectedElement.Find(u => u == element));
                                        if (selectedElement.Count == 0)
                                        {
                                            Bloc_Button(false);
                                        }
                                        return;
                                    }
                                }
                                statekey = CoreWindow.GetForCurrentThread().GetKeyState(Windows.System.VirtualKey.LeftShift);
                                if (statekey != CoreVirtualKeyStates.None && statekey != CoreVirtualKeyStates.Locked)
                                {
                                    if (selectedElement.Find(u => u == element) == null && selectedElement.Count != 0)
                                    {
                                        selectedElement.Add(element);
                                        selectedElement.Find(u => u == element).PostEffects = string.IsNullOrEmpty(selectedElement.Find(u => u == element).PostEffects) ? "border[color:#00FFDE]" : null;
                                        Bloc_Button(true);
                                        return;
                                    }
                                }
                            }
                            break;
                        case 2:
                            nameZon = "";
                            statekey = CoreWindow.GetForCurrentThread().GetKeyState(Windows.System.VirtualKey.LeftMenu);
                            if (statekey != CoreVirtualKeyStates.None && statekey != CoreVirtualKeyStates.Locked)
                            {
                                if (selectedElement.Count != 0)
                                {
                                    selectedElement.ForEach(ModifyList);
                                    selectedElement.Clear();
                                    selectedElementstartposition.ForEach(ModifyList);
                                    return;
                                }
                                else if (selectedElementstartposition.Count != 0)
                                {
                                    selectedElementstartposition.ForEach(ModifyList);
                                    selectedElementstartposition.Clear();
                                    nameZon = "";
                                    Name.Text = "";
                                    ColorPickerZon.Color = Color.FromArgb(255, 255, 255, 255);
                                    Bloc_Button(false);
                                    return;
                                }
                            }
                            else
                            {
                                if (selectedElement.Count == 0)
                                {
                                    if (Employer_View.unityObjects.Find(u => u.unityObjectId == ((OITModel)element.DataContext).NameModel).siteZone != null)
                                    {
                                        nameZon = Employer_View.unityObjects.Find(u => u.unityObjectId == ((OITModel)element.DataContext).NameModel).siteZone.title;
                                        Name.Text = Employer_View.unityObjects.Find(u => u.unityObjectId == ((OITModel)element.DataContext).NameModel).siteZone.description;
                                        for (int i = 0; i <= ((OITModel)element.DataContext).IndexMass.Count - 1; i++)
                                        {
                                            selectedElement.Add((GeometryModel3D)((ItemsModel3D)element.Parent).Children.ElementAt(((OITModel)element.DataContext).IndexMass[i]));
                                            selectedElementstartposition.Add((GeometryModel3D)((ItemsModel3D)element.Parent).Children.ElementAt(((OITModel)element.DataContext).IndexMass[i]));
                                        }
                                        selectedElement.ForEach(ModifyList);
                                        var a = (byte)220f;
                                        var r = (byte)System.Convert.ToUInt32(Employer_View.unityObjects.Find(u => u.unityObjectId == ((OITModel)element.DataContext).NameModel).siteZone.color.Substring(1, 2), 16);
                                        var g = (byte)System.Convert.ToUInt32(Employer_View.unityObjects.Find(u => u.unityObjectId == ((OITModel)element.DataContext).NameModel).siteZone.color.Substring(3, 2), 16);
                                        var b = (byte)System.Convert.ToUInt32(Employer_View.unityObjects.Find(u => u.unityObjectId == ((OITModel)element.DataContext).NameModel).siteZone.color.Substring(5, 2), 16);
                                        ColorPickerZon.Color = Color.FromArgb(a, r, g, b);
                                        Bloc_Button(true);
                                        return;
                                    }
                                }
                                else if ((CoreWindow.GetForCurrentThread().GetKeyState(Windows.System.VirtualKey.LeftControl) == CoreVirtualKeyStates.None
                                    || CoreWindow.GetForCurrentThread().GetKeyState(Windows.System.VirtualKey.LeftControl) == CoreVirtualKeyStates.Locked) &&
                                    (CoreWindow.GetForCurrentThread().GetKeyState(Windows.System.VirtualKey.LeftShift) == CoreVirtualKeyStates.None
                                    || CoreWindow.GetForCurrentThread().GetKeyState(Windows.System.VirtualKey.LeftShift) == CoreVirtualKeyStates.Locked) &&
                                    (CoreWindow.GetForCurrentThread().GetKeyState(Windows.System.VirtualKey.LeftMenu) == CoreVirtualKeyStates.None
                                    || CoreWindow.GetForCurrentThread().GetKeyState(Windows.System.VirtualKey.LeftMenu) == CoreVirtualKeyStates.Locked))
                                {
                                    if (Employer_View.unityObjects.Find(u => u.unityObjectId == ((OITModel)element.DataContext).NameModel).siteZone.title != nameZon)
                                    {
                                        List<Element3D> massElemet = new List<Element3D>();
                                        selectedElement.ForEach(ModifyList);
                                        selectedElement.Clear();
                                        selectedElementstartposition.Clear();
                                        for (int i = 0; i <= ((OITModel)element.DataContext).IndexMass.Count - 1; i++)
                                        {
                                            selectedElement.Add((GeometryModel3D)((ItemsModel3D)element.Parent).Children.ElementAt(((OITModel)element.DataContext).IndexMass[i]));
                                            selectedElementstartposition.Add((GeometryModel3D)((ItemsModel3D)element.Parent).Children.ElementAt(((OITModel)element.DataContext).IndexMass[i]));
                                        }
                                        selectedElement.ForEach(ModifyList);
                                        var a = (byte)220f;
                                        var r = (byte)System.Convert.ToUInt32(Employer_View.unityObjects.Find(u => u.unityObjectId == ((OITModel)element.DataContext).NameModel).siteZone.color.Substring(1, 2), 16);
                                        var g = (byte)System.Convert.ToUInt32(Employer_View.unityObjects.Find(u => u.unityObjectId == ((OITModel)element.DataContext).NameModel).siteZone.color.Substring(3, 2), 16);
                                        var b = (byte)System.Convert.ToUInt32(Employer_View.unityObjects.Find(u => u.unityObjectId == ((OITModel)element.DataContext).NameModel).siteZone.color.Substring(5, 2), 16);
                                        ColorPickerZon.Color = Color.FromArgb(a, r, g, b);
                                        Name.Text = Employer_View.unityObjects.Find(u => u.unityObjectId == ((OITModel)element.DataContext).NameModel).siteZone.description;
                                        return;
                                    }
                                }
                                statekey = CoreWindow.GetForCurrentThread().GetKeyState(Windows.System.VirtualKey.LeftControl);
                                if (statekey != CoreVirtualKeyStates.None && statekey != CoreVirtualKeyStates.Locked)
                                {
                                    if (selectedElement.Find(u => u == element) != null)
                                    {
                                        selectedElement.Find(u => u == element).PostEffects = null;
                                        selectedElement.Remove(selectedElement.Find(u => u == element));
                                        if (selectedElement.Count == 0)
                                        {
                                            selectedElementstartposition.Clear();
                                            nameZon = "";
                                            Name.Text = "";
                                            ColorPickerZon.Color = Color.FromArgb(255, 255, 255, 255);
                                            Bloc_Button(false);
                                        }
                                        return;
                                    }
                                }
                                statekey = CoreWindow.GetForCurrentThread().GetKeyState(Windows.System.VirtualKey.LeftShift);
                                if (statekey != CoreVirtualKeyStates.None && statekey != CoreVirtualKeyStates.Locked)
                                {
                                    if (selectedElement.Find(u => u == element) == null && selectedElement.Count != 0)
                                    {
                                        selectedElement.Add(element);
                                        selectedElement.Find(u => u == element).PostEffects = string.IsNullOrEmpty(selectedElement.Find(u => u == element).PostEffects) ? "border[color:#00FFDE]" : null;
                                        Bloc_Button(true);
                                        return;
                                    }
                                }
                            }
                            break;
                        case 3:
                            nameZon = "";
                            statekey = CoreWindow.GetForCurrentThread().GetKeyState(Windows.System.VirtualKey.LeftMenu);
                            if (statekey != CoreVirtualKeyStates.None && statekey != CoreVirtualKeyStates.Locked)
                            {
                                if (selectedElement.Count != 0)
                                {
                                    selectedElement.ForEach(ModifyList);
                                    selectedElement.Clear();
                                    ColorPickerZon.Color = Color.FromArgb(255, 255, 255, 255);
                                    return;
                                }
                            }
                            else
                            {
                                if (selectedElement.Count == 0)
                                {
                                    if (Employer_View.unityObjects.Find(u => u.unityObjectId == ((OITModel)element.DataContext).NameModel).siteZone != null)
                                    {
                                        nameZon = Employer_View.unityObjects.Find(u => u.unityObjectId == ((OITModel)element.DataContext).NameModel).siteZone.title;
                                        Name.Text = Employer_View.unityObjects.Find(u => u.unityObjectId == ((OITModel)element.DataContext).NameModel).siteZone.description;
                                        for (int i = 0; i <= ((OITModel)element.DataContext).IndexMass.Count - 1; i++)
                                        {
                                            selectedElement.Add((GeometryModel3D)((ItemsModel3D)element.Parent).Children.ElementAt(((OITModel)element.DataContext).IndexMass[i]));
                                        }
                                        selectedElement.ForEach(ModifyList);
                                        var a = (byte)220f;
                                        var r = (byte)System.Convert.ToUInt32(Employer_View.unityObjects.Find(u => u.unityObjectId == ((OITModel)element.DataContext).NameModel).siteZone.color.Substring(1, 2), 16);
                                        var g = (byte)System.Convert.ToUInt32(Employer_View.unityObjects.Find(u => u.unityObjectId == ((OITModel)element.DataContext).NameModel).siteZone.color.Substring(3, 2), 16);
                                        var b = (byte)System.Convert.ToUInt32(Employer_View.unityObjects.Find(u => u.unityObjectId == ((OITModel)element.DataContext).NameModel).siteZone.color.Substring(5, 2), 16);
                                        ColorPickerZon.Color = Color.FromArgb(a, r, g, b);
                                        Bloc_Button(true);
                                        return;
                                    }
                                }
                                else if ((CoreWindow.GetForCurrentThread().GetKeyState(Windows.System.VirtualKey.LeftControl) == CoreVirtualKeyStates.None
                                    || CoreWindow.GetForCurrentThread().GetKeyState(Windows.System.VirtualKey.LeftControl) == CoreVirtualKeyStates.Locked) &&
                                    (CoreWindow.GetForCurrentThread().GetKeyState(Windows.System.VirtualKey.LeftShift) == CoreVirtualKeyStates.None
                                    || CoreWindow.GetForCurrentThread().GetKeyState(Windows.System.VirtualKey.LeftShift) == CoreVirtualKeyStates.Locked) &&
                                    (CoreWindow.GetForCurrentThread().GetKeyState(Windows.System.VirtualKey.LeftMenu) == CoreVirtualKeyStates.None
                                    || CoreWindow.GetForCurrentThread().GetKeyState(Windows.System.VirtualKey.LeftMenu) == CoreVirtualKeyStates.Locked))
                                {
                                    if (Employer_View.unityObjects.Find(u => u.unityObjectId == ((OITModel)element.DataContext).NameModel).siteZone.title != nameZon)
                                    {
                                        selectedElement.ForEach(ModifyList);
                                        selectedElement.Clear();
                                        selectedElementstartposition.Clear();
                                        for (int i = 0; i <= ((OITModel)element.DataContext).IndexMass.Count - 1; i++)
                                        {
                                            selectedElement.Add((GeometryModel3D)((ItemsModel3D)element.Parent).Children.ElementAt(((OITModel)element.DataContext).IndexMass[i]));
                                        }
                                        selectedElement.ForEach(ModifyList);
                                        var a = (byte)220f;
                                        var r = (byte)System.Convert.ToUInt32(Employer_View.unityObjects.Find(u => u.unityObjectId == ((OITModel)element.DataContext).NameModel).siteZone.color.Substring(1, 2), 16);
                                        var g = (byte)System.Convert.ToUInt32(Employer_View.unityObjects.Find(u => u.unityObjectId == ((OITModel)element.DataContext).NameModel).siteZone.color.Substring(3, 2), 16);
                                        var b = (byte)System.Convert.ToUInt32(Employer_View.unityObjects.Find(u => u.unityObjectId == ((OITModel)element.DataContext).NameModel).siteZone.color.Substring(5, 2), 16);
                                        ColorPickerZon.Color = Color.FromArgb(a, r, g, b);
                                        Name.Text = Employer_View.unityObjects.Find(u => u.unityObjectId == ((OITModel)element.DataContext).NameModel).siteZone.description;
                                        return;
                                    }
                                }
                            }
                            break;
                    }
                }
                else
                {
                    switch (state)
                    {
                        case 1:
                            statekey = CoreWindow.GetForCurrentThread().GetKeyState(Windows.System.VirtualKey.LeftMenu);
                            if (statekey != CoreVirtualKeyStates.None && statekey != CoreVirtualKeyStates.Locked)
                            {
                                if (selectedElement.Count != 0)
                                {
                                    selectedElement.ForEach(ModifyList);
                                    selectedElement.Clear();
                                    ColorPickerZon.Color = Color.FromArgb(255, 255, 255, 255);
                                    Bloc_Button(false);
                                    return;
                                }
                            }
                            break;
                        case 2:
                            nameZon = "";
                            statekey = CoreWindow.GetForCurrentThread().GetKeyState(Windows.System.VirtualKey.LeftMenu);
                            if (statekey != CoreVirtualKeyStates.None && statekey != CoreVirtualKeyStates.Locked)
                            {
                                if (selectedElement.Count != 0)
                                {
                                    selectedElement.ForEach(ModifyList);
                                    selectedElement.Clear();
                                    selectedElementstartposition.ForEach(ModifyList);
                                    return;
                                }
                                else if (selectedElementstartposition.Count != 0)
                                {
                                    selectedElementstartposition.ForEach(ModifyList);
                                    selectedElementstartposition.Clear();
                                    nameZon = "";
                                    Name.Text = "";
                                    ColorPickerZon.Color = Color.FromArgb(255, 255, 255, 255);
                                    Bloc_Button(false);
                                    return;
                                }
                            }
                            break;
                        case 3:
                            nameZon = "";
                            statekey = CoreWindow.GetForCurrentThread().GetKeyState(Windows.System.VirtualKey.LeftMenu);
                            if (statekey != CoreVirtualKeyStates.None && statekey != CoreVirtualKeyStates.Locked)
                            {
                                if (selectedElement.Count != 0)
                                {
                                    selectedElement.ForEach(ModifyList);
                                    selectedElement.Clear();
                                    ColorPickerZon.Color = Color.FromArgb(255, 255, 255, 255);
                                    return;
                                }
                            }
                            break;
                    }
                }
            }
            else
            {
                switch (state)
                {
                    case 1:
                        statekey = CoreWindow.GetForCurrentThread().GetKeyState(Windows.System.VirtualKey.LeftMenu);
                        if (statekey != CoreVirtualKeyStates.None && statekey != CoreVirtualKeyStates.Locked)
                        {
                            if (selectedElement.Count != 0)
                            {
                                selectedElement.ForEach(ModifyList);
                                selectedElement.Clear();
                                Bloc_Button(false);
                                ColorPickerZon.Color = Color.FromArgb(255, 255, 255, 255);
                                return;
                            }
                        }
                        break;
                    case 2:
                        nameZon = "";
                        statekey = CoreWindow.GetForCurrentThread().GetKeyState(Windows.System.VirtualKey.LeftMenu);
                        if (statekey != CoreVirtualKeyStates.None && statekey != CoreVirtualKeyStates.Locked)
                        {
                            if (selectedElement.Count != 0)
                            {
                                selectedElement.ForEach(ModifyList);
                                selectedElement.Clear();
                                selectedElementstartposition.ForEach(ModifyList);
                                return;
                            }
                            else if (selectedElementstartposition.Count != 0)
                            {
                                selectedElementstartposition.ForEach(ModifyList);
                                selectedElementstartposition.Clear();
                                nameZon = "";
                                Name.Text = "";
                                ColorPickerZon.Color = Color.FromArgb(255, 255, 255, 255);
                                Bloc_Button(false);
                                return;
                            }
                        }
                        break;
                    case 3:
                        nameZon = "";
                        statekey = CoreWindow.GetForCurrentThread().GetKeyState(Windows.System.VirtualKey.LeftMenu);
                        if (statekey != CoreVirtualKeyStates.None && statekey != CoreVirtualKeyStates.Locked)
                        {
                            if (selectedElement.Count != 0)
                            {
                                selectedElement.ForEach(ModifyList);
                                selectedElement.Clear();
                                ColorPickerZon.Color = Color.FromArgb(255, 255, 255, 255);
                                return;
                            }
                        }
                        break;
                }
            }

        }// Работы с 3D объектом, отвечает за взаимодействие

        private void Create_Click(object sender, RoutedEventArgs e)
        {
            if(Create.Style == this.Resources["Button_Zoning_normal"] as Style)
            {
                switch(state)
                {
                    case 0:
                        Create.Style = this.Resources["Button_Zoning_presed"] as Style;
                        break;
                    case 2:
                        Bloc_Button(false);
                        Modify.Style = this.Resources["Button_Zoning_normal"] as Style;
                        Create.Style = this.Resources["Button_Zoning_presed"] as Style;
                        break;
                    case 3:
                        Bloc_Button(false);
                        Deleate.Style = this.Resources["Button_Zoning_normal"] as Style;
                        Create.Style = this.Resources["Button_Zoning_presed"] as Style;
                        break;
                }
                Name.Text = "";
                state = 1;
                PanelInfo.Visibility = Visibility.Visible;
                statusdatagrid = true;
                ColorPickerZon.Color = Color.FromArgb(255, 255, 255, 255);
                NoSelection();
            }
            else
            {
                statusdatagrid = false;
                Bloc_Button(false);
                Create.Style = this.Resources["Button_Zoning_normal"] as Style;
                Name.Text = "";
                state = 0;
                PanelInfo.Visibility = Visibility.Collapsed;
                ColorPickerZon.Color = Color.FromArgb(255, 255, 255, 255);
                NoSelection();
            }
        }

        private void Modify_Click(object sender, RoutedEventArgs e)
        {

            if (Modify.Style == this.Resources["Button_Zoning_normal"] as Style)
            {
                switch (state)
                {
                    case 0:
                        Modify.Style = this.Resources["Button_Zoning_presed"] as Style;
                        break;
                    case 1:
                        Bloc_Button(false);
                        Create.Style = this.Resources["Button_Zoning_normal"] as Style;
                        Modify.Style = this.Resources["Button_Zoning_presed"] as Style;
                        break;
                    case 3:
                        Bloc_Button(false);
                        Deleate.Style = this.Resources["Button_Zoning_normal"] as Style;
                        Modify.Style = this.Resources["Button_Zoning_presed"] as Style;
                        break;
                }
                Name.Text = "";
                state = 2;
                PanelInfo.Visibility = Visibility.Visible;
                ColorPickerZon.Color = Color.FromArgb(255, 255, 255, 255);
                statusdatagrid = true;
                NoSelection();
            }
            else
            {
                statusdatagrid = false;
                Bloc_Button(false);
                Modify.Style = this.Resources["Button_Zoning_normal"] as Style;
                Name.Text = "";
                state = 0;
                PanelInfo.Visibility = Visibility.Collapsed;
                ColorPickerZon.Color = Color.FromArgb(255, 255, 255, 255);
                NoSelection();
            }
        }

        private void Deleate_Click(object sender, RoutedEventArgs e)
        {
            if (Deleate.Style == this.Resources["Button_Zoning_normal"] as Style)
            {
                switch (state)
                {
                    case 0:
                        Deleate.Style = this.Resources["Button_Zoning_presed"] as Style;
                        break;
                    case 1:
                        Bloc_Button(false);
                        Create.Style = this.Resources["Button_Zoning_normal"] as Style;
                        Deleate.Style = this.Resources["Button_Zoning_presed"] as Style;
                        break;
                    case 2:
                        Bloc_Button(false);
                        Modify.Style = this.Resources["Button_Zoning_normal"] as Style;
                        Deleate.Style = this.Resources["Button_Zoning_presed"] as Style;
                        break;
                }
                statusdatagrid = false;
                Name.Text = "";
                state = 3;
                PanelInfo.Visibility = Visibility.Visible;
                ColorPickerZon.Color = Color.FromArgb(255, 255, 255, 255);
                NoSelection();
            }
            else
            {
                statusdatagrid = false;
                Bloc_Button(false);
                Deleate.Style = this.Resources["Button_Zoning_normal"] as Style;
                Name.Text = "";
                state = 0;
                PanelInfo.Visibility = Visibility.Collapsed;
                ColorPickerZon.Color = Color.FromArgb(255, 255, 255, 255);
                NoSelection();
            }
        }
        private void Update_Color(GeometryModel3D model)
        {
            //var a = (byte)220f;
            //var r = ColorPickerZon.Color.R;
            //var g = ColorPickerZon.Color.G;
            //var b = ColorPickerZon.Color.B;
            //SharpDX.Color4 Color_zon = new SharpDX.Color4(r,g,b,a);
            //((PhongMaterial)((MeshGeometryModel3D)model).Material).EmissiveColor = Color_zon;
            //((PhongMaterial)((MeshGeometryModel3D)model).Material).DiffuseColor = Color_zon;
            //((PhongMaterial)((MeshGeometryModel3D)model).Material).ReflectiveColor = Color_zon;
            //((PhongMaterial)((MeshGeometryModel3D)model).Material).SpecularColor = Color_zon;
            //((PhongMaterial)((MeshGeometryModel3D)model).Material).AmbientColor = Color_zon;
        }

        private void Color1(ColorPicker sender, ColorChangedEventArgs args)
        {
            if (selectedElement.Count != 0)
            {
                selectedElement.ForEach(Update_Color);
            }
        }

        private void ApplyClick(object sender, RoutedEventArgs e)
        {
            if (ColorPickerZon.Color != Color.FromArgb(255, 255, 255, 255) && Name.Text != "" && selectedElement.Count != 0)
            {
                LoadingControl.IsLoading = true;
                switch (state)
                {
                    case 1:
                        Apply_CreateAsync();
                        break;
                    case 2:
                        Apply_UpdateAsync();
                        break;
                    case 3:
                        Apply_DeleateAsync();
                        break;
                }

            }
        }

        public async System.Threading.Tasks.Task Timer()
        {
            ThreadPoolTimer DelayTimer = ThreadPoolTimer.CreateTimer(
            async (source) =>
            {
            }, TimeSpan.FromSeconds(1));
        }


        private async System.Threading.Tasks.Task Apply_DeleateAsync()
        {
            string id = Employer_View.unityObjects.Find(u => u.unityObjectId == ((OITModel)selectedElement[0].DataContext).NameModel).siteZone.id;
            string name = Employer_View.unityObjects.Find(u => u.unityObjectId == ((OITModel)selectedElement[0].DataContext).NameModel).siteZone.title;
            string answer = await Employer_View.DeleateZon(id);
            int y = 0;
            while (answer != "OK" && y <= 5)
            {
                await Timer();
                answer = await Employer_View.DeleateZon(Employer_View.unityObjects.Find(u => u.unityObjectId == ((OITModel)selectedElement[0].DataContext).NameModel).siteZone.id);
                y += 1;
            }
            if (answer == "OK")
            {
                Employer_View.Zon.Remove(Employer_View.Zon.Find(u => u.id == id));
                OITDemoViewModel.ModelGeometry.RemoveAt(OITDemoViewModel.Collection.Find(u => u.nameZone == name).indexZon);
                OITDemoViewModel.Collection.Remove(OITDemoViewModel.Collection.Find(u => u.nameZone == name));
                SharpDX.Color colorZon = SharpDX.Color.White;
                foreach (GeometryModel3D v in selectedElement)
                {
                    ((PhongMaterial)((OITModel)v.DataContext).Material).DiffuseColor = colorZon;
                    ((PhongMaterial)((OITModel)v.DataContext).Material).EmissiveColor = colorZon;
                    ((OITModel)v.DataContext).IndexMass.Clear();
                    Employer_View.unityObjects.Find(u => u.unityObjectId == ((OITModel)v.DataContext).NameModel).siteZone = null;
                }
                NoSelection();
                await DescriptionDatasource.UpdateDataAsync();
                LoadingControl.IsLoading = false;
            }
        }


        private async System.Threading.Tasks.Task Apply_CreateAsync()
        {
            string color = ColorPickerZon.Color.ToString();
            string description = Name.Text;
            bool flagid = false;
            int idi = 1;
            string id = "";
            string title = "";
            if(Employer_View.Zon.Find(u => u.id == "Zone_id_" + (Employer_View.Zon.Count + 1).ToString()) != null)
            {
                while (flagid == false)
                {
                    if (Employer_View.Zon.Find(u => u.id == ("Zone_id_" + idi.ToString())) == null)
                    {
                        id = "Zone_id_" + idi;
                        title = "UR" + level + " Zon" + (idi - 1).ToString();
                        flagid = true;
                    }
                    idi += 1;
                }
            }
            else
            {
                id = "Zone_id_" + (Employer_View.Zon.Count + 1).ToString();
                title = "UR" + level + " Zon" + (Employer_View.Zon.Count).ToString();
            }
            List<string> unityobject = new List<string>();
            List<int> index = new List<int>();
            List<string> nameZone = new List<string>();
            foreach (GeometryModel3D v in selectedElement)
            {
                unityobject.Add(Employer_View.unityObjects.Find(u => u.unityObjectId == ((OITModel)v.DataContext).NameModel).id);
                if (Employer_View.unityObjects.Find(u => u.unityObjectId == ((OITModel)v.DataContext).NameModel).siteZone != null)
                {
                    nameZone.Add(Employer_View.unityObjects.Find(u => u.unityObjectId == ((OITModel)v.DataContext).NameModel).siteZone.title);
                    string nameZone1 = Employer_View.unityObjects.Find(u => u.unityObjectId == ((OITModel)v.DataContext).NameModel).siteZone.title;
                    index.Add(OITDemoViewModel.Collection.Find(u => u.nameZone == nameZone1).indexZon);
                }
            }
            string answer = await Employer_View.CreateZon(color.Substring(3), title, description, id, unityobject.ToArray());
            int y = 0;
            while (answer.ToUpper() != "OK" && y <= 5)
            {
                await Timer();
                answer = await Employer_View.CreateZon(color.Substring(3), title, description, id, unityobject.ToArray());
                y += 1;
            }
            if (answer.ToUpper() == "OK")
            {
                answer = await Employer_View.LoadZonNameAsync(id, unityobject.ToArray());
                foreach (GeometryModel3D v in selectedElement)
                {
                    Employer_View.unityObjects.Find(u => u.unityObjectId == ((OITModel)v.DataContext).NameModel).siteZone = Employer_View.Zon.Find(u => u.id == id);
                }
                y = 0;
                while (answer.ToUpper() != "OK" && y <= 5)
                {
                    await Timer();
                    answer = await Employer_View.LoadZonNameAsync(id, unityobject.ToArray());
                    y += 1;
                }
                if (answer.ToUpper() == "OK")
                {
                    List<string> nameuniq = nameZone.Distinct().ToList();
                    foreach (string v in nameuniq)
                    {
                        if (Employer_View.Zon.Find(u => u.title == v).unityObjects.Length == nameZone.FindAll(u => u == v).Count)
                        {
                            string namedeleateZon = Employer_View.Zon.Find(u => u.title == v).title;
                            string iddeleateZon = Employer_View.Zon.Find(u => u.title == v).id;
                            answer = await Employer_View.DeleateZon(iddeleateZon);
                            y = 0;
                            while (answer.ToUpper() != "OK" && y <= 5)
                            {
                                await Timer();
                                answer = await Employer_View.DeleateZon(iddeleateZon);
                                y += 1;
                            }
                            if (answer.ToUpper() == "OK")
                            {
                                Employer_View.Zon.Remove(Employer_View.Zon.Find(u => u.id == iddeleateZon));
                                OITDemoViewModel.ModelGeometry.RemoveAt(OITDemoViewModel.Collection.Find(u => u.nameZone == namedeleateZon).indexZon);
                                OITDemoViewModel.Collection.Remove(OITDemoViewModel.Collection.Find(u => u.nameZone == namedeleateZon));
                                foreach (GeometryModel3D h in selectedElement)
                                {
                                    Employer_View.unityObjects.Find(t => t.unityObjectId == ((OITModel)h.DataContext).NameModel).siteZone = Employer_View.Zon.Find(u => u.title == title);
                                }
                            }
                        }
                        else
                        {
                            ZonDataBase modifyzon = Employer_View.Zon.Find(u => u.title == v);
                            List<string> unityobjectput = new List<string>();
                            unityobjectput = modifyzon.unityObjects.ToList();
                            foreach (GeometryModel3D h in selectedElement)
                            {
                                Employer_View.unityObjects.Find(t => t.unityObjectId == ((OITModel)h.DataContext).NameModel).siteZone = Employer_View.Zon.Find(u => u.title == title);
                                if (unityobjectput.Find(u => u == Employer_View.unityObjects.Find(t => t.unityObjectId == ((OITModel)h.DataContext).NameModel).id) != null)
                                {
                                    unityobjectput.Remove(unityobjectput.Find(u => u == Employer_View.unityObjects.Find(t => t.unityObjectId == ((OITModel)h.DataContext).NameModel).id));
                                }
                            }
                            answer = await Employer_View.PutZon(modifyzon.color, modifyzon.title, modifyzon.description, modifyzon.id, unityobjectput.ToArray());
                            y = 0;
                            while (answer.ToUpper() != "OK" && y <= 5)
                            {
                                await Timer();
                                answer = await Employer_View.PutZon(modifyzon.color, modifyzon.title, modifyzon.description, modifyzon.id, unityobjectput.ToArray());
                                y += 1;
                            }
                            if (answer.ToUpper() == "OK")
                            {
                                OITDemoViewModel.Collection.Find(u => u.nameZone == v).nameUnityObject = Employer_View.Zon.Find(u => u.title == v).unityObjects.ToList();
                                List<string> Unityobject = modifyzon.unityObjects.ToList();
                                foreach (string h in Unityobject)
                                {
                                    if (selectedElement.Find(u => Employer_View.unityObjects.Find(t => t.unityObjectId == ((OITModel)u.DataContext).NameModel).id == h) != null)
                                    {
                                        int idexremove = ((OITModel)selectedElement.Find(u => Employer_View.unityObjects.Find(t => t.unityObjectId == ((OITModel)u.DataContext).NameModel).id == h).DataContext).Indexposition;
                                        List<int> indexmass = ((OITModel)selectedElement.Find(u => Employer_View.unityObjects.Find(t => t.unityObjectId == ((OITModel)u.DataContext).NameModel).id == h).DataContext).IndexMass;
                                        foreach (int t in indexmass)
                                        {
                                            if (t != idexremove)
                                            {
                                                OITDemoViewModel.ModelGeometryZons[t].IndexMass.Remove(OITDemoViewModel.ModelGeometryZons[t].IndexMass.Find(u => u == idexremove));
                                            }
                                        }
                                    }
                                }
                                modifyzon.unityObjects = unityobjectput.ToArray();
                                answer = await Employer_View.LoadZonNameUpdateAsync(modifyzon.id);
                                y = 0;
                                while (answer.ToUpper() != "OK" && y <= 5)
                                {
                                    await Timer();
                                    answer = await Employer_View.LoadZonNameUpdateAsync(modifyzon.id);
                                    y += 1;
                                }
                                if (answer.ToUpper() == "OK")
                                {
                                    answer = await OITDemoViewModel.ModifyZon(OITDemoViewModel.Collection.Find(u => u.nameZone == modifyzon.title).indexZon, modifyzon);
                                    y = 0;
                                    while (answer.ToUpper() != "OK" && y <= 5)
                                    {
                                        await Timer();
                                        answer = await OITDemoViewModel.ModifyZon(OITDemoViewModel.Collection.Find(u => u.nameZone == modifyzon.title).indexZon, modifyzon);
                                        y += 1;
                                    }
                                }
                            }
                        }
                    }
                    if (answer.ToUpper() == "OK")
                    {
                        answer = await Employer_View.PutZon("#" + color.Substring(3), title, description, id, unityobject.ToArray());
                        y = 0;
                        while (answer.ToUpper() != "OK" && y <= 5)
                        {
                            await Timer();
                            answer = await Employer_View.PutZon("#" + color.Substring(3), title, description, id, unityobject.ToArray());
                            y += 1;
                        }
                        if (answer.ToUpper() == "OK")
                        {
                            answer = await OITDemoViewModel.CreateZone(title, selectedElement, Employer_View.Zon.Find(u => u.title == title).color);
                            NoSelection();
                            await DescriptionDatasource.UpdateDataAsync();
                            LoadingControl.IsLoading = false;
                        }
                    }
                }
                //index = index.Distinct().ToList();
                //foreach (int v in index)
                //{
                //    OITDemoViewModel.ModelGeometry.RemoveAt(v);
                //}
            }
        }

        private async System.Threading.Tasks.Task Apply_UpdateAsync()
        {
            string color = ColorPickerZon.Color.ToString();
            string title = Employer_View.unityObjects.Find(u => u.unityObjectId == ((OITModel)selectedElementstartposition[0].DataContext).NameModel).siteZone.title;
            string description = Name.Text;
            string id = Employer_View.unityObjects.Find(u => u.unityObjectId == ((OITModel)selectedElementstartposition[0].DataContext).NameModel).siteZone.id;
            List<string> Unityobjectbew = new List<string>();
            IEnumerable<GeometryModel3D> addunityobject = new List<GeometryModel3D>();
            IEnumerable<GeometryModel3D> removeunityobject = new List<GeometryModel3D>();
            addunityobject = selectedElement.Except(selectedElementstartposition);
            removeunityobject = selectedElementstartposition.Except(selectedElement);
            List<string> nameZone = new List<string>();
            string answer = "";
            int y = 0;

            foreach (GeometryModel3D v in removeunityobject)
            {
                Employer_View.unityObjects.Find(u => u.unityObjectId == ((OITModel)v.DataContext).NameModel).siteZone = null;
                ((PhongMaterial)((OITModel)v.DataContext).Material).DiffuseColor = SharpDX.Color.White;
                ((PhongMaterial)((OITModel)v.DataContext).Material).EmissiveColor = SharpDX.Color.White;
                ((OITModel)v.DataContext).IndexMass.Clear();
            }

            foreach (GeometryModel3D v in addunityobject)
            {
                if (Employer_View.unityObjects.Find(u => u.unityObjectId == ((OITModel)v.DataContext).NameModel).siteZone != null)
                {
                    nameZone.Add(Employer_View.unityObjects.Find(u => u.unityObjectId == ((OITModel)v.DataContext).NameModel).siteZone.title);
                }
            }
            foreach (GeometryModel3D h in selectedElement)
            {
                Employer_View.unityObjects.Find(t => t.unityObjectId == ((OITModel)h.DataContext).NameModel).siteZone = Employer_View.Zon.Find(u => u.title == title);
                Unityobjectbew.Add(Employer_View.unityObjects.Find(u => u.unityObjectId == ((OITModel)h.DataContext).NameModel).id);
            }
            List<string> nameuniq = nameZone.Distinct().ToList();
            foreach (string v in nameuniq)
            {
                if (Employer_View.Zon.Find(u => u.title == v).unityObjects.Length == nameZone.FindAll(u => u == v).Count)
                {
                    string namedeleateZon = Employer_View.Zon.Find(u => u.title == v).title;
                    string iddeleateZon = Employer_View.Zon.Find(u => u.title == v).id;
                    answer = await Employer_View.DeleateZon(iddeleateZon);
                    y = 0;
                    while (answer.ToUpper() != "OK" && y <= 5)
                    {
                        await Timer();
                        answer = await Employer_View.DeleateZon(iddeleateZon);
                        y += 1;
                    }
                    if (answer.ToUpper() == "OK")
                    {
                        Employer_View.Zon.Remove(Employer_View.Zon.Find(u => u.id == iddeleateZon));
                        OITDemoViewModel.ModelGeometry.RemoveAt(OITDemoViewModel.Collection.Find(u => u.nameZone == namedeleateZon).indexZon);
                        OITDemoViewModel.Collection.Remove(OITDemoViewModel.Collection.Find(u => u.nameZone == namedeleateZon));
                    }
                }
                else
                {
                    ZonDataBase modifyzon = Employer_View.Zon.Find(u => u.title == v);
                    List<string> unityobjectput = new List<string>();
                    unityobjectput = modifyzon.unityObjects.ToList();
                    foreach (GeometryModel3D h in selectedElement)
                    {
                        if (unityobjectput.Find(u => u == Employer_View.unityObjects.Find(t => t.unityObjectId == ((OITModel)h.DataContext).NameModel).id) != null)
                        {
                            unityobjectput.Remove(unityobjectput.Find(u => u == Employer_View.unityObjects.Find(t => t.unityObjectId == ((OITModel)h.DataContext).NameModel).id));
                        }
                    }
                    answer = await Employer_View.PutZon(modifyzon.color, modifyzon.title, modifyzon.description, modifyzon.id, unityobjectput.ToArray());
                    y = 0;
                    while (answer.ToUpper() != "OK" && y <= 5)
                    {
                        await Timer();
                        answer = await Employer_View.PutZon(modifyzon.color, modifyzon.title, modifyzon.description, modifyzon.id, unityobjectput.ToArray());
                        y += 1;
                    }
                    if (answer.ToUpper() == "OK")
                    {
                        OITDemoViewModel.Collection.Find(u => u.nameZone == v).nameUnityObject = unityobjectput;
                        List<string> Unityobject = modifyzon.unityObjects.ToList();
                        foreach (string h in Unityobject)
                        {
                            if (selectedElement.Find(u => Employer_View.unityObjects.Find(t => t.unityObjectId == ((OITModel)u.DataContext).NameModel).id == h) != null)
                            {
                                int idexremove = ((OITModel)selectedElement.Find(u => Employer_View.unityObjects.Find(t => t.unityObjectId == ((OITModel)u.DataContext).NameModel).id == h).DataContext).Indexposition;
                                List<int> indexmass = ((OITModel)selectedElement.Find(u => Employer_View.unityObjects.Find(t => t.unityObjectId == ((OITModel)u.DataContext).NameModel).id == h).DataContext).IndexMass;
                                foreach (int t in indexmass)
                                {
                                    if (t != idexremove)
                                    {
                                        OITDemoViewModel.ModelGeometryZons[t].IndexMass.Remove(OITDemoViewModel.ModelGeometryZons[t].IndexMass.Find(u => u == idexremove));
                                    }
                                }
                            }
                        }
                        modifyzon.unityObjects = unityobjectput.ToArray();
                        answer = await Employer_View.LoadZonNameUpdateAsync(modifyzon.id);
                        y = 0;
                        while (answer.ToUpper() != "OK" && y <= 5)
                        {
                            await Timer();
                            answer = await Employer_View.LoadZonNameUpdateAsync(modifyzon.id);
                            y += 1;
                        }
                        if (answer.ToUpper() == "OK")
                        {
                            answer = await OITDemoViewModel.ModifyZon(OITDemoViewModel.Collection.Find(u => u.nameZone == modifyzon.title).indexZon, modifyzon);
                            y = 0;
                            while (answer.ToUpper() != "OK" && y <= 5)
                            {
                                await Timer();
                                answer = await OITDemoViewModel.ModifyZon(OITDemoViewModel.Collection.Find(u => u.nameZone == modifyzon.title).indexZon, modifyzon);
                                y += 1;
                            }
                        }
                    }
                }
            }
            if (answer.ToUpper() == "OK" || nameuniq.Count == 0)
            {
                // Написать код модификации зоны. Проверить работоспособность обновления зоны. Все индексы должны корректно обновиться.
                answer = await Employer_View.PutZon("#" + color.Substring(3), title, description, id, Unityobjectbew.ToArray());
                y = 0;
                while (answer.ToUpper() != "OK" && y <= 5)
                {
                    await Timer();
                    answer = await Employer_View.PutZon("#" + color.Substring(3), title, description, id, Unityobjectbew.ToArray());
                    y += 1;
                }
                if (answer.ToUpper() == "OK")
                {
                    answer = await Employer_View.LoadZonNameUpdateAsync(id);
                    y = 0;
                    while (answer.ToUpper() != "OK" && y <= 5)
                    {
                        await Timer();
                        answer = await Employer_View.LoadZonNameUpdateAsync(id);
                        y += 1;
                    }
                    if (answer.ToUpper() == "OK")
                    {
                        answer = await OITDemoViewModel.UpdateZon(title, selectedElement, Employer_View.Zon.Find(u => u.title == title).color, OITDemoViewModel.Collection.Find(u => u.nameZone == title).indexZon);
                        y = 0;
                        while (answer.ToUpper() != "OK" && y <= 5)
                        {
                            await Timer();
                            answer = await OITDemoViewModel.UpdateZon(title, selectedElement, Employer_View.Zon.Find(u => u.title == title).color, OITDemoViewModel.Collection.Find(u => u.nameZone == title).indexZon);
                            y += 1;
                        }
                        NoSelection();
                        await DescriptionDatasource.UpdateDataAsync();
                        LoadingControl.IsLoading = false;
                    }
                }
            }
        }

        private void  NoSelection()
        {
            if(selectedElementstartposition.Count > 0)
            {
                selectedElementstartposition.Clear();
            }
            if(selectedElement.Count > 0)
            {
                if (dataGrid.SelectedItem != null)
                {
                    dataGrid.SelectedItem = null;
                }
                selectedElement.ForEach(ModifyList);
                selectedElement.Clear();
            }
        }

        private void Enter_Click(object sender, RoutedEventArgs e)
        {
            if (((TextBlock)((Viewbox)Enter.Content).Child).Text == "Войти")
            {
                if (Login.Text == "admin" && Password.Password == "admin")
                {
                    Login.Visibility = Visibility.Collapsed;
                    Password.Visibility = Visibility.Collapsed;
                    NameUser.Text = Login.Text;
                    EnterPage.Visibility = Visibility.Visible;
                    ButtonModify.Visibility = Visibility.Visible;
                    ((TextBlock)((Viewbox)Enter.Content).Child).Text = "Выйти";
                    Login.Text = "";
                    Password.Password = "";
                }
                else
                {
                    Login.Text = "";
                    Password.Password = "";
                }
            }
            else
            {
                statusdatagrid = false;
                Bloc_Button(false);
                NoSelection();
                Login.Visibility = Visibility.Visible;
                Password.Visibility = Visibility.Visible;
                NameUser.Text = "";
                switch (state)
                {
                    case 1:
                        Create.Style = this.Resources["Button_Zoning_normal"] as Style;
                        break;
                    case 2:
                        Modify.Style = this.Resources["Button_Zoning_normal"] as Style;
                        break;
                    case 3:
                        Deleate.Style = this.Resources["Button_Zoning_normal"] as Style;
                        break;
                }
                state = 0;
                EnterPage.Visibility = Visibility.Collapsed;
                ButtonModify.Visibility = Visibility.Collapsed;
                PanelInfo.Visibility = Visibility.Collapsed;
                ((TextBlock)((Viewbox)Enter.Content).Child).Text = "Войти";
            }
        }
    }
}
