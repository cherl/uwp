﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading.Tasks;
using Vagrant_V1.Data;
using Vagrant_V1.Server;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Storage;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// Документацию по шаблону элемента "Пустая страница" см. по адресу https://go.microsoft.com/fwlink/?LinkId=234238

namespace Vagrant_V1.Pageees
{
    /// <summary>
    /// Пустая страница, которую можно использовать саму по себе или для перехода внутри фрейма.
    /// </summary>
    public sealed partial class EnterPage : Page
    {
        public float loadingprogress = 0;

        public EnterPage()
        {
            this.InitializeComponent();
            Employer_View.Simulate = "true";
            Simylate.IsChecked = true;
            _ = Create_FolderAsync();
            //Employer_View.GenerateUnityObjectAsync();
            //Employer_View.GenerateZonAsync();
        }

        public async Task Initialization()
        {
            Employer_View database = new Employer_View();
            await database.Employer_Viewr();
            LoadingControl.IsLoading = false;
            Loadstatus.Visibility = Visibility.Collapsed;
            Frame.Navigate(typeof(framePage), "NOENABLE");
        }

        private async Task Create_FolderAsync()
        {
            StorageFolder currentPath = await Windows.ApplicationModel.Package.Current.InstalledLocation.GetFolderAsync("Setting");
            StorageFile file = await currentPath.GetFileAsync("Setting.txt");
            string text = await FileIO.ReadTextAsync(file);
            string[] words = text.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
            for (int i =0; i < words.Length; i++)
            {
                if(words[i] == "URl")
                {
                    Employer_View.URl_start = words[i + 1];
                }
            }
        }

        private void Enter(object sender, RoutedEventArgs e)
        {
            if (Login.Text == "admin" && Password.Password == "admin")
            {
                if(((Button)sender).FocusState != FocusState.Unfocused)
                {
                    ((Button)sender).UseSystemFocusVisuals = false;
                }
                Login.Text = "";
                Password.Password = "";
                Loadstatus.Visibility = Visibility.Visible;
                LoadingControl.IsLoading = true;
                Initialization();
            }
            else
            {
                Login.Text = "";
                Password.Password = "";
            }
        }


        private void Simylate_Click(object sender, RoutedEventArgs e)
        {
            if(Simylate.IsChecked == false)
            {
                Simylate.IsChecked = true;
            }
            Employer_View.Simulate = "true";
            Real.IsChecked = false;
        }

        private void Real_Click(object sender, RoutedEventArgs e)
        {
            if (Real.IsChecked == false)
            {
                Real.IsChecked = true;
            }
            Employer_View.Simulate = "false";
            Simylate.IsChecked = false;
        }
    }
}
