﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.System.Threading;
using Windows.UI.Core;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// Документацию по шаблону элемента "Пустая страница" см. по адресу https://go.microsoft.com/fwlink/?LinkId=234238

namespace Vagrant_V1.Pageees
{
    /// <summary>
    /// Пустая страница, которую можно использовать саму по себе или для перехода внутри фрейма.
    /// </summary>
    public sealed partial class framePage : Page
    {

        private object mainactive = null;
        public static bool flagRename = false;
        public static framePage framestart;
        public static int numberpage = 0;
        public static double Ycoordinate = 0;
        public static double Xcoordinate = 0;
        public static double lastYcoordinate = 0;
        public static double lastXcoordinate = 0;

        public framePage()
        {
            this.InitializeComponent();
            framestart = this;
            myFrame.Navigate(typeof(MainPage));
            mainactive = Control;
            CLick_Button((Button)mainactive);
        }

        private void CLick_Button(Button button)
        {
            button.Style = this.Resources["Button_up_menu_height"] as Style;
            ((TextBlock)((Viewbox)button.Content).Child).Style = this.Resources["Textonbutton_height"] as Style;
        }

        private void CLick_Button_last_state(Button button)
        {
            button.Style = this.Resources["Button_up_menu_normal"] as Style;
            ((TextBlock)((Viewbox)button.Content).Child).Style = this.Resources["Textonbutton_normal"] as Style;
        }


        private void Control_Click(object sender, RoutedEventArgs e)
        {
            if (numberpage != 6)
            {
                if (mainactive != e.OriginalSource)
                {
                    CLick_Button_last_state((Button)mainactive);
                    mainactive = e.OriginalSource;
                    CLick_Button((Button)mainactive);
                    numberpage = 0;
                    Wait(numberpage);
                    flagRename = false;
                }
                else if (MainPage.flag3D != false)
                {
                    numberpage = 0;
                    Wait(numberpage);
                    MainPage.flag3D = false;
                    flagRename = false;
                }
            }
        }


        private async void NewPage(int number)
        {
            await this.Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () => // Перенаправляем поток
            {
                switch (number)
                {
                    case 0:
                        myFrame.Navigate(typeof(MainPage));
                        Loadstatus.Visibility = Visibility.Collapsed;
                        LoadingControl.IsLoading = false;
                        break;
                    case 1:
                        myFrame.Navigate(typeof(AdministrationPage));
                        Loadstatus.Visibility = Visibility.Collapsed;
                        LoadingControl.IsLoading = false;
                        break;
                    case 2:
                        myFrame.Navigate(typeof(StaticPage));
                        Loadstatus.Visibility = Visibility.Collapsed;
                        LoadingControl.IsLoading = false;
                        break;
                    case 3:
                        myFrame.Navigate(typeof(ZoningPage));
                        Loadstatus.Visibility = Visibility.Collapsed;
                        LoadingControl.IsLoading = false;
                        break;
                    case 4:
                        myFrame.Navigate(typeof(InfoPage));
                        Loadstatus.Visibility = Visibility.Collapsed;
                        LoadingControl.IsLoading = false;
                        break;
                    case 5:
                        Frame.Navigate(typeof(ThreeD));
                        Loadstatus.Visibility = Visibility.Collapsed;
                        LoadingControl.IsLoading = false;
                        break;
                    case 6:
                        Frame.GoBack();
                        Loadstatus.Visibility = Visibility.Collapsed;
                        LoadingControl.IsLoading = false;
                        break;
                }
            });
        }



        public void Wait(int number)
        {
            if(numberpage != 6)
            {
                Loadstatus.Visibility = Visibility.Visible;
                LoadingControl.IsLoading = true;
                ThreadPoolTimer DelayTimer = ThreadPoolTimer.CreateTimer(
                async (source) =>
                {
                    NewPage(number);

                }, TimeSpan.FromMilliseconds(10));
            }
        }

        private void Administration_Click(object sender, RoutedEventArgs e)
        {
            if (numberpage != 6)
            {
                CLick_Button_last_state((Button)mainactive);
                mainactive = e.OriginalSource;
                CLick_Button((Button)mainactive);
                numberpage = 1;
                Wait(numberpage);
                flagRename = true;
            }
        }

        private void Statistic_Click(object sender, RoutedEventArgs e)
        {
            if (numberpage != 6)
            {
                CLick_Button_last_state((Button)mainactive);
                mainactive = e.OriginalSource;
                CLick_Button((Button)mainactive);
                flagRename = false;
                numberpage = 2;
                Wait(numberpage);
            }
        }

        private void level_assest_Click(object sender, RoutedEventArgs e)
        {
            if (numberpage != 6)
            {
                if (mainactive != e.OriginalSource)
                {
                    CLick_Button_last_state((Button)mainactive);
                    mainactive = e.OriginalSource;
                    CLick_Button((Button)mainactive);
                    numberpage = 3;
                    Wait(numberpage);
                    flagRename = false;
                }
            }
        }

        private void Info_Click(object sender, RoutedEventArgs e)
        {
            if (numberpage != 6)
            {
                CLick_Button_last_state((Button)mainactive);
                mainactive = e.OriginalSource;
                CLick_Button((Button)mainactive);
                flagRename = false;
                numberpage = 4;
                Wait(numberpage);
            }
        }

        private void Grid_KeyDown(object sender, KeyRoutedEventArgs e)
        {   
            switch (numberpage)
            {
                case 0:
                    MainPage.KeyDownOnpage(e);
                    break;
                case 1:
                    break;
                case 2:
                    break;
                case 3:
                    break;
                case 4:
                    break;
                case 5:
                    ThreeD.KeyDownOnpage(e);
                    break;
                case 6:
                    StatisticModel.KeyDownOnpage(e);
                    break;
            }
        }


        private void Tapped(object sender, TappedRoutedEventArgs e)
        {
            //((Windows.Devices.Input.PointerDeviceType)(e.PointerDeviceType)).GetType().
            switch (numberpage)
            {
                case 0:
                    break;
                case 1:
                    break;
                case 2:
                    break;
                case 3:
                    break;
                case 4:
                    break;
                case 5:
                    break;
                case 6:
                    break;
            }
        }

        private void MouseMove(object sender, PointerRoutedEventArgs e)
        {

            //switch (numberpage)
            //{
            //    case 0:
            //        break;
            //    case 1:
            //        break;
            //    case 2:
            //        break;
            //    case 3:
            //        break;
            //    case 4:
            //        break;
            //    case 5:
            //        if (e.Pointer.PointerDeviceType == Windows.Devices.Input.PointerDeviceType.Mouse)
            //        {
            //            var properties = e.GetCurrentPoint(this).Properties;
            //            if(!properties.IsLeftButtonPressed && !properties.IsRightButtonPressed)
            //            {
            //                lastYcoordinate = properties.ContactRect.Y;
            //                lastXcoordinate = properties.ContactRect.X;
            //            }
            //            if (properties.IsLeftButtonPressed)
            //            {
            //                Ycoordinate = properties.ContactRect.Y;
            //                if(Ycoordinate > lastYcoordinate)
            //                {
            //                    //Вращение объекта к 2D виду
            //                }
            //                else if(Ycoordinate < lastYcoordinate)
            //                {
            //                    //Вращение объекта от 2D вида
            //                }
            //                lastYcoordinate = properties.ContactRect.Y;
            //            }
            //            else if (properties.IsRightButtonPressed)
            //            {
            //                Xcoordinate = properties.ContactRect.X;
            //                lastXcoordinate = properties.ContactRect.X;
            //            }
            //        }
            //        break;
            //}
        }
    }
}
