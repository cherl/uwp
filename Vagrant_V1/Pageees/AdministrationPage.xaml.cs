﻿using HelixToolkit.UWP;
using Microsoft.Toolkit.Uwp.UI.Controls;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading;
using System.Threading.Tasks;
using Vagrant_V1.Data;
using Vagrant_V1.Modeling3d._2D;
using Vagrant_V1.Pageees;
using Vagrant_V1.Server;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.System.Threading;
using Windows.UI.Core;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using SharpDX;
using Windows.Devices.Input;
using System.Collections.ObjectModel;

// Документацию по шаблону элемента "Пустая страница" см. по адресу https://go.microsoft.com/fwlink/?LinkId=234238

namespace Vagrant_V1.Pageees
{
    /// <summary>
    /// Пустая страница, которую можно использовать саму по себе или для перехода внутри фрейма.
    /// </summary>

    public sealed partial class AdministrationPage : Page
    {
        private static DataGridDataSourceadmin viewModeladmin = new DataGridDataSourceadmin();
        public string name = "";
        private static SelectionChangedEventArgs element;
        private static bool Answer = false;
        private static DataGridComboBoxColumn ColumnTag;
        public static ContentDialog AnswerUpdate = new ContentDialog()
        {
            Title = "Принять изменения?",
            PrimaryButtonText = "Да",
            CloseButtonText = "Нет"

        };


        public AdministrationPage()
        {
            this.InitializeComponent();
            NavigationCacheMode = NavigationCacheMode.Enabled;
            OnXamlRendered();
        }


        private void AutoSuggestBox_TextChanged(AutoSuggestBox sender, AutoSuggestBoxTextChangedEventArgs args)
        {
            if (args.Reason == AutoSuggestionBoxTextChangeReason.UserInput)
            {
                List<string> name = DataGridDataSource.name2.FindAll(u => u.StartsWith(Find.Text));
                sender.ItemsSource = name;
            }
        }


        private void AutoSuggestBox_SuggestionChosen(AutoSuggestBox sender, AutoSuggestBoxSuggestionChosenEventArgs args)
        {
            //Set sender.Text. You can use args.SelectedItem to build your text string.
        }


        private void AutoSuggestBox_QuerySubmitted(AutoSuggestBox sender, AutoSuggestBoxQuerySubmittedEventArgs args)
        {
            foreach (DataGridDataItem v in ((ObservableCollection<DataGridDataItem>)dataGridAdmin.ItemsSource))
            {
                if (v.FIO == Find.Text || v.Id_Employer == Find.Text || v.Tagg == Find.Text)
                {
                    dataGridAdmin.SelectedItem = v;
                    dataGridAdmin.ScrollIntoView(dataGridAdmin.SelectedItem, dataGridAdmin.Columns[0]);
                }
            }
        }

        public async void OnXamlRendered()
        {
            if (dataGridAdmin != null)
            {
                dataGridAdmin.Sorting -= DataGrid_Sorting;
                dataGridAdmin.LoadingRowGroup -= DataGrid_LoadingRowGroup;
            }

            if (dataGridAdmin != null)
            {
                dataGridAdmin.Sorting += DataGrid_Sorting;
                dataGridAdmin.LoadingRowGroup += DataGrid_LoadingRowGroup;
                if (dataGridAdmin.ItemsSource == null)
                {
                    dataGridAdmin.ItemsSource = await viewModeladmin.GetDataAsync();
                }
                else
                {
                    dataGridAdmin.ItemsSource = await viewModeladmin.GetDataAsync();
                }
                var comboBoxColumn = dataGridAdmin.Columns.FirstOrDefault(x => x.Tag.Equals("Id_Employer")) as DataGridComboBoxColumn;
                if (comboBoxColumn != null)
                {
                    comboBoxColumn.ItemsSource = await viewModeladmin.GetFIO();
                }

                var comboBoxColumnTag = dataGridAdmin.Columns.FirstOrDefault(x => x.Tag.Equals("Tagg")) as DataGridComboBoxColumn;
                ColumnTag = comboBoxColumnTag;
                if (comboBoxColumnTag != null)
                {
                    comboBoxColumnTag.ItemsSource = await viewModeladmin.GetTag();
                }
            }
        }
            private void DataGrid_LoadingRowGroup(object sender, DataGridRowGroupHeaderEventArgs e)
            {
                ICollectionViewGroup group = e.RowGroupHeader.CollectionViewGroup;
                DataGridDataItem item = group.GroupItems[0] as DataGridDataItem;
                e.RowGroupHeader.PropertyValue = item.Team.ToString();
            }

            private void GroupButton_Click(object sender, RoutedEventArgs e)
            {
                if (dataGridAdmin != null)
                {
                dataGridAdmin.ItemsSource = viewModeladmin.GroupData().View;
                }
            }

            private void DataGrid_Sorting(object sender, DataGridColumnEventArgs e)
            {
                // Clear previous sorted column if we start sorting a different column
                string previousSortedColumn = viewModeladmin.CachedSortedColumn;
                if (previousSortedColumn != string.Empty)
                {
                    foreach (DataGridColumn dataGridColumn in dataGridAdmin.Columns)
                    {
                        if (dataGridColumn.Tag != null && dataGridColumn.Tag.ToString() == previousSortedColumn &&
                            (e.Column.Tag == null || previousSortedColumn != e.Column.Tag.ToString()))
                        {
                            dataGridColumn.SortDirection = null;
                        }
                    }
                }

                // Toggle clicked column's sorting method
                if (e.Column.Tag != null)
                {
                    if (e.Column.SortDirection == null)
                    {
                    dataGridAdmin.ItemsSource = viewModeladmin.SortData(e.Column.Tag.ToString(), true, name);
                        e.Column.SortDirection = DataGridSortDirection.Ascending;
                    }
                    else if (e.Column.SortDirection == DataGridSortDirection.Ascending)
                    {
                    dataGridAdmin.ItemsSource = viewModeladmin.SortData(e.Column.Tag.ToString(), false, name);
                        e.Column.SortDirection = DataGridSortDirection.Descending;
                    }
                    else
                    {
                        e.Column.SortDirection = null;
                    viewModeladmin.CachedSortedColumn = "";
                    DataGridDataSourceadmin._cachedSortedColumnstatus = "";

                    }
                }
            }
        public static async void Update_DataAsync(string data, string lastdata)
        {
            string answer = "";
            int y = 0;
            if (Answer == false)
            {
                ContentDialogResult result = await AnswerUpdate.ShowAsync();
                {
                    if (result == ContentDialogResult.Primary)
                    {
                       if(lastdata != "")
                       {
                            if(data == "")
                            {
                                answer = await Employer_View.ClearTag(Employer_View.Tag.Find(u => u.title == lastdata), "");
                                y = 0;
                                while (answer.ToUpper() != "OK" && y <= 5)
                                {
                                    answer = await Employer_View.ClearTag(Employer_View.Tag.Find(u => u.title == lastdata), "");
                                    y += 1;
                                }
                                if (answer.ToUpper() == "OK")
                                {
                                    Employer_View.EmployerDataBase.Find(u => u.title == ((DataGridDataItem)element.AddedItems[0]).FIO).tag = null;
                                    ColumnTag.ItemsSource = await DataGridDataSourceadmin.Update();
                                    Employer_View.Tag.Find(u => u.id == Employer_View.Tag.Find(t => t.title == lastdata).id).employee = "";
                                    MainPage.grid.ItemsSource = await DataGridDataSource.UpdateTag();                                
                                }
                            }
                            else
                            {
                                answer = await Employer_View.ClearTag(Employer_View.Tag.Find(u => u.title == lastdata), "");
                                y = 0;
                                while (answer.ToUpper() != "OK" && y <= 5)
                                {
                                    answer = await Employer_View.ClearTag(Employer_View.Tag.Find(u => u.title == lastdata), "");
                                    y += 1;
                                }
                                if (answer.ToUpper() == "OK")
                                {
                                    answer = await Employer_View.ClearTag(Employer_View.Tag.Find(u => u.title == data), Employer_View.EmployerDataBase.Find(u => u.title == ((DataGridDataItem)element.AddedItems[0]).FIO).id);
                                }
                                y = 0;
                                while (answer.ToUpper() != "OK" && y <= 5)
                                {
                                    answer = await Employer_View.ClearTag(Employer_View.Tag.Find(u => u.title == data), Employer_View.EmployerDataBase.Find(u => u.title == ((DataGridDataItem)element.AddedItems[0]).FIO).id);
                                    y += 1;
                                }
                                Employer_View.EmployerDataBase.Find(u => u.title == ((DataGridDataItem)element.AddedItems[0]).FIO).tag = Employer_View.Tag.Find(u => u.title == data);
                                ColumnTag.ItemsSource = await DataGridDataSourceadmin.Update();
                                MainPage.grid.ItemsSource = await DataGridDataSource.UpdateTag();
                            }
                        }
                       else
                       {
                            answer = await Employer_View.ClearTag(Employer_View.Tag.Find(u => u.title == data), Employer_View.EmployerDataBase.Find(u => u.title == ((DataGridDataItem)element.AddedItems[0]).FIO).id);
                            y = 0;
                            while (answer.ToUpper() != "OK" && y <= 5)
                            {
                                answer = await Employer_View.ClearTag(Employer_View.Tag.Find(u => u.title == data), Employer_View.EmployerDataBase.Find(u => u.title == ((DataGridDataItem)element.AddedItems[0]).FIO).id);
                                y += 1;
                            }
                            Employer_View.EmployerDataBase.Find(u => u.title == ((DataGridDataItem)element.AddedItems[0]).FIO).tag = Employer_View.Tag.Find(u => u.title == data);
                            ColumnTag.ItemsSource = await DataGridDataSourceadmin.Update();
                            MainPage.grid.ItemsSource = await DataGridDataSource.UpdateTag();
                        }
                    }
                    else
                    {
                        Answer = true;
                        ((DataGridDataItem)element.AddedItems[0]).Tagg = lastdata;
                    }
                }
            }
            else
            {
                Answer = false;
            }
        }


        private void Changed(object sender, SelectionChangedEventArgs e)
        {
            element = e;
        }
    }
}
