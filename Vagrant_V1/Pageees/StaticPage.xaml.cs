﻿using Microsoft.Toolkit.Uwp.UI.Controls;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Vagrant_V1.Data;
using Vagrant_V1.Server;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Storage;
using Windows.Storage.Pickers;
using Windows.UI;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// Документацию по шаблону элемента "Пустая страница" см. по адресу https://go.microsoft.com/fwlink/?LinkId=234238

namespace Vagrant_V1.Pageees
{
    /// <summary>
    /// Пустая страница, которую можно использовать саму по себе или для перехода внутри фрейма.
    /// </summary>
    public sealed partial class StaticPage : Page
    {
        private static DataGridDatastatistic viewModelstatistic = new DataGridDatastatistic();
        public string name = "";
        private static SelectionChangedEventArgs element;
        private List<CalendarViewDayItem> calendaritem = new List<CalendarViewDayItem>();
        private List<CalendarViewDayItem> view = new List<CalendarViewDayItem>();
        public static DataGrid dstagridnew = new DataGrid();
        public static DateTime start;
        public static DateTime finish;
        public StaticPage()
        {
            this.InitializeComponent();
            NavigationCacheMode = NavigationCacheMode.Enabled;
            OnXamlRendered();
            CalendarDate.MaxDate = DateTime.Today;
            CalendarDate.MinDate = new DateTime(DateTime.Today.Year, DateTime.Today.Month - 6, DateTime.Today.Day);
            CalendarDate.SelectedDates.Add(DateTimeOffset.Now);
            StartDate.Date = DateTimeOffset.Now;
            EndDate.Date = DateTimeOffset.Now;
            dstagridnew = dataGrid;
        }

        private void AutoSuggestBox_TextChanged(AutoSuggestBox sender, AutoSuggestBoxTextChangedEventArgs args)
        {
            if (args.Reason == AutoSuggestionBoxTextChangeReason.UserInput)
            {
                List<string> name = DataGridDataSource.name2.FindAll(u => u.StartsWith(Find.Text));
                sender.ItemsSource = name;
            }
        }


        private void AutoSuggestBox_SuggestionChosen(AutoSuggestBox sender, AutoSuggestBoxSuggestionChosenEventArgs args)
        {
            //Set sender.Text. You can use args.SelectedItem to build your text string.
        }

        public  static void Clear_Data()
        {
            start = DateTime.Today;
            finish = DateTime.Today;
            dstagridnew.SelectedItems.Clear();
        }


        private void AutoSuggestBox_QuerySubmitted(AutoSuggestBox sender, AutoSuggestBoxQuerySubmittedEventArgs args)
        {
            foreach (DataGridDataItem v in ((ObservableCollection<DataGridDataItem>)dataGrid.ItemsSource))
            {
                if (v.FIO == Find.Text || v.Id_Employer == Find.Text || v.Tagg == Find.Text)
                {
                    dataGrid.SelectedItem = v;
                    dataGrid.ScrollIntoView(dataGrid.SelectedItem, dataGrid.Columns[0]);
                }
            }
        }

        public async void OnXamlRendered()
        {
            if (dataGrid != null)
            {
                dataGrid.Sorting -= DataGrid_Sorting;
                dataGrid.LoadingRowGroup -= DataGrid_LoadingRowGroup;
            }

            if (dataGrid != null)
            {
                dataGrid.Sorting += DataGrid_Sorting;
                dataGrid.LoadingRowGroup += DataGrid_LoadingRowGroup;
                if (dataGrid.ItemsSource == null)
                {
                    dataGrid.ItemsSource = await viewModelstatistic.GetDataAsync();
                }
                else
                {
                    dataGrid.ItemsSource = await viewModelstatistic.GetDataAsync();
                }
            }
            List<string> nameColumn = new List<string>();
            for (int i = 3; i < dataGrid.Columns.Count; i++)
            {
                nameColumn.Add(dataGrid.Columns[i].Header.ToString());
            }
            Namecolumn.ItemsSource = nameColumn;
        }
        private void DataGrid_LoadingRowGroup(object sender, DataGridRowGroupHeaderEventArgs e)
        {
            ICollectionViewGroup group = e.RowGroupHeader.CollectionViewGroup;
            DataGridDataItem item = group.GroupItems[0] as DataGridDataItem;
            e.RowGroupHeader.PropertyValue = item.Team.ToString();
        }

        private void GroupButton_Click(object sender, RoutedEventArgs e)
        {
            if (dataGrid != null)
            {
                dataGrid.ItemsSource = viewModelstatistic.GroupData().View;
            }
        }

        private void DataGrid_Sorting(object sender, DataGridColumnEventArgs e)
        {
            // Clear previous sorted column if we start sorting a different column
            string previousSortedColumn = viewModelstatistic.CachedSortedColumn;
            if (previousSortedColumn != string.Empty)
            {
                foreach (DataGridColumn dataGridColumn in dataGrid.Columns)
                {
                    if (dataGridColumn.Tag != null && dataGridColumn.Tag.ToString() == previousSortedColumn &&
                        (e.Column.Tag == null || previousSortedColumn != e.Column.Tag.ToString()))
                    {
                        dataGridColumn.SortDirection = null;
                    }
                }
            }

            // Toggle clicked column's sorting method
            if (e.Column.Tag != null)
            {
                if (e.Column.SortDirection == null)
                {
                    dataGrid.ItemsSource = viewModelstatistic.SortData(e.Column.Tag.ToString(), true, name);
                    e.Column.SortDirection = DataGridSortDirection.Ascending;
                }
                else if (e.Column.SortDirection == DataGridSortDirection.Ascending)
                {
                    dataGrid.ItemsSource = viewModelstatistic.SortData(e.Column.Tag.ToString(), false, name);
                    e.Column.SortDirection = DataGridSortDirection.Descending;
                }
                else
                {
                    e.Column.SortDirection = null;
                    viewModelstatistic.CachedSortedColumn = "";
                    DataGridDatastatistic._cachedSortedColumnstatus = "";

                }
            }
        }

        //private void filter_TextChanged(AutoSuggestBox sender, AutoSuggestBoxTextChangedEventArgs args)
        //{
        //    if (args.Reason == AutoSuggestionBoxTextChangeReason.UserInput)
        //    {
        //        List<string> name = DataGridDatastatistic.filter.FindAll(u => u.StartsWith(filter.Text));
        //        sender.ItemsSource = name;
        //    }
        //}

        //private void filter_QuerySubmitted(AutoSuggestBox sender, AutoSuggestBoxQuerySubmittedEventArgs args)
        //{
        //    dataGrid.ItemsSource = DataGridDatastatistic.FilterData(filter.Text);
        //}

        //private void filter_SuggestionChosen(AutoSuggestBox sender, AutoSuggestBoxSuggestionChosenEventArgs args)
        //{
        //}

        private void DateSelect(CalendarView sender, CalendarViewSelectedDatesChangedEventArgs args)
        {
            if(sender.SelectedDates.Count > 2)
            {
                sender.SelectedDates.Remove(sender.SelectedDates[2]);
            }
            if (sender.SelectedDates.Count == 2)
            {
                DateTime mindate;
                DateTime maxdate;
                if(sender.SelectedDates[0] > sender.SelectedDates[1])
                {
                    mindate = sender.SelectedDates[1].Date;
                    maxdate = sender.SelectedDates[0].Date;
                    if (StartDate.Style == this.Resources["DatePickerStyle1no"] as Style)
                    {
                        StartDate.Style = this.Resources["DatePickerStyle1"] as Style;
                        EndDate.Style = this.Resources["DatePickerStyle1"] as Style;
                    }
                    StartDate.Date = mindate;
                    EndDate.Date = maxdate;
                }
                else
                {
                    mindate = sender.SelectedDates[0].Date;
                    maxdate = sender.SelectedDates[1].Date;
                    if (StartDate.Style == this.Resources["DatePickerStyle1no"] as Style)
                    {
                        StartDate.Style = this.Resources["DatePickerStyle1"] as Style;
                        EndDate.Style = this.Resources["DatePickerStyle1"] as Style;
                    }
                    StartDate.Date = mindate;
                    EndDate.Date = maxdate;
                }
                while(mindate < (maxdate.AddDays(-1)))
                {
                    mindate = mindate.AddDays(1);
                    CalendarViewDayItem dateupdate = calendaritem.Find(u => u.Date.Day == mindate.Day && u.Date.Month == mindate.Month);
                    //List<Windows.UI.Color> densityColors = new List<Windows.UI.Color> 
                    //{
                    //    Colors.AliceBlue,
                    //    Colors.AliceBlue,
                    //    Colors.AliceBlue,
                    //    Colors.AliceBlue,
                    //    Colors.AliceBlue,
                    //    Colors.AliceBlue,
                    //    Colors.AliceBlue,
                    //    Colors.AliceBlue,
                    //    Colors.AliceBlue,
                    //    Colors.AliceBlue

                    //};
                    view.Add(dateupdate);
                    //dateupdate.SetDensityColors(densityColors);
                    dateupdate.Background = new SolidColorBrush(Colors.LightCoral);
                }
            }
            if(sender.SelectedDates.Count == 1)
            {
                //List<Windows.UI.Color> densityColors = new List<Windows.UI.Color>
                //    {
                //        Colors.Transparent,
                //        Colors.Transparent,
                //        Colors.Transparent,
                //        Colors.Transparent,
                //        Colors.Transparent,
                //        Colors.Transparent,
                //        Colors.Transparent,
                //        Colors.Transparent,
                //        Colors.Transparent,
                //        Colors.Transparent

                //    };
                foreach (CalendarViewDayItem v in view)
                {
                    //v.SetDensityColors(densityColors);
                    v.Background = null;
                }
                view.Clear();
                if (StartDate.Style == this.Resources["DatePickerStyle1no"] as Style)
                {
                    StartDate.Style = this.Resources["DatePickerStyle1"] as Style;
                    EndDate.Style = this.Resources["DatePickerStyle1"] as Style;
                }
                StartDate.Date = CalendarDate.SelectedDates[0].Date;
                EndDate.Date = CalendarDate.SelectedDates[0].Date;
            }
            if (sender.SelectedDates.Count == 0)
            {
                StartDate.Style = this.Resources["DatePickerStyle1no"] as Style;
                EndDate.Style = this.Resources["DatePickerStyle1no"] as Style;
            }
        }

        private void ItemChanging(CalendarView sender, CalendarViewDayItemChangingEventArgs args)
        {
            calendaritem.Add(args.Item);
        }

        private void Selection(object sender, SelectionChangedEventArgs e)
        {
            if(Namecolumn.SelectedItem.ToString() == "Должность")
            {
                Parametr.ItemsSource = DataGridDatastatistic.filterPosition;
            }
            else if(Namecolumn.SelectedItem.ToString() == "Отдел")
            {
                Parametr.ItemsSource = DataGridDatastatistic.filterUnit;
            }
            else if (Namecolumn.SelectedItem.ToString() == "Бригада")
            {
                Parametr.ItemsSource = DataGridDatastatistic.filterteam;
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            start = new DateTime(StartDate.Date.Year, StartDate.Date.Month, StartDate.Date.Day, StartTime.Time.Hours, StartTime.Time.Minutes, StartTime.Time.Seconds);
            finish = new DateTime(EndDate.Date.Year, EndDate.Date.Month, EndDate.Date.Day, EndTime.Time.Hours, EndTime.Time.Minutes, EndTime.Time.Seconds);
            if(dataGrid.SelectedItems.Count != 0 && start < finish)
            {
                framePage.numberpage = 6;
                Frame.Navigate(typeof(StatisticModel));
            }
        }

        private void Report_Click(object sender, RoutedEventArgs e)
        {
            Generate_Report();
        }

        private async void Generate_Report()
        {
            //FileSavePicker savePicker = new FileSavePicker();
            //savePicker.SuggestedStartLocation = PickerLocationId.DocumentsLibrary;
            //savePicker.FileTypeChoices.Add("Plain Text", new List<string>() { ".txt" });
            //savePicker.SuggestedFileName = "New Document";
            start = new DateTime(StartDate.Date.Year, StartDate.Date.Month, StartDate.Date.Day, StartTime.Time.Hours, StartTime.Time.Minutes, StartTime.Time.Seconds);
            finish = new DateTime(EndDate.Date.Year, EndDate.Date.Month, EndDate.Date.Day, EndTime.Time.Hours, EndTime.Time.Minutes, EndTime.Time.Seconds);
            if(start < finish && dataGrid.SelectedItems.Count != 0)
            {
                LoadingControl.IsLoading = true;
                FolderPicker folderpick = new Windows.Storage.Pickers.FolderPicker();
                folderpick.SuggestedStartLocation = Windows.Storage.Pickers.PickerLocationId.Desktop;
                folderpick.FileTypeFilter.Add("*");
                //StorageFile folder = await savePicker.PickSaveFileAsync();
                StorageFolder folder = await folderpick.PickSingleFolderAsync();
                if(folder != null)
                {

                    foreach (DataGridDataItem v in dataGrid.SelectedItems)
                    {
                        string answer = await Employer_View.PostReport(start, finish, v.Id_Employer, 5, v.FIO, folder);
                    }
                }
                LoadingControl.IsLoading = false;
            }
        }

        private void Ok_Click(object sender, RoutedEventArgs e)
        {
            if(Namecolumn.SelectedItem != null)
            {
                if(Parametr.SelectedItem != null)
                {
                    DataGridDatastatistic.FilterData(Namecolumn.SelectedItem.ToString(), Parametr.SelectedItem.ToString());
                }
            }
        }

        private void No_Click(object sender, RoutedEventArgs e)
        {
            DataGridDatastatistic.FilterData("Очистить", "");
        }

    }
}
