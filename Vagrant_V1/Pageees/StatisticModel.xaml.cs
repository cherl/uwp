﻿using HelixToolkit.UWP;
using SharpDX;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Vagrant_V1.Data;
using Vagrant_V1.ModelData;
using Vagrant_V1.Modeling3d._2D;
using Vagrant_V1.Server;
using Windows.System.Threading;
using Windows.UI.Core;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// Документацию по шаблону элемента "Пустая страница" см. по адресу https://go.microsoft.com/fwlink/?LinkId=234238

namespace Vagrant_V1.Pageees
{
    /// <summary>
    /// Пустая страница, которую можно использовать саму по себе или для перехода внутри фрейма.
    /// </summary>
    public sealed partial class StatisticModel : Page
    {
        public int level = 5;
        private Button Statelast = null;
        private static Viewport3DX view;
        private static ItemsModel3D item;
        private static float timepreset = 40;
        private float scale = 1;
        private static float rotationSpeed = 1;
        private double Ycoordinate = 0;
        private double Xcoordinate = 0;
        private double lastYcoordinate = 0;
        private double lastXcoordinate = 0;
        private bool flagshowzon = true;
        private static BillboardTextModel3D bilboard1 = new BillboardTextModel3D();
        private List<PositionDataBase> ZonPosition = new List<PositionDataBase>();
        private float timer = 0;
        private float speed = 1;
        private float back = 1;
        private bool flagpause = true;
        private DateTime Tick = new DateTime();
        private static ObservableCollection<TextInfo> last { set; get; } = new ObservableCollection<TextInfo>();
        private List<ZonKol> kol = new List<ZonKol>();
        public StatisticModel()
        {
            OITDemoViewStatistic.Statistic.Clear();
            this.InitializeComponent();
            view = viewport;
            item = ItemsData;
            bilboard1 = Bilboard;
            ItemsData.Transform3D *= global::SharpDX.Matrix.Scaling((float)this.scale) * global::SharpDX.Matrix.RotationX(rotationSpeed * -0.87f);
            Bilboard.Transform3D = ItemsData.Transform3D;
            Tick = StaticPage.start;
            Kolpeople.Text = StaticPage.dstagridnew.SelectedItems.Count.ToString();
            date.Text = Tick.Date.Date.ToString() + "  " + Tick.TimeOfDay.ToString();
            PauseButton.Style = this.Resources["Button_Zoning_presed"] as Style;
            Speed1.Style = this.Resources["Button_Zoning_presed"] as Style;
        }

        public void Update_Zon(object button) // Управляем уровнями отображения моделей
        {
            if (flagshowzon == true)
            {
                foreach (Element3D v in ItemsData.Children)
                {
                    string name1 = ((OITModel)((Element3D)v).DataContext).NameModel;
                    if (((TextBlock)((Viewbox)((Button)button).Content).Child).Text != "")
                    {
                        if (name1.Substring(0, 3) != "UR" + level && name1.Substring(0, 1) != "W"
                            || name1.Substring(name1.Length - 5, 1) != level.ToString() && name1.Substring(0, 1) != "U")
                        {
                            ((Element3D)v).Visibility = Visibility.Collapsed;
                        }
                        else
                        {
                            ((Element3D)v).Visibility = Visibility.Visible;
                        }
                    }
                    else
                    {
                        ((Element3D)v).Visibility = Visibility.Visible;
                    }
                }


                if (level != 5)
                {
                    last.Clear();
                    foreach (TextInfo v in Statistic3DMain.AxisLabelGeometry.TextInfo)
                    {
                        if (Employer_View.Zon.Find(t => t.id == (kol.Find(u => u.centre == v.Origin).name)).title.Substring(0, 3) != "UR" + level)
                        {
                            last.Add(new TextInfo(v.Text, v.Origin) { Foreground = Color.Transparent });
                        }
                        else
                        {
                            last.Add(new TextInfo(v.Text, v.Origin) { Foreground = Color.Black, Scale = 2 });
                        }
                    }
                    Statistic3DMain.AxisLabelGeometry.TextInfo.Clear();
                    foreach (TextInfo v in last)
                    {
                        Statistic3DMain.AxisLabelGeometry.TextInfo.Add(v);
                    }
                }
                else
                {
                    last.Clear();
                    foreach (TextInfo v in Statistic3DMain.AxisLabelGeometry.TextInfo)
                    {
                        last.Add(new TextInfo(v.Text, v.Origin) { Foreground = Color.Black, Scale = 2 });
                    }
                    Statistic3DMain.AxisLabelGeometry.TextInfo.Clear();
                    foreach (TextInfo v in last)
                    {
                        Statistic3DMain.AxisLabelGeometry.TextInfo.Add(v);
                    }
                }
            }
            else
            {
                foreach (Element3D v in ItemsData.Children)
                {
                    string name1 = ((OITModel)((Element3D)v).DataContext).NameModel;
                    if (((TextBlock)((Viewbox)((Button)button).Content).Child).Text != "")
                    {
                        if (name1.Substring(name1.Length - 5, 1) == level.ToString() && name1.Substring(0, 1) != "U")
                        {
                            ((Element3D)v).Visibility = Visibility.Visible;
                        }
                        else
                        {
                            ((Element3D)v).Visibility = Visibility.Collapsed;
                        }
                    }
                    else
                    {
                        if (((OITModel)v.DataContext).Mesh != null)
                        {
                            ((Element3D)v).Visibility = Visibility.Collapsed;
                        }
                        else
                        {
                            ((Element3D)v).Visibility = Visibility.Visible;
                        }
                    }
                }
                if (level != 5)
                {
                    last.Clear();
                    foreach (TextInfo v in Statistic3DMain.AxisLabelGeometry.TextInfo)
                    {
                        if (Employer_View.Zon.Find(t=> t.id ==(kol.Find(u => u.centre == v.Origin).name)).title.Substring(0, 3) != "UR" + level)
                        {
                            last.Add(new TextInfo(v.Text, v.Origin) { Foreground = Color.Transparent });
                        }
                        else
                        {
                            last.Add(new TextInfo(v.Text, v.Origin) { Foreground = Color.Black, Scale = 2 });
                        }
                    }
                    Statistic3DMain.AxisLabelGeometry.TextInfo.Clear();
                    foreach (TextInfo v in last)
                    {
                        Statistic3DMain.AxisLabelGeometry.TextInfo.Add(v);
                    }
                }
                else
                {
                    last.Clear();
                    foreach (TextInfo v in Statistic3DMain.AxisLabelGeometry.TextInfo)
                    {
                        last.Add(new TextInfo(v.Text, v.Origin) { Foreground = Color.Black, Scale = 2 });
                    }
                    Statistic3DMain.AxisLabelGeometry.TextInfo.Clear();
                    foreach (TextInfo v in last)
                    {
                        Statistic3DMain.AxisLabelGeometry.TextInfo.Add(v);
                    }
                }
            }
        }

        private void VievLevel(object sender, RoutedEventArgs e)
        {
            if (Statelast != null)
            {
                Statelast.Style = this.Resources["Button_Zoning_normal"] as Style;
            }
            if (((TextBlock)((Viewbox)((Button)sender).Content).Child).Text != "")
            {
                level = int.Parse(((TextBlock)((Viewbox)((Button)sender).Content).Child).Text);
            }
            else
            {
                level = 5;
            }
            Statelast = (Button)sender;
            Statelast.Style = this.Resources["Button_Zoning_presed"] as Style;
            Update_Zon(sender);
        }

        private void LoadData(object sender, RoutedEventArgs e) //Срабатывает по завершинии инициализации 3D моделей
        {
            Wait();//пауза необходима для внесения данных
        }


        public void Wait()
        {
            ThreadPoolTimer DelayTimer = ThreadPoolTimer.CreateTimer(
            async (source) =>
            {
                await this.Dispatcher.RunAsync(CoreDispatcherPriority.Normal, async () => // Перенаправляем поток
                {
                    VievLevel(levelAll, null);
                    Create_textAsync();

                });

            }, TimeSpan.FromSeconds(1));
        }

        public void StartPlay()
        {
            ThreadPoolTimer DelayTimer = ThreadPoolTimer.CreateTimer(
            async (source) =>
            {
                await this.Dispatcher.RunAsync(CoreDispatcherPriority.Normal, async () => // Перенаправляем поток
                {
                    if(flagpause == false)
                    {
                        if(Tick >= StaticPage.finish)
                        {
                            flagpause = true;
                            StaticPage.Clear_Data();
                            Frame.GoBack();
                        }
                        else
                        {
                            Tick = Tick.AddSeconds(speed * back);
                            date.Text = Tick.Date.Date.ToString() + "  " + Tick.TimeOfDay.ToString();
                            timer += back;
                            if (Tick < StaticPage.start)
                            {
                                back = 1;
                                Tick = StaticPage.start;
                                date.Text = Tick.Date.Date.ToString() + "  " + Tick.TimeOfDay.ToString();
                                SppedBack.Style = this.Resources["Button_Zoning_normal"] as Style;
                                PauseButton_Click(PauseButton, null);
                                timer = 0;
                            }
                            else
                            if (timer == 5 || timer == 0)
                            {
                                ZonPosition.Clear();
                                ZonPosition = await Employer_View.LoadPositionAsync(Tick);
                                Update_text();
                                if (timer == 5) { timer = 0; }
                                else { timer = 5; }
                            }
                            StartPlay();
                        }
                    }
                });

            }, TimeSpan.FromSeconds(1));
        }


        private void Update_text()
        {
            List<OITModel> infoZon = new List<OITModel>();
            infoZon = OITDemoViewStatistic.Statistic.ToList();
            kol.Clear();
            foreach (DataGridDataItem v in StaticPage.dstagridnew.SelectedItems)
            {
                bool flag = false;
                if (kol.Find(u => u.name == ZonPosition.Find(t => t.id == v.Id_Employer).gridElementId) != null)
                {
                    kol.Find(u => u.name == ZonPosition.Find(t => t.id == v.Id_Employer).gridElementId).kol += 1;
                    flag = true;
                }
                if (flag == false)
                {
                    if (ZonPosition.Find(u => u.id == v.Id_Employer) != null)
                    {
                        if (ZonPosition.Find(u => u.id == v.Id_Employer).gridElementId != null)
                        {
                            kol.Add(new ZonKol
                            {
                                name = ZonPosition.Find(u => u.id == v.Id_Employer).gridElementId,
                                kol = 1,
                                centre = (infoZon.Find(u => u.NameModel == Employer_View.Zon.Find(k => k.id == ZonPosition.Find(t => t.id == v.Id_Employer).gridElementId).title).Mesh.Bounds.Center
                                + new Vector3(0, 0, 2))
                            });
                        }
                        else
                        {
                            kol.Add(new ZonKol
                            {
                                name = "",
                                kol = 0,
                                centre = new Vector3(0, 0, 0)
                            }); ;
                        }
                    }
                    else
                    {
                        kol.Add(new ZonKol
                        {
                            name = "",
                            kol = 0,
                            centre = new Vector3(0, 0, 0)
                        }); ;
                    }
                }
            }
            Statistic3DMain.AxisLabelGeometry.TextInfo.Clear();
            foreach (ZonKol v in kol)
            {
                if(v.name != "")
                {
                    if (Employer_View.Zon.Find(j => j.id == v.name).title.Substring(0, 3) == "UR" + level || level == 5)
                    {
                        Statistic3DMain.AxisLabelGeometry.TextInfo.Add(new TextInfo(v.kol.ToString(), v.centre) { Foreground = Color.Black, Scale = 2 });
                    }
                    else
                    {
                        Statistic3DMain.AxisLabelGeometry.TextInfo.Add(new TextInfo(v.kol.ToString(), v.centre) { Foreground = Color.Transparent });
                    }
                }
            }

        }

        private async System.Threading.Tasks.Task Create_textAsync()
        {
            List<OITModel> infoZon = new List<OITModel>();
            infoZon = OITDemoViewStatistic.Statistic.ToList();
            ZonPosition = await Employer_View.LoadPositionAsync(StaticPage.start);
            foreach (DataGridDataItem v  in StaticPage.dstagridnew.SelectedItems)
            {
                bool flag = false;
                if(kol.Find(u=> u.name == ZonPosition.Find(t => t.id == v.Id_Employer).gridElementId) != null)
                {
                    kol.Find(u => u.name == ZonPosition.Find(t => t.id == v.Id_Employer).gridElementId).kol += 1;
                    flag = true;
                }
                if(flag == false)
                {
                    if (ZonPosition.Find(u => u.id == v.Id_Employer) != null)
                    {
                        if (ZonPosition.Find(u => u.id == v.Id_Employer).gridElementId != null)
                        {
                            kol.Add(new ZonKol
                            {
                                name = ZonPosition.Find(u => u.id == v.Id_Employer).gridElementId,
                                kol = 1,
                                centre = (infoZon.Find(u => u.NameModel == Employer_View.Zon.Find(k => k.id == ZonPosition.Find(t => t.id == v.Id_Employer).gridElementId).title).Mesh.Bounds.Center
                                + new Vector3(0, 0, 2))
                            });
                        }
                        else
                        {
                            kol.Add(new ZonKol
                            {
                                name = "",
                                kol = 0,
                                centre = new Vector3(0, 0, 0)
                            }); ;
                        }
                    }
                    else
                    {
                        kol.Add(new ZonKol
                        {
                            name = "",
                            kol = 0,
                            centre = new Vector3(0, 0, 0)
                        }); ;
                    }
                }
            }
            foreach (ZonKol v in kol)
            {
                if(v.name != "")
                {
                    Statistic3DMain.AxisLabelGeometry.TextInfo.Add(new TextInfo(v.kol.ToString(), v.centre) { Foreground = Color.Black, Scale = 2 });
                }
            }
        }

        public static void KeyDownOnpage(KeyRoutedEventArgs e) //Обработка нажатий клавишь в окне
        {
            if (e.Key == Windows.System.VirtualKey.W)
            {
                view.AddZoomForce(-0.1);
            }
            if (e.Key == Windows.System.VirtualKey.S)
            {
                view.AddZoomForce(0.1);
            }
            if (e.Key == Windows.System.VirtualKey.A)
            {
                item.Transform3D *= global::SharpDX.Matrix.Scaling(1) * global::SharpDX.Matrix.RotationZ(rotationSpeed * -0.02f) * global::SharpDX.Matrix.RotationY(rotationSpeed * -0.02f);
                bilboard1.Transform3D = item.Transform3D;
            }
            if (e.Key == Windows.System.VirtualKey.D)
            {
                item.Transform3D *= global::SharpDX.Matrix.Scaling(1) * global::SharpDX.Matrix.RotationZ(rotationSpeed * +0.02f) * global::SharpDX.Matrix.RotationY(rotationSpeed * +0.02f);
                bilboard1.Transform3D = item.Transform3D;
            }
            if (e.Key == Windows.System.VirtualKey.Left)
            {
                if (MainPageViewModel3D.Camera.Position.X < 97)
                {
                    Vector3 Positionleft = new Vector3(0.01f, 0, 0) * timepreset;
                    MainPageViewModel3D.Camera.Position += Positionleft;
                }
            }
            if (e.Key == Windows.System.VirtualKey.Right)
            {
                if (MainPageViewModel3D.Camera.Position.X > -89)
                {
                    Vector3 Positionleft = new Vector3(-0.01f, 0, 0) * timepreset;
                    MainPageViewModel3D.Camera.Position += Positionleft;
                }
            }
            if (e.Key == Windows.System.VirtualKey.Up)
            {
                if (MainPageViewModel3D.Camera.Position.Y > -64)
                {
                    Vector3 Positionleft = new Vector3(0, -0.01f, 0) * timepreset;
                    MainPageViewModel3D.Camera.Position += Positionleft;
                }
            }
            if (e.Key == Windows.System.VirtualKey.Down)
            {
                if (MainPageViewModel3D.Camera.Position.Y < 64)
                {
                    Vector3 Positionleft = new Vector3(0, 0.01f, 0) * timepreset;
                    MainPageViewModel3D.Camera.Position += Positionleft;
                }
            }
        }

        private void Moved(object sender, PointerRoutedEventArgs e)
        {
            if (e.Pointer.PointerDeviceType == Windows.Devices.Input.PointerDeviceType.Mouse)
            {
                var properties = e.GetCurrentPoint(this).Properties;
                if (!properties.IsLeftButtonPressed && !properties.IsRightButtonPressed)
                {
                    lastYcoordinate = properties.ContactRect.Y;
                    lastXcoordinate = properties.ContactRect.X;
                }
                if (properties.IsRightButtonPressed)
                {
                    Ycoordinate = properties.ContactRect.Y;
                    if (Ycoordinate > lastYcoordinate)
                    {
                        //if(ItemsData.Transform3D.Up.Z > -0.63)
                        //{
                        //    ItemsData.Transform3D *= global::SharpDX.Matrix.Scaling((float)this.scale) * global::SharpDX.Matrix.RotationX(rotationSpeed * -0.02f);//Вращение объекта к 2D виду
                        //}
                        ItemsData.Transform3D *= global::SharpDX.Matrix.Scaling((float)this.scale) * global::SharpDX.Matrix.RotationX(rotationSpeed * -0.02f);
                        Bilboard.Transform3D = ItemsData.Transform3D;
                    }
                    else if (Ycoordinate < lastYcoordinate)
                    {
                        //if (ItemsData.Transform3D.Up.Z < 0.8)
                        //{
                        //    ItemsData.Transform3D *= global::SharpDX.Matrix.Scaling((float)this.scale) * global::SharpDX.Matrix.RotationX(rotationSpeed * +0.02f);//Вращение объекта к 2D виду
                        //}
                        ItemsData.Transform3D *= global::SharpDX.Matrix.Scaling((float)this.scale) * global::SharpDX.Matrix.RotationX(rotationSpeed * +0.02f);
                        Bilboard.Transform3D = ItemsData.Transform3D;
                    }
                    lastYcoordinate = properties.ContactRect.Y;
                }
                else if (properties.IsLeftButtonPressed)
                {
                    Xcoordinate = properties.ContactRect.X;
                    if (Xcoordinate > lastXcoordinate)
                    {
                        ItemsData.Transform3D *= global::SharpDX.Matrix.Scaling((float)this.scale) * global::SharpDX.Matrix.RotationZ(rotationSpeed * -0.02f) * global::SharpDX.Matrix.RotationY(rotationSpeed * -0.02f);
                        Bilboard.Transform3D = ItemsData.Transform3D;
                    }
                    else if (Xcoordinate < lastXcoordinate)
                    {
                        ItemsData.Transform3D *= global::SharpDX.Matrix.Scaling((float)this.scale) * global::SharpDX.Matrix.RotationZ(rotationSpeed * +0.02f) * global::SharpDX.Matrix.RotationY(rotationSpeed * +0.02f);
                        Bilboard.Transform3D = ItemsData.Transform3D;
                    }
                    lastXcoordinate = properties.ContactRect.X;
                }
            }
        }

        private void CheckBox_Click(object sender, RoutedEventArgs e)
        {
            if (showZon.IsChecked == true)
            {
                flagshowzon = false;
                ShowZon();
            }
            else if (showZon.IsChecked == false)
            {
                flagshowzon = true;
                ShowZon();
            }
        }

        private void ShowZon()
        {
            if (flagshowzon == false)
            {
                foreach (Element3D v in ItemsData.Children)
                {
                    string name1 = ((OITModel)((Element3D)v).DataContext).NameModel;
                    if (level != 5)
                    {
                        if (name1.Substring(name1.Length - 5, 1) == level.ToString() && name1.Substring(0, 1) != "U")
                        {
                            ((Element3D)v).Visibility = Visibility.Visible;
                        }
                        else
                        {
                            ((Element3D)v).Visibility = Visibility.Collapsed;
                        }
                    }
                    else
                    {
                        if (((OITModel)v.DataContext).Mesh != null)
                        {
                            ((Element3D)v).Visibility = Visibility.Collapsed;
                        }
                        else
                        {
                            ((Element3D)v).Visibility = Visibility.Visible;
                        }
                    }
                }
            }
            else
            {
                foreach (Element3D v in ItemsData.Children)
                {
                    string name1 = ((OITModel)((Element3D)v).DataContext).NameModel;
                    if (level != 5)
                    {
                        if (name1.Substring(0, 3) != "UR" + level && name1.Substring(0, 1) != "W"
                            || name1.Substring(name1.Length - 5, 1) != level.ToString() && name1.Substring(0, 1) != "U")
                        {
                            ((Element3D)v).Visibility = Visibility.Collapsed;
                        }
                        else
                        {
                            ((Element3D)v).Visibility = Visibility.Visible;
                        }
                    }
                    else
                    {
                        ((Element3D)v).Visibility = Visibility.Visible;
                    }
                }
            }
        }

        private void StartButton_Click(object sender, RoutedEventArgs e)
        {
            if(((Button)sender).Style != this.Resources["Button_Zoning_presed"] as Style)
            {
                if (flagpause == true)
                {
                    flagpause = false;
                    PauseButton.Style = this.Resources["Button_Zoning_normal"] as Style;
                    ((Button)sender).Style = this.Resources["Button_Zoning_presed"] as Style;
                    StartPlay();
                }
            }
        }

        private void PauseButton_Click(object sender, RoutedEventArgs e)
        {
            if (((Button)sender).Style != this.Resources["Button_Zoning_presed"] as Style)
            {
                if (flagpause == false)
                {
                    StartButton.Style = this.Resources["Button_Zoning_normal"] as Style;
                    ((Button)sender).Style = this.Resources["Button_Zoning_presed"] as Style;
                    flagpause = true;
                }
            }
        }

        private void StopButton_Click(object sender, RoutedEventArgs e)
        {
            flagpause = true;
            StaticPage.Clear_Data();
            framePage.numberpage = 2;
            Frame.GoBack();
        }

        private void Speed10_Click(object sender, RoutedEventArgs e)
        {
            if (((Button)sender).Style != this.Resources["Button_Zoning_presed"] as Style)
            {
                Speed1.Style = this.Resources["Button_Zoning_normal"] as Style;
                Speed5.Style = this.Resources["Button_Zoning_normal"] as Style;
                ((Button)sender).Style = this.Resources["Button_Zoning_presed"] as Style;
                speed = 10;
            }
        }

        private void Speed5_Click(object sender, RoutedEventArgs e)
        {
            if (((Button)sender).Style != this.Resources["Button_Zoning_presed"] as Style)
            {
                Speed1.Style = this.Resources["Button_Zoning_normal"] as Style;
                Speed10.Style = this.Resources["Button_Zoning_normal"] as Style;
                ((Button)sender).Style = this.Resources["Button_Zoning_presed"] as Style;
                speed = 5;
            }
        }

        private void Speed1_Click(object sender, RoutedEventArgs e)
        {
            if (((Button)sender).Style != this.Resources["Button_Zoning_presed"] as Style)
            {
                Speed5.Style = this.Resources["Button_Zoning_normal"] as Style;
                Speed10.Style = this.Resources["Button_Zoning_normal"] as Style;
                ((Button)sender).Style = this.Resources["Button_Zoning_presed"] as Style;
                speed = 1;
            }
        }

        private void SppedBack_Click(object sender, RoutedEventArgs e)
        {
            if (((Button)sender).Style != this.Resources["Button_Zoning_presed"] as Style)
            {
                ((Button)sender).Style = this.Resources["Button_Zoning_presed"] as Style;
            }
            else
            {
                ((Button)sender).Style = this.Resources["Button_Zoning_normal"] as Style;
            }
                back = (-back);
        }
    }
}
