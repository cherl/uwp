﻿using GalaSoft.MvvmLight;
using HelixToolkit.Logger;
using HelixToolkit.UWP;
using SharpDX;
using System;
using System.IO;
using Vagrant_V1.Modeling3d._2D;

namespace Vagrant_V1.Pageees
{
    public class MainPageViewModel3D : ObservableObject
    {
        public OITDemoViewModel OITVM { get; } = new OITDemoViewModel();

        private Vector3 upDirection = Vector3.UnitY;
        public static BillboardText3D AxisLabelGeometry { set; get; }
        public Vector3 UpDirection
        {
            private set
            {
                if (Set(ref upDirection, value))
                {
                    ResetCamera();
                }
            }
            get { return upDirection; }
        }

        public IEffectsManager EffectsManager { private set; get; }
        public static Camera Camera { private set; get; }
        public TextureModel EnvironmentMap { private set; get; }
        public TextureModel BackgroundTexture {get; }
        public Vector3 DirectionalLightDirection { get; } = new Vector3(-0.5f, -1, 0);
        public Geometry3D Sphere { private set; get; }
        public Geometry3D Geometry { private set; get; }
        public PhongMaterial Material { private set; get; }
        private TextureModel particleTexture;
        public TextureModel ParticleTexture
        {
            set
            {
                Set(ref particleTexture, value);
            }
            get
            {
                return particleTexture;
            }
        }


        #region Commands
        #endregion

        public MainPageViewModel3D()
        {
            EffectsManager = new DefaultEffectsManager(new Logger());
            //Camera = new PerspectiveCamera()
            //{
            //    Position = new Vector3(1.5f, -76.8f, 69.8f),
            //    LookDirection = new Vector3(-0.5f, 74.8f, -66.2f),
            //    UpDirection = new Vector3(0.006f, 0.6f, 0.7f),
            //    FarPlaneDistance = 3000,
            //    NearPlaneDistance = 0.1,
            //};
            Camera = new PerspectiveCamera() 
            {
                Position = new Vector3(0, 0, 100),
                LookDirection = new Vector3(0, 2, -100),
                UpDirection = new Vector3(0.006f, 0.6f, 0.7f),
                FarPlaneDistance = 500,
                NearPlaneDistance = 0.1
            };



            //var builder = new MeshBuilder(true, true, true);
            //builder.AddBox(new SharpDX.Vector3(-80, 0, 0), 40, 25f, 20);
            //Geometry = builder.ToMesh();
            //Geometry.UpdateOctree();
            //builder = new MeshBuilder();
            //Sphere = builder.ToMesh();
            //Sphere.UpdateOctree();
            //Material = new PhongMaterial()
            //{
            //    AmbientColor = Color.Gray,
            //    DiffuseColor = new Color4(0.75f, 0.75f, 0.75f, 1.0f),
            //    SpecularColor = Color.White,
            //    SpecularShininess = 10f,
            //    ReflectiveColor = new Color4(0.2f, 0.2f, 0.2f, 0.5f),
            //    RenderEnvironmentMap = true
            //};
            //Material.DiffuseMap = LoadTexture("msars.png");
            //Material.NormalMap = LoadTexture("msars.png");

            AxisLabelGeometry = new BillboardText3D();
            //AxisLabelGeometry.TextInfo.Add(new TextInfo("X", new Vector3(5.5f, 0, 0)) { Foreground = Color.Red });
            //AxisLabelGeometry.TextInfo.Add(new TextInfo("Y", new Vector3(0, 5.5f, 0)) { Foreground = Color.Green });
            //AxisLabelGeometry.TextInfo.Add(new TextInfo("Z", new Vector3(0, 0, 5.5f)) { Foreground = Color.Blue });

            //EnvironmentMap = LoadTexture("Fon.dds");
            //BackgroundTexture = new TextureModel(
            //LoadTexture("D:/uwp/Visual2019/Vagrant_V1/vagrantupdate/vagrantuwp/Vagrant_V1/Image/3D/msarsFon.png"));
            //BitmapExtensions.CreateLinearGradientBitmapStream(EffectsManager, 128, 128,
            //Direct2DImageFormat.Png,
            //          new Vector2(0, 0), new Vector2(0, 128), new SharpDX.Direct2D1.GradientStop[]
            //           {
            //                      new SharpDX.Direct2D1.GradientStop(){ Color = Color.White.ToColor4(), Position = 0f },
            //                      new SharpDX.Direct2D1.GradientStop(){ Color = Color.DarkGray.ToColor4(), Position = 1f }
            //           }));
            //ParticleTexture = LoadTexture("msarsFon.png");
        }

        private Stream LoadTexture(string file)
        {
            var packageFolder = Path.Combine(Windows.ApplicationModel.Package.Current.InstalledLocation.Path, "");
            var bytecode = global::SharpDX.IO.NativeFile.ReadAllBytes(packageFolder + @"\" + file);
            return new MemoryStream(bytecode);
        }


        public class Logger : ILogger
        {
            public void Log<MsgType>(LogLevel logLevel, MsgType msg, string className, string methodName, int lineNumber)
            {
                switch (logLevel)
                {
                    case LogLevel.Warning:
                    case LogLevel.Error:
                        Console.WriteLine($"Level:{logLevel}; Msg:{msg}");
                        break;
                }
            }
        }

        private void ResetCamera()
        {
            Camera.UpDirection = UpDirection;
            if (UpDirection == Vector3.UnitY || UpDirection == Vector3.UnitX)
            {
                Camera.Position = new Vector3(0, 0, -15);
                Camera.LookDirection = new Vector3(0, 0, 15);
            }
            else
            {
                Camera.Position = new Vector3(0, -15, 0);
                Camera.LookDirection = new Vector3(0, 15, 0);
            }
        }
    }
}
