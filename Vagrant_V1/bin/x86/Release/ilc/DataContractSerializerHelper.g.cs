using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;
using System.Threading;
using System.Xml;

[assembly: global::System.Reflection.AssemblyVersion("4.0.0.0")]



namespace System.Runtime.Serialization.Generated
{
    [global::System.Runtime.CompilerServices.__BlockReflection]
    public static partial class DataContractSerializerHelper
    {
        static void InitDataContracts()
        {
            global::System.Collections.Generic.Dictionary<global::System.Type, global::System.Runtime.Serialization.DataContract> dataContracts = global::System.Runtime.Serialization.DataContract.GetDataContracts();
            PopulateContractDictionary(dataContracts);
        }
        static int[] s_knownContractsLists = new int[] {
              -1, }
        ;
        // Count = 473
        static int[] s_xmlDictionaryStrings = new int[] {
                0, // array length: 0
                3, // array length: 3
                -1, // string: null
                -1, // string: null
                -1, // string: null
                1, // array length: 1
                283, // index: 283, string: "http://schemas.datacontract.org/2004/07/HelixToolkit.UWP.Shaders"
                3, // array length: 3
                369, // index: 369, string: "InputLayoutDescription"
                4018, // index: 4018, string: "Name"
                4023, // index: 4023, string: "PassDescriptions"
                3, // array length: 3
                283, // index: 283, string: "http://schemas.datacontract.org/2004/07/HelixToolkit.UWP.Shaders"
                283, // index: 283, string: "http://schemas.datacontract.org/2004/07/HelixToolkit.UWP.Shaders"
                283, // index: 283, string: "http://schemas.datacontract.org/2004/07/HelixToolkit.UWP.Shaders"
                2, // array length: 2
                412, // index: 412, string: "http://schemas.datacontract.org/2004/07/SharpDX.Direct3D11"
                -1, // string: null
                1, // array length: 1
                283, // index: 283, string: "http://schemas.datacontract.org/2004/07/HelixToolkit.UWP.Shaders"
                2, // array length: 2
                4040, // index: 4040, string: "InputElements"
                4054, // index: 4054, string: "ShaderByteCode"
                2, // array length: 2
                283, // index: 283, string: "http://schemas.datacontract.org/2004/07/HelixToolkit.UWP.Shaders"
                283, // index: 283, string: "http://schemas.datacontract.org/2004/07/HelixToolkit.UWP.Shaders"
                7, // array length: 7
                -1, // string: null
                -1, // string: null
                -1, // string: null
                -1, // string: null
                -1, // string: null
                -1, // string: null
                -1, // string: null
                1, // array length: 1
                412, // index: 412, string: "http://schemas.datacontract.org/2004/07/SharpDX.Direct3D11"
                7, // array length: 7
                4069, // index: 4069, string: "AlignedByteOffset"
                4087, // index: 4087, string: "Classification"
                534, // index: 534, string: "Format"
                4102, // index: 4102, string: "InstanceDataStepRate"
                4123, // index: 4123, string: "SemanticIndex"
                4137, // index: 4137, string: "SemanticName"
                4150, // index: 4150, string: "Slot"
                7, // array length: 7
                412, // index: 412, string: "http://schemas.datacontract.org/2004/07/SharpDX.Direct3D11"
                412, // index: 412, string: "http://schemas.datacontract.org/2004/07/SharpDX.Direct3D11"
                412, // index: 412, string: "http://schemas.datacontract.org/2004/07/SharpDX.Direct3D11"
                412, // index: 412, string: "http://schemas.datacontract.org/2004/07/SharpDX.Direct3D11"
                412, // index: 412, string: "http://schemas.datacontract.org/2004/07/SharpDX.Direct3D11"
                412, // index: 412, string: "http://schemas.datacontract.org/2004/07/SharpDX.Direct3D11"
                412, // index: 412, string: "http://schemas.datacontract.org/2004/07/SharpDX.Direct3D11"
                2, // array length: 2
                504, // index: 504, string: "PerVertexData"
                518, // index: 518, string: "PerInstanceData"
                119, // array length: 119
                594, // index: 594, string: "Unknown"
                602, // index: 602, string: "R32G32B32A32_Typeless"
                624, // index: 624, string: "R32G32B32A32_Float"
                643, // index: 643, string: "R32G32B32A32_UInt"
                661, // index: 661, string: "R32G32B32A32_SInt"
                679, // index: 679, string: "R32G32B32_Typeless"
                698, // index: 698, string: "R32G32B32_Float"
                714, // index: 714, string: "R32G32B32_UInt"
                729, // index: 729, string: "R32G32B32_SInt"
                744, // index: 744, string: "R16G16B16A16_Typeless"
                766, // index: 766, string: "R16G16B16A16_Float"
                785, // index: 785, string: "R16G16B16A16_UNorm"
                804, // index: 804, string: "R16G16B16A16_UInt"
                822, // index: 822, string: "R16G16B16A16_SNorm"
                841, // index: 841, string: "R16G16B16A16_SInt"
                859, // index: 859, string: "R32G32_Typeless"
                875, // index: 875, string: "R32G32_Float"
                888, // index: 888, string: "R32G32_UInt"
                900, // index: 900, string: "R32G32_SInt"
                912, // index: 912, string: "R32G8X24_Typeless"
                930, // index: 930, string: "D32_Float_S8X24_UInt"
                951, // index: 951, string: "R32_Float_X8X24_Typeless"
                976, // index: 976, string: "X32_Typeless_G8X24_UInt"
                1000, // index: 1000, string: "R10G10B10A2_Typeless"
                1021, // index: 1021, string: "R10G10B10A2_UNorm"
                1039, // index: 1039, string: "R10G10B10A2_UInt"
                1056, // index: 1056, string: "R11G11B10_Float"
                1072, // index: 1072, string: "R8G8B8A8_Typeless"
                1090, // index: 1090, string: "R8G8B8A8_UNorm"
                1105, // index: 1105, string: "R8G8B8A8_UNorm_SRgb"
                1125, // index: 1125, string: "R8G8B8A8_UInt"
                1139, // index: 1139, string: "R8G8B8A8_SNorm"
                1154, // index: 1154, string: "R8G8B8A8_SInt"
                1168, // index: 1168, string: "R16G16_Typeless"
                1184, // index: 1184, string: "R16G16_Float"
                1197, // index: 1197, string: "R16G16_UNorm"
                1210, // index: 1210, string: "R16G16_UInt"
                1222, // index: 1222, string: "R16G16_SNorm"
                1235, // index: 1235, string: "R16G16_SInt"
                1247, // index: 1247, string: "R32_Typeless"
                1260, // index: 1260, string: "D32_Float"
                1270, // index: 1270, string: "R32_Float"
                1280, // index: 1280, string: "R32_UInt"
                1289, // index: 1289, string: "R32_SInt"
                1298, // index: 1298, string: "R24G8_Typeless"
                1313, // index: 1313, string: "D24_UNorm_S8_UInt"
                1331, // index: 1331, string: "R24_UNorm_X8_Typeless"
                1353, // index: 1353, string: "X24_Typeless_G8_UInt"
                1374, // index: 1374, string: "R8G8_Typeless"
                1388, // index: 1388, string: "R8G8_UNorm"
                1399, // index: 1399, string: "R8G8_UInt"
                1409, // index: 1409, string: "R8G8_SNorm"
                1420, // index: 1420, string: "R8G8_SInt"
                1430, // index: 1430, string: "R16_Typeless"
                1443, // index: 1443, string: "R16_Float"
                1453, // index: 1453, string: "D16_UNorm"
                1463, // index: 1463, string: "R16_UNorm"
                1473, // index: 1473, string: "R16_UInt"
                1482, // index: 1482, string: "R16_SNorm"
                1492, // index: 1492, string: "R16_SInt"
                1501, // index: 1501, string: "R8_Typeless"
                1513, // index: 1513, string: "R8_UNorm"
                1522, // index: 1522, string: "R8_UInt"
                1530, // index: 1530, string: "R8_SNorm"
                1539, // index: 1539, string: "R8_SInt"
                1547, // index: 1547, string: "A8_UNorm"
                1556, // index: 1556, string: "R1_UNorm"
                1565, // index: 1565, string: "R9G9B9E5_Sharedexp"
                1584, // index: 1584, string: "R8G8_B8G8_UNorm"
                1600, // index: 1600, string: "G8R8_G8B8_UNorm"
                1616, // index: 1616, string: "BC1_Typeless"
                1629, // index: 1629, string: "BC1_UNorm"
                1639, // index: 1639, string: "BC1_UNorm_SRgb"
                1654, // index: 1654, string: "BC2_Typeless"
                1667, // index: 1667, string: "BC2_UNorm"
                1677, // index: 1677, string: "BC2_UNorm_SRgb"
                1692, // index: 1692, string: "BC3_Typeless"
                1705, // index: 1705, string: "BC3_UNorm"
                1715, // index: 1715, string: "BC3_UNorm_SRgb"
                1730, // index: 1730, string: "BC4_Typeless"
                1743, // index: 1743, string: "BC4_UNorm"
                1753, // index: 1753, string: "BC4_SNorm"
                1763, // index: 1763, string: "BC5_Typeless"
                1776, // index: 1776, string: "BC5_UNorm"
                1786, // index: 1786, string: "BC5_SNorm"
                1796, // index: 1796, string: "B5G6R5_UNorm"
                1809, // index: 1809, string: "B5G5R5A1_UNorm"
                1824, // index: 1824, string: "B8G8R8A8_UNorm"
                1839, // index: 1839, string: "B8G8R8X8_UNorm"
                1854, // index: 1854, string: "R10G10B10_Xr_Bias_A2_UNorm"
                1881, // index: 1881, string: "B8G8R8A8_Typeless"
                1899, // index: 1899, string: "B8G8R8A8_UNorm_SRgb"
                1919, // index: 1919, string: "B8G8R8X8_Typeless"
                1937, // index: 1937, string: "B8G8R8X8_UNorm_SRgb"
                1957, // index: 1957, string: "BC6H_Typeless"
                1971, // index: 1971, string: "BC6H_Uf16"
                1981, // index: 1981, string: "BC6H_Sf16"
                1991, // index: 1991, string: "BC7_Typeless"
                2004, // index: 2004, string: "BC7_UNorm"
                2014, // index: 2014, string: "BC7_UNorm_SRgb"
                2029, // index: 2029, string: "AYUV"
                2034, // index: 2034, string: "Y410"
                2039, // index: 2039, string: "Y416"
                2044, // index: 2044, string: "NV12"
                2049, // index: 2049, string: "P010"
                2054, // index: 2054, string: "P016"
                2059, // index: 2059, string: "Opaque420"
                2069, // index: 2069, string: "YUY2"
                2074, // index: 2074, string: "Y210"
                2079, // index: 2079, string: "Y216"
                2084, // index: 2084, string: "NV11"
                2089, // index: 2089, string: "AI44"
                2094, // index: 2094, string: "IA44"
                2099, // index: 2099, string: "P8"
                2102, // index: 2102, string: "A8P8"
                2107, // index: 2107, string: "B4G4R4A4_UNorm"
                2122, // index: 2122, string: "P208"
                2127, // index: 2127, string: "V208"
                2132, // index: 2132, string: "V408"
                10, // array length: 10
                2195, // index: 2195, string: "http://schemas.datacontract.org/2004/07/SharpDX"
                -1, // string: null
                -1, // string: null
                -1, // string: null
                -1, // string: null
                -1, // string: null
                -1, // string: null
                -1, // string: null
                -1, // string: null
                -1, // string: null
                1, // array length: 1
                283, // index: 283, string: "http://schemas.datacontract.org/2004/07/HelixToolkit.UWP.Shaders"
                10, // array length: 10
                4155, // index: 4155, string: "BlendFactor"
                4167, // index: 4167, string: "BlendStateDescSerialization"
                4195, // index: 4195, string: "DepthStencilStateDescSerialization"
                369, // index: 369, string: "InputLayoutDescription"
                4018, // index: 4018, string: "Name"
                4230, // index: 4230, string: "RasterizerStateDescSerialization"
                4263, // index: 4263, string: "SampleMask"
                4274, // index: 4274, string: "ShaderList"
                4285, // index: 4285, string: "StencilRef"
                4296, // index: 4296, string: "Topology"
                10, // array length: 10
                283, // index: 283, string: "http://schemas.datacontract.org/2004/07/HelixToolkit.UWP.Shaders"
                283, // index: 283, string: "http://schemas.datacontract.org/2004/07/HelixToolkit.UWP.Shaders"
                283, // index: 283, string: "http://schemas.datacontract.org/2004/07/HelixToolkit.UWP.Shaders"
                283, // index: 283, string: "http://schemas.datacontract.org/2004/07/HelixToolkit.UWP.Shaders"
                283, // index: 283, string: "http://schemas.datacontract.org/2004/07/HelixToolkit.UWP.Shaders"
                283, // index: 283, string: "http://schemas.datacontract.org/2004/07/HelixToolkit.UWP.Shaders"
                283, // index: 283, string: "http://schemas.datacontract.org/2004/07/HelixToolkit.UWP.Shaders"
                283, // index: 283, string: "http://schemas.datacontract.org/2004/07/HelixToolkit.UWP.Shaders"
                283, // index: 283, string: "http://schemas.datacontract.org/2004/07/HelixToolkit.UWP.Shaders"
                283, // index: 283, string: "http://schemas.datacontract.org/2004/07/HelixToolkit.UWP.Shaders"
                4, // array length: 4
                -1, // string: null
                -1, // string: null
                -1, // string: null
                -1, // string: null
                1, // array length: 1
                2195, // index: 2195, string: "http://schemas.datacontract.org/2004/07/SharpDX"
                4, // array length: 4
                4305, // index: 4305, string: "Alpha"
                4311, // index: 4311, string: "Blue"
                4316, // index: 4316, string: "Green"
                4322, // index: 4322, string: "Red"
                4, // array length: 4
                2195, // index: 2195, string: "http://schemas.datacontract.org/2004/07/SharpDX"
                2195, // index: 2195, string: "http://schemas.datacontract.org/2004/07/SharpDX"
                2195, // index: 2195, string: "http://schemas.datacontract.org/2004/07/SharpDX"
                2195, // index: 2195, string: "http://schemas.datacontract.org/2004/07/SharpDX"
                3, // array length: 3
                -1, // string: null
                -1, // string: null
                -1, // string: null
                1, // array length: 1
                283, // index: 283, string: "http://schemas.datacontract.org/2004/07/HelixToolkit.UWP.Shaders"
                3, // array length: 3
                4326, // index: 4326, string: "AlphaToCoverageEnable"
                4348, // index: 4348, string: "IndependentBlendEnable"
                4371, // index: 4371, string: "RenderTarget"
                3, // array length: 3
                283, // index: 283, string: "http://schemas.datacontract.org/2004/07/HelixToolkit.UWP.Shaders"
                283, // index: 283, string: "http://schemas.datacontract.org/2004/07/HelixToolkit.UWP.Shaders"
                283, // index: 283, string: "http://schemas.datacontract.org/2004/07/HelixToolkit.UWP.Shaders"
                8, // array length: 8
                -1, // string: null
                -1, // string: null
                -1, // string: null
                -1, // string: null
                -1, // string: null
                -1, // string: null
                -1, // string: null
                -1, // string: null
                1, // array length: 1
                283, // index: 283, string: "http://schemas.datacontract.org/2004/07/HelixToolkit.UWP.Shaders"
                8, // array length: 8
                4384, // index: 4384, string: "AlphaBlendOperation"
                4404, // index: 4404, string: "BlendOperation"
                4419, // index: 4419, string: "DestinationAlphaBlend"
                4441, // index: 4441, string: "DestinationBlend"
                4458, // index: 4458, string: "IsBlendEnabled"
                4473, // index: 4473, string: "RenderTargetWriteMask"
                4495, // index: 4495, string: "SourceAlphaBlend"
                4512, // index: 4512, string: "SourceBlend"
                8, // array length: 8
                283, // index: 283, string: "http://schemas.datacontract.org/2004/07/HelixToolkit.UWP.Shaders"
                283, // index: 283, string: "http://schemas.datacontract.org/2004/07/HelixToolkit.UWP.Shaders"
                283, // index: 283, string: "http://schemas.datacontract.org/2004/07/HelixToolkit.UWP.Shaders"
                283, // index: 283, string: "http://schemas.datacontract.org/2004/07/HelixToolkit.UWP.Shaders"
                283, // index: 283, string: "http://schemas.datacontract.org/2004/07/HelixToolkit.UWP.Shaders"
                283, // index: 283, string: "http://schemas.datacontract.org/2004/07/HelixToolkit.UWP.Shaders"
                283, // index: 283, string: "http://schemas.datacontract.org/2004/07/HelixToolkit.UWP.Shaders"
                283, // index: 283, string: "http://schemas.datacontract.org/2004/07/HelixToolkit.UWP.Shaders"
                8, // array length: 8
                -1, // string: null
                -1, // string: null
                -1, // string: null
                -1, // string: null
                -1, // string: null
                -1, // string: null
                -1, // string: null
                -1, // string: null
                1, // array length: 1
                283, // index: 283, string: "http://schemas.datacontract.org/2004/07/HelixToolkit.UWP.Shaders"
                8, // array length: 8
                4524, // index: 4524, string: "BackFace"
                4533, // index: 4533, string: "DepthComparison"
                4549, // index: 4549, string: "DepthWriteMask"
                4564, // index: 4564, string: "FrontFace"
                4574, // index: 4574, string: "IsDepthEnabled"
                4589, // index: 4589, string: "IsStencilEnabled"
                4606, // index: 4606, string: "StencilReadMask"
                4622, // index: 4622, string: "StencilWriteMask"
                8, // array length: 8
                283, // index: 283, string: "http://schemas.datacontract.org/2004/07/HelixToolkit.UWP.Shaders"
                283, // index: 283, string: "http://schemas.datacontract.org/2004/07/HelixToolkit.UWP.Shaders"
                283, // index: 283, string: "http://schemas.datacontract.org/2004/07/HelixToolkit.UWP.Shaders"
                283, // index: 283, string: "http://schemas.datacontract.org/2004/07/HelixToolkit.UWP.Shaders"
                283, // index: 283, string: "http://schemas.datacontract.org/2004/07/HelixToolkit.UWP.Shaders"
                283, // index: 283, string: "http://schemas.datacontract.org/2004/07/HelixToolkit.UWP.Shaders"
                283, // index: 283, string: "http://schemas.datacontract.org/2004/07/HelixToolkit.UWP.Shaders"
                283, // index: 283, string: "http://schemas.datacontract.org/2004/07/HelixToolkit.UWP.Shaders"
                4, // array length: 4
                -1, // string: null
                -1, // string: null
                -1, // string: null
                -1, // string: null
                1, // array length: 1
                283, // index: 283, string: "http://schemas.datacontract.org/2004/07/HelixToolkit.UWP.Shaders"
                4, // array length: 4
                4639, // index: 4639, string: "Comparison"
                4650, // index: 4650, string: "DepthFailOperation"
                4669, // index: 4669, string: "FailOperation"
                4683, // index: 4683, string: "PassOperation"
                4, // array length: 4
                283, // index: 283, string: "http://schemas.datacontract.org/2004/07/HelixToolkit.UWP.Shaders"
                283, // index: 283, string: "http://schemas.datacontract.org/2004/07/HelixToolkit.UWP.Shaders"
                283, // index: 283, string: "http://schemas.datacontract.org/2004/07/HelixToolkit.UWP.Shaders"
                283, // index: 283, string: "http://schemas.datacontract.org/2004/07/HelixToolkit.UWP.Shaders"
                10, // array length: 10
                -1, // string: null
                -1, // string: null
                -1, // string: null
                -1, // string: null
                -1, // string: null
                -1, // string: null
                -1, // string: null
                -1, // string: null
                -1, // string: null
                -1, // string: null
                1, // array length: 1
                283, // index: 283, string: "http://schemas.datacontract.org/2004/07/HelixToolkit.UWP.Shaders"
                10, // array length: 10
                4697, // index: 4697, string: "CullMode"
                4706, // index: 4706, string: "DepthBias"
                4716, // index: 4716, string: "DepthBiasClamp"
                4731, // index: 4731, string: "FillMode"
                4740, // index: 4740, string: "IsAntialiasedLineEnabled"
                4765, // index: 4765, string: "IsDepthClipEnabled"
                4784, // index: 4784, string: "IsFrontCounterClockwise"
                4808, // index: 4808, string: "IsMultisampleEnabled"
                4829, // index: 4829, string: "IsScissorEnabled"
                4846, // index: 4846, string: "SlopeScaledDepthBias"
                10, // array length: 10
                283, // index: 283, string: "http://schemas.datacontract.org/2004/07/HelixToolkit.UWP.Shaders"
                283, // index: 283, string: "http://schemas.datacontract.org/2004/07/HelixToolkit.UWP.Shaders"
                283, // index: 283, string: "http://schemas.datacontract.org/2004/07/HelixToolkit.UWP.Shaders"
                283, // index: 283, string: "http://schemas.datacontract.org/2004/07/HelixToolkit.UWP.Shaders"
                283, // index: 283, string: "http://schemas.datacontract.org/2004/07/HelixToolkit.UWP.Shaders"
                283, // index: 283, string: "http://schemas.datacontract.org/2004/07/HelixToolkit.UWP.Shaders"
                283, // index: 283, string: "http://schemas.datacontract.org/2004/07/HelixToolkit.UWP.Shaders"
                283, // index: 283, string: "http://schemas.datacontract.org/2004/07/HelixToolkit.UWP.Shaders"
                283, // index: 283, string: "http://schemas.datacontract.org/2004/07/HelixToolkit.UWP.Shaders"
                283, // index: 283, string: "http://schemas.datacontract.org/2004/07/HelixToolkit.UWP.Shaders"
                7, // array length: 7
                -1, // string: null
                412, // index: 412, string: "http://schemas.datacontract.org/2004/07/SharpDX.Direct3D11"
                -1, // string: null
                2526, // index: 2526, string: "http://schemas.microsoft.com/2003/10/Serialization/Arrays"
                -1, // string: null
                -1, // string: null
                -1, // string: null
                1, // array length: 1
                283, // index: 283, string: "http://schemas.datacontract.org/2004/07/HelixToolkit.UWP.Shaders"
                7, // array length: 7
                4867, // index: 4867, string: "ByteCode"
                4876, // index: 4876, string: "GSSOElement"
                4888, // index: 4888, string: "GSSORasterized"
                4903, // index: 4903, string: "GSSOStrides"
                4915, // index: 4915, string: "IsGSStreamOut"
                4018, // index: 4018, string: "Name"
                4929, // index: 4929, string: "ShaderType"
                7, // array length: 7
                283, // index: 283, string: "http://schemas.datacontract.org/2004/07/HelixToolkit.UWP.Shaders"
                283, // index: 283, string: "http://schemas.datacontract.org/2004/07/HelixToolkit.UWP.Shaders"
                283, // index: 283, string: "http://schemas.datacontract.org/2004/07/HelixToolkit.UWP.Shaders"
                283, // index: 283, string: "http://schemas.datacontract.org/2004/07/HelixToolkit.UWP.Shaders"
                283, // index: 283, string: "http://schemas.datacontract.org/2004/07/HelixToolkit.UWP.Shaders"
                283, // index: 283, string: "http://schemas.datacontract.org/2004/07/HelixToolkit.UWP.Shaders"
                283, // index: 283, string: "http://schemas.datacontract.org/2004/07/HelixToolkit.UWP.Shaders"
                6, // array length: 6
                -1, // string: null
                -1, // string: null
                -1, // string: null
                -1, // string: null
                -1, // string: null
                -1, // string: null
                1, // array length: 1
                412, // index: 412, string: "http://schemas.datacontract.org/2004/07/SharpDX.Direct3D11"
                6, // array length: 6
                4940, // index: 4940, string: "ComponentCount"
                4955, // index: 4955, string: "OutputSlot"
                4123, // index: 4123, string: "SemanticIndex"
                4137, // index: 4137, string: "SemanticName"
                4966, // index: 4966, string: "StartComponent"
                4981, // index: 4981, string: "Stream"
                6, // array length: 6
                412, // index: 412, string: "http://schemas.datacontract.org/2004/07/SharpDX.Direct3D11"
                412, // index: 412, string: "http://schemas.datacontract.org/2004/07/SharpDX.Direct3D11"
                412, // index: 412, string: "http://schemas.datacontract.org/2004/07/SharpDX.Direct3D11"
                412, // index: 412, string: "http://schemas.datacontract.org/2004/07/SharpDX.Direct3D11"
                412, // index: 412, string: "http://schemas.datacontract.org/2004/07/SharpDX.Direct3D11"
                412, // index: 412, string: "http://schemas.datacontract.org/2004/07/SharpDX.Direct3D11"
                7, // array length: 7
                2653, // index: 2653, string: "None"
                2658, // index: 2658, string: "Vertex"
                2665, // index: 2665, string: "Hull"
                2670, // index: 2670, string: "Domain"
                2677, // index: 2677, string: "Geometry"
                2686, // index: 2686, string: "Pixel"
                2692, // index: 2692, string: "Compute"
                42, // array length: 42
                2775, // index: 2775, string: "Undefined"
                2785, // index: 2785, string: "PointList"
                2795, // index: 2795, string: "LineList"
                2804, // index: 2804, string: "LineStrip"
                2814, // index: 2814, string: "TriangleList"
                2827, // index: 2827, string: "TriangleStrip"
                2841, // index: 2841, string: "LineListWithAdjacency"
                2863, // index: 2863, string: "LineStripWithAdjacency"
                2886, // index: 2886, string: "TriangleListWithAdjacency"
                2912, // index: 2912, string: "TriangleStripWithAdjacency"
                2939, // index: 2939, string: "PatchListWith1ControlPoints"
                2967, // index: 2967, string: "PatchListWith2ControlPoints"
                2995, // index: 2995, string: "PatchListWith3ControlPoints"
                3023, // index: 3023, string: "PatchListWith4ControlPoints"
                3051, // index: 3051, string: "PatchListWith5ControlPoints"
                3079, // index: 3079, string: "PatchListWith6ControlPoints"
                3107, // index: 3107, string: "PatchListWith7ControlPoints"
                3135, // index: 3135, string: "PatchListWith8ControlPoints"
                3163, // index: 3163, string: "PatchListWith9ControlPoints"
                3191, // index: 3191, string: "PatchListWith10ControlPoints"
                3220, // index: 3220, string: "PatchListWith11ControlPoints"
                3249, // index: 3249, string: "PatchListWith12ControlPoints"
                3278, // index: 3278, string: "PatchListWith13ControlPoints"
                3307, // index: 3307, string: "PatchListWith14ControlPoints"
                3336, // index: 3336, string: "PatchListWith15ControlPoints"
                3365, // index: 3365, string: "PatchListWith16ControlPoints"
                3394, // index: 3394, string: "PatchListWith17ControlPoints"
                3423, // index: 3423, string: "PatchListWith18ControlPoints"
                3452, // index: 3452, string: "PatchListWith19ControlPoints"
                3481, // index: 3481, string: "PatchListWith20ControlPoints"
                3510, // index: 3510, string: "PatchListWith21ControlPoints"
                3539, // index: 3539, string: "PatchListWith22ControlPoints"
                3568, // index: 3568, string: "PatchListWith23ControlPoints"
                3597, // index: 3597, string: "PatchListWith24ControlPoints"
                3626, // index: 3626, string: "PatchListWith25ControlPoints"
                3655, // index: 3655, string: "PatchListWith26ControlPoints"
                3684, // index: 3684, string: "PatchListWith27ControlPoints"
                3713, // index: 3713, string: "PatchListWith28ControlPoints"
                3742, // index: 3742, string: "PatchListWith29ControlPoints"
                3771, // index: 3771, string: "PatchListWith30ControlPoints"
                3800, // index: 3800, string: "PatchListWith31ControlPoints"
                3829, // index: 3829, string: "PatchListWith32ControlPoints"
                2, // array length: 2
                -1, // string: null
                -1, // string: null
                1, // array length: 1
                2526, // index: 2526, string: "http://schemas.microsoft.com/2003/10/Serialization/Arrays"
                2, // array length: 2
                3913, // index: 3913, string: "Key"
                3917, // index: 3917, string: "Value"
                2, // array length: 2
                2526, // index: 2526, string: "http://schemas.microsoft.com/2003/10/Serialization/Arrays"
                2526, // index: 2526, string: "http://schemas.microsoft.com/2003/10/Serialization/Arrays"
                2, // array length: 2
                -1, // string: null
                -1, // string: null
                1, // array length: 1
                3951, // index: 3951, string: "http://schemas.datacontract.org/2004/07/System.Collections.Generic"
                2, // array length: 2
                4988, // index: 4988, string: "key"
                4992, // index: 4992, string: "value"
                2, // array length: 2
                3951, // index: 3951, string: "http://schemas.datacontract.org/2004/07/System.Collections.Generic"
                3951  // index: 3951, string: "http://schemas.datacontract.org/2004/07/System.Collections.Generic"
        };
        // Count = 170
        static global::MemberEntry[] s_dataMemberLists = new global::MemberEntry[] {
                new global::MemberEntry() {
                    EmitDefaultValue = true,
                    NameIndex = 504, // PerVertexData
                }, 
                new global::MemberEntry() {
                    EmitDefaultValue = true,
                    NameIndex = 518, // PerInstanceData
                    Value = 1,
                }, 
                new global::MemberEntry() {
                    EmitDefaultValue = true,
                    NameIndex = 594, // Unknown
                }, 
                new global::MemberEntry() {
                    EmitDefaultValue = true,
                    NameIndex = 602, // R32G32B32A32_Typeless
                    Value = 1,
                }, 
                new global::MemberEntry() {
                    EmitDefaultValue = true,
                    NameIndex = 624, // R32G32B32A32_Float
                    Value = 2,
                }, 
                new global::MemberEntry() {
                    EmitDefaultValue = true,
                    NameIndex = 643, // R32G32B32A32_UInt
                    Value = 3,
                }, 
                new global::MemberEntry() {
                    EmitDefaultValue = true,
                    NameIndex = 661, // R32G32B32A32_SInt
                    Value = 4,
                }, 
                new global::MemberEntry() {
                    EmitDefaultValue = true,
                    NameIndex = 679, // R32G32B32_Typeless
                    Value = 5,
                }, 
                new global::MemberEntry() {
                    EmitDefaultValue = true,
                    NameIndex = 698, // R32G32B32_Float
                    Value = 6,
                }, 
                new global::MemberEntry() {
                    EmitDefaultValue = true,
                    NameIndex = 714, // R32G32B32_UInt
                    Value = 7,
                }, 
                new global::MemberEntry() {
                    EmitDefaultValue = true,
                    NameIndex = 729, // R32G32B32_SInt
                    Value = 8,
                }, 
                new global::MemberEntry() {
                    EmitDefaultValue = true,
                    NameIndex = 744, // R16G16B16A16_Typeless
                    Value = 9,
                }, 
                new global::MemberEntry() {
                    EmitDefaultValue = true,
                    NameIndex = 766, // R16G16B16A16_Float
                    Value = 10,
                }, 
                new global::MemberEntry() {
                    EmitDefaultValue = true,
                    NameIndex = 785, // R16G16B16A16_UNorm
                    Value = 11,
                }, 
                new global::MemberEntry() {
                    EmitDefaultValue = true,
                    NameIndex = 804, // R16G16B16A16_UInt
                    Value = 12,
                }, 
                new global::MemberEntry() {
                    EmitDefaultValue = true,
                    NameIndex = 822, // R16G16B16A16_SNorm
                    Value = 13,
                }, 
                new global::MemberEntry() {
                    EmitDefaultValue = true,
                    NameIndex = 841, // R16G16B16A16_SInt
                    Value = 14,
                }, 
                new global::MemberEntry() {
                    EmitDefaultValue = true,
                    NameIndex = 859, // R32G32_Typeless
                    Value = 15,
                }, 
                new global::MemberEntry() {
                    EmitDefaultValue = true,
                    NameIndex = 875, // R32G32_Float
                    Value = 16,
                }, 
                new global::MemberEntry() {
                    EmitDefaultValue = true,
                    NameIndex = 888, // R32G32_UInt
                    Value = 17,
                }, 
                new global::MemberEntry() {
                    EmitDefaultValue = true,
                    NameIndex = 900, // R32G32_SInt
                    Value = 18,
                }, 
                new global::MemberEntry() {
                    EmitDefaultValue = true,
                    NameIndex = 912, // R32G8X24_Typeless
                    Value = 19,
                }, 
                new global::MemberEntry() {
                    EmitDefaultValue = true,
                    NameIndex = 930, // D32_Float_S8X24_UInt
                    Value = 20,
                }, 
                new global::MemberEntry() {
                    EmitDefaultValue = true,
                    NameIndex = 951, // R32_Float_X8X24_Typeless
                    Value = 21,
                }, 
                new global::MemberEntry() {
                    EmitDefaultValue = true,
                    NameIndex = 976, // X32_Typeless_G8X24_UInt
                    Value = 22,
                }, 
                new global::MemberEntry() {
                    EmitDefaultValue = true,
                    NameIndex = 1000, // R10G10B10A2_Typeless
                    Value = 23,
                }, 
                new global::MemberEntry() {
                    EmitDefaultValue = true,
                    NameIndex = 1021, // R10G10B10A2_UNorm
                    Value = 24,
                }, 
                new global::MemberEntry() {
                    EmitDefaultValue = true,
                    NameIndex = 1039, // R10G10B10A2_UInt
                    Value = 25,
                }, 
                new global::MemberEntry() {
                    EmitDefaultValue = true,
                    NameIndex = 1056, // R11G11B10_Float
                    Value = 26,
                }, 
                new global::MemberEntry() {
                    EmitDefaultValue = true,
                    NameIndex = 1072, // R8G8B8A8_Typeless
                    Value = 27,
                }, 
                new global::MemberEntry() {
                    EmitDefaultValue = true,
                    NameIndex = 1090, // R8G8B8A8_UNorm
                    Value = 28,
                }, 
                new global::MemberEntry() {
                    EmitDefaultValue = true,
                    NameIndex = 1105, // R8G8B8A8_UNorm_SRgb
                    Value = 29,
                }, 
                new global::MemberEntry() {
                    EmitDefaultValue = true,
                    NameIndex = 1125, // R8G8B8A8_UInt
                    Value = 30,
                }, 
                new global::MemberEntry() {
                    EmitDefaultValue = true,
                    NameIndex = 1139, // R8G8B8A8_SNorm
                    Value = 31,
                }, 
                new global::MemberEntry() {
                    EmitDefaultValue = true,
                    NameIndex = 1154, // R8G8B8A8_SInt
                    Value = 32,
                }, 
                new global::MemberEntry() {
                    EmitDefaultValue = true,
                    NameIndex = 1168, // R16G16_Typeless
                    Value = 33,
                }, 
                new global::MemberEntry() {
                    EmitDefaultValue = true,
                    NameIndex = 1184, // R16G16_Float
                    Value = 34,
                }, 
                new global::MemberEntry() {
                    EmitDefaultValue = true,
                    NameIndex = 1197, // R16G16_UNorm
                    Value = 35,
                }, 
                new global::MemberEntry() {
                    EmitDefaultValue = true,
                    NameIndex = 1210, // R16G16_UInt
                    Value = 36,
                }, 
                new global::MemberEntry() {
                    EmitDefaultValue = true,
                    NameIndex = 1222, // R16G16_SNorm
                    Value = 37,
                }, 
                new global::MemberEntry() {
                    EmitDefaultValue = true,
                    NameIndex = 1235, // R16G16_SInt
                    Value = 38,
                }, 
                new global::MemberEntry() {
                    EmitDefaultValue = true,
                    NameIndex = 1247, // R32_Typeless
                    Value = 39,
                }, 
                new global::MemberEntry() {
                    EmitDefaultValue = true,
                    NameIndex = 1260, // D32_Float
                    Value = 40,
                }, 
                new global::MemberEntry() {
                    EmitDefaultValue = true,
                    NameIndex = 1270, // R32_Float
                    Value = 41,
                }, 
                new global::MemberEntry() {
                    EmitDefaultValue = true,
                    NameIndex = 1280, // R32_UInt
                    Value = 42,
                }, 
                new global::MemberEntry() {
                    EmitDefaultValue = true,
                    NameIndex = 1289, // R32_SInt
                    Value = 43,
                }, 
                new global::MemberEntry() {
                    EmitDefaultValue = true,
                    NameIndex = 1298, // R24G8_Typeless
                    Value = 44,
                }, 
                new global::MemberEntry() {
                    EmitDefaultValue = true,
                    NameIndex = 1313, // D24_UNorm_S8_UInt
                    Value = 45,
                }, 
                new global::MemberEntry() {
                    EmitDefaultValue = true,
                    NameIndex = 1331, // R24_UNorm_X8_Typeless
                    Value = 46,
                }, 
                new global::MemberEntry() {
                    EmitDefaultValue = true,
                    NameIndex = 1353, // X24_Typeless_G8_UInt
                    Value = 47,
                }, 
                new global::MemberEntry() {
                    EmitDefaultValue = true,
                    NameIndex = 1374, // R8G8_Typeless
                    Value = 48,
                }, 
                new global::MemberEntry() {
                    EmitDefaultValue = true,
                    NameIndex = 1388, // R8G8_UNorm
                    Value = 49,
                }, 
                new global::MemberEntry() {
                    EmitDefaultValue = true,
                    NameIndex = 1399, // R8G8_UInt
                    Value = 50,
                }, 
                new global::MemberEntry() {
                    EmitDefaultValue = true,
                    NameIndex = 1409, // R8G8_SNorm
                    Value = 51,
                }, 
                new global::MemberEntry() {
                    EmitDefaultValue = true,
                    NameIndex = 1420, // R8G8_SInt
                    Value = 52,
                }, 
                new global::MemberEntry() {
                    EmitDefaultValue = true,
                    NameIndex = 1430, // R16_Typeless
                    Value = 53,
                }, 
                new global::MemberEntry() {
                    EmitDefaultValue = true,
                    NameIndex = 1443, // R16_Float
                    Value = 54,
                }, 
                new global::MemberEntry() {
                    EmitDefaultValue = true,
                    NameIndex = 1453, // D16_UNorm
                    Value = 55,
                }, 
                new global::MemberEntry() {
                    EmitDefaultValue = true,
                    NameIndex = 1463, // R16_UNorm
                    Value = 56,
                }, 
                new global::MemberEntry() {
                    EmitDefaultValue = true,
                    NameIndex = 1473, // R16_UInt
                    Value = 57,
                }, 
                new global::MemberEntry() {
                    EmitDefaultValue = true,
                    NameIndex = 1482, // R16_SNorm
                    Value = 58,
                }, 
                new global::MemberEntry() {
                    EmitDefaultValue = true,
                    NameIndex = 1492, // R16_SInt
                    Value = 59,
                }, 
                new global::MemberEntry() {
                    EmitDefaultValue = true,
                    NameIndex = 1501, // R8_Typeless
                    Value = 60,
                }, 
                new global::MemberEntry() {
                    EmitDefaultValue = true,
                    NameIndex = 1513, // R8_UNorm
                    Value = 61,
                }, 
                new global::MemberEntry() {
                    EmitDefaultValue = true,
                    NameIndex = 1522, // R8_UInt
                    Value = 62,
                }, 
                new global::MemberEntry() {
                    EmitDefaultValue = true,
                    NameIndex = 1530, // R8_SNorm
                    Value = 63,
                }, 
                new global::MemberEntry() {
                    EmitDefaultValue = true,
                    NameIndex = 1539, // R8_SInt
                    Value = 64,
                }, 
                new global::MemberEntry() {
                    EmitDefaultValue = true,
                    NameIndex = 1547, // A8_UNorm
                    Value = 65,
                }, 
                new global::MemberEntry() {
                    EmitDefaultValue = true,
                    NameIndex = 1556, // R1_UNorm
                    Value = 66,
                }, 
                new global::MemberEntry() {
                    EmitDefaultValue = true,
                    NameIndex = 1565, // R9G9B9E5_Sharedexp
                    Value = 67,
                }, 
                new global::MemberEntry() {
                    EmitDefaultValue = true,
                    NameIndex = 1584, // R8G8_B8G8_UNorm
                    Value = 68,
                }, 
                new global::MemberEntry() {
                    EmitDefaultValue = true,
                    NameIndex = 1600, // G8R8_G8B8_UNorm
                    Value = 69,
                }, 
                new global::MemberEntry() {
                    EmitDefaultValue = true,
                    NameIndex = 1616, // BC1_Typeless
                    Value = 70,
                }, 
                new global::MemberEntry() {
                    EmitDefaultValue = true,
                    NameIndex = 1629, // BC1_UNorm
                    Value = 71,
                }, 
                new global::MemberEntry() {
                    EmitDefaultValue = true,
                    NameIndex = 1639, // BC1_UNorm_SRgb
                    Value = 72,
                }, 
                new global::MemberEntry() {
                    EmitDefaultValue = true,
                    NameIndex = 1654, // BC2_Typeless
                    Value = 73,
                }, 
                new global::MemberEntry() {
                    EmitDefaultValue = true,
                    NameIndex = 1667, // BC2_UNorm
                    Value = 74,
                }, 
                new global::MemberEntry() {
                    EmitDefaultValue = true,
                    NameIndex = 1677, // BC2_UNorm_SRgb
                    Value = 75,
                }, 
                new global::MemberEntry() {
                    EmitDefaultValue = true,
                    NameIndex = 1692, // BC3_Typeless
                    Value = 76,
                }, 
                new global::MemberEntry() {
                    EmitDefaultValue = true,
                    NameIndex = 1705, // BC3_UNorm
                    Value = 77,
                }, 
                new global::MemberEntry() {
                    EmitDefaultValue = true,
                    NameIndex = 1715, // BC3_UNorm_SRgb
                    Value = 78,
                }, 
                new global::MemberEntry() {
                    EmitDefaultValue = true,
                    NameIndex = 1730, // BC4_Typeless
                    Value = 79,
                }, 
                new global::MemberEntry() {
                    EmitDefaultValue = true,
                    NameIndex = 1743, // BC4_UNorm
                    Value = 80,
                }, 
                new global::MemberEntry() {
                    EmitDefaultValue = true,
                    NameIndex = 1753, // BC4_SNorm
                    Value = 81,
                }, 
                new global::MemberEntry() {
                    EmitDefaultValue = true,
                    NameIndex = 1763, // BC5_Typeless
                    Value = 82,
                }, 
                new global::MemberEntry() {
                    EmitDefaultValue = true,
                    NameIndex = 1776, // BC5_UNorm
                    Value = 83,
                }, 
                new global::MemberEntry() {
                    EmitDefaultValue = true,
                    NameIndex = 1786, // BC5_SNorm
                    Value = 84,
                }, 
                new global::MemberEntry() {
                    EmitDefaultValue = true,
                    NameIndex = 1796, // B5G6R5_UNorm
                    Value = 85,
                }, 
                new global::MemberEntry() {
                    EmitDefaultValue = true,
                    NameIndex = 1809, // B5G5R5A1_UNorm
                    Value = 86,
                }, 
                new global::MemberEntry() {
                    EmitDefaultValue = true,
                    NameIndex = 1824, // B8G8R8A8_UNorm
                    Value = 87,
                }, 
                new global::MemberEntry() {
                    EmitDefaultValue = true,
                    NameIndex = 1839, // B8G8R8X8_UNorm
                    Value = 88,
                }, 
                new global::MemberEntry() {
                    EmitDefaultValue = true,
                    NameIndex = 1854, // R10G10B10_Xr_Bias_A2_UNorm
                    Value = 89,
                }, 
                new global::MemberEntry() {
                    EmitDefaultValue = true,
                    NameIndex = 1881, // B8G8R8A8_Typeless
                    Value = 90,
                }, 
                new global::MemberEntry() {
                    EmitDefaultValue = true,
                    NameIndex = 1899, // B8G8R8A8_UNorm_SRgb
                    Value = 91,
                }, 
                new global::MemberEntry() {
                    EmitDefaultValue = true,
                    NameIndex = 1919, // B8G8R8X8_Typeless
                    Value = 92,
                }, 
                new global::MemberEntry() {
                    EmitDefaultValue = true,
                    NameIndex = 1937, // B8G8R8X8_UNorm_SRgb
                    Value = 93,
                }, 
                new global::MemberEntry() {
                    EmitDefaultValue = true,
                    NameIndex = 1957, // BC6H_Typeless
                    Value = 94,
                }, 
                new global::MemberEntry() {
                    EmitDefaultValue = true,
                    NameIndex = 1971, // BC6H_Uf16
                    Value = 95,
                }, 
                new global::MemberEntry() {
                    EmitDefaultValue = true,
                    NameIndex = 1981, // BC6H_Sf16
                    Value = 96,
                }, 
                new global::MemberEntry() {
                    EmitDefaultValue = true,
                    NameIndex = 1991, // BC7_Typeless
                    Value = 97,
                }, 
                new global::MemberEntry() {
                    EmitDefaultValue = true,
                    NameIndex = 2004, // BC7_UNorm
                    Value = 98,
                }, 
                new global::MemberEntry() {
                    EmitDefaultValue = true,
                    NameIndex = 2014, // BC7_UNorm_SRgb
                    Value = 99,
                }, 
                new global::MemberEntry() {
                    EmitDefaultValue = true,
                    NameIndex = 2029, // AYUV
                    Value = 100,
                }, 
                new global::MemberEntry() {
                    EmitDefaultValue = true,
                    NameIndex = 2034, // Y410
                    Value = 101,
                }, 
                new global::MemberEntry() {
                    EmitDefaultValue = true,
                    NameIndex = 2039, // Y416
                    Value = 102,
                }, 
                new global::MemberEntry() {
                    EmitDefaultValue = true,
                    NameIndex = 2044, // NV12
                    Value = 103,
                }, 
                new global::MemberEntry() {
                    EmitDefaultValue = true,
                    NameIndex = 2049, // P010
                    Value = 104,
                }, 
                new global::MemberEntry() {
                    EmitDefaultValue = true,
                    NameIndex = 2054, // P016
                    Value = 105,
                }, 
                new global::MemberEntry() {
                    EmitDefaultValue = true,
                    NameIndex = 2059, // Opaque420
                    Value = 106,
                }, 
                new global::MemberEntry() {
                    EmitDefaultValue = true,
                    NameIndex = 2069, // YUY2
                    Value = 107,
                }, 
                new global::MemberEntry() {
                    EmitDefaultValue = true,
                    NameIndex = 2074, // Y210
                    Value = 108,
                }, 
                new global::MemberEntry() {
                    EmitDefaultValue = true,
                    NameIndex = 2079, // Y216
                    Value = 109,
                }, 
                new global::MemberEntry() {
                    EmitDefaultValue = true,
                    NameIndex = 2084, // NV11
                    Value = 110,
                }, 
                new global::MemberEntry() {
                    EmitDefaultValue = true,
                    NameIndex = 2089, // AI44
                    Value = 111,
                }, 
                new global::MemberEntry() {
                    EmitDefaultValue = true,
                    NameIndex = 2094, // IA44
                    Value = 112,
                }, 
                new global::MemberEntry() {
                    EmitDefaultValue = true,
                    NameIndex = 2099, // P8
                    Value = 113,
                }, 
                new global::MemberEntry() {
                    EmitDefaultValue = true,
                    NameIndex = 2102, // A8P8
                    Value = 114,
                }, 
                new global::MemberEntry() {
                    EmitDefaultValue = true,
                    NameIndex = 2107, // B4G4R4A4_UNorm
                    Value = 115,
                }, 
                new global::MemberEntry() {
                    EmitDefaultValue = true,
                    NameIndex = 2122, // P208
                    Value = 130,
                }, 
                new global::MemberEntry() {
                    EmitDefaultValue = true,
                    NameIndex = 2127, // V208
                    Value = 131,
                }, 
                new global::MemberEntry() {
                    EmitDefaultValue = true,
                    NameIndex = 2132, // V408
                    Value = 132,
                }, 
                new global::MemberEntry() {
                    EmitDefaultValue = true,
                    NameIndex = 2653, // None
                }, 
                new global::MemberEntry() {
                    EmitDefaultValue = true,
                    NameIndex = 2658, // Vertex
                    Value = 1,
                }, 
                new global::MemberEntry() {
                    EmitDefaultValue = true,
                    NameIndex = 2665, // Hull
                    Value = 4,
                }, 
                new global::MemberEntry() {
                    EmitDefaultValue = true,
                    NameIndex = 2670, // Domain
                    Value = 8,
                }, 
                new global::MemberEntry() {
                    EmitDefaultValue = true,
                    NameIndex = 2677, // Geometry
                    Value = 16,
                }, 
                new global::MemberEntry() {
                    EmitDefaultValue = true,
                    NameIndex = 2686, // Pixel
                    Value = 32,
                }, 
                new global::MemberEntry() {
                    EmitDefaultValue = true,
                    NameIndex = 2692, // Compute
                    Value = 64,
                }, 
                new global::MemberEntry() {
                    EmitDefaultValue = true,
                    NameIndex = 2775, // Undefined
                }, 
                new global::MemberEntry() {
                    EmitDefaultValue = true,
                    NameIndex = 2785, // PointList
                    Value = 1,
                }, 
                new global::MemberEntry() {
                    EmitDefaultValue = true,
                    NameIndex = 2795, // LineList
                    Value = 2,
                }, 
                new global::MemberEntry() {
                    EmitDefaultValue = true,
                    NameIndex = 2804, // LineStrip
                    Value = 3,
                }, 
                new global::MemberEntry() {
                    EmitDefaultValue = true,
                    NameIndex = 2814, // TriangleList
                    Value = 4,
                }, 
                new global::MemberEntry() {
                    EmitDefaultValue = true,
                    NameIndex = 2827, // TriangleStrip
                    Value = 5,
                }, 
                new global::MemberEntry() {
                    EmitDefaultValue = true,
                    NameIndex = 2841, // LineListWithAdjacency
                    Value = 10,
                }, 
                new global::MemberEntry() {
                    EmitDefaultValue = true,
                    NameIndex = 2863, // LineStripWithAdjacency
                    Value = 11,
                }, 
                new global::MemberEntry() {
                    EmitDefaultValue = true,
                    NameIndex = 2886, // TriangleListWithAdjacency
                    Value = 12,
                }, 
                new global::MemberEntry() {
                    EmitDefaultValue = true,
                    NameIndex = 2912, // TriangleStripWithAdjacency
                    Value = 13,
                }, 
                new global::MemberEntry() {
                    EmitDefaultValue = true,
                    NameIndex = 2939, // PatchListWith1ControlPoints
                    Value = 33,
                }, 
                new global::MemberEntry() {
                    EmitDefaultValue = true,
                    NameIndex = 2967, // PatchListWith2ControlPoints
                    Value = 34,
                }, 
                new global::MemberEntry() {
                    EmitDefaultValue = true,
                    NameIndex = 2995, // PatchListWith3ControlPoints
                    Value = 35,
                }, 
                new global::MemberEntry() {
                    EmitDefaultValue = true,
                    NameIndex = 3023, // PatchListWith4ControlPoints
                    Value = 36,
                }, 
                new global::MemberEntry() {
                    EmitDefaultValue = true,
                    NameIndex = 3051, // PatchListWith5ControlPoints
                    Value = 37,
                }, 
                new global::MemberEntry() {
                    EmitDefaultValue = true,
                    NameIndex = 3079, // PatchListWith6ControlPoints
                    Value = 38,
                }, 
                new global::MemberEntry() {
                    EmitDefaultValue = true,
                    NameIndex = 3107, // PatchListWith7ControlPoints
                    Value = 39,
                }, 
                new global::MemberEntry() {
                    EmitDefaultValue = true,
                    NameIndex = 3135, // PatchListWith8ControlPoints
                    Value = 40,
                }, 
                new global::MemberEntry() {
                    EmitDefaultValue = true,
                    NameIndex = 3163, // PatchListWith9ControlPoints
                    Value = 41,
                }, 
                new global::MemberEntry() {
                    EmitDefaultValue = true,
                    NameIndex = 3191, // PatchListWith10ControlPoints
                    Value = 42,
                }, 
                new global::MemberEntry() {
                    EmitDefaultValue = true,
                    NameIndex = 3220, // PatchListWith11ControlPoints
                    Value = 43,
                }, 
                new global::MemberEntry() {
                    EmitDefaultValue = true,
                    NameIndex = 3249, // PatchListWith12ControlPoints
                    Value = 44,
                }, 
                new global::MemberEntry() {
                    EmitDefaultValue = true,
                    NameIndex = 3278, // PatchListWith13ControlPoints
                    Value = 45,
                }, 
                new global::MemberEntry() {
                    EmitDefaultValue = true,
                    NameIndex = 3307, // PatchListWith14ControlPoints
                    Value = 46,
                }, 
                new global::MemberEntry() {
                    EmitDefaultValue = true,
                    NameIndex = 3336, // PatchListWith15ControlPoints
                    Value = 47,
                }, 
                new global::MemberEntry() {
                    EmitDefaultValue = true,
                    NameIndex = 3365, // PatchListWith16ControlPoints
                    Value = 48,
                }, 
                new global::MemberEntry() {
                    EmitDefaultValue = true,
                    NameIndex = 3394, // PatchListWith17ControlPoints
                    Value = 49,
                }, 
                new global::MemberEntry() {
                    EmitDefaultValue = true,
                    NameIndex = 3423, // PatchListWith18ControlPoints
                    Value = 50,
                }, 
                new global::MemberEntry() {
                    EmitDefaultValue = true,
                    NameIndex = 3452, // PatchListWith19ControlPoints
                    Value = 51,
                }, 
                new global::MemberEntry() {
                    EmitDefaultValue = true,
                    NameIndex = 3481, // PatchListWith20ControlPoints
                    Value = 52,
                }, 
                new global::MemberEntry() {
                    EmitDefaultValue = true,
                    NameIndex = 3510, // PatchListWith21ControlPoints
                    Value = 53,
                }, 
                new global::MemberEntry() {
                    EmitDefaultValue = true,
                    NameIndex = 3539, // PatchListWith22ControlPoints
                    Value = 54,
                }, 
                new global::MemberEntry() {
                    EmitDefaultValue = true,
                    NameIndex = 3568, // PatchListWith23ControlPoints
                    Value = 55,
                }, 
                new global::MemberEntry() {
                    EmitDefaultValue = true,
                    NameIndex = 3597, // PatchListWith24ControlPoints
                    Value = 56,
                }, 
                new global::MemberEntry() {
                    EmitDefaultValue = true,
                    NameIndex = 3626, // PatchListWith25ControlPoints
                    Value = 57,
                }, 
                new global::MemberEntry() {
                    EmitDefaultValue = true,
                    NameIndex = 3655, // PatchListWith26ControlPoints
                    Value = 58,
                }, 
                new global::MemberEntry() {
                    EmitDefaultValue = true,
                    NameIndex = 3684, // PatchListWith27ControlPoints
                    Value = 59,
                }, 
                new global::MemberEntry() {
                    EmitDefaultValue = true,
                    NameIndex = 3713, // PatchListWith28ControlPoints
                    Value = 60,
                }, 
                new global::MemberEntry() {
                    EmitDefaultValue = true,
                    NameIndex = 3742, // PatchListWith29ControlPoints
                    Value = 61,
                }, 
                new global::MemberEntry() {
                    EmitDefaultValue = true,
                    NameIndex = 3771, // PatchListWith30ControlPoints
                    Value = 62,
                }, 
                new global::MemberEntry() {
                    EmitDefaultValue = true,
                    NameIndex = 3800, // PatchListWith31ControlPoints
                    Value = 63,
                }, 
                new global::MemberEntry() {
                    EmitDefaultValue = true,
                    NameIndex = 3829, // PatchListWith32ControlPoints
                    Value = 64,
                }
        };
        static readonly byte[] s_dataContractMap_Hashtable = null;
        // Count=77
        [global::System.Runtime.CompilerServices.PreInitialized]
        static readonly global::DataContractMapEntry[] s_dataContractMap = new global::DataContractMapEntry[] {
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Boolean, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                    TableIndex = 0, // 0x0
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Nullable`1[[System.Boolean, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089]]" +
                                ", mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                    TableIndex = 0, // 0x0
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Byte[], mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                    TableIndex = 16, // 0x10
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Char, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                    TableIndex = 32, // 0x20
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Nullable`1[[System.Char, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089]], m" +
                                "scorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                    TableIndex = 32, // 0x20
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.DateTime, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                    TableIndex = 48, // 0x30
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Nullable`1[[System.DateTime, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089]" +
                                "], mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                    TableIndex = 48, // 0x30
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Decimal, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                    TableIndex = 64, // 0x40
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Nullable`1[[System.Decimal, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089]]" +
                                ", mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                    TableIndex = 64, // 0x40
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Double, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                    TableIndex = 80, // 0x50
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Nullable`1[[System.Double, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089]]," +
                                " mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                    TableIndex = 80, // 0x50
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Single, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                    TableIndex = 96, // 0x60
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Nullable`1[[System.Single, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089]]," +
                                " mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                    TableIndex = 96, // 0x60
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Guid, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                    TableIndex = 112, // 0x70
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Nullable`1[[System.Guid, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089]], m" +
                                "scorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                    TableIndex = 112, // 0x70
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Int32, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                    TableIndex = 128, // 0x80
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Nullable`1[[System.Int32, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089]], " +
                                "mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                    TableIndex = 128, // 0x80
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Int64, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                    TableIndex = 144, // 0x90
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Nullable`1[[System.Int64, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089]], " +
                                "mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                    TableIndex = 144, // 0x90
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Object, System.Private.CoreLib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                    TableIndex = 160, // 0xa0
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Xml.XmlQualifiedName, System.Private.Xml, Version=4.0.1.0, Culture=neutral, PublicKeyToken=cc7b13ffcd2ddd" +
                                "51")),
                    TableIndex = 176, // 0xb0
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Int16, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                    TableIndex = 192, // 0xc0
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Nullable`1[[System.Int16, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089]], " +
                                "mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                    TableIndex = 192, // 0xc0
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.SByte, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                    TableIndex = 208, // 0xd0
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Nullable`1[[System.SByte, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089]], " +
                                "mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                    TableIndex = 208, // 0xd0
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.String, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                    TableIndex = 224, // 0xe0
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.TimeSpan, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                    TableIndex = 240, // 0xf0
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Nullable`1[[System.TimeSpan, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089]" +
                                "], mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                    TableIndex = 240, // 0xf0
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Byte, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                    TableIndex = 256, // 0x100
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Nullable`1[[System.Byte, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089]], m" +
                                "scorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                    TableIndex = 256, // 0x100
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.UInt32, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                    TableIndex = 272, // 0x110
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Nullable`1[[System.UInt32, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089]]," +
                                " mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                    TableIndex = 272, // 0x110
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.UInt64, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                    TableIndex = 288, // 0x120
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Nullable`1[[System.UInt64, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089]]," +
                                " mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                    TableIndex = 288, // 0x120
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.UInt16, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                    TableIndex = 304, // 0x130
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Nullable`1[[System.UInt16, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089]]," +
                                " mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                    TableIndex = 304, // 0x130
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Uri, System.Private.Uri, Version=4.0.5.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                    TableIndex = 320, // 0x140
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf(@"System.Collections.Generic.List`1[[HelixToolkit.UWP.Shaders.TechniqueDescription, HelixToolkit.UWP, Version=2.10.0.0, Culture=neutral, PublicKeyToken=52aa3500039caf0d]], System.Private.CoreLib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                    TableIndex = 2, // 0x2
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("HelixToolkit.UWP.Shaders.TechniqueDescription, HelixToolkit.UWP, Version=2.10.0.0, Culture=neutral, PublicKeyTok" +
                                "en=52aa3500039caf0d")),
                    TableIndex = 1, // 0x1
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("HelixToolkit.UWP.Shaders.InputLayoutDescription, HelixToolkit.UWP, Version=2.10.0.0, Culture=neutral, PublicKeyT" +
                                "oken=52aa3500039caf0d")),
                    TableIndex = 17, // 0x11
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("SharpDX.Direct3D11.InputElement[], SharpDX.Direct3D11, Version=4.2.0.0, Culture=neutral, PublicKeyToken=b4dcf0f3" +
                                "5e5521f1")),
                    TableIndex = 18, // 0x12
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("SharpDX.Direct3D11.InputElement, SharpDX.Direct3D11, Version=4.2.0.0, Culture=neutral, PublicKeyToken=b4dcf0f35e" +
                                "5521f1")),
                    TableIndex = 33, // 0x21
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Nullable`1[[SharpDX.Direct3D11.InputElement, SharpDX.Direct3D11, Version=4.2.0.0, Culture=neutral, Public" +
                                "KeyToken=b4dcf0f35e5521f1]], mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                    TableIndex = 33, // 0x21
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("SharpDX.Direct3D11.InputClassification, SharpDX.Direct3D11, Version=4.2.0.0, Culture=neutral, PublicKeyToken=b4d" +
                                "cf0f35e5521f1")),
                    TableIndex = 3, // 0x3
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Nullable`1[[SharpDX.Direct3D11.InputClassification, SharpDX.Direct3D11, Version=4.2.0.0, Culture=neutral," +
                                " PublicKeyToken=b4dcf0f35e5521f1]], mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" +
                                "")),
                    TableIndex = 3, // 0x3
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("SharpDX.DXGI.Format, SharpDX.DXGI, Version=4.2.0.0, Culture=neutral, PublicKeyToken=b4dcf0f35e5521f1")),
                    TableIndex = 19, // 0x13
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Nullable`1[[SharpDX.DXGI.Format, SharpDX.DXGI, Version=4.2.0.0, Culture=neutral, PublicKeyToken=b4dcf0f35" +
                                "e5521f1]], mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                    TableIndex = 19, // 0x13
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf(@"System.Collections.Generic.IList`1[[HelixToolkit.UWP.Shaders.ShaderPassDescription, HelixToolkit.UWP, Version=2.10.0.0, Culture=neutral, PublicKeyToken=52aa3500039caf0d]], System.Private.CoreLib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                    TableIndex = 34, // 0x22
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("HelixToolkit.UWP.Shaders.ShaderPassDescription, HelixToolkit.UWP, Version=2.10.0.0, Culture=neutral, PublicKeyTo" +
                                "ken=52aa3500039caf0d")),
                    TableIndex = 49, // 0x31
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("SharpDX.Color4, SharpDX.Mathematics, Version=4.2.0.0, Culture=neutral, PublicKeyToken=b4dcf0f35e5521f1")),
                    TableIndex = 65, // 0x41
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Nullable`1[[SharpDX.Color4, SharpDX.Mathematics, Version=4.2.0.0, Culture=neutral, PublicKeyToken=b4dcf0f" +
                                "35e5521f1]], mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                    TableIndex = 65, // 0x41
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("HelixToolkit.UWP.Shaders.BlendStateDataContract, HelixToolkit.UWP, Version=2.10.0.0, Culture=neutral, PublicKeyT" +
                                "oken=52aa3500039caf0d")),
                    TableIndex = 81, // 0x51
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Nullable`1[[HelixToolkit.UWP.Shaders.BlendStateDataContract, HelixToolkit.UWP, Version=2.10.0.0, Culture=" +
                                "neutral, PublicKeyToken=52aa3500039caf0d]], mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c56" +
                                "1934e089")),
                    TableIndex = 81, // 0x51
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("HelixToolkit.UWP.Shaders.RenderTargetBlendDataContract[], HelixToolkit.UWP, Version=2.10.0.0, Culture=neutral, P" +
                                "ublicKeyToken=52aa3500039caf0d")),
                    TableIndex = 50, // 0x32
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("HelixToolkit.UWP.Shaders.RenderTargetBlendDataContract, HelixToolkit.UWP, Version=2.10.0.0, Culture=neutral, Pub" +
                                "licKeyToken=52aa3500039caf0d")),
                    TableIndex = 97, // 0x61
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Nullable`1[[HelixToolkit.UWP.Shaders.RenderTargetBlendDataContract, HelixToolkit.UWP, Version=2.10.0.0, C" +
                                "ulture=neutral, PublicKeyToken=52aa3500039caf0d]], mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b" +
                                "77a5c561934e089")),
                    TableIndex = 97, // 0x61
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("HelixToolkit.UWP.Shaders.DepthStencilStateDataContract, HelixToolkit.UWP, Version=2.10.0.0, Culture=neutral, Pub" +
                                "licKeyToken=52aa3500039caf0d")),
                    TableIndex = 113, // 0x71
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Nullable`1[[HelixToolkit.UWP.Shaders.DepthStencilStateDataContract, HelixToolkit.UWP, Version=2.10.0.0, C" +
                                "ulture=neutral, PublicKeyToken=52aa3500039caf0d]], mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b" +
                                "77a5c561934e089")),
                    TableIndex = 113, // 0x71
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("HelixToolkit.UWP.Shaders.DepthStencilOperationDataContract, HelixToolkit.UWP, Version=2.10.0.0, Culture=neutral," +
                                " PublicKeyToken=52aa3500039caf0d")),
                    TableIndex = 129, // 0x81
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Nullable`1[[HelixToolkit.UWP.Shaders.DepthStencilOperationDataContract, HelixToolkit.UWP, Version=2.10.0." +
                                "0, Culture=neutral, PublicKeyToken=52aa3500039caf0d]], mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyTok" +
                                "en=b77a5c561934e089")),
                    TableIndex = 129, // 0x81
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("HelixToolkit.UWP.Shaders.RasterizerStateDataContract, HelixToolkit.UWP, Version=2.10.0.0, Culture=neutral, Publi" +
                                "cKeyToken=52aa3500039caf0d")),
                    TableIndex = 145, // 0x91
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Nullable`1[[HelixToolkit.UWP.Shaders.RasterizerStateDataContract, HelixToolkit.UWP, Version=2.10.0.0, Cul" +
                                "ture=neutral, PublicKeyToken=52aa3500039caf0d]], mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77" +
                                "a5c561934e089")),
                    TableIndex = 145, // 0x91
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf(@"System.Collections.Generic.IList`1[[HelixToolkit.UWP.Shaders.ShaderDescription, HelixToolkit.UWP, Version=2.10.0.0, Culture=neutral, PublicKeyToken=52aa3500039caf0d]], System.Private.CoreLib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                    TableIndex = 66, // 0x42
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("HelixToolkit.UWP.Shaders.ShaderDescription, HelixToolkit.UWP, Version=2.10.0.0, Culture=neutral, PublicKeyToken=" +
                                "52aa3500039caf0d")),
                    TableIndex = 161, // 0xa1
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("SharpDX.Direct3D11.StreamOutputElement[], SharpDX.Direct3D11, Version=4.2.0.0, Culture=neutral, PublicKeyToken=b" +
                                "4dcf0f35e5521f1")),
                    TableIndex = 82, // 0x52
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("SharpDX.Direct3D11.StreamOutputElement, SharpDX.Direct3D11, Version=4.2.0.0, Culture=neutral, PublicKeyToken=b4d" +
                                "cf0f35e5521f1")),
                    TableIndex = 177, // 0xb1
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Nullable`1[[SharpDX.Direct3D11.StreamOutputElement, SharpDX.Direct3D11, Version=4.2.0.0, Culture=neutral," +
                                " PublicKeyToken=b4dcf0f35e5521f1]], mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" +
                                "")),
                    TableIndex = 177, // 0xb1
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Int32[], System.Private.CoreLib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                    TableIndex = 98, // 0x62
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("HelixToolkit.UWP.ShaderStage, HelixToolkit.UWP, Version=2.10.0.0, Culture=neutral, PublicKeyToken=52aa3500039caf" +
                                "0d")),
                    TableIndex = 35, // 0x23
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Nullable`1[[HelixToolkit.UWP.ShaderStage, HelixToolkit.UWP, Version=2.10.0.0, Culture=neutral, PublicKeyT" +
                                "oken=52aa3500039caf0d]], mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                    TableIndex = 35, // 0x23
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("SharpDX.Direct3D.PrimitiveTopology, SharpDX, Version=4.2.0.0, Culture=neutral, PublicKeyToken=b4dcf0f35e5521f1")),
                    TableIndex = 51, // 0x33
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Nullable`1[[SharpDX.Direct3D.PrimitiveTopology, SharpDX, Version=4.2.0.0, Culture=neutral, PublicKeyToken" +
                                "=b4dcf0f35e5521f1]], mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                    TableIndex = 51, // 0x33
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf(@"System.Collections.Generic.IDictionary`2[[System.String, System.Private.CoreLib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a],[System.Object, System.Private.CoreLib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a]], System.Private.CoreLib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                    TableIndex = 114, // 0x72
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf(@"System.Runtime.Serialization.KeyValue`2[[System.String, System.Private.CoreLib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a],[System.Object, System.Private.CoreLib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a]], System.Private.DataContractSerialization, Version=4.1.4.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                    TableIndex = 193, // 0xc1
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf(@"System.Nullable`1[[System.Runtime.Serialization.KeyValue`2[[System.String, System.Private.CoreLib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a],[System.Object, System.Private.CoreLib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a]], System.Private.DataContractSerialization, Version=4.1.4.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a]], mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                    TableIndex = 193, // 0xc1
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf(@"System.Collections.Generic.KeyValuePair`2[[System.String, System.Private.CoreLib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a],[System.Object, System.Private.CoreLib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a]], System.Private.CoreLib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                    TableIndex = 209, // 0xd1
                }, 
                new global::DataContractMapEntry() {
                    UserCodeType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf(@"System.Nullable`1[[System.Collections.Generic.KeyValuePair`2[[System.String, System.Private.CoreLib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a],[System.Object, System.Private.CoreLib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a]], System.Private.CoreLib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a]], mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                    TableIndex = 209, // 0xd1
                }
        };
        static readonly byte[] s_dataContracts_Hashtable = null;
        // Count=21
        [global::System.Runtime.CompilerServices.PreInitialized]
        static readonly global::DataContractEntry[] s_dataContracts = new global::DataContractEntry[] {
                new global::DataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        IsBuiltInDataContract = true,
                        IsValueType = true,
                        NameIndex = 0, // boolean
                        NamespaceIndex = 8, // http://www.w3.org/2001/XMLSchema
                        StableNameIndex = 0, // boolean
                        StableNameNamespaceIndex = 8, // http://www.w3.org/2001/XMLSchema
                        TopLevelElementNameIndex = 0, // boolean
                        TopLevelElementNamespaceIndex = 41, // http://schemas.microsoft.com/2003/10/Serialization/
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Boolean, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Boolean, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                    },
                    Kind = global::DataContractKind.BooleanDataContract,
                }, 
                new global::DataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        IsBuiltInDataContract = true,
                        NameIndex = 93, // base64Binary
                        NamespaceIndex = 8, // http://www.w3.org/2001/XMLSchema
                        StableNameIndex = 93, // base64Binary
                        StableNameNamespaceIndex = 8, // http://www.w3.org/2001/XMLSchema
                        TopLevelElementNameIndex = 93, // base64Binary
                        TopLevelElementNamespaceIndex = 41, // http://schemas.microsoft.com/2003/10/Serialization/
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Byte[], mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Byte[], mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                    },
                    Kind = global::DataContractKind.ByteArrayDataContract,
                }, 
                new global::DataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        IsBuiltInDataContract = true,
                        IsValueType = true,
                        NameIndex = 106, // char
                        NamespaceIndex = 41, // http://schemas.microsoft.com/2003/10/Serialization/
                        StableNameIndex = 106, // char
                        StableNameNamespaceIndex = 41, // http://schemas.microsoft.com/2003/10/Serialization/
                        TopLevelElementNameIndex = 106, // char
                        TopLevelElementNamespaceIndex = 41, // http://schemas.microsoft.com/2003/10/Serialization/
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Char, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Char, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                    },
                    Kind = global::DataContractKind.CharDataContract,
                }, 
                new global::DataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        IsBuiltInDataContract = true,
                        IsValueType = true,
                        NameIndex = 111, // dateTime
                        NamespaceIndex = 8, // http://www.w3.org/2001/XMLSchema
                        StableNameIndex = 111, // dateTime
                        StableNameNamespaceIndex = 8, // http://www.w3.org/2001/XMLSchema
                        TopLevelElementNameIndex = 111, // dateTime
                        TopLevelElementNamespaceIndex = 41, // http://schemas.microsoft.com/2003/10/Serialization/
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.DateTime, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.DateTime, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                    },
                    Kind = global::DataContractKind.DateTimeDataContract,
                }, 
                new global::DataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        IsBuiltInDataContract = true,
                        IsValueType = true,
                        NameIndex = 120, // decimal
                        NamespaceIndex = 8, // http://www.w3.org/2001/XMLSchema
                        StableNameIndex = 120, // decimal
                        StableNameNamespaceIndex = 8, // http://www.w3.org/2001/XMLSchema
                        TopLevelElementNameIndex = 120, // decimal
                        TopLevelElementNamespaceIndex = 41, // http://schemas.microsoft.com/2003/10/Serialization/
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Decimal, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Decimal, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                    },
                    Kind = global::DataContractKind.DecimalDataContract,
                }, 
                new global::DataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        IsBuiltInDataContract = true,
                        IsValueType = true,
                        NameIndex = 128, // double
                        NamespaceIndex = 8, // http://www.w3.org/2001/XMLSchema
                        StableNameIndex = 128, // double
                        StableNameNamespaceIndex = 8, // http://www.w3.org/2001/XMLSchema
                        TopLevelElementNameIndex = 128, // double
                        TopLevelElementNamespaceIndex = 41, // http://schemas.microsoft.com/2003/10/Serialization/
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Double, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Double, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                    },
                    Kind = global::DataContractKind.DoubleDataContract,
                }, 
                new global::DataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        IsBuiltInDataContract = true,
                        IsValueType = true,
                        NameIndex = 135, // float
                        NamespaceIndex = 8, // http://www.w3.org/2001/XMLSchema
                        StableNameIndex = 135, // float
                        StableNameNamespaceIndex = 8, // http://www.w3.org/2001/XMLSchema
                        TopLevelElementNameIndex = 135, // float
                        TopLevelElementNamespaceIndex = 41, // http://schemas.microsoft.com/2003/10/Serialization/
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Single, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Single, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                    },
                    Kind = global::DataContractKind.FloatDataContract,
                }, 
                new global::DataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        IsBuiltInDataContract = true,
                        IsValueType = true,
                        NameIndex = 141, // guid
                        NamespaceIndex = 41, // http://schemas.microsoft.com/2003/10/Serialization/
                        StableNameIndex = 141, // guid
                        StableNameNamespaceIndex = 41, // http://schemas.microsoft.com/2003/10/Serialization/
                        TopLevelElementNameIndex = 141, // guid
                        TopLevelElementNamespaceIndex = 41, // http://schemas.microsoft.com/2003/10/Serialization/
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Guid, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Guid, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                    },
                    Kind = global::DataContractKind.GuidDataContract,
                }, 
                new global::DataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        IsBuiltInDataContract = true,
                        IsValueType = true,
                        NameIndex = 146, // int
                        NamespaceIndex = 8, // http://www.w3.org/2001/XMLSchema
                        StableNameIndex = 146, // int
                        StableNameNamespaceIndex = 8, // http://www.w3.org/2001/XMLSchema
                        TopLevelElementNameIndex = 146, // int
                        TopLevelElementNamespaceIndex = 41, // http://schemas.microsoft.com/2003/10/Serialization/
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Int32, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Int32, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                    },
                    Kind = global::DataContractKind.IntDataContract,
                }, 
                new global::DataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        IsBuiltInDataContract = true,
                        IsValueType = true,
                        NameIndex = 150, // long
                        NamespaceIndex = 8, // http://www.w3.org/2001/XMLSchema
                        StableNameIndex = 150, // long
                        StableNameNamespaceIndex = 8, // http://www.w3.org/2001/XMLSchema
                        TopLevelElementNameIndex = 150, // long
                        TopLevelElementNamespaceIndex = 41, // http://schemas.microsoft.com/2003/10/Serialization/
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Int64, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Int64, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                    },
                    Kind = global::DataContractKind.LongDataContract,
                }, 
                new global::DataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        IsBuiltInDataContract = true,
                        NameIndex = 155, // anyType
                        NamespaceIndex = 8, // http://www.w3.org/2001/XMLSchema
                        StableNameIndex = 155, // anyType
                        StableNameNamespaceIndex = 8, // http://www.w3.org/2001/XMLSchema
                        TopLevelElementNameIndex = 155, // anyType
                        TopLevelElementNamespaceIndex = 41, // http://schemas.microsoft.com/2003/10/Serialization/
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Object, System.Private.CoreLib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Object, System.Private.CoreLib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                    },
                    Kind = global::DataContractKind.ObjectDataContract,
                }, 
                new global::DataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        IsBuiltInDataContract = true,
                        NameIndex = 163, // QName
                        NamespaceIndex = 8, // http://www.w3.org/2001/XMLSchema
                        StableNameIndex = 163, // QName
                        StableNameNamespaceIndex = 8, // http://www.w3.org/2001/XMLSchema
                        TopLevelElementNameIndex = 163, // QName
                        TopLevelElementNamespaceIndex = 41, // http://schemas.microsoft.com/2003/10/Serialization/
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Xml.XmlQualifiedName, System.Private.Xml, Version=4.0.1.0, Culture=neutral, PublicKeyToken=cc7b13ffcd2ddd" +
                                    "51")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Xml.XmlQualifiedName, System.Private.Xml, Version=4.0.1.0, Culture=neutral, PublicKeyToken=cc7b13ffcd2ddd" +
                                    "51")),
                    },
                    Kind = global::DataContractKind.QNameDataContract,
                }, 
                new global::DataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        IsBuiltInDataContract = true,
                        IsValueType = true,
                        NameIndex = 169, // short
                        NamespaceIndex = 8, // http://www.w3.org/2001/XMLSchema
                        StableNameIndex = 169, // short
                        StableNameNamespaceIndex = 8, // http://www.w3.org/2001/XMLSchema
                        TopLevelElementNameIndex = 169, // short
                        TopLevelElementNamespaceIndex = 41, // http://schemas.microsoft.com/2003/10/Serialization/
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Int16, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Int16, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                    },
                    Kind = global::DataContractKind.ShortDataContract,
                }, 
                new global::DataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        IsBuiltInDataContract = true,
                        IsValueType = true,
                        NameIndex = 175, // byte
                        NamespaceIndex = 8, // http://www.w3.org/2001/XMLSchema
                        StableNameIndex = 175, // byte
                        StableNameNamespaceIndex = 8, // http://www.w3.org/2001/XMLSchema
                        TopLevelElementNameIndex = 175, // byte
                        TopLevelElementNamespaceIndex = 41, // http://schemas.microsoft.com/2003/10/Serialization/
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.SByte, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.SByte, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                    },
                    Kind = global::DataContractKind.SignedByteDataContract,
                }, 
                new global::DataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        IsBuiltInDataContract = true,
                        NameIndex = 180, // string
                        NamespaceIndex = 8, // http://www.w3.org/2001/XMLSchema
                        StableNameIndex = 180, // string
                        StableNameNamespaceIndex = 8, // http://www.w3.org/2001/XMLSchema
                        TopLevelElementNameIndex = 180, // string
                        TopLevelElementNamespaceIndex = 41, // http://schemas.microsoft.com/2003/10/Serialization/
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.String, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.String, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                    },
                    Kind = global::DataContractKind.StringDataContract,
                }, 
                new global::DataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        IsBuiltInDataContract = true,
                        IsValueType = true,
                        NameIndex = 187, // duration
                        NamespaceIndex = 41, // http://schemas.microsoft.com/2003/10/Serialization/
                        StableNameIndex = 187, // duration
                        StableNameNamespaceIndex = 41, // http://schemas.microsoft.com/2003/10/Serialization/
                        TopLevelElementNameIndex = 187, // duration
                        TopLevelElementNamespaceIndex = 41, // http://schemas.microsoft.com/2003/10/Serialization/
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.TimeSpan, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.TimeSpan, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                    },
                    Kind = global::DataContractKind.TimeSpanDataContract,
                }, 
                new global::DataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        IsBuiltInDataContract = true,
                        IsValueType = true,
                        NameIndex = 196, // unsignedByte
                        NamespaceIndex = 8, // http://www.w3.org/2001/XMLSchema
                        StableNameIndex = 196, // unsignedByte
                        StableNameNamespaceIndex = 8, // http://www.w3.org/2001/XMLSchema
                        TopLevelElementNameIndex = 196, // unsignedByte
                        TopLevelElementNamespaceIndex = 41, // http://schemas.microsoft.com/2003/10/Serialization/
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Byte, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Byte, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                    },
                    Kind = global::DataContractKind.UnsignedByteDataContract,
                }, 
                new global::DataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        IsBuiltInDataContract = true,
                        IsValueType = true,
                        NameIndex = 209, // unsignedInt
                        NamespaceIndex = 8, // http://www.w3.org/2001/XMLSchema
                        StableNameIndex = 209, // unsignedInt
                        StableNameNamespaceIndex = 8, // http://www.w3.org/2001/XMLSchema
                        TopLevelElementNameIndex = 209, // unsignedInt
                        TopLevelElementNamespaceIndex = 41, // http://schemas.microsoft.com/2003/10/Serialization/
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.UInt32, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.UInt32, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                    },
                    Kind = global::DataContractKind.UnsignedIntDataContract,
                }, 
                new global::DataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        IsBuiltInDataContract = true,
                        IsValueType = true,
                        NameIndex = 221, // unsignedLong
                        NamespaceIndex = 8, // http://www.w3.org/2001/XMLSchema
                        StableNameIndex = 221, // unsignedLong
                        StableNameNamespaceIndex = 8, // http://www.w3.org/2001/XMLSchema
                        TopLevelElementNameIndex = 221, // unsignedLong
                        TopLevelElementNamespaceIndex = 41, // http://schemas.microsoft.com/2003/10/Serialization/
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.UInt64, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.UInt64, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                    },
                    Kind = global::DataContractKind.UnsignedLongDataContract,
                }, 
                new global::DataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        IsBuiltInDataContract = true,
                        IsValueType = true,
                        NameIndex = 234, // unsignedShort
                        NamespaceIndex = 8, // http://www.w3.org/2001/XMLSchema
                        StableNameIndex = 234, // unsignedShort
                        StableNameNamespaceIndex = 8, // http://www.w3.org/2001/XMLSchema
                        TopLevelElementNameIndex = 234, // unsignedShort
                        TopLevelElementNamespaceIndex = 41, // http://schemas.microsoft.com/2003/10/Serialization/
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.UInt16, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.UInt16, mscorlib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089")),
                    },
                    Kind = global::DataContractKind.UnsignedShortDataContract,
                }, 
                new global::DataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        IsBuiltInDataContract = true,
                        NameIndex = 248, // anyURI
                        NamespaceIndex = 8, // http://www.w3.org/2001/XMLSchema
                        StableNameIndex = 248, // anyURI
                        StableNameNamespaceIndex = 8, // http://www.w3.org/2001/XMLSchema
                        TopLevelElementNameIndex = 248, // anyURI
                        TopLevelElementNamespaceIndex = 41, // http://schemas.microsoft.com/2003/10/Serialization/
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Uri, System.Private.Uri, Version=4.0.5.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Uri, System.Private.Uri, Version=4.0.5.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                    },
                    Kind = global::DataContractKind.UriDataContract,
                }
        };
        static readonly byte[] s_classDataContracts_Hashtable = null;
        // Count=14
        [global::System.Runtime.CompilerServices.PreInitialized]
        static readonly global::ClassDataContractEntry[] s_classDataContracts = new global::ClassDataContractEntry[] {
                new global::ClassDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 348, // TechniqueDescription
                        NamespaceIndex = 283, // http://schemas.datacontract.org/2004/07/HelixToolkit.UWP.Shaders
                        StableNameIndex = 348, // TechniqueDescription
                        StableNameNamespaceIndex = 283, // http://schemas.datacontract.org/2004/07/HelixToolkit.UWP.Shaders
                        TopLevelElementNameIndex = 348, // TechniqueDescription
                        TopLevelElementNamespaceIndex = 283, // http://schemas.datacontract.org/2004/07/HelixToolkit.UWP.Shaders
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("HelixToolkit.UWP.Shaders.TechniqueDescription, HelixToolkit.UWP, Version=2.10.0.0, Culture=neutral, PublicKeyTok" +
                                    "en=52aa3500039caf0d")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("HelixToolkit.UWP.Shaders.TechniqueDescription, HelixToolkit.UWP, Version=2.10.0.0, Culture=neutral, PublicKeyTok" +
                                    "en=52aa3500039caf0d")),
                    },
                    HasDataContract = true,
                    XmlFormatReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.XmlFormatClassReaderDelegate>(global::Type3.ReadTechniqueDescriptionFromXml),
                    XmlFormatWriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.XmlFormatClassWriterDelegate>(global::Type4.WriteTechniqueDescriptionToXml),
                    ChildElementNamespacesListIndex = 1,
                    ContractNamespacesListIndex = 5,
                    MemberNamesListIndex = 7,
                    MemberNamespacesListIndex = 11,
                }, 
                new global::ClassDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 369, // InputLayoutDescription
                        NamespaceIndex = 283, // http://schemas.datacontract.org/2004/07/HelixToolkit.UWP.Shaders
                        StableNameIndex = 369, // InputLayoutDescription
                        StableNameNamespaceIndex = 283, // http://schemas.datacontract.org/2004/07/HelixToolkit.UWP.Shaders
                        TopLevelElementNameIndex = 369, // InputLayoutDescription
                        TopLevelElementNamespaceIndex = 283, // http://schemas.datacontract.org/2004/07/HelixToolkit.UWP.Shaders
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("HelixToolkit.UWP.Shaders.InputLayoutDescription, HelixToolkit.UWP, Version=2.10.0.0, Culture=neutral, PublicKeyT" +
                                    "oken=52aa3500039caf0d")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("HelixToolkit.UWP.Shaders.InputLayoutDescription, HelixToolkit.UWP, Version=2.10.0.0, Culture=neutral, PublicKeyT" +
                                    "oken=52aa3500039caf0d")),
                    },
                    HasDataContract = true,
                    XmlFormatReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.XmlFormatClassReaderDelegate>(global::Type5.ReadInputLayoutDescriptionFromXml),
                    XmlFormatWriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.XmlFormatClassWriterDelegate>(global::Type6.WriteInputLayoutDescriptionToXml),
                    ChildElementNamespacesListIndex = 15,
                    ContractNamespacesListIndex = 18,
                    MemberNamesListIndex = 20,
                    MemberNamespacesListIndex = 23,
                }, 
                new global::ClassDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        IsValueType = true,
                        NameIndex = 471, // InputElement
                        NamespaceIndex = 412, // http://schemas.datacontract.org/2004/07/SharpDX.Direct3D11
                        StableNameIndex = 471, // InputElement
                        StableNameNamespaceIndex = 412, // http://schemas.datacontract.org/2004/07/SharpDX.Direct3D11
                        TopLevelElementNameIndex = 471, // InputElement
                        TopLevelElementNamespaceIndex = 412, // http://schemas.datacontract.org/2004/07/SharpDX.Direct3D11
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("SharpDX.Direct3D11.InputElement, SharpDX.Direct3D11, Version=4.2.0.0, Culture=neutral, PublicKeyToken=b4dcf0f35e" +
                                    "5521f1")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("SharpDX.Direct3D11.InputElement, SharpDX.Direct3D11, Version=4.2.0.0, Culture=neutral, PublicKeyToken=b4dcf0f35e" +
                                    "5521f1")),
                    },
                    XmlFormatReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.XmlFormatClassReaderDelegate>(global::Type10.ReadInputElementFromXml),
                    XmlFormatWriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.XmlFormatClassWriterDelegate>(global::Type11.WriteInputElementToXml),
                    ChildElementNamespacesListIndex = 26,
                    ContractNamespacesListIndex = 34,
                    MemberNamesListIndex = 36,
                    MemberNamespacesListIndex = 44,
                }, 
                new global::ClassDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 2166, // ShaderPassDescription
                        NamespaceIndex = 283, // http://schemas.datacontract.org/2004/07/HelixToolkit.UWP.Shaders
                        StableNameIndex = 2166, // ShaderPassDescription
                        StableNameNamespaceIndex = 283, // http://schemas.datacontract.org/2004/07/HelixToolkit.UWP.Shaders
                        TopLevelElementNameIndex = 2166, // ShaderPassDescription
                        TopLevelElementNamespaceIndex = 283, // http://schemas.datacontract.org/2004/07/HelixToolkit.UWP.Shaders
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("HelixToolkit.UWP.Shaders.ShaderPassDescription, HelixToolkit.UWP, Version=2.10.0.0, Culture=neutral, PublicKeyTo" +
                                    "ken=52aa3500039caf0d")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("HelixToolkit.UWP.Shaders.ShaderPassDescription, HelixToolkit.UWP, Version=2.10.0.0, Culture=neutral, PublicKeyTo" +
                                    "ken=52aa3500039caf0d")),
                    },
                    HasDataContract = true,
                    XmlFormatReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.XmlFormatClassReaderDelegate>(global::Type15.ReadShaderPassDescriptionFromXml),
                    XmlFormatWriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.XmlFormatClassWriterDelegate>(global::Type16.WriteShaderPassDescriptionToXml),
                    ChildElementNamespacesListIndex = 175,
                    ContractNamespacesListIndex = 186,
                    MemberNamesListIndex = 188,
                    MemberNamespacesListIndex = 199,
                }, 
                new global::ClassDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        IsValueType = true,
                        NameIndex = 2188, // Color4
                        NamespaceIndex = 2195, // http://schemas.datacontract.org/2004/07/SharpDX
                        StableNameIndex = 2188, // Color4
                        StableNameNamespaceIndex = 2195, // http://schemas.datacontract.org/2004/07/SharpDX
                        TopLevelElementNameIndex = 2188, // Color4
                        TopLevelElementNamespaceIndex = 2195, // http://schemas.datacontract.org/2004/07/SharpDX
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("SharpDX.Color4, SharpDX.Mathematics, Version=4.2.0.0, Culture=neutral, PublicKeyToken=b4dcf0f35e5521f1")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("SharpDX.Color4, SharpDX.Mathematics, Version=4.2.0.0, Culture=neutral, PublicKeyToken=b4dcf0f35e5521f1")),
                    },
                    XmlFormatReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.XmlFormatClassReaderDelegate>(global::Type17.ReadColor4FromXml),
                    XmlFormatWriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.XmlFormatClassWriterDelegate>(global::Type18.WriteColor4ToXml),
                    ChildElementNamespacesListIndex = 210,
                    ContractNamespacesListIndex = 215,
                    MemberNamesListIndex = 217,
                    MemberNamespacesListIndex = 222,
                }, 
                new global::ClassDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        IsValueType = true,
                        NameIndex = 2243, // BlendStateDataContract
                        NamespaceIndex = 283, // http://schemas.datacontract.org/2004/07/HelixToolkit.UWP.Shaders
                        StableNameIndex = 2243, // BlendStateDataContract
                        StableNameNamespaceIndex = 283, // http://schemas.datacontract.org/2004/07/HelixToolkit.UWP.Shaders
                        TopLevelElementNameIndex = 2243, // BlendStateDataContract
                        TopLevelElementNamespaceIndex = 283, // http://schemas.datacontract.org/2004/07/HelixToolkit.UWP.Shaders
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("HelixToolkit.UWP.Shaders.BlendStateDataContract, HelixToolkit.UWP, Version=2.10.0.0, Culture=neutral, PublicKeyT" +
                                    "oken=52aa3500039caf0d")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("HelixToolkit.UWP.Shaders.BlendStateDataContract, HelixToolkit.UWP, Version=2.10.0.0, Culture=neutral, PublicKeyT" +
                                    "oken=52aa3500039caf0d")),
                    },
                    HasDataContract = true,
                    XmlFormatReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.XmlFormatClassReaderDelegate>(global::Type19.ReadBlendStateDataContractFromXml),
                    XmlFormatWriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.XmlFormatClassWriterDelegate>(global::Type20.WriteBlendStateDataContractToXml),
                    ChildElementNamespacesListIndex = 227,
                    ContractNamespacesListIndex = 231,
                    MemberNamesListIndex = 233,
                    MemberNamespacesListIndex = 237,
                }, 
                new global::ClassDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        IsValueType = true,
                        NameIndex = 2303, // RenderTargetBlendDataContract
                        NamespaceIndex = 283, // http://schemas.datacontract.org/2004/07/HelixToolkit.UWP.Shaders
                        StableNameIndex = 2303, // RenderTargetBlendDataContract
                        StableNameNamespaceIndex = 283, // http://schemas.datacontract.org/2004/07/HelixToolkit.UWP.Shaders
                        TopLevelElementNameIndex = 2303, // RenderTargetBlendDataContract
                        TopLevelElementNamespaceIndex = 283, // http://schemas.datacontract.org/2004/07/HelixToolkit.UWP.Shaders
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("HelixToolkit.UWP.Shaders.RenderTargetBlendDataContract, HelixToolkit.UWP, Version=2.10.0.0, Culture=neutral, Pub" +
                                    "licKeyToken=52aa3500039caf0d")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("HelixToolkit.UWP.Shaders.RenderTargetBlendDataContract, HelixToolkit.UWP, Version=2.10.0.0, Culture=neutral, Pub" +
                                    "licKeyToken=52aa3500039caf0d")),
                    },
                    HasDataContract = true,
                    XmlFormatReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.XmlFormatClassReaderDelegate>(global::Type24.ReadRenderTargetBlendDataContractFromXml),
                    XmlFormatWriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.XmlFormatClassWriterDelegate>(global::Type25.WriteRenderTargetBlendDataContractToXml),
                    ChildElementNamespacesListIndex = 241,
                    ContractNamespacesListIndex = 250,
                    MemberNamesListIndex = 252,
                    MemberNamespacesListIndex = 261,
                }, 
                new global::ClassDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        IsValueType = true,
                        NameIndex = 2333, // DepthStencilStateDataContract
                        NamespaceIndex = 283, // http://schemas.datacontract.org/2004/07/HelixToolkit.UWP.Shaders
                        StableNameIndex = 2333, // DepthStencilStateDataContract
                        StableNameNamespaceIndex = 283, // http://schemas.datacontract.org/2004/07/HelixToolkit.UWP.Shaders
                        TopLevelElementNameIndex = 2333, // DepthStencilStateDataContract
                        TopLevelElementNamespaceIndex = 283, // http://schemas.datacontract.org/2004/07/HelixToolkit.UWP.Shaders
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("HelixToolkit.UWP.Shaders.DepthStencilStateDataContract, HelixToolkit.UWP, Version=2.10.0.0, Culture=neutral, Pub" +
                                    "licKeyToken=52aa3500039caf0d")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("HelixToolkit.UWP.Shaders.DepthStencilStateDataContract, HelixToolkit.UWP, Version=2.10.0.0, Culture=neutral, Pub" +
                                    "licKeyToken=52aa3500039caf0d")),
                    },
                    HasDataContract = true,
                    XmlFormatReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.XmlFormatClassReaderDelegate>(global::Type26.ReadDepthStencilStateDataContractFromXml),
                    XmlFormatWriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.XmlFormatClassWriterDelegate>(global::Type27.WriteDepthStencilStateDataContractToXml),
                    ChildElementNamespacesListIndex = 270,
                    ContractNamespacesListIndex = 279,
                    MemberNamesListIndex = 281,
                    MemberNamespacesListIndex = 290,
                }, 
                new global::ClassDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        IsValueType = true,
                        NameIndex = 2363, // DepthStencilOperationDataContract
                        NamespaceIndex = 283, // http://schemas.datacontract.org/2004/07/HelixToolkit.UWP.Shaders
                        StableNameIndex = 2363, // DepthStencilOperationDataContract
                        StableNameNamespaceIndex = 283, // http://schemas.datacontract.org/2004/07/HelixToolkit.UWP.Shaders
                        TopLevelElementNameIndex = 2363, // DepthStencilOperationDataContract
                        TopLevelElementNamespaceIndex = 283, // http://schemas.datacontract.org/2004/07/HelixToolkit.UWP.Shaders
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("HelixToolkit.UWP.Shaders.DepthStencilOperationDataContract, HelixToolkit.UWP, Version=2.10.0.0, Culture=neutral," +
                                    " PublicKeyToken=52aa3500039caf0d")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("HelixToolkit.UWP.Shaders.DepthStencilOperationDataContract, HelixToolkit.UWP, Version=2.10.0.0, Culture=neutral," +
                                    " PublicKeyToken=52aa3500039caf0d")),
                    },
                    HasDataContract = true,
                    XmlFormatReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.XmlFormatClassReaderDelegate>(global::Type28.ReadDepthStencilOperationDataContractFromXml),
                    XmlFormatWriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.XmlFormatClassWriterDelegate>(global::Type29.WriteDepthStencilOperationDataContractToXml),
                    ChildElementNamespacesListIndex = 299,
                    ContractNamespacesListIndex = 304,
                    MemberNamesListIndex = 306,
                    MemberNamespacesListIndex = 311,
                }, 
                new global::ClassDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        IsValueType = true,
                        NameIndex = 2397, // RasterizerStateDataContract
                        NamespaceIndex = 283, // http://schemas.datacontract.org/2004/07/HelixToolkit.UWP.Shaders
                        StableNameIndex = 2397, // RasterizerStateDataContract
                        StableNameNamespaceIndex = 283, // http://schemas.datacontract.org/2004/07/HelixToolkit.UWP.Shaders
                        TopLevelElementNameIndex = 2397, // RasterizerStateDataContract
                        TopLevelElementNamespaceIndex = 283, // http://schemas.datacontract.org/2004/07/HelixToolkit.UWP.Shaders
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("HelixToolkit.UWP.Shaders.RasterizerStateDataContract, HelixToolkit.UWP, Version=2.10.0.0, Culture=neutral, Publi" +
                                    "cKeyToken=52aa3500039caf0d")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("HelixToolkit.UWP.Shaders.RasterizerStateDataContract, HelixToolkit.UWP, Version=2.10.0.0, Culture=neutral, Publi" +
                                    "cKeyToken=52aa3500039caf0d")),
                    },
                    HasDataContract = true,
                    XmlFormatReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.XmlFormatClassReaderDelegate>(global::Type30.ReadRasterizerStateDataContractFromXml),
                    XmlFormatWriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.XmlFormatClassWriterDelegate>(global::Type31.WriteRasterizerStateDataContractToXml),
                    ChildElementNamespacesListIndex = 316,
                    ContractNamespacesListIndex = 327,
                    MemberNamesListIndex = 329,
                    MemberNamespacesListIndex = 340,
                }, 
                new global::ClassDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 2450, // ShaderDescription
                        NamespaceIndex = 283, // http://schemas.datacontract.org/2004/07/HelixToolkit.UWP.Shaders
                        StableNameIndex = 2450, // ShaderDescription
                        StableNameNamespaceIndex = 283, // http://schemas.datacontract.org/2004/07/HelixToolkit.UWP.Shaders
                        TopLevelElementNameIndex = 2450, // ShaderDescription
                        TopLevelElementNamespaceIndex = 283, // http://schemas.datacontract.org/2004/07/HelixToolkit.UWP.Shaders
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("HelixToolkit.UWP.Shaders.ShaderDescription, HelixToolkit.UWP, Version=2.10.0.0, Culture=neutral, PublicKeyToken=" +
                                    "52aa3500039caf0d")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("HelixToolkit.UWP.Shaders.ShaderDescription, HelixToolkit.UWP, Version=2.10.0.0, Culture=neutral, PublicKeyToken=" +
                                    "52aa3500039caf0d")),
                    },
                    HasDataContract = true,
                    XmlFormatReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.XmlFormatClassReaderDelegate>(global::Type35.ReadShaderDescriptionFromXml),
                    XmlFormatWriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.XmlFormatClassWriterDelegate>(global::Type36.WriteShaderDescriptionToXml),
                    ChildElementNamespacesListIndex = 351,
                    ContractNamespacesListIndex = 359,
                    MemberNamesListIndex = 361,
                    MemberNamespacesListIndex = 369,
                }, 
                new global::ClassDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        IsValueType = true,
                        NameIndex = 2495, // StreamOutputElement
                        NamespaceIndex = 412, // http://schemas.datacontract.org/2004/07/SharpDX.Direct3D11
                        StableNameIndex = 2495, // StreamOutputElement
                        StableNameNamespaceIndex = 412, // http://schemas.datacontract.org/2004/07/SharpDX.Direct3D11
                        TopLevelElementNameIndex = 2495, // StreamOutputElement
                        TopLevelElementNamespaceIndex = 412, // http://schemas.datacontract.org/2004/07/SharpDX.Direct3D11
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("SharpDX.Direct3D11.StreamOutputElement, SharpDX.Direct3D11, Version=4.2.0.0, Culture=neutral, PublicKeyToken=b4d" +
                                    "cf0f35e5521f1")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("SharpDX.Direct3D11.StreamOutputElement, SharpDX.Direct3D11, Version=4.2.0.0, Culture=neutral, PublicKeyToken=b4d" +
                                    "cf0f35e5521f1")),
                    },
                    XmlFormatReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.XmlFormatClassReaderDelegate>(global::Type40.ReadStreamOutputElementFromXml),
                    XmlFormatWriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.XmlFormatClassWriterDelegate>(global::Type41.WriteStreamOutputElementToXml),
                    ChildElementNamespacesListIndex = 377,
                    ContractNamespacesListIndex = 384,
                    MemberNamesListIndex = 386,
                    MemberNamespacesListIndex = 393,
                }, 
                new global::ClassDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        IsValueType = true,
                        NameIndex = 3889, // KeyValueOfstringanyType
                        NamespaceIndex = 2526, // http://schemas.microsoft.com/2003/10/Serialization/Arrays
                        StableNameIndex = 3889, // KeyValueOfstringanyType
                        StableNameNamespaceIndex = 2526, // http://schemas.microsoft.com/2003/10/Serialization/Arrays
                        TopLevelElementNameIndex = 3889, // KeyValueOfstringanyType
                        TopLevelElementNamespaceIndex = 2526, // http://schemas.microsoft.com/2003/10/Serialization/Arrays
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf(@"System.Runtime.Serialization.KeyValue`2[[System.String, System.Private.CoreLib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a],[System.Object, System.Private.CoreLib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a]], System.Private.DataContractSerialization, Version=4.1.4.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf(@"System.Runtime.Serialization.KeyValue`2[[System.String, System.Private.CoreLib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a],[System.Object, System.Private.CoreLib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a]], System.Private.DataContractSerialization, Version=4.1.4.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                        GenericTypeDefinition = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Runtime.Serialization.KeyValue`2, System.Private.DataContractSerialization, Version=4.1.4.0, Culture=neut" +
                                    "ral, PublicKeyToken=b03f5f7f11d50a3a")),
                    },
                    HasDataContract = true,
                    XmlFormatReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.XmlFormatClassReaderDelegate>(global::Type48.ReadKeyValueOfstringanyTypeFromXml),
                    XmlFormatWriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.XmlFormatClassWriterDelegate>(global::Type49.WriteKeyValueOfstringanyTypeToXml),
                    ChildElementNamespacesListIndex = 451,
                    ContractNamespacesListIndex = 454,
                    MemberNamesListIndex = 456,
                    MemberNamespacesListIndex = 459,
                }, 
                new global::ClassDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        IsValueType = true,
                        NameIndex = 3923, // KeyValuePairOfstringanyType
                        NamespaceIndex = 3951, // http://schemas.datacontract.org/2004/07/System.Collections.Generic
                        StableNameIndex = 3923, // KeyValuePairOfstringanyType
                        StableNameNamespaceIndex = 3951, // http://schemas.datacontract.org/2004/07/System.Collections.Generic
                        TopLevelElementNameIndex = 3923, // KeyValuePairOfstringanyType
                        TopLevelElementNamespaceIndex = 3951, // http://schemas.datacontract.org/2004/07/System.Collections.Generic
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf(@"System.Collections.Generic.KeyValuePair`2[[System.String, System.Private.CoreLib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a],[System.Object, System.Private.CoreLib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a]], System.Private.CoreLib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf(@"System.Collections.Generic.KeyValuePair`2[[System.String, System.Private.CoreLib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a],[System.Object, System.Private.CoreLib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a]], System.Private.CoreLib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                        GenericTypeDefinition = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Collections.Generic.KeyValuePair`2, System.Private.CoreLib, Version=4.0.0.0, Culture=neutral, PublicKeyTo" +
                                    "ken=b03f5f7f11d50a3a")),
                    },
                    XmlFormatReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.XmlFormatClassReaderDelegate>(global::Type50.ReadKeyValuePairOfstringanyTypeFromXml),
                    XmlFormatWriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.XmlFormatClassWriterDelegate>(global::Type51.WriteKeyValuePairOfstringanyTypeToXml),
                    ChildElementNamespacesListIndex = 462,
                    ContractNamespacesListIndex = 465,
                    MemberNamesListIndex = 467,
                    MemberNamespacesListIndex = 470,
                }
        };
        static readonly byte[] s_collectionDataContracts_Hashtable = null;
        // Count=8
        [global::System.Runtime.CompilerServices.PreInitialized]
        static readonly global::CollectionDataContractEntry[] s_collectionDataContracts = new global::CollectionDataContractEntry[] {
                new global::CollectionDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 255, // ArrayOfTechniqueDescription
                        NamespaceIndex = 283, // http://schemas.datacontract.org/2004/07/HelixToolkit.UWP.Shaders
                        StableNameIndex = 255, // ArrayOfTechniqueDescription
                        StableNameNamespaceIndex = 283, // http://schemas.datacontract.org/2004/07/HelixToolkit.UWP.Shaders
                        TopLevelElementNameIndex = 255, // ArrayOfTechniqueDescription
                        TopLevelElementNamespaceIndex = 283, // http://schemas.datacontract.org/2004/07/HelixToolkit.UWP.Shaders
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf(@"System.Collections.Generic.List`1[[HelixToolkit.UWP.Shaders.TechniqueDescription, HelixToolkit.UWP, Version=2.10.0.0, Culture=neutral, PublicKeyToken=52aa3500039caf0d]], System.Private.CoreLib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf(@"System.Collections.Generic.List`1[[HelixToolkit.UWP.Shaders.TechniqueDescription, HelixToolkit.UWP, Version=2.10.0.0, Culture=neutral, PublicKeyToken=52aa3500039caf0d]], System.Private.CoreLib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                        GenericTypeDefinition = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Collections.Generic.List`1, System.Private.CoreLib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f" +
                                    "5f7f11d50a3a")),
                    },
                    XmlFormatReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.XmlFormatCollectionReaderDelegate>(global::Type0.ReadArrayOfTechniqueDescriptionFromXml),
                    XmlFormatWriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.XmlFormatCollectionWriterDelegate>(global::Type1.WriteArrayOfTechniqueDescriptionToXml),
                    XmlFormatGetOnlyCollectionReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.XmlFormatGetOnlyCollectionReaderDelegate>(global::Type2.ReadArrayOfTechniqueDescriptionFromXmlIsGetOnly),
                    CollectionItemNameIndex = 348, // TechniqueDescription
                    KeyNameIndex = -1,
                    ItemNameIndex = 348, // TechniqueDescription
                    ValueNameIndex = -1,
                    CollectionContractKind = global::System.Runtime.Serialization.CollectionKind.GenericList,
                    ItemType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("HelixToolkit.UWP.Shaders.TechniqueDescription, HelixToolkit.UWP, Version=2.10.0.0, Culture=neutral, PublicKeyTok" +
                                "en=52aa3500039caf0d")),
                }, 
                new global::CollectionDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 392, // ArrayOfInputElement
                        NamespaceIndex = 412, // http://schemas.datacontract.org/2004/07/SharpDX.Direct3D11
                        StableNameIndex = 392, // ArrayOfInputElement
                        StableNameNamespaceIndex = 412, // http://schemas.datacontract.org/2004/07/SharpDX.Direct3D11
                        TopLevelElementNameIndex = 392, // ArrayOfInputElement
                        TopLevelElementNamespaceIndex = 412, // http://schemas.datacontract.org/2004/07/SharpDX.Direct3D11
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("SharpDX.Direct3D11.InputElement[], SharpDX.Direct3D11, Version=4.2.0.0, Culture=neutral, PublicKeyToken=b4dcf0f3" +
                                    "5e5521f1")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("SharpDX.Direct3D11.InputElement[], SharpDX.Direct3D11, Version=4.2.0.0, Culture=neutral, PublicKeyToken=b4dcf0f3" +
                                    "5e5521f1")),
                    },
                    XmlFormatReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.XmlFormatCollectionReaderDelegate>(global::Type7.ReadArrayOfInputElementFromXml),
                    XmlFormatWriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.XmlFormatCollectionWriterDelegate>(global::Type8.WriteArrayOfInputElementToXml),
                    XmlFormatGetOnlyCollectionReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.XmlFormatGetOnlyCollectionReaderDelegate>(global::Type9.ReadArrayOfInputElementFromXmlIsGetOnly),
                    CollectionItemNameIndex = 471, // InputElement
                    KeyNameIndex = -1,
                    ItemNameIndex = 471, // InputElement
                    ValueNameIndex = -1,
                    CollectionContractKind = global::System.Runtime.Serialization.CollectionKind.Array,
                    ItemType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("SharpDX.Direct3D11.InputElement, SharpDX.Direct3D11, Version=4.2.0.0, Culture=neutral, PublicKeyToken=b4dcf0f35e" +
                                "5521f1")),
                }, 
                new global::CollectionDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        TypeIsCollectionInterface = true,
                        TypeIsInterface = true,
                        NameIndex = 2137, // ArrayOfShaderPassDescription
                        NamespaceIndex = 283, // http://schemas.datacontract.org/2004/07/HelixToolkit.UWP.Shaders
                        StableNameIndex = 2137, // ArrayOfShaderPassDescription
                        StableNameNamespaceIndex = 283, // http://schemas.datacontract.org/2004/07/HelixToolkit.UWP.Shaders
                        TopLevelElementNameIndex = 2137, // ArrayOfShaderPassDescription
                        TopLevelElementNamespaceIndex = 283, // http://schemas.datacontract.org/2004/07/HelixToolkit.UWP.Shaders
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf(@"System.Collections.Generic.IList`1[[HelixToolkit.UWP.Shaders.ShaderPassDescription, HelixToolkit.UWP, Version=2.10.0.0, Culture=neutral, PublicKeyToken=52aa3500039caf0d]], System.Private.CoreLib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf(@"System.Collections.Generic.IList`1[[HelixToolkit.UWP.Shaders.ShaderPassDescription, HelixToolkit.UWP, Version=2.10.0.0, Culture=neutral, PublicKeyToken=52aa3500039caf0d]], System.Private.CoreLib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                        GenericTypeDefinition = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Collections.Generic.IList`1, System.Runtime, Version=4.2.1.0, Culture=neutral, PublicKeyToken=b03f5f7f11d" +
                                    "50a3a")),
                    },
                    XmlFormatReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.XmlFormatCollectionReaderDelegate>(global::Type12.ReadArrayOfShaderPassDescriptionFromXml),
                    XmlFormatWriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.XmlFormatCollectionWriterDelegate>(global::Type13.WriteArrayOfShaderPassDescriptionToXml),
                    XmlFormatGetOnlyCollectionReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.XmlFormatGetOnlyCollectionReaderDelegate>(global::Type14.ReadArrayOfShaderPassDescriptionFromXmlIsGetOnly),
                    CollectionItemNameIndex = 2166, // ShaderPassDescription
                    KeyNameIndex = -1,
                    ItemNameIndex = 2166, // ShaderPassDescription
                    ValueNameIndex = -1,
                    CollectionContractKind = global::System.Runtime.Serialization.CollectionKind.GenericList,
                    ItemType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("HelixToolkit.UWP.Shaders.ShaderPassDescription, HelixToolkit.UWP, Version=2.10.0.0, Culture=neutral, PublicKeyTo" +
                                "ken=52aa3500039caf0d")),
                }, 
                new global::CollectionDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 2266, // ArrayOfRenderTargetBlendDataContract
                        NamespaceIndex = 283, // http://schemas.datacontract.org/2004/07/HelixToolkit.UWP.Shaders
                        StableNameIndex = 2266, // ArrayOfRenderTargetBlendDataContract
                        StableNameNamespaceIndex = 283, // http://schemas.datacontract.org/2004/07/HelixToolkit.UWP.Shaders
                        TopLevelElementNameIndex = 2266, // ArrayOfRenderTargetBlendDataContract
                        TopLevelElementNamespaceIndex = 283, // http://schemas.datacontract.org/2004/07/HelixToolkit.UWP.Shaders
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("HelixToolkit.UWP.Shaders.RenderTargetBlendDataContract[], HelixToolkit.UWP, Version=2.10.0.0, Culture=neutral, P" +
                                    "ublicKeyToken=52aa3500039caf0d")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("HelixToolkit.UWP.Shaders.RenderTargetBlendDataContract[], HelixToolkit.UWP, Version=2.10.0.0, Culture=neutral, P" +
                                    "ublicKeyToken=52aa3500039caf0d")),
                    },
                    XmlFormatReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.XmlFormatCollectionReaderDelegate>(global::Type21.ReadArrayOfRenderTargetBlendDataContractFromXml),
                    XmlFormatWriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.XmlFormatCollectionWriterDelegate>(global::Type22.WriteArrayOfRenderTargetBlendDataContractToXml),
                    XmlFormatGetOnlyCollectionReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.XmlFormatGetOnlyCollectionReaderDelegate>(global::Type23.ReadArrayOfRenderTargetBlendDataContractFromXmlIsGetOnly),
                    CollectionItemNameIndex = 2303, // RenderTargetBlendDataContract
                    KeyNameIndex = -1,
                    ItemNameIndex = 2303, // RenderTargetBlendDataContract
                    ValueNameIndex = -1,
                    CollectionContractKind = global::System.Runtime.Serialization.CollectionKind.Array,
                    ItemType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("HelixToolkit.UWP.Shaders.RenderTargetBlendDataContract, HelixToolkit.UWP, Version=2.10.0.0, Culture=neutral, Pub" +
                                "licKeyToken=52aa3500039caf0d")),
                }, 
                new global::CollectionDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        TypeIsCollectionInterface = true,
                        TypeIsInterface = true,
                        NameIndex = 2425, // ArrayOfShaderDescription
                        NamespaceIndex = 283, // http://schemas.datacontract.org/2004/07/HelixToolkit.UWP.Shaders
                        StableNameIndex = 2425, // ArrayOfShaderDescription
                        StableNameNamespaceIndex = 283, // http://schemas.datacontract.org/2004/07/HelixToolkit.UWP.Shaders
                        TopLevelElementNameIndex = 2425, // ArrayOfShaderDescription
                        TopLevelElementNamespaceIndex = 283, // http://schemas.datacontract.org/2004/07/HelixToolkit.UWP.Shaders
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf(@"System.Collections.Generic.IList`1[[HelixToolkit.UWP.Shaders.ShaderDescription, HelixToolkit.UWP, Version=2.10.0.0, Culture=neutral, PublicKeyToken=52aa3500039caf0d]], System.Private.CoreLib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf(@"System.Collections.Generic.IList`1[[HelixToolkit.UWP.Shaders.ShaderDescription, HelixToolkit.UWP, Version=2.10.0.0, Culture=neutral, PublicKeyToken=52aa3500039caf0d]], System.Private.CoreLib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                        GenericTypeDefinition = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Collections.Generic.IList`1, System.Runtime, Version=4.2.1.0, Culture=neutral, PublicKeyToken=b03f5f7f11d" +
                                    "50a3a")),
                    },
                    XmlFormatReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.XmlFormatCollectionReaderDelegate>(global::Type32.ReadArrayOfShaderDescriptionFromXml),
                    XmlFormatWriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.XmlFormatCollectionWriterDelegate>(global::Type33.WriteArrayOfShaderDescriptionToXml),
                    XmlFormatGetOnlyCollectionReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.XmlFormatGetOnlyCollectionReaderDelegate>(global::Type34.ReadArrayOfShaderDescriptionFromXmlIsGetOnly),
                    CollectionItemNameIndex = 2450, // ShaderDescription
                    KeyNameIndex = -1,
                    ItemNameIndex = 2450, // ShaderDescription
                    ValueNameIndex = -1,
                    CollectionContractKind = global::System.Runtime.Serialization.CollectionKind.GenericList,
                    ItemType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("HelixToolkit.UWP.Shaders.ShaderDescription, HelixToolkit.UWP, Version=2.10.0.0, Culture=neutral, PublicKeyToken=" +
                                "52aa3500039caf0d")),
                }, 
                new global::CollectionDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 2468, // ArrayOfStreamOutputElement
                        NamespaceIndex = 412, // http://schemas.datacontract.org/2004/07/SharpDX.Direct3D11
                        StableNameIndex = 2468, // ArrayOfStreamOutputElement
                        StableNameNamespaceIndex = 412, // http://schemas.datacontract.org/2004/07/SharpDX.Direct3D11
                        TopLevelElementNameIndex = 2468, // ArrayOfStreamOutputElement
                        TopLevelElementNamespaceIndex = 412, // http://schemas.datacontract.org/2004/07/SharpDX.Direct3D11
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("SharpDX.Direct3D11.StreamOutputElement[], SharpDX.Direct3D11, Version=4.2.0.0, Culture=neutral, PublicKeyToken=b" +
                                    "4dcf0f35e5521f1")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("SharpDX.Direct3D11.StreamOutputElement[], SharpDX.Direct3D11, Version=4.2.0.0, Culture=neutral, PublicKeyToken=b" +
                                    "4dcf0f35e5521f1")),
                    },
                    XmlFormatReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.XmlFormatCollectionReaderDelegate>(global::Type37.ReadArrayOfStreamOutputElementFromXml),
                    XmlFormatWriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.XmlFormatCollectionWriterDelegate>(global::Type38.WriteArrayOfStreamOutputElementToXml),
                    XmlFormatGetOnlyCollectionReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.XmlFormatGetOnlyCollectionReaderDelegate>(global::Type39.ReadArrayOfStreamOutputElementFromXmlIsGetOnly),
                    CollectionItemNameIndex = 2495, // StreamOutputElement
                    KeyNameIndex = -1,
                    ItemNameIndex = 2495, // StreamOutputElement
                    ValueNameIndex = -1,
                    CollectionContractKind = global::System.Runtime.Serialization.CollectionKind.Array,
                    ItemType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("SharpDX.Direct3D11.StreamOutputElement, SharpDX.Direct3D11, Version=4.2.0.0, Culture=neutral, PublicKeyToken=b4d" +
                                "cf0f35e5521f1")),
                }, 
                new global::CollectionDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        NameIndex = 2515, // ArrayOfint
                        NamespaceIndex = 2526, // http://schemas.microsoft.com/2003/10/Serialization/Arrays
                        StableNameIndex = 2515, // ArrayOfint
                        StableNameNamespaceIndex = 2526, // http://schemas.microsoft.com/2003/10/Serialization/Arrays
                        TopLevelElementNameIndex = 2515, // ArrayOfint
                        TopLevelElementNamespaceIndex = 2526, // http://schemas.microsoft.com/2003/10/Serialization/Arrays
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Int32[], System.Private.CoreLib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Int32[], System.Private.CoreLib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                    },
                    XmlFormatReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.XmlFormatCollectionReaderDelegate>(global::Type42.ReadArrayOfintFromXml),
                    XmlFormatWriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.XmlFormatCollectionWriterDelegate>(global::Type43.WriteArrayOfintToXml),
                    XmlFormatGetOnlyCollectionReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.XmlFormatGetOnlyCollectionReaderDelegate>(global::Type44.ReadArrayOfintFromXmlIsGetOnly),
                    CollectionItemNameIndex = 146, // int
                    KeyNameIndex = -1,
                    ItemNameIndex = 146, // int
                    ValueNameIndex = -1,
                    CollectionContractKind = global::System.Runtime.Serialization.CollectionKind.Array,
                    ItemType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Int32, System.Private.CoreLib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                }, 
                new global::CollectionDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        TypeIsCollectionInterface = true,
                        TypeIsInterface = true,
                        NameIndex = 3858, // ArrayOfKeyValueOfstringanyType
                        NamespaceIndex = 2526, // http://schemas.microsoft.com/2003/10/Serialization/Arrays
                        StableNameIndex = 3858, // ArrayOfKeyValueOfstringanyType
                        StableNameNamespaceIndex = 2526, // http://schemas.microsoft.com/2003/10/Serialization/Arrays
                        TopLevelElementNameIndex = 3858, // ArrayOfKeyValueOfstringanyType
                        TopLevelElementNamespaceIndex = 2526, // http://schemas.microsoft.com/2003/10/Serialization/Arrays
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf(@"System.Collections.Generic.IDictionary`2[[System.String, System.Private.CoreLib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a],[System.Object, System.Private.CoreLib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a]], System.Private.CoreLib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf(@"System.Collections.Generic.IDictionary`2[[System.String, System.Private.CoreLib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a],[System.Object, System.Private.CoreLib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a]], System.Private.CoreLib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                        GenericTypeDefinition = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("System.Collections.Generic.IDictionary`2, System.Private.CoreLib, Version=4.0.0.0, Culture=neutral, PublicKeyTok" +
                                    "en=b03f5f7f11d50a3a")),
                    },
                    XmlFormatReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.XmlFormatCollectionReaderDelegate>(global::Type45.ReadArrayOfKeyValueOfstringanyTypeFromXml),
                    XmlFormatWriterDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.XmlFormatCollectionWriterDelegate>(global::Type46.WriteArrayOfKeyValueOfstringanyTypeToXml),
                    XmlFormatGetOnlyCollectionReaderDelegate = global::SgIntrinsics.AddrOf<global::System.Runtime.Serialization.XmlFormatGetOnlyCollectionReaderDelegate>(global::Type47.ReadArrayOfKeyValueOfstringanyTypeFromXmlIsGetOnly),
                    CollectionItemNameIndex = 3889, // KeyValueOfstringanyType
                    KeyNameIndex = 3913, // Key
                    ItemNameIndex = 3889, // KeyValueOfstringanyType
                    ValueNameIndex = 3917, // Value
                    CollectionContractKind = global::System.Runtime.Serialization.CollectionKind.GenericDictionary,
                    ItemType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf(@"System.Runtime.Serialization.KeyValue`2[[System.String, System.Private.CoreLib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a],[System.Object, System.Private.CoreLib, Version=4.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a]], System.Private.DataContractSerialization, Version=4.1.4.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")),
                }
        };
        static readonly byte[] s_enumDataContracts_Hashtable = null;
        // Count=4
        [global::System.Runtime.CompilerServices.PreInitialized]
        static readonly global::EnumDataContractEntry[] s_enumDataContracts = new global::EnumDataContractEntry[] {
                new global::EnumDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        IsValueType = true,
                        NameIndex = 484, // InputClassification
                        NamespaceIndex = 412, // http://schemas.datacontract.org/2004/07/SharpDX.Direct3D11
                        StableNameIndex = 484, // InputClassification
                        StableNameNamespaceIndex = 412, // http://schemas.datacontract.org/2004/07/SharpDX.Direct3D11
                        TopLevelElementNameIndex = 484, // InputClassification
                        TopLevelElementNamespaceIndex = 412, // http://schemas.datacontract.org/2004/07/SharpDX.Direct3D11
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("SharpDX.Direct3D11.InputClassification, SharpDX.Direct3D11, Version=4.2.0.0, Culture=neutral, PublicKeyToken=b4d" +
                                    "cf0f35e5521f1")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("SharpDX.Direct3D11.InputClassification, SharpDX.Direct3D11, Version=4.2.0.0, Culture=neutral, PublicKeyToken=b4d" +
                                    "cf0f35e5521f1")),
                    },
                    BaseContractNameIndex = -1,
                    BaseContractNamespaceIndex = -1,
                    ChildElementNamesListIndex = 52,
                    MemberCount = 2,
                }, 
                new global::EnumDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        IsValueType = true,
                        NameIndex = 534, // Format
                        NamespaceIndex = 541, // http://schemas.datacontract.org/2004/07/SharpDX.DXGI
                        StableNameIndex = 534, // Format
                        StableNameNamespaceIndex = 541, // http://schemas.datacontract.org/2004/07/SharpDX.DXGI
                        TopLevelElementNameIndex = 534, // Format
                        TopLevelElementNamespaceIndex = 541, // http://schemas.datacontract.org/2004/07/SharpDX.DXGI
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("SharpDX.DXGI.Format, SharpDX.DXGI, Version=4.2.0.0, Culture=neutral, PublicKeyToken=b4dcf0f35e5521f1")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("SharpDX.DXGI.Format, SharpDX.DXGI, Version=4.2.0.0, Culture=neutral, PublicKeyToken=b4dcf0f35e5521f1")),
                    },
                    BaseContractNameIndex = -1,
                    BaseContractNamespaceIndex = -1,
                    ChildElementNamesListIndex = 55,
                    MemberCount = 119,
                    MemberListIndex = 2,
                }, 
                new global::EnumDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        IsValueType = true,
                        NameIndex = 2584, // ShaderStage
                        NamespaceIndex = 2596, // http://schemas.datacontract.org/2004/07/HelixToolkit.UWP
                        StableNameIndex = 2584, // ShaderStage
                        StableNameNamespaceIndex = 2596, // http://schemas.datacontract.org/2004/07/HelixToolkit.UWP
                        TopLevelElementNameIndex = 2584, // ShaderStage
                        TopLevelElementNamespaceIndex = 2596, // http://schemas.datacontract.org/2004/07/HelixToolkit.UWP
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("HelixToolkit.UWP.ShaderStage, HelixToolkit.UWP, Version=2.10.0.0, Culture=neutral, PublicKeyToken=52aa3500039caf" +
                                    "0d")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("HelixToolkit.UWP.ShaderStage, HelixToolkit.UWP, Version=2.10.0.0, Culture=neutral, PublicKeyToken=52aa3500039caf" +
                                    "0d")),
                    },
                    IsFlags = true,
                    BaseContractNameIndex = -1,
                    BaseContractNamespaceIndex = -1,
                    ChildElementNamesListIndex = 400,
                    MemberCount = 7,
                    MemberListIndex = 121,
                }, 
                new global::EnumDataContractEntry() {
                    Common = new global::CommonContractEntry() {
                        HasRoot = true,
                        IsValueType = true,
                        NameIndex = 2700, // PrimitiveTopology
                        NamespaceIndex = 2718, // http://schemas.datacontract.org/2004/07/SharpDX.Direct3D
                        StableNameIndex = 2700, // PrimitiveTopology
                        StableNameNamespaceIndex = 2718, // http://schemas.datacontract.org/2004/07/SharpDX.Direct3D
                        TopLevelElementNameIndex = 2700, // PrimitiveTopology
                        TopLevelElementNamespaceIndex = 2718, // http://schemas.datacontract.org/2004/07/SharpDX.Direct3D
                        OriginalUnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("SharpDX.Direct3D.PrimitiveTopology, SharpDX, Version=4.2.0.0, Culture=neutral, PublicKeyToken=b4dcf0f35e5521f1")),
                        UnderlyingType = new global::Internal.Runtime.CompilerServices.FixupRuntimeTypeHandle(global::System.Runtime.InteropServices.TypeOfHelper.RuntimeTypeHandleOf("SharpDX.Direct3D.PrimitiveTopology, SharpDX, Version=4.2.0.0, Culture=neutral, PublicKeyToken=b4dcf0f35e5521f1")),
                    },
                    BaseContractNameIndex = -1,
                    BaseContractNamespaceIndex = -1,
                    ChildElementNamesListIndex = 408,
                    MemberCount = 42,
                    MemberListIndex = 128,
                }
        };
        static readonly byte[] s_xmlDataContracts_Hashtable = null;
        // Count=0
        [global::System.Runtime.CompilerServices.PreInitialized]
        static readonly global::XmlDataContractEntry[] s_xmlDataContracts = new global::XmlDataContractEntry[0];
        static char[] s_stringPool = new char[] {
            'b','o','o','l','e','a','n','\0','h','t','t','p',':','/','/','w','w','w','.','w','3','.','o','r','g','/','2','0','0','1',
            '/','X','M','L','S','c','h','e','m','a','\0','h','t','t','p',':','/','/','s','c','h','e','m','a','s','.','m','i','c','r',
            'o','s','o','f','t','.','c','o','m','/','2','0','0','3','/','1','0','/','S','e','r','i','a','l','i','z','a','t','i','o',
            'n','/','\0','b','a','s','e','6','4','B','i','n','a','r','y','\0','c','h','a','r','\0','d','a','t','e','T','i','m','e','\0',
            'd','e','c','i','m','a','l','\0','d','o','u','b','l','e','\0','f','l','o','a','t','\0','g','u','i','d','\0','i','n','t','\0',
            'l','o','n','g','\0','a','n','y','T','y','p','e','\0','Q','N','a','m','e','\0','s','h','o','r','t','\0','b','y','t','e','\0',
            's','t','r','i','n','g','\0','d','u','r','a','t','i','o','n','\0','u','n','s','i','g','n','e','d','B','y','t','e','\0','u',
            'n','s','i','g','n','e','d','I','n','t','\0','u','n','s','i','g','n','e','d','L','o','n','g','\0','u','n','s','i','g','n',
            'e','d','S','h','o','r','t','\0','a','n','y','U','R','I','\0','A','r','r','a','y','O','f','T','e','c','h','n','i','q','u',
            'e','D','e','s','c','r','i','p','t','i','o','n','\0','h','t','t','p',':','/','/','s','c','h','e','m','a','s','.','d','a',
            't','a','c','o','n','t','r','a','c','t','.','o','r','g','/','2','0','0','4','/','0','7','/','H','e','l','i','x','T','o',
            'o','l','k','i','t','.','U','W','P','.','S','h','a','d','e','r','s','\0','T','e','c','h','n','i','q','u','e','D','e','s',
            'c','r','i','p','t','i','o','n','\0','I','n','p','u','t','L','a','y','o','u','t','D','e','s','c','r','i','p','t','i','o',
            'n','\0','A','r','r','a','y','O','f','I','n','p','u','t','E','l','e','m','e','n','t','\0','h','t','t','p',':','/','/','s',
            'c','h','e','m','a','s','.','d','a','t','a','c','o','n','t','r','a','c','t','.','o','r','g','/','2','0','0','4','/','0',
            '7','/','S','h','a','r','p','D','X','.','D','i','r','e','c','t','3','D','1','1','\0','I','n','p','u','t','E','l','e','m',
            'e','n','t','\0','I','n','p','u','t','C','l','a','s','s','i','f','i','c','a','t','i','o','n','\0','P','e','r','V','e','r',
            't','e','x','D','a','t','a','\0','P','e','r','I','n','s','t','a','n','c','e','D','a','t','a','\0','F','o','r','m','a','t',
            '\0','h','t','t','p',':','/','/','s','c','h','e','m','a','s','.','d','a','t','a','c','o','n','t','r','a','c','t','.','o',
            'r','g','/','2','0','0','4','/','0','7','/','S','h','a','r','p','D','X','.','D','X','G','I','\0','U','n','k','n','o','w',
            'n','\0','R','3','2','G','3','2','B','3','2','A','3','2','_','T','y','p','e','l','e','s','s','\0','R','3','2','G','3','2',
            'B','3','2','A','3','2','_','F','l','o','a','t','\0','R','3','2','G','3','2','B','3','2','A','3','2','_','U','I','n','t',
            '\0','R','3','2','G','3','2','B','3','2','A','3','2','_','S','I','n','t','\0','R','3','2','G','3','2','B','3','2','_','T',
            'y','p','e','l','e','s','s','\0','R','3','2','G','3','2','B','3','2','_','F','l','o','a','t','\0','R','3','2','G','3','2',
            'B','3','2','_','U','I','n','t','\0','R','3','2','G','3','2','B','3','2','_','S','I','n','t','\0','R','1','6','G','1','6',
            'B','1','6','A','1','6','_','T','y','p','e','l','e','s','s','\0','R','1','6','G','1','6','B','1','6','A','1','6','_','F',
            'l','o','a','t','\0','R','1','6','G','1','6','B','1','6','A','1','6','_','U','N','o','r','m','\0','R','1','6','G','1','6',
            'B','1','6','A','1','6','_','U','I','n','t','\0','R','1','6','G','1','6','B','1','6','A','1','6','_','S','N','o','r','m',
            '\0','R','1','6','G','1','6','B','1','6','A','1','6','_','S','I','n','t','\0','R','3','2','G','3','2','_','T','y','p','e',
            'l','e','s','s','\0','R','3','2','G','3','2','_','F','l','o','a','t','\0','R','3','2','G','3','2','_','U','I','n','t','\0',
            'R','3','2','G','3','2','_','S','I','n','t','\0','R','3','2','G','8','X','2','4','_','T','y','p','e','l','e','s','s','\0',
            'D','3','2','_','F','l','o','a','t','_','S','8','X','2','4','_','U','I','n','t','\0','R','3','2','_','F','l','o','a','t',
            '_','X','8','X','2','4','_','T','y','p','e','l','e','s','s','\0','X','3','2','_','T','y','p','e','l','e','s','s','_','G',
            '8','X','2','4','_','U','I','n','t','\0','R','1','0','G','1','0','B','1','0','A','2','_','T','y','p','e','l','e','s','s',
            '\0','R','1','0','G','1','0','B','1','0','A','2','_','U','N','o','r','m','\0','R','1','0','G','1','0','B','1','0','A','2',
            '_','U','I','n','t','\0','R','1','1','G','1','1','B','1','0','_','F','l','o','a','t','\0','R','8','G','8','B','8','A','8',
            '_','T','y','p','e','l','e','s','s','\0','R','8','G','8','B','8','A','8','_','U','N','o','r','m','\0','R','8','G','8','B',
            '8','A','8','_','U','N','o','r','m','_','S','R','g','b','\0','R','8','G','8','B','8','A','8','_','U','I','n','t','\0','R',
            '8','G','8','B','8','A','8','_','S','N','o','r','m','\0','R','8','G','8','B','8','A','8','_','S','I','n','t','\0','R','1',
            '6','G','1','6','_','T','y','p','e','l','e','s','s','\0','R','1','6','G','1','6','_','F','l','o','a','t','\0','R','1','6',
            'G','1','6','_','U','N','o','r','m','\0','R','1','6','G','1','6','_','U','I','n','t','\0','R','1','6','G','1','6','_','S',
            'N','o','r','m','\0','R','1','6','G','1','6','_','S','I','n','t','\0','R','3','2','_','T','y','p','e','l','e','s','s','\0',
            'D','3','2','_','F','l','o','a','t','\0','R','3','2','_','F','l','o','a','t','\0','R','3','2','_','U','I','n','t','\0','R',
            '3','2','_','S','I','n','t','\0','R','2','4','G','8','_','T','y','p','e','l','e','s','s','\0','D','2','4','_','U','N','o',
            'r','m','_','S','8','_','U','I','n','t','\0','R','2','4','_','U','N','o','r','m','_','X','8','_','T','y','p','e','l','e',
            's','s','\0','X','2','4','_','T','y','p','e','l','e','s','s','_','G','8','_','U','I','n','t','\0','R','8','G','8','_','T',
            'y','p','e','l','e','s','s','\0','R','8','G','8','_','U','N','o','r','m','\0','R','8','G','8','_','U','I','n','t','\0','R',
            '8','G','8','_','S','N','o','r','m','\0','R','8','G','8','_','S','I','n','t','\0','R','1','6','_','T','y','p','e','l','e',
            's','s','\0','R','1','6','_','F','l','o','a','t','\0','D','1','6','_','U','N','o','r','m','\0','R','1','6','_','U','N','o',
            'r','m','\0','R','1','6','_','U','I','n','t','\0','R','1','6','_','S','N','o','r','m','\0','R','1','6','_','S','I','n','t',
            '\0','R','8','_','T','y','p','e','l','e','s','s','\0','R','8','_','U','N','o','r','m','\0','R','8','_','U','I','n','t','\0',
            'R','8','_','S','N','o','r','m','\0','R','8','_','S','I','n','t','\0','A','8','_','U','N','o','r','m','\0','R','1','_','U',
            'N','o','r','m','\0','R','9','G','9','B','9','E','5','_','S','h','a','r','e','d','e','x','p','\0','R','8','G','8','_','B',
            '8','G','8','_','U','N','o','r','m','\0','G','8','R','8','_','G','8','B','8','_','U','N','o','r','m','\0','B','C','1','_',
            'T','y','p','e','l','e','s','s','\0','B','C','1','_','U','N','o','r','m','\0','B','C','1','_','U','N','o','r','m','_','S',
            'R','g','b','\0','B','C','2','_','T','y','p','e','l','e','s','s','\0','B','C','2','_','U','N','o','r','m','\0','B','C','2',
            '_','U','N','o','r','m','_','S','R','g','b','\0','B','C','3','_','T','y','p','e','l','e','s','s','\0','B','C','3','_','U',
            'N','o','r','m','\0','B','C','3','_','U','N','o','r','m','_','S','R','g','b','\0','B','C','4','_','T','y','p','e','l','e',
            's','s','\0','B','C','4','_','U','N','o','r','m','\0','B','C','4','_','S','N','o','r','m','\0','B','C','5','_','T','y','p',
            'e','l','e','s','s','\0','B','C','5','_','U','N','o','r','m','\0','B','C','5','_','S','N','o','r','m','\0','B','5','G','6',
            'R','5','_','U','N','o','r','m','\0','B','5','G','5','R','5','A','1','_','U','N','o','r','m','\0','B','8','G','8','R','8',
            'A','8','_','U','N','o','r','m','\0','B','8','G','8','R','8','X','8','_','U','N','o','r','m','\0','R','1','0','G','1','0',
            'B','1','0','_','X','r','_','B','i','a','s','_','A','2','_','U','N','o','r','m','\0','B','8','G','8','R','8','A','8','_',
            'T','y','p','e','l','e','s','s','\0','B','8','G','8','R','8','A','8','_','U','N','o','r','m','_','S','R','g','b','\0','B',
            '8','G','8','R','8','X','8','_','T','y','p','e','l','e','s','s','\0','B','8','G','8','R','8','X','8','_','U','N','o','r',
            'm','_','S','R','g','b','\0','B','C','6','H','_','T','y','p','e','l','e','s','s','\0','B','C','6','H','_','U','f','1','6',
            '\0','B','C','6','H','_','S','f','1','6','\0','B','C','7','_','T','y','p','e','l','e','s','s','\0','B','C','7','_','U','N',
            'o','r','m','\0','B','C','7','_','U','N','o','r','m','_','S','R','g','b','\0','A','Y','U','V','\0','Y','4','1','0','\0','Y',
            '4','1','6','\0','N','V','1','2','\0','P','0','1','0','\0','P','0','1','6','\0','O','p','a','q','u','e','4','2','0','\0','Y',
            'U','Y','2','\0','Y','2','1','0','\0','Y','2','1','6','\0','N','V','1','1','\0','A','I','4','4','\0','I','A','4','4','\0','P',
            '8','\0','A','8','P','8','\0','B','4','G','4','R','4','A','4','_','U','N','o','r','m','\0','P','2','0','8','\0','V','2','0',
            '8','\0','V','4','0','8','\0','A','r','r','a','y','O','f','S','h','a','d','e','r','P','a','s','s','D','e','s','c','r','i',
            'p','t','i','o','n','\0','S','h','a','d','e','r','P','a','s','s','D','e','s','c','r','i','p','t','i','o','n','\0','C','o',
            'l','o','r','4','\0','h','t','t','p',':','/','/','s','c','h','e','m','a','s','.','d','a','t','a','c','o','n','t','r','a',
            'c','t','.','o','r','g','/','2','0','0','4','/','0','7','/','S','h','a','r','p','D','X','\0','B','l','e','n','d','S','t',
            'a','t','e','D','a','t','a','C','o','n','t','r','a','c','t','\0','A','r','r','a','y','O','f','R','e','n','d','e','r','T',
            'a','r','g','e','t','B','l','e','n','d','D','a','t','a','C','o','n','t','r','a','c','t','\0','R','e','n','d','e','r','T',
            'a','r','g','e','t','B','l','e','n','d','D','a','t','a','C','o','n','t','r','a','c','t','\0','D','e','p','t','h','S','t',
            'e','n','c','i','l','S','t','a','t','e','D','a','t','a','C','o','n','t','r','a','c','t','\0','D','e','p','t','h','S','t',
            'e','n','c','i','l','O','p','e','r','a','t','i','o','n','D','a','t','a','C','o','n','t','r','a','c','t','\0','R','a','s',
            't','e','r','i','z','e','r','S','t','a','t','e','D','a','t','a','C','o','n','t','r','a','c','t','\0','A','r','r','a','y',
            'O','f','S','h','a','d','e','r','D','e','s','c','r','i','p','t','i','o','n','\0','S','h','a','d','e','r','D','e','s','c',
            'r','i','p','t','i','o','n','\0','A','r','r','a','y','O','f','S','t','r','e','a','m','O','u','t','p','u','t','E','l','e',
            'm','e','n','t','\0','S','t','r','e','a','m','O','u','t','p','u','t','E','l','e','m','e','n','t','\0','A','r','r','a','y',
            'O','f','i','n','t','\0','h','t','t','p',':','/','/','s','c','h','e','m','a','s','.','m','i','c','r','o','s','o','f','t',
            '.','c','o','m','/','2','0','0','3','/','1','0','/','S','e','r','i','a','l','i','z','a','t','i','o','n','/','A','r','r',
            'a','y','s','\0','S','h','a','d','e','r','S','t','a','g','e','\0','h','t','t','p',':','/','/','s','c','h','e','m','a','s',
            '.','d','a','t','a','c','o','n','t','r','a','c','t','.','o','r','g','/','2','0','0','4','/','0','7','/','H','e','l','i',
            'x','T','o','o','l','k','i','t','.','U','W','P','\0','N','o','n','e','\0','V','e','r','t','e','x','\0','H','u','l','l','\0',
            'D','o','m','a','i','n','\0','G','e','o','m','e','t','r','y','\0','P','i','x','e','l','\0','C','o','m','p','u','t','e','\0',
            'P','r','i','m','i','t','i','v','e','T','o','p','o','l','o','g','y','\0','h','t','t','p',':','/','/','s','c','h','e','m',
            'a','s','.','d','a','t','a','c','o','n','t','r','a','c','t','.','o','r','g','/','2','0','0','4','/','0','7','/','S','h',
            'a','r','p','D','X','.','D','i','r','e','c','t','3','D','\0','U','n','d','e','f','i','n','e','d','\0','P','o','i','n','t',
            'L','i','s','t','\0','L','i','n','e','L','i','s','t','\0','L','i','n','e','S','t','r','i','p','\0','T','r','i','a','n','g',
            'l','e','L','i','s','t','\0','T','r','i','a','n','g','l','e','S','t','r','i','p','\0','L','i','n','e','L','i','s','t','W',
            'i','t','h','A','d','j','a','c','e','n','c','y','\0','L','i','n','e','S','t','r','i','p','W','i','t','h','A','d','j','a',
            'c','e','n','c','y','\0','T','r','i','a','n','g','l','e','L','i','s','t','W','i','t','h','A','d','j','a','c','e','n','c',
            'y','\0','T','r','i','a','n','g','l','e','S','t','r','i','p','W','i','t','h','A','d','j','a','c','e','n','c','y','\0','P',
            'a','t','c','h','L','i','s','t','W','i','t','h','1','C','o','n','t','r','o','l','P','o','i','n','t','s','\0','P','a','t',
            'c','h','L','i','s','t','W','i','t','h','2','C','o','n','t','r','o','l','P','o','i','n','t','s','\0','P','a','t','c','h',
            'L','i','s','t','W','i','t','h','3','C','o','n','t','r','o','l','P','o','i','n','t','s','\0','P','a','t','c','h','L','i',
            's','t','W','i','t','h','4','C','o','n','t','r','o','l','P','o','i','n','t','s','\0','P','a','t','c','h','L','i','s','t',
            'W','i','t','h','5','C','o','n','t','r','o','l','P','o','i','n','t','s','\0','P','a','t','c','h','L','i','s','t','W','i',
            't','h','6','C','o','n','t','r','o','l','P','o','i','n','t','s','\0','P','a','t','c','h','L','i','s','t','W','i','t','h',
            '7','C','o','n','t','r','o','l','P','o','i','n','t','s','\0','P','a','t','c','h','L','i','s','t','W','i','t','h','8','C',
            'o','n','t','r','o','l','P','o','i','n','t','s','\0','P','a','t','c','h','L','i','s','t','W','i','t','h','9','C','o','n',
            't','r','o','l','P','o','i','n','t','s','\0','P','a','t','c','h','L','i','s','t','W','i','t','h','1','0','C','o','n','t',
            'r','o','l','P','o','i','n','t','s','\0','P','a','t','c','h','L','i','s','t','W','i','t','h','1','1','C','o','n','t','r',
            'o','l','P','o','i','n','t','s','\0','P','a','t','c','h','L','i','s','t','W','i','t','h','1','2','C','o','n','t','r','o',
            'l','P','o','i','n','t','s','\0','P','a','t','c','h','L','i','s','t','W','i','t','h','1','3','C','o','n','t','r','o','l',
            'P','o','i','n','t','s','\0','P','a','t','c','h','L','i','s','t','W','i','t','h','1','4','C','o','n','t','r','o','l','P',
            'o','i','n','t','s','\0','P','a','t','c','h','L','i','s','t','W','i','t','h','1','5','C','o','n','t','r','o','l','P','o',
            'i','n','t','s','\0','P','a','t','c','h','L','i','s','t','W','i','t','h','1','6','C','o','n','t','r','o','l','P','o','i',
            'n','t','s','\0','P','a','t','c','h','L','i','s','t','W','i','t','h','1','7','C','o','n','t','r','o','l','P','o','i','n',
            't','s','\0','P','a','t','c','h','L','i','s','t','W','i','t','h','1','8','C','o','n','t','r','o','l','P','o','i','n','t',
            's','\0','P','a','t','c','h','L','i','s','t','W','i','t','h','1','9','C','o','n','t','r','o','l','P','o','i','n','t','s',
            '\0','P','a','t','c','h','L','i','s','t','W','i','t','h','2','0','C','o','n','t','r','o','l','P','o','i','n','t','s','\0',
            'P','a','t','c','h','L','i','s','t','W','i','t','h','2','1','C','o','n','t','r','o','l','P','o','i','n','t','s','\0','P',
            'a','t','c','h','L','i','s','t','W','i','t','h','2','2','C','o','n','t','r','o','l','P','o','i','n','t','s','\0','P','a',
            't','c','h','L','i','s','t','W','i','t','h','2','3','C','o','n','t','r','o','l','P','o','i','n','t','s','\0','P','a','t',
            'c','h','L','i','s','t','W','i','t','h','2','4','C','o','n','t','r','o','l','P','o','i','n','t','s','\0','P','a','t','c',
            'h','L','i','s','t','W','i','t','h','2','5','C','o','n','t','r','o','l','P','o','i','n','t','s','\0','P','a','t','c','h',
            'L','i','s','t','W','i','t','h','2','6','C','o','n','t','r','o','l','P','o','i','n','t','s','\0','P','a','t','c','h','L',
            'i','s','t','W','i','t','h','2','7','C','o','n','t','r','o','l','P','o','i','n','t','s','\0','P','a','t','c','h','L','i',
            's','t','W','i','t','h','2','8','C','o','n','t','r','o','l','P','o','i','n','t','s','\0','P','a','t','c','h','L','i','s',
            't','W','i','t','h','2','9','C','o','n','t','r','o','l','P','o','i','n','t','s','\0','P','a','t','c','h','L','i','s','t',
            'W','i','t','h','3','0','C','o','n','t','r','o','l','P','o','i','n','t','s','\0','P','a','t','c','h','L','i','s','t','W',
            'i','t','h','3','1','C','o','n','t','r','o','l','P','o','i','n','t','s','\0','P','a','t','c','h','L','i','s','t','W','i',
            't','h','3','2','C','o','n','t','r','o','l','P','o','i','n','t','s','\0','A','r','r','a','y','O','f','K','e','y','V','a',
            'l','u','e','O','f','s','t','r','i','n','g','a','n','y','T','y','p','e','\0','K','e','y','V','a','l','u','e','O','f','s',
            't','r','i','n','g','a','n','y','T','y','p','e','\0','K','e','y','\0','V','a','l','u','e','\0','K','e','y','V','a','l','u',
            'e','P','a','i','r','O','f','s','t','r','i','n','g','a','n','y','T','y','p','e','\0','h','t','t','p',':','/','/','s','c',
            'h','e','m','a','s','.','d','a','t','a','c','o','n','t','r','a','c','t','.','o','r','g','/','2','0','0','4','/','0','7',
            '/','S','y','s','t','e','m','.','C','o','l','l','e','c','t','i','o','n','s','.','G','e','n','e','r','i','c','\0','N','a',
            'm','e','\0','P','a','s','s','D','e','s','c','r','i','p','t','i','o','n','s','\0','I','n','p','u','t','E','l','e','m','e',
            'n','t','s','\0','S','h','a','d','e','r','B','y','t','e','C','o','d','e','\0','A','l','i','g','n','e','d','B','y','t','e',
            'O','f','f','s','e','t','\0','C','l','a','s','s','i','f','i','c','a','t','i','o','n','\0','I','n','s','t','a','n','c','e',
            'D','a','t','a','S','t','e','p','R','a','t','e','\0','S','e','m','a','n','t','i','c','I','n','d','e','x','\0','S','e','m',
            'a','n','t','i','c','N','a','m','e','\0','S','l','o','t','\0','B','l','e','n','d','F','a','c','t','o','r','\0','B','l','e',
            'n','d','S','t','a','t','e','D','e','s','c','S','e','r','i','a','l','i','z','a','t','i','o','n','\0','D','e','p','t','h',
            'S','t','e','n','c','i','l','S','t','a','t','e','D','e','s','c','S','e','r','i','a','l','i','z','a','t','i','o','n','\0',
            'R','a','s','t','e','r','i','z','e','r','S','t','a','t','e','D','e','s','c','S','e','r','i','a','l','i','z','a','t','i',
            'o','n','\0','S','a','m','p','l','e','M','a','s','k','\0','S','h','a','d','e','r','L','i','s','t','\0','S','t','e','n','c',
            'i','l','R','e','f','\0','T','o','p','o','l','o','g','y','\0','A','l','p','h','a','\0','B','l','u','e','\0','G','r','e','e',
            'n','\0','R','e','d','\0','A','l','p','h','a','T','o','C','o','v','e','r','a','g','e','E','n','a','b','l','e','\0','I','n',
            'd','e','p','e','n','d','e','n','t','B','l','e','n','d','E','n','a','b','l','e','\0','R','e','n','d','e','r','T','a','r',
            'g','e','t','\0','A','l','p','h','a','B','l','e','n','d','O','p','e','r','a','t','i','o','n','\0','B','l','e','n','d','O',
            'p','e','r','a','t','i','o','n','\0','D','e','s','t','i','n','a','t','i','o','n','A','l','p','h','a','B','l','e','n','d',
            '\0','D','e','s','t','i','n','a','t','i','o','n','B','l','e','n','d','\0','I','s','B','l','e','n','d','E','n','a','b','l',
            'e','d','\0','R','e','n','d','e','r','T','a','r','g','e','t','W','r','i','t','e','M','a','s','k','\0','S','o','u','r','c',
            'e','A','l','p','h','a','B','l','e','n','d','\0','S','o','u','r','c','e','B','l','e','n','d','\0','B','a','c','k','F','a',
            'c','e','\0','D','e','p','t','h','C','o','m','p','a','r','i','s','o','n','\0','D','e','p','t','h','W','r','i','t','e','M',
            'a','s','k','\0','F','r','o','n','t','F','a','c','e','\0','I','s','D','e','p','t','h','E','n','a','b','l','e','d','\0','I',
            's','S','t','e','n','c','i','l','E','n','a','b','l','e','d','\0','S','t','e','n','c','i','l','R','e','a','d','M','a','s',
            'k','\0','S','t','e','n','c','i','l','W','r','i','t','e','M','a','s','k','\0','C','o','m','p','a','r','i','s','o','n','\0',
            'D','e','p','t','h','F','a','i','l','O','p','e','r','a','t','i','o','n','\0','F','a','i','l','O','p','e','r','a','t','i',
            'o','n','\0','P','a','s','s','O','p','e','r','a','t','i','o','n','\0','C','u','l','l','M','o','d','e','\0','D','e','p','t',
            'h','B','i','a','s','\0','D','e','p','t','h','B','i','a','s','C','l','a','m','p','\0','F','i','l','l','M','o','d','e','\0',
            'I','s','A','n','t','i','a','l','i','a','s','e','d','L','i','n','e','E','n','a','b','l','e','d','\0','I','s','D','e','p',
            't','h','C','l','i','p','E','n','a','b','l','e','d','\0','I','s','F','r','o','n','t','C','o','u','n','t','e','r','C','l',
            'o','c','k','w','i','s','e','\0','I','s','M','u','l','t','i','s','a','m','p','l','e','E','n','a','b','l','e','d','\0','I',
            's','S','c','i','s','s','o','r','E','n','a','b','l','e','d','\0','S','l','o','p','e','S','c','a','l','e','d','D','e','p',
            't','h','B','i','a','s','\0','B','y','t','e','C','o','d','e','\0','G','S','S','O','E','l','e','m','e','n','t','\0','G','S',
            'S','O','R','a','s','t','e','r','i','z','e','d','\0','G','S','S','O','S','t','r','i','d','e','s','\0','I','s','G','S','S',
            't','r','e','a','m','O','u','t','\0','S','h','a','d','e','r','T','y','p','e','\0','C','o','m','p','o','n','e','n','t','C',
            'o','u','n','t','\0','O','u','t','p','u','t','S','l','o','t','\0','S','t','a','r','t','C','o','m','p','o','n','e','n','t',
            '\0','S','t','r','e','a','m','\0','k','e','y','\0','v','a','l','u','e','\0'};
    }
}
