﻿#pragma checksum "D:\uwp\Visual2019\final\vagrantuwp\Vagrant_V1\Pageees\ZoningPage.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "F8C3FC3D66585D9B2CC9596BBF461D12"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Vagrant_V1.Pageees
{
    partial class ZoningPage : 
        global::Windows.UI.Xaml.Controls.Page, 
        global::Windows.UI.Xaml.Markup.IComponentConnector,
        global::Windows.UI.Xaml.Markup.IComponentConnector2
    {
        /// <summary>
        /// Connect()
        /// </summary>
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Windows.UI.Xaml.Build.Tasks"," 10.0.17.0")]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        public void Connect(int connectionId, object target)
        {
            switch(connectionId)
            {
            case 1: // Pageees\ZoningPage.xaml line 1
                {
                    global::Windows.UI.Xaml.Controls.Page element1 = (global::Windows.UI.Xaml.Controls.Page)(target);
                    ((global::Windows.UI.Xaml.Controls.Page)element1).Loaded += this.LoadData;
                }
                break;
            case 2: // Pageees\ZoningPage.xaml line 313
                {
                    this.LoadingControl = (global::Microsoft.Toolkit.Uwp.UI.Controls.Loading)(target);
                }
                break;
            case 3: // Pageees\ZoningPage.xaml line 269
                {
                    this.sharedModel = (global::HelixToolkit.UWP.ModelContainer3DX)(target);
                }
                break;
            case 4: // Pageees\ZoningPage.xaml line 286
                {
                    this.viewport = (global::HelixToolkit.UWP.Viewport3DX)(target);
                    ((global::HelixToolkit.UWP.Viewport3DX)this.viewport).OnMouse3DDown += this.Viewport3DX_OnMouse3DDown;
                }
                break;
            case 5: // Pageees\ZoningPage.xaml line 270
                {
                    this.ItemsDataZon = (global::HelixToolkit.UWP.ItemsModel3D)(target);
                }
                break;
            case 7: // Pageees\ZoningPage.xaml line 282
                {
                    this.Items = (global::HelixToolkit.UWP.GeometryModel3DOctreeManager)(target);
                }
                break;
            case 8: // Pageees\ZoningPage.xaml line 247
                {
                    this.Level1 = (global::Windows.UI.Xaml.Controls.Button)(target);
                    ((global::Windows.UI.Xaml.Controls.Button)this.Level1).Click += this.Clik_Zon_Level;
                }
                break;
            case 9: // Pageees\ZoningPage.xaml line 252
                {
                    this.Level2 = (global::Windows.UI.Xaml.Controls.Button)(target);
                    ((global::Windows.UI.Xaml.Controls.Button)this.Level2).Click += this.Clik_Zon_Level;
                }
                break;
            case 10: // Pageees\ZoningPage.xaml line 257
                {
                    this.Level3 = (global::Windows.UI.Xaml.Controls.Button)(target);
                    ((global::Windows.UI.Xaml.Controls.Button)this.Level3).Click += this.Clik_Zon_Level;
                }
                break;
            case 11: // Pageees\ZoningPage.xaml line 262
                {
                    this.Level4 = (global::Windows.UI.Xaml.Controls.Button)(target);
                    ((global::Windows.UI.Xaml.Controls.Button)this.Level4).Click += this.Clik_Zon_Level;
                }
                break;
            case 12: // Pageees\ZoningPage.xaml line 159
                {
                    this.ButtonModify = (global::Windows.UI.Xaml.Controls.Grid)(target);
                }
                break;
            case 13: // Pageees\ZoningPage.xaml line 181
                {
                    this.PanelInfo = (global::Windows.UI.Xaml.Controls.Grid)(target);
                }
                break;
            case 14: // Pageees\ZoningPage.xaml line 219
                {
                    this.ColorPickerZon = (global::Windows.UI.Xaml.Controls.ColorPicker)(target);
                    ((global::Windows.UI.Xaml.Controls.ColorPicker)this.ColorPickerZon).ColorChanged += this.Color1;
                }
                break;
            case 15: // Pageees\ZoningPage.xaml line 207
                {
                    this.Apply = (global::Windows.UI.Xaml.Controls.Button)(target);
                    ((global::Windows.UI.Xaml.Controls.Button)this.Apply).Click += this.ApplyClick;
                }
                break;
            case 16: // Pageees\ZoningPage.xaml line 199
                {
                    this.Name = (global::Windows.UI.Xaml.Controls.TextBox)(target);
                }
                break;
            case 17: // Pageees\ZoningPage.xaml line 165
                {
                    this.Create = (global::Windows.UI.Xaml.Controls.Button)(target);
                    ((global::Windows.UI.Xaml.Controls.Button)this.Create).Click += this.Create_Click;
                }
                break;
            case 18: // Pageees\ZoningPage.xaml line 170
                {
                    this.Modify = (global::Windows.UI.Xaml.Controls.Button)(target);
                    ((global::Windows.UI.Xaml.Controls.Button)this.Modify).Click += this.Modify_Click;
                }
                break;
            case 19: // Pageees\ZoningPage.xaml line 175
                {
                    this.Deleate = (global::Windows.UI.Xaml.Controls.Button)(target);
                    ((global::Windows.UI.Xaml.Controls.Button)this.Deleate).Click += this.Deleate_Click;
                }
                break;
            case 20: // Pageees\ZoningPage.xaml line 150
                {
                    this.Enter = (global::Windows.UI.Xaml.Controls.Button)(target);
                    ((global::Windows.UI.Xaml.Controls.Button)this.Enter).Click += this.Enter_Click;
                }
                break;
            case 21: // Pageees\ZoningPage.xaml line 109
                {
                    this.EnterPage = (global::Windows.UI.Xaml.Controls.Grid)(target);
                }
                break;
            case 22: // Pageees\ZoningPage.xaml line 130
                {
                    this.NameUser = (global::Windows.UI.Xaml.Controls.TextBlock)(target);
                }
                break;
            case 23: // Pageees\ZoningPage.xaml line 101
                {
                    this.Password = (global::Windows.UI.Xaml.Controls.PasswordBox)(target);
                }
                break;
            case 24: // Pageees\ZoningPage.xaml line 91
                {
                    this.Login = (global::Windows.UI.Xaml.Controls.TextBox)(target);
                }
                break;
            case 25: // Pageees\ZoningPage.xaml line 44
                {
                    this.dataGrid = (global::Microsoft.Toolkit.Uwp.UI.Controls.DataGrid)(target);
                    ((global::Microsoft.Toolkit.Uwp.UI.Controls.DataGrid)this.dataGrid).SelectionChanged += this.dataGrid_SelectionChanged;
                }
                break;
            case 26: // Pageees\ZoningPage.xaml line 54
                {
                    this.Columntwo = (global::Microsoft.Toolkit.Uwp.UI.Controls.DataGridTemplateColumn)(target);
                }
                break;
            default:
                break;
            }
            this._contentLoaded = true;
        }

        /// <summary>
        /// GetBindingConnector(int connectionId, object target)
        /// </summary>
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Windows.UI.Xaml.Build.Tasks"," 10.0.17.0")]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        public global::Windows.UI.Xaml.Markup.IComponentConnector GetBindingConnector(int connectionId, object target)
        {
            global::Windows.UI.Xaml.Markup.IComponentConnector returnValue = null;
            return returnValue;
        }
    }
}

